<?php

/* CMSBundle:Pessoas:list.html.twig */
class __TwigTemplate_d6f233a872d03c0a94bd2418a9b53f09a701acc76c0a9baf601d850a4870fa91 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CMSBundle:Default:list.html.twig", "CMSBundle:Pessoas:list.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'kit_grid_thead_column' => array($this, 'block_kit_grid_thead_column'),
            'kit_grid_tbody_column' => array($this, 'block_kit_grid_tbody_column'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CMSBundle:Default:list.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (isset($context["titulo"]) ? $context["titulo"] : null), "html", null, true);
    }

    // line 3
    public function block_kit_grid_thead_column($context, array $blocks = array())
    {
        // line 4
        echo "    <th>Opções</th>
";
    }

    // line 6
    public function block_kit_grid_tbody_column($context, array $blocks = array())
    {
        // line 7
        echo "    <td>
        <div class=\"dropdown\">
          <button class=\"btn btn-default dropdown-toggle\" type=\"button\" id=\"dropdownMenu_";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "item.id", array(), "array"), "html", null, true);
        echo "\" data-toggle=\"dropdown\">
            <i class=\"fa fa-dashboard\"></i>
                Configurações do Usuário
            <span class=\"caret\"></span>
          </button>
          <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownMenu_";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "item.id", array(), "array"), "html", null, true);
        echo "\">
            <li role=\"presentation\" class=\"dropdown-header\">
                Opções
            </li>
              <li role=\"presentation\">
                  <a role=\"menuitem\" tabindex=\"-1\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cms_pessoas_atualizar_perfil", array("id" => $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "item.id", array(), "array"))), "html", null, true);
        echo "\">
                      <span class=\"fa fa-user\"></span> Liberar Acessos
                  </a>
              </li>
            <li role=\"presentation\">
                <a role=\"menuitem\" tabindex=\"-1\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cms_pessoas_atualizar", array("id" => $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "item.id", array(), "array"), "tipo" => 1)), "html", null, true);
        echo "\">
                   <span class=\"glyphicon glyphicon-edit\"></span> Pessoa Jurídica
                </a>
            </li>
            <li role=\"presentation\">
                <a role=\"menuitem\" tabindex=\"-1\" href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cms_pessoas_atualizar", array("id" => $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "item.id", array(), "array"), "tipo" => 2)), "html", null, true);
        echo "\">
                    <span class=\"glyphicon glyphicon-edit\"></span> Pessoa Física
                </a>
            </li>
            <li role=\"presentation\">
                <a role=\"menuitem\" tabindex=\"-1\" href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cms_pessoas_atualizar_foto", array("id" => $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "item.id", array(), "array"))), "html", null, true);
        echo "\">
                    <span class=\"fa fa-image\"></span> Atualizar Foto
                </a>
            </li>

            <li role=\"presentation\">
                <a role=\"menuitem\" tabindex=\"-1\" href=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cms_pessoas_visualizar", array("id" => $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "item.id", array(), "array"))), "html", null, true);
        echo "\">
                    <i class=\"fa fa-search\"></i> Visualizar dados do Cliente
                </a>
            </li>
          </ul>
        </div>
    </td>
";
    }

    public function getTemplateName()
    {
        return "CMSBundle:Pessoas:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 40,  91 => 34,  83 => 29,  75 => 24,  67 => 19,  59 => 14,  51 => 9,  47 => 7,  44 => 6,  39 => 4,  36 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends "CMSBundle:Default:list.html.twig" %}*/
/* {% block title %}{{ titulo }}{% endblock %}*/
/* {% block kit_grid_thead_column %}*/
/*     <th>Opções</th>*/
/* {% endblock %}*/
/* {% block kit_grid_tbody_column %}*/
/*     <td>*/
/*         <div class="dropdown">*/
/*           <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu_{{ item['item.id'] }}" data-toggle="dropdown">*/
/*             <i class="fa fa-dashboard"></i>*/
/*                 Configurações do Usuário*/
/*             <span class="caret"></span>*/
/*           </button>*/
/*           <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu_{{ item['item.id'] }}">*/
/*             <li role="presentation" class="dropdown-header">*/
/*                 Opções*/
/*             </li>*/
/*               <li role="presentation">*/
/*                   <a role="menuitem" tabindex="-1" href="{{ path ("cms_pessoas_atualizar_perfil", {"id": item['item.id']}) }}">*/
/*                       <span class="fa fa-user"></span> Liberar Acessos*/
/*                   </a>*/
/*               </li>*/
/*             <li role="presentation">*/
/*                 <a role="menuitem" tabindex="-1" href="{{ path ("cms_pessoas_atualizar", {"id": item['item.id'], "tipo" : 1}) }}">*/
/*                    <span class="glyphicon glyphicon-edit"></span> Pessoa Jurídica*/
/*                 </a>*/
/*             </li>*/
/*             <li role="presentation">*/
/*                 <a role="menuitem" tabindex="-1" href="{{ path ("cms_pessoas_atualizar", {"id": item['item.id'], "tipo" : 2}) }}">*/
/*                     <span class="glyphicon glyphicon-edit"></span> Pessoa Física*/
/*                 </a>*/
/*             </li>*/
/*             <li role="presentation">*/
/*                 <a role="menuitem" tabindex="-1" href="{{ path ("cms_pessoas_atualizar_foto", {"id": item['item.id']}) }}">*/
/*                     <span class="fa fa-image"></span> Atualizar Foto*/
/*                 </a>*/
/*             </li>*/
/* */
/*             <li role="presentation">*/
/*                 <a role="menuitem" tabindex="-1" href="{{ path ("cms_pessoas_visualizar", {"id": item['item.id']}) }}">*/
/*                     <i class="fa fa-search"></i> Visualizar dados do Cliente*/
/*                 </a>*/
/*             </li>*/
/*           </ul>*/
/*         </div>*/
/*     </td>*/
/* {% endblock %}*/

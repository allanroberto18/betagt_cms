<?php

namespace BetaGT\Bundles\CMSBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GaleriasControllerTest extends WebTestCase
{
    public function testNovo()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Galerias/novo');
    }

    public function testUpdate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Galerias/{id}/atualizar');
    }

    public function testList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Galerias/listar');
    }

    public function testView()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Galerias/{id}/visualizar');
    }

    public function testDelete()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Galerias/{id}/delete');
    }

}

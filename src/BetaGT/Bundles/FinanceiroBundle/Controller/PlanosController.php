<?php

namespace BetaGT\Bundles\FinanceiroBundle\Controller;

use BetaGT\Bundles\CMSBundle\Controller\MainController;
use BetaGT\Bundles\FinanceiroBundle\Entity\Planos;
use BetaGT\Bundles\FinanceiroBundle\Form\PlanosType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class PlanosController extends MainController
{
    /**
     * @Route("/Planos/novo", name="fin_planos_novo")
     * @Template("FinanceiroBundle:Planos:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction(Request $request)
    {
        $entity = new Planos();

        $form = $this->createForm(new PlanosType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'fin_planos_novo'
                : 'fin_planos_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home'));
        $breadcrumbs->addItem('Planos: Listar Registros', $this->get('router')->generate('fin_planos_listar', array()));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Planos', 'descricao' => 'Serviços Oferecidos'),
        );
    }

    /**
     * @Route("/Planos/{id}/atualizar", name="fin_planos_atualizar")
     * @Template("CMSBundle:Planos:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('FinanceiroBundle:Planos')->find($id);
        if (!$entity instanceof Planos) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('fin_planos_listar', array());
        }

        $form = $this->createForm(new PlanosType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'fin_planos_novo'
                : 'fin_planos_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Planos: Listar Registros', $this->get('router')->generate('fin_planos_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $entity->getTitulo(), $this->get('router')->generate('fin_planos_visualizar', array('id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Planos', 'descricao' => 'Serviços oferecidos'),
        );
    }

    /**
     * @Route("/Planos/listar", name="fin_planos_listar")
     * @Template("CMSBundle:Planos:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('FinanceiroBundle:Planos');
        $queryBuilder = $repository->createQueryBuilder('item')
            ->where('item.status = :status')
            ->setParameter('status', '1');

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('item.nome', array('label' => 'Nome do Serviço', 'filterable' => 'true', 'sortable' => true)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Planos: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('fin_planos_delete_selecionado'),
            'novo' => $this->generateUrl('fin_planos_novo'),
            'modulo' => array('titulo' => 'Planos', 'descricao' => 'Serviços oferecidos'),
        );
    }

    /**
     * @Route("/Planos/{id}/visualizar", name="fin_planos_visualizar")
     * @Template("CMSBundle:Planos:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('FinanceiroBundle:Planos')->find($id);
        if (!$entity instanceof Planos) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('fin_planos_listar', array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Planos: Listar Registros', $this->get('router')->generate('fin_planos_listar', array()));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar: ' . $entity->getNome(),
            'entity' => $entity,
            'modulo' => array('titulo' => 'Planos', 'descricao' => 'Serviços oferecidos'),
        );   
    }

    /**
     * @Route("/Planos/{id}/delete", name="fin_planos_delete")
     * @Method("GET")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('FinanceiroBundle:Planos')->find($id);
        if (!$entity instanceof Planos) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('fin_planos_listar', array());
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('fin_planos_listar', array());
    }

    /**
     * @Route("/Planos/delete/selecionados", name="fin_planos_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction(Request $request)
    {
        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('fin_planos_listar', array());
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('FinanceiroBundle:Planos')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('fin_planos_listar', array());
    }
}

<?php

namespace BetaGT\Bundles\FinanceiroBundle\Entity;

use BetaGT\Bundles\CMSBundle\Entity\EntityMaster;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Empresas
 *
 * @ORM\Table(name="empresas")
 * @ORM\Entity
 */
class Empresas extends EntityMaster
{
    /**
     * @var string
     * @Assert\NotBlank(message="O campo Razão Social é obrigatório")
     * @ORM\Column(name="razao_social", type="string", length=255, nullable=true)
     */
    private $razaoSocial;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Nome Fantasia é obrigatório")
     * @ORM\Column(name="nome_fantasia", type="string", length=255, nullable=true)
     */
    private $nomeFantasia;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo CNPJ é obrigatório")
     * @ORM\Column(name="cnpj", type="string", length=18, nullable=true)
     */
    private $cnpj;

    /**
     * @var string
     *
     * @ORM\Column(name="inscricao_estadual", type="string", length=45, nullable=true)
     */
    private $inscricaoEstadual;

    /**
     * @var string
     *
     * @ORM\Column(name="inscricao_municiapl", type="string", length=45, nullable=true)
     */
    private $inscricaoMuniciapl;

    /**
     * Set razaoSocial
     *
     * @param string $razaoSocial
     * @return Empresas
     */
    public function setRazaoSocial($razaoSocial)
    {
        $this->razaoSocial = $razaoSocial;

        return $this;
    }

    /**
     * Get razaoSocial
     *
     * @return string
     */
    public function getRazaoSocial()
    {
        return $this->razaoSocial;
    }

    /**
     * Set nomeFantasia
     *
     * @param string $nomeFantasia
     * @return Empresas
     */
    public function setNomeFantasia($nomeFantasia)
    {
        $this->nomeFantasia = $nomeFantasia;

        return $this;
    }

    /**
     * Get nomeFantasia
     *
     * @return string
     */
    public function getNomeFantasia()
    {
        return $this->nomeFantasia;
    }

    /**
     * Set cnpj
     *
     * @param string $cnpj
     * @return Empresas
     */
    public function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;

        return $this;
    }

    /**
     * Get cnpj
     *
     * @return string
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * Set inscricaoEstadual
     *
     * @param string $inscricaoEstadual
     * @return Empresas
     */
    public function setInscricaoEstadual($inscricaoEstadual)
    {
        $this->inscricaoEstadual = $inscricaoEstadual;

        return $this;
    }

    /**
     * Get inscricaoEstadual
     *
     * @return string
     */
    public function getInscricaoEstadual()
    {
        return $this->inscricaoEstadual;
    }

    /**
     * Set inscricaoMuniciapl
     *
     * @param string $inscricaoMuniciapl
     * @return Empresas
     */
    public function setInscricaoMuniciapl($inscricaoMuniciapl)
    {
        $this->inscricaoMuniciapl = $inscricaoMuniciapl;

        return $this;
    }

    /**
     * Get inscricaoMuniciapl
     *
     * @return string
     */
    public function getInscricaoMuniciapl()
    {
        return $this->inscricaoMuniciapl;
    }

    /*
     * @Assert\False(message = "CNPJ inválido")
     */
    public function validateCNPJ()
    {
        $cnpj = $this->cnpj;
        $j = 0;
        for ($i = 0; $i < (strlen($cnpj)); $i++) {
            if (is_numeric($cnpj[$i])) {
                $num[$j] = $cnpj[$i];
                $j++;
            }
        }
        if (count($num) != 14) {
            $isCnpjValid = false;
        }
        if ($num[0] == 0 && $num[1] == 0 && $num[2] == 0 && $num[3] == 0 && $num[4] == 0 && $num[5] == 0 && $num[6] == 0 && $num[7] == 0 && $num[8] == 0 && $num[9] == 0 && $num[10] == 0 && $num[11] == 0) {
            $isCnpjValid = false;
        }
        else {
            $j = 5;
            for ($i = 0; $i < 4; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $j = 9;
            for ($i = 4; $i < 12; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $resto = $soma % 11;
            if ($resto < 2) {
                $dg = 0;
            } else {
                $dg = 11 - $resto;
            }
            if ($dg != $num[12]) {
                $isCnpjValid = false;
            }
        }
        if (!isset($isCnpjValid)) {
            $j = 6;
            for ($i = 0; $i < 5; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $j = 9;
            for ($i = 5; $i < 13; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $resto = $soma % 11;
            if ($resto < 2) {
                $dg = 0;
            } else {
                $dg = 11 - $resto;
            }
            if ($dg != $num[13]) {
                $isCnpjValid = false;
            } else {
                $isCnpjValid = true;
            }
        }
        return $isCnpjValid;
    }
}

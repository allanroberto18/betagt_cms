<?php

namespace BetaGT\Bundles\CMSBundle\Controller;

use BetaGT\Bundles\CMSBundle\Entity\GaleriasPaginas;
use BetaGT\Bundles\CMSBundle\Entity\Paginas;
use BetaGT\Bundles\CMSBundle\Form\GaleriasPaginasType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class GaleriasPaginasController extends MainController
{
    /**
     * @Route("/paginas/{idPagina}/galerias/novo", name="cms_galerias_paginas_novo")
     * @Template("CMSBundle:GaleriasPaginas:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction($idPagina, Request $request)
    {
        $pagina = $this->checkParent($idPagina, 'CMSBundle', 'Paginas', 'cms_paginas_listar', null);

        $entity = new GaleriasPaginas();

        $form = $this->createForm(new GaleriasPaginasType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setPagina($pagina);

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_galerias_paginas_novo'
                : 'cms_galerias_paginas_listar';

            return $this->redirectToRoute($nextAction, array('idPagina' => $idPagina));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home'));
        $breadcrumbs->addItem('Paginas: Listar Registros', $this->get('router')->generate('cms_paginas_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $pagina->getTitulo(), $this->get('router')->generate('cms_paginas_visualizar', array('id' => $idPagina)));
        $breadcrumbs->addItem('Álbum da Página: Listar Registros', $this->get('router')->generate('cms_galerias_paginas_listar', array('idPagina' => $idPagina)));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Álbum da Página', 'descricao' => ''),
        );
    }

    /**
     * @Route("/paginas/{idPagina}/galerias/{id}/atualizar", name="cms_galerias_paginas_atualizar")
     * @Template("CMSBundle:GaleriasPaginas:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($idPagina, $id, Request $request)
    {
        $pagina = $this->checkParent($idPagina, 'CMSBundle', 'Paginas', 'cms_paginas_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:GaleriasPaginas')->find($id);
        if (!$entity instanceof GaleriasPaginas) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_galerias_paginas_listar', array('idPagina' => $idPagina));
        }

        $form = $this->createForm(new GaleriasPaginasType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_galerias_novo'
                : 'cms_galerias_paginas_listar';

            return $this->redirectToRoute($nextAction, array('idPagina' => $idPagina));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Paginas: Listar Registros', $this->get('router')->generate('cms_paginas_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $pagina->getTitulo(), $this->get('router')->generate('cms_paginas_visualizar', array('id' => $idPagina)));
        $breadcrumbs->addItem('Álbum da Página: Listar Registros', $this->get('router')->generate('cms_galerias_paginas_listar', array('idPagina' => $idPagina)));
        $breadcrumbs->addItem('Visualizar: ' . $entity->getTitulo(), $this->get('router')->generate('cms_galerias_paginas_visualizar', array('idPagina' => $idPagina, 'id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Álbum da Página', 'descricao' => ''),
        );
    }

    /**
     * @Route("/paginas/{idPagina}/galerias/listar", name="cms_galerias_paginas_listar")
     * @Template("CMSBundle:GaleriasPaginas:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction($idPagina, Request $request)
    {
        $pagina = $this->checkParent($idPagina, 'CMSBundle', 'Paginas', 'cms_paginas_listar', null);

        $repository = $this->getDoctrine()->getRepository('CMSBundle:GaleriasPaginas');
        $queryBuilder = $repository->createQueryBuilder()
            ->select('item, galeria')
            ->from('CMSBundle:GaleriasPaginas', 'item')
            ->leftJoin('item.galeria', 'galeria')
            ->where('item.status = :status')
            ->andWhere('item.pagina = :pagina')
            ->setParameter('status', '1')
            ->setParameter('pagina', $idPagina)
        ;

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('galeria.titulo', array('label' => 'Galeria de Fotos', 'filterable' => 'true', 'sortable' => true)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Paginas: Listar Registros', $this->get('router')->generate('cms_paginas_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $pagina->getTitulo(), $this->get('router')->generate('cms_paginas_visualizar', array('id' => $idPagina)));
        $breadcrumbs->addItem('Álbum da Página: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('cms_galerias_paginas_delete_selecionado', ['idPagina' => $idPagina]),
            'novo' => $this->generateUrl('cms_galerias_paginas_novo', ['idPagina' => $idPagina]),
            'modulo' => array('titulo' => 'Álbum da Página', 'descricao' => ''),
        );
    }

    /**
     * @Route("/paginas/{idPagina}/galerias/{id}/visualizar", name="cms_galerias_paginas_visualizar")
     * @Template("CMSBundle:GaleriasPaginas:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($idPagina, $id)
    {
        $pagina = $this->checkParent($idPagina, 'CMSBundle', 'Paginas', 'cms_paginas_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:GaleriasPaginas')->find($id);
        if (!$entity instanceof GaleriasPaginas) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_galerias_paginas_listar', array('idPagina' => $idPagina));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Paginas: Listar Registros', $this->get('router')->generate('cms_paginas_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $pagina->getTitulo(), $this->get('router')->generate('cms_paginas_visualizar', array('id' => $idPagina)));
        $breadcrumbs->addItem('Álbum da Página: Listar Registros', $this->get('router')->generate('cms_galerias_paginas_listar', array('idPagina' => $idPagina)));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar: ',
            'entity' => $entity,
            'modulo' => array('titulo' => 'Álbum da Página', 'descricao' => ''),
        );
    }

    /**
     * @Route("/paginas/{idPagina}/galerias/{id}/delete", name="cms_galerias_paginas_delete")
     * @Method("GET")
     */
    public function deleteAction($idPagina, $id)
    {
        $this->checkParent($idPagina, 'CMSBundle', 'Paginas', 'cms_paginas_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:GaleriasPaginas')->find($id);
        if (!$entity instanceof GaleriasPaginas) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_galerias_paginas_listar', array('idPagina' => $idPagina));
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('cms_galerias_paginas_listar', array('idPagina' => $idPagina));
    }

    /**
     * @Route("/paginas/{idPagina}/galerias/delete/selecionados", name="cms_galerias_paginas_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction($idPagina, Request $request)
    {
        $this->checkParent($idPagina, 'CMSBundle', 'Paginas', 'cms_paginas_listar', null);

        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('cms_galerias_paginas_listar', array('idPagina' => $idPagina));
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('CMSBundle:GaleriasPaginas')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('cms_galerias_paginas_listar', array('idPagina' => $idPagina));
    }
}

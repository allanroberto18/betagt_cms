<?php

namespace BetaGT\Bundles\ProdutosBundle\Entity;

use BetaGT\Bundles\CMSBundle\Entity\EntityMaster;
use BetaGT\Bundles\CMSBundle\Entity\TSlug;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CategoriasProdutos
 *
 * @ORM\Table(name="categorias_produtos")
 * @ORM\Entity
 */
class CategoriasProdutos extends EntityMaster
{
    use TSlug;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Título é obrigatório")
     * @ORM\Column(name="titulo", type="string", length=250, nullable=false)
     */
    private $titulo;

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return CategoriasProdutos
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    function __toString()
    {
        return $this->titulo;
    }


}

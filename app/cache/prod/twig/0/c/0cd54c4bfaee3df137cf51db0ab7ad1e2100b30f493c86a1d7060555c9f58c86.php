<?php

/* CMSBundle:Default:base_menu.html.twig */
class __TwigTemplate_31783ce59818f34b226ff87f7a55fafa870dc28a241565362437bdd7c27a0ad3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("knp_menu_base.html.twig", "CMSBundle:Default:base_menu.html.twig", 1);
        $this->blocks = array(
            'compressed_root' => array($this, 'block_compressed_root'),
            'root' => array($this, 'block_root'),
            'list' => array($this, 'block_list'),
            'children' => array($this, 'block_children'),
            'item' => array($this, 'block_item'),
            'linkElement' => array($this, 'block_linkElement'),
            'spanElement' => array($this, 'block_spanElement'),
            'label' => array($this, 'block_label'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "knp_menu_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 10
    public function block_compressed_root($context, array $blocks = array())
    {
        // line 11
        echo "    ";
        ob_start();
        // line 12
        echo "        ";
        $this->displayBlock("root", $context, $blocks);
        echo "
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 16
    public function block_root($context, array $blocks = array())
    {
        // line 17
        echo "    ";
        $context["listAttributes"] = $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "childrenAttributes", array());
        // line 18
        echo "    ";
        $this->displayBlock("list", $context, $blocks);
    }

    // line 21
    public function block_list($context, array $blocks = array())
    {
        // line 22
        echo "    ";
        if ((($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "hasChildren", array()) &&  !($this->getAttribute((isset($context["options"]) ? $context["options"] : null), "depth", array()) === 0)) && $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "displayChildren", array()))) {
            // line 23
            echo "        ";
            $context["knp_menu"] = $this;
            // line 24
            echo "        <ul ";
            echo $context["knp_menu"]->getattributes((isset($context["listAttributes"]) ? $context["listAttributes"] : null));
            echo ">
            ";
            // line 25
            $this->displayBlock("children", $context, $blocks);
            echo "
        </ul>
    ";
        }
    }

    // line 30
    public function block_children($context, array $blocks = array())
    {
        // line 31
        echo "    ";
        // line 32
        echo "    ";
        $context["currentOptions"] = (isset($context["options"]) ? $context["options"] : null);
        // line 33
        echo "    ";
        $context["currentItem"] = (isset($context["item"]) ? $context["item"] : null);
        // line 34
        echo "    ";
        // line 35
        echo "    ";
        if ( !(null === $this->getAttribute((isset($context["options"]) ? $context["options"] : null), "depth", array()))) {
            // line 36
            echo "        ";
            $context["options"] = twig_array_merge((isset($context["options"]) ? $context["options"] : null), array("depth" => ($this->getAttribute((isset($context["currentOptions"]) ? $context["currentOptions"] : null), "depth", array()) - 1)));
            // line 37
            echo "    ";
        }
        // line 38
        echo "    ";
        // line 39
        echo "    ";
        if (( !(null === $this->getAttribute((isset($context["options"]) ? $context["options"] : null), "matchingDepth", array())) && ($this->getAttribute((isset($context["options"]) ? $context["options"] : null), "matchingDepth", array()) > 0))) {
            // line 40
            echo "        ";
            $context["options"] = twig_array_merge((isset($context["options"]) ? $context["options"] : null), array("matchingDepth" => ($this->getAttribute((isset($context["currentOptions"]) ? $context["currentOptions"] : null), "matchingDepth", array()) - 1)));
            // line 41
            echo "    ";
        }
        // line 42
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["currentItem"]) ? $context["currentItem"] : null), "children", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 43
            echo "        ";
            $this->displayBlock("item", $context, $blocks);
            echo "
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "    ";
        // line 46
        echo "    ";
        $context["item"] = (isset($context["currentItem"]) ? $context["currentItem"] : null);
        // line 47
        echo "    ";
        $context["options"] = (isset($context["currentOptions"]) ? $context["currentOptions"] : null);
    }

    // line 50
    public function block_item($context, array $blocks = array())
    {
        // line 51
        echo "    ";
        if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "displayed", array())) {
            // line 52
            echo "        ";
            // line 53
            $context["classes"] = (( !twig_test_empty($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "attribute", array(0 => "class"), "method"))) ? (array(0 => $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "attribute", array(0 => "class"), "method"))) : (array()));
            // line 54
            if ($this->getAttribute((isset($context["matcher"]) ? $context["matcher"] : null), "isCurrent", array(0 => (isset($context["item"]) ? $context["item"] : null)), "method")) {
                // line 55
                $context["classes"] = twig_array_merge((isset($context["classes"]) ? $context["classes"] : null), array(0 => $this->getAttribute((isset($context["options"]) ? $context["options"] : null), "currentClass", array())));
            } elseif ($this->getAttribute(            // line 56
(isset($context["matcher"]) ? $context["matcher"] : null), "isAncestor", array(0 => (isset($context["item"]) ? $context["item"] : null), 1 => $this->getAttribute((isset($context["options"]) ? $context["options"] : null), "matchingDepth", array())), "method")) {
                // line 57
                $context["classes"] = twig_array_merge((isset($context["classes"]) ? $context["classes"] : null), array(0 => $this->getAttribute((isset($context["options"]) ? $context["options"] : null), "ancestorClass", array())));
            }
            // line 59
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "actsLikeFirst", array())) {
                // line 60
                $context["classes"] = twig_array_merge((isset($context["classes"]) ? $context["classes"] : null), array(0 => $this->getAttribute((isset($context["options"]) ? $context["options"] : null), "firstClass", array())));
            }
            // line 62
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "actsLikeLast", array())) {
                // line 63
                $context["classes"] = twig_array_merge((isset($context["classes"]) ? $context["classes"] : null), array(0 => $this->getAttribute((isset($context["options"]) ? $context["options"] : null), "lastClass", array())));
            }
            // line 65
            echo "
        ";
            // line 67
            echo "        ";
            if (($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "hasChildren", array()) &&  !($this->getAttribute((isset($context["options"]) ? $context["options"] : null), "depth", array()) === 0))) {
                // line 68
                echo "            ";
                if (( !twig_test_empty($this->getAttribute((isset($context["options"]) ? $context["options"] : null), "branch_class", array())) && $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "displayChildren", array()))) {
                    // line 69
                    $context["classes"] = twig_array_merge((isset($context["classes"]) ? $context["classes"] : null), array(0 => $this->getAttribute((isset($context["options"]) ? $context["options"] : null), "branch_class", array())));
                    // line 70
                    echo "            ";
                }
                // line 71
                echo "        ";
            } elseif ( !twig_test_empty($this->getAttribute((isset($context["options"]) ? $context["options"] : null), "leaf_class", array()))) {
                // line 72
                $context["classes"] = twig_array_merge((isset($context["classes"]) ? $context["classes"] : null), array(0 => $this->getAttribute((isset($context["options"]) ? $context["options"] : null), "leaf_class", array())));
            }
            // line 75
            $context["attributes"] = $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "attributes", array());
            // line 76
            if ( !twig_test_empty((isset($context["classes"]) ? $context["classes"] : null))) {
                // line 77
                $context["attributes"] = twig_array_merge((isset($context["attributes"]) ? $context["attributes"] : null), array("class" => twig_join_filter((isset($context["classes"]) ? $context["classes"] : null), " ")));
            }
            // line 79
            echo "        ";
            // line 80
            echo "        ";
            $context["knp_menu"] = $this;
            // line 81
            echo "        <li ";
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "attribute", array(0 => "class"), "method")) {
                echo "class=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "attribute", array(0 => "class"), "method"), "html", null, true);
                echo "\" ";
            }
            echo " >";
            // line 82
            if (( !twig_test_empty($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "uri", array())) && ( !$this->getAttribute((isset($context["matcher"]) ? $context["matcher"] : null), "isCurrent", array(0 => (isset($context["item"]) ? $context["item"] : null)), "method") || $this->getAttribute((isset($context["options"]) ? $context["options"] : null), "currentAsLink", array())))) {
                // line 83
                echo "                ";
                $this->displayBlock("linkElement", $context, $blocks);
            } else {
                // line 85
                echo "                ";
                $this->displayBlock("spanElement", $context, $blocks);
            }
            // line 87
            echo "            ";
            // line 88
            $context["childrenClasses"] = (( !twig_test_empty($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "childrenAttribute", array(0 => "class"), "method"))) ? (array(0 => $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "childrenAttribute", array(0 => "class"), "method"))) : (array()));
            // line 89
            echo "            ";
            // line 90
            $context["listAttributes"] = twig_array_merge($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "childrenAttributes", array()), array("class" => twig_join_filter((isset($context["childrenClasses"]) ? $context["childrenClasses"] : null), " ")));
            // line 91
            echo "            ";
            $this->displayBlock("list", $context, $blocks);
            echo "
        </li>
        ";
            // line 94
            echo "    ";
        }
    }

    // line 97
    public function block_linkElement($context, array $blocks = array())
    {
        // line 98
        echo "    ";
        $context["knp_menu"] = $this;
        // line 99
        echo "    <a href=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "uri", array()), "html", null, true);
        echo "\"";
        echo $context["knp_menu"]->getattributes($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "linkAttributes", array()));
        echo ">
        <i class=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "attribute", array(0 => "icon"), "method"), "html", null, true);
        echo "\"></i><span>";
        $this->displayBlock("label", $context, $blocks);
        echo "</span>
    </a>
";
    }

    // line 104
    public function block_spanElement($context, array $blocks = array())
    {
        // line 105
        echo "    ";
        $context["knp_menu"] = $this;
        // line 106
        echo "    <span";
        echo $context["knp_menu"]->getattributes($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "labelAttributes", array()));
        echo ">";
        $this->displayBlock("label", $context, $blocks);
        echo "</span>
";
    }

    // line 109
    public function block_label($context, array $blocks = array())
    {
        if (($this->getAttribute((isset($context["options"]) ? $context["options"] : null), "allow_safe_labels", array()) && $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "getExtra", array(0 => "safe_label", 1 => false), "method"))) {
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "label", array());
        } else {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "label", array()), "html", null, true);
        }
    }

    // line 3
    public function getattributes($__attributes__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "attributes" => $__attributes__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 4
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["attributes"]) ? $context["attributes"] : null));
            foreach ($context['_seq'] as $context["name"] => $context["value"]) {
                // line 5
                if (( !(null === $context["value"]) &&  !($context["value"] === false))) {
                    // line 6
                    echo sprintf(" %s=\"%s\"", $context["name"], ((($context["value"] === true)) ? (twig_escape_filter($this->env, $context["name"])) : (twig_escape_filter($this->env, $context["value"]))));
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['name'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "CMSBundle:Default:base_menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  324 => 6,  322 => 5,  317 => 4,  305 => 3,  295 => 109,  286 => 106,  283 => 105,  280 => 104,  271 => 100,  264 => 99,  261 => 98,  258 => 97,  253 => 94,  247 => 91,  245 => 90,  243 => 89,  241 => 88,  239 => 87,  235 => 85,  231 => 83,  229 => 82,  221 => 81,  218 => 80,  216 => 79,  213 => 77,  211 => 76,  209 => 75,  206 => 72,  203 => 71,  200 => 70,  198 => 69,  195 => 68,  192 => 67,  189 => 65,  186 => 63,  184 => 62,  181 => 60,  179 => 59,  176 => 57,  174 => 56,  172 => 55,  170 => 54,  168 => 53,  166 => 52,  163 => 51,  160 => 50,  155 => 47,  152 => 46,  150 => 45,  133 => 43,  115 => 42,  112 => 41,  109 => 40,  106 => 39,  104 => 38,  101 => 37,  98 => 36,  95 => 35,  93 => 34,  90 => 33,  87 => 32,  85 => 31,  82 => 30,  74 => 25,  69 => 24,  66 => 23,  63 => 22,  60 => 21,  55 => 18,  52 => 17,  49 => 16,  41 => 12,  38 => 11,  35 => 10,  11 => 1,);
    }
}
/* {% extends 'knp_menu_base.html.twig' %}*/
/* */
/* {% macro attributes(attributes) %}*/
/*     {% for name, value in attributes %}*/
/*         {%- if value is not none and value is not sameas(false) -%}*/
/*             {{- ' %s="%s"'|format(name, value is sameas(true) ? name|e : value|e)|raw -}}*/
/*         {%- endif -%}*/
/*     {%- endfor -%}*/
/* {% endmacro %}*/
/* {% block compressed_root %}*/
/*     {% spaceless %}*/
/*         {{ block('root') }}*/
/*     {% endspaceless %}*/
/* {% endblock %}*/
/* */
/* {% block root %}*/
/*     {% set listAttributes = item.childrenAttributes %}*/
/*     {{ block('list') -}}*/
/* {% endblock %}*/
/* */
/* {% block list %}*/
/*     {% if item.hasChildren and options.depth is not sameas(0) and item.displayChildren %}*/
/*         {% import _self as knp_menu %}*/
/*         <ul {{ knp_menu.attributes(listAttributes) }}>*/
/*             {{ block('children') }}*/
/*         </ul>*/
/*     {% endif %}*/
/* {% endblock %}*/
/* */
/* {% block children %}*/
/*     {# save current variables #}*/
/*     {% set currentOptions = options %}*/
/*     {% set currentItem = item %}*/
/*     {# update the depth for children #}*/
/*     {% if options.depth is not none %}*/
/*         {% set options = options|merge({'depth': currentOptions.depth - 1}) %}*/
/*     {% endif %}*/
/*     {# update the matchingDepth for children #}*/
/*     {% if options.matchingDepth is not none and options.matchingDepth > 0 %}*/
/*         {% set options = options|merge({'matchingDepth': currentOptions.matchingDepth - 1}) %}*/
/*     {% endif %}*/
/*     {% for item in currentItem.children %}*/
/*         {{ block('item') }}*/
/*     {% endfor %}*/
/*     {# restore current variables #}*/
/*     {% set item = currentItem %}*/
/*     {% set options = currentOptions %}*/
/* {% endblock %}*/
/* */
/* {% block item %}*/
/*     {% if item.displayed %}*/
/*         {# building the class of the item #}*/
/*         {%- set classes = item.attribute('class') is not empty ? [item.attribute('class')] : [] %}*/
/*         {%- if matcher.isCurrent(item) %}*/
/*             {%- set classes = classes|merge([options.currentClass]) %}*/
/*         {%- elseif matcher.isAncestor(item, options.matchingDepth) %}*/
/*             {%- set classes = classes|merge([options.ancestorClass]) %}*/
/*         {%- endif %}*/
/*         {%- if item.actsLikeFirst %}*/
/*             {%- set classes = classes|merge([options.firstClass]) %}*/
/*         {%- endif %}*/
/*         {%- if item.actsLikeLast %}*/
/*             {%- set classes = classes|merge([options.lastClass]) %}*/
/*         {%- endif %}*/
/* */
/*         {# Mark item as "leaf" (no children) or as "branch" (has children that are displayed) #}*/
/*         {% if item.hasChildren and options.depth is not sameas(0) %}*/
/*             {% if options.branch_class is not empty and item.displayChildren %}*/
/*                 {%- set classes = classes|merge([options.branch_class]) %}*/
/*             {% endif %}*/
/*         {% elseif options.leaf_class is not empty %}*/
/*             {%- set classes = classes|merge([options.leaf_class]) %}*/
/*         {%- endif %}*/
/* */
/*         {%- set attributes = item.attributes %}*/
/*         {%- if classes is not empty %}*/
/*             {%- set attributes = attributes|merge({'class': classes|join(' ')}) %}*/
/*         {%- endif %}*/
/*         {# displaying the item #}*/
/*         {% import _self as knp_menu %}*/
/*         <li {% if item.attribute('class') %}class="{{ item.attribute('class') }}" {% endif %} >*/
/*             {%- if item.uri is not empty and (not matcher.isCurrent(item) or options.currentAsLink) %}*/
/*                 {{ block('linkElement') }}*/
/*             {%- else %}*/
/*                 {{ block('spanElement') }}*/
/*             {%- endif %}*/
/*             {# render the list of children#}*/
/*             {%- set childrenClasses = item.childrenAttribute('class') is not empty ? [item.childrenAttribute('class')] : [] %}*/
/*             {#{%- set childrenClasses = childrenClasses|merge(['menu_level_' ~ item.level]) %}#}*/
/*             {%- set listAttributes = item.childrenAttributes|merge({'class': childrenClasses|join(' ') }) %}*/
/*             {{ block('list') }}*/
/*         </li>*/
/*         {#{{ dump(item.attributes) }}#}*/
/*     {% endif %}*/
/* {% endblock %}*/
/* */
/* {% block linkElement %}*/
/*     {% import _self as knp_menu %}*/
/*     <a href="{{ item.uri }}"{{ knp_menu.attributes(item.linkAttributes) }}>*/
/*         <i class="{{ item.attribute('icon') }}"></i><span>{{ block('label') }}</span>*/
/*     </a>*/
/* {% endblock %}*/
/* */
/* {% block spanElement %}*/
/*     {% import _self as knp_menu %}*/
/*     <span{{ knp_menu.attributes(item.labelAttributes) }}>{{ block('label') }}</span>*/
/* {% endblock %}*/
/* */
/* {% block label %}{% if options.allow_safe_labels and item.getExtra('safe_label', false) %}{{ item.label|raw }}{% else %}{{ item.label }}{% endif %}{% endblock %}*/
/* */

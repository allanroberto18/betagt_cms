<?php

namespace BetaGT\Bundles\PortalBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/")
 */
class HomeController extends MainController
{
    /**
     * @Route("/", name="front_home")
     * @Template("PortalBundle:home:index.html.twig")
     * @Method("GET")
     */
    public function indexAction()
    {
        return [
				"titulo" => "Portal",
				"pagNossasNoticias" => $this->container->get('portal.paginas')->returnRegister(2),
				"pagFaleConosco" => $this->container->get('portal.paginas')->returnRegister(1),
				"pagBemVindo" => $this->container->get('portal.paginas')->returnRegister(3),
				"pagSecundaria" => $this->container->get('portal.paginas')->returnRegister(4),
				"pagTerciaria" => $this->container->get('portal.paginas')->returnRegister(5),
				"pagSobre" => $this->container->get('portal.paginas')->returnRegister(6),
				"pagProduto" => $this->container->get('portal.paginas')->returnRegister(13),
				"pagReceita" => $this->container->get('portal.paginas')->returnRegister(14),
				"pagSobre1" => [
					0 => $this->container->get('portal.paginas')->returnRegister(7),
					1 => $this->container->get('portal.paginas')->returnRegister(8),
					2 => $this->container->get('portal.paginas')->returnRegister(9),
				],
				"pagSobre2" => [
					0 => $this->container->get('portal.paginas')->returnRegister(10),
					1 => $this->container->get('portal.paginas')->returnRegister(11),
				],
				"galerias" => $this->container->get('portal.galerias')->returnAll(),
				"assuntos" => $this->container->get('portal.assuntos')->returnAll(),
				"receitas" => $this->container->get('portal.receitas')->returnAll(),
				"noticias" => $this->container->get('portal.noticias')->returnNoticias(3,1),
				"banners" => $this->container->get('portal.banners')->returnAll(),
				"produtos" => $this->container->get('portal.produtos')->returnAll(),
        ];
    }
}
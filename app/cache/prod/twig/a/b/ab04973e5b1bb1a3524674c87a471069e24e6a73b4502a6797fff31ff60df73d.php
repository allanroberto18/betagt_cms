<?php

/* TwigBundle:Exception:error403.html.twig */
class __TwigTemplate_25ee03a86074b6419c10c129a96ba2c7c6152561dedecf80aca026dbd619bd90 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CMSBundle:Default:security.html.twig", "TwigBundle:Exception:error403.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CMSBundle:Default:security.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "    <h1>Problema de acesso</h1>
    <h2>Você não tem permissão para acessar essa área.</h2>
    <div>
        Contate o administrador do site
    </div>
";
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error403.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "CMSBundle:Default:security.html.twig" %}*/
/* {% block body %}*/
/*     <h1>Problema de acesso</h1>*/
/*     <h2>Você não tem permissão para acessar essa área.</h2>*/
/*     <div>*/
/*         Contate o administrador do site*/
/*     </div>*/
/* {% endblock %}*/

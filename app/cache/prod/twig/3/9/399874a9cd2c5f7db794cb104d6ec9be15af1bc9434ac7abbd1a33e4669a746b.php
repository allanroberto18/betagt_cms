<?php

/* PortalBundle:home:index.html.twig */
class __TwigTemplate_c44778280ed4f9c7e38bc05d70fd1651faff002bbf1f28ca288b39cd85b2ed2f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("PortalBundle:Default:default.html.twig", "PortalBundle:home:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "PortalBundle:Default:default.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (isset($context["titulo"]) ? $context["titulo"] : null), "html", null, true);
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    <!-- bem vindo -->
    <section id=\"welcome\" class=\"welcome section\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"section-title animated\" data-animation=\"fadeInUp\" data-animation-delay=\"700\">
                    <h2><span class=\"extrabold\">";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pagBemVindo"]) ? $context["pagBemVindo"] : null), "retranca", array()), "html", null, true);
        echo "</span> ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pagBemVindo"]) ? $context["pagBemVindo"] : null), "titulo", array()), "html", null, true);
        echo "</h2>
                    <div class=\"section-title-line animated\" data-animation=\"fadeInUp\" data-animation-delay=\"900\">
                        <div class=\"section-title-icon\">
                            <img src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/images/shrimp_icon.png"), "html", null, true);
        echo "\" alt=\"\">
                        </div>
                        <hr>
                    </div>
                    <p>
                        ";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pagBemVindo"]) ? $context["pagBemVindo"] : null), "resumo", array()), "html", null, true);
        echo "
                    </p>
                </div><!-- end .section-title -->
                <div class=\"welcome-image text-center\">
                    <img src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl(("uploads/paginas/" . $this->getAttribute((isset($context["pagBemVindo"]) ? $context["pagBemVindo"] : null), "imageName", array()))), "html", null, true);
        echo "\" alt=\"\">
                </div>
                <br />
                <div class=\"col-md-6\">
                    <h3 class=\"without-margin\">
                        <span class=\"extrabold\">
                            ";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pagSecundaria"]) ? $context["pagSecundaria"] : null), "retranca", array()), "html", null, true);
        echo "
                        </span>
                    </h3>
                    <h3>";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pagSecundaria"]) ? $context["pagSecundaria"] : null), "titulo", array()), "html", null, true);
        echo "</h3>
                    <p>
                        ";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pagSecundaria"]) ? $context["pagSecundaria"] : null), "resumo", array()), "html", null, true);
        echo "
                        <br/>
                        <a href=\"fron\">Leia mais</a>
                    </p>
                </div>
                <div class=\"col-md-6\">
                    <h3 class=\"without-margin\">
                        <span class=\"extrabold\">
                            ";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pagTerciaria"]) ? $context["pagTerciaria"] : null), "retranca", array()), "html", null, true);
        echo "
                        </span>
                    </h3>
                    <h3>";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pagTerciaria"]) ? $context["pagTerciaria"] : null), "titulo", array()), "html", null, true);
        echo "</h3>
                    <p>
                        ";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pagTerciaria"]) ? $context["pagTerciaria"] : null), "resumo", array()), "html", null, true);
        echo "
                    </p>
                </div>
            </div><!-- end .row -->
        </div><!-- end .container -->
    </section>
    <!-- bem vindo -->
    <!-- about us -->
    <section id=\"about\" class=\"about section grey section-without-pb\">
        <div class=\"container-fluid\">
            <div class=\"row\">
                <div class=\"section-title animated\" data-animation=\"fadeInUp\" data-animation-delay=\"700\">
                    <h2><span class=\"extrabold\">";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pagSobre"]) ? $context["pagSobre"] : null), "retranca", array()), "html", null, true);
        echo "</h2>
                    <div class=\"section-title-line\">
                        <div class=\"section-title-icon\">
                            <img src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/images/shrimp_icon.png"), "html", null, true);
        echo "\" alt=\"\">
                        </div>
                        <hr>
                    </div>
                    <p>
                        ";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pagSobre"]) ? $context["pagSobre"] : null), "resumo", array()), "html", null, true);
        echo "
                    </p>
                </div><!-- end .section-title -->
                <div class=\"col-md-6 side-image-left\">
                    <div class=\"image-slider owl-carousel animated\" data-animation=\"fadeInLeft\" data-animation-delay=\"1000\">
                        <div class=\"swiper-slide\"><img src=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl(("uploads/paginas/" . $this->getAttribute((isset($context["pagSobre"]) ? $context["pagSobre"] : null), "imageName", array()))), "html", null, true);
        echo "\" alt=\"\"></div>
                    </div><!-- end .image-slider -->
                </div>
                <div class=\"col-md-6\">
                    <div class=\"tab-set animated\" data-animation=\"fadeInRight\" data-animation-delay=\"1000\">
                        <ul class=\"tabs-titles\">
                            ";
        // line 76
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagSobre1"]) ? $context["pagSobre1"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 77
            echo "                            <li>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "titulo", array()), "html", null, true);
            echo "</li>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 79
        echo "                        </ul>
                        ";
        // line 80
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagSobre1"]) ? $context["pagSobre1"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 81
            echo "                        <div class=\"tab-content\">
                            <p>
                                ";
            // line 83
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "resumo", array()), "html", null, true);
            echo "
                            </p>
                        </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 87
        echo "                    </div><!-- end .tab-set -->

                    <div class=\"accordion animated\" data-animation=\"fadeInRight\" data-animation-delay=\"1000\">
                        ";
        // line 90
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagSobre2"]) ? $context["pagSobre2"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 91
            echo "                        <div class=\"acc-item\">
                            <div class=\"acc-head ";
            // line 92
            if ($this->getAttribute($context["loop"], "first", array())) {
                echo "current";
            }
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "titulo", array()), "html", null, true);
            echo "</div>
                            <div class=\"acc-content\">
                                <p>
                                    ";
            // line 95
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "resumo", array()), "html", null, true);
            echo "
                                </p>
                            </div><!-- end .acc-content -->
                        </div><!-- end .acc-item -->
                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 100
        echo "                    </div><!-- end .accordion -->
                </div>
            </div><!-- end .row -->
        </div><!-- end .container -->
    </section>
    <!-- notices -->
    <section id=\"blog\" class=\"blog section\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"section-title animated\" data-animation=\"fadeInUp\" data-animation-delay=\"700\">
                    <h2><span class=\"extrabold\">Nossas</span> Notícias</h2>
                    <div class=\"section-title-line\">
                        <div class=\"section-title-icon\">
                            <img src=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/images/shrimp_icon.png"), "html", null, true);
        echo "\" alt=\"\">
                        </div>
                        <hr>
                    </div>
                    ";
        // line 117
        echo $this->getAttribute((isset($context["pagNossasNoticias"]) ? $context["pagNossasNoticias"] : null), "texto", array());
        echo "
                </div><!-- end .section-title -->

                ";
        // line 120
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["noticias"]) ? $context["noticias"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 121
            echo "                    <article class=\"post-item\">
                        <div class=\"row\">
                            <div class=\"col-md-6\">
                                <div class=\"post-image\">
                                    <a href=\"";
            // line 125
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("front_noticia", array("id" => $this->getAttribute($context["item"], "id", array()), "slug" => $this->getAttribute($context["item"], "slug", array()))), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl(("uploads/noticias/" . $this->getAttribute($context["item"], "imageName", array()))), "html", null, true);
            echo "\" alt=\"\"></a>
                                </div><!-- end .post-image -->
                            </div><!-- end .col -->
                            <div class=\"col-md-6\">
                                <div class=\"post-content\">
                                    <h3 class=\"post-title\"><a href=\"";
            // line 130
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("front_noticia", array("id" => $this->getAttribute($context["item"], "id", array()), "slug" => $this->getAttribute($context["item"], "slug", array()))), "html", null, true);
            echo "\"><span class=\"extrabold\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "titulo", array()), "html", null, true);
            echo "</span></a></h3>
                                    <div class=\"post-meta\">
                                        <span class=\"post-date\">";
            // line 132
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["item"], "created", array()), "d/m/Y H:i:s"), "html", null, true);
            echo "</span> por <a href=\"#\">Departamento de Comunicação</a>
                                    </div><!-- end .post-meta -->
                                    <div class=\"post-entry\">
                                        <p>
                                            ";
            // line 136
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "resumo", array()), "html", null, true);
            echo "
                                        </p>
                                    </div><!-- end .post-entry -->
                                </div><!-- end .post-content -->
                            </div><!-- end .col -->
                        </div><!-- end .row -->
                    </article>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 144
        echo "            </div><!-- end .row -->
        </div><!-- end .container -->
    </section>
";
    }

    public function getTemplateName()
    {
        return "PortalBundle:home:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  320 => 144,  306 => 136,  299 => 132,  292 => 130,  282 => 125,  276 => 121,  272 => 120,  266 => 117,  259 => 113,  244 => 100,  225 => 95,  215 => 92,  212 => 91,  195 => 90,  190 => 87,  180 => 83,  176 => 81,  172 => 80,  169 => 79,  160 => 77,  156 => 76,  147 => 70,  139 => 65,  131 => 60,  125 => 57,  110 => 45,  105 => 43,  99 => 40,  88 => 32,  83 => 30,  77 => 27,  68 => 21,  61 => 17,  53 => 12,  45 => 9,  38 => 4,  35 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends 'PortalBundle:Default:default.html.twig' %}*/
/* {% block title %}{{ titulo }}{% endblock %}*/
/* {% block body %}*/
/*     <!-- bem vindo -->*/
/*     <section id="welcome" class="welcome section">*/
/*         <div class="container">*/
/*             <div class="row">*/
/*                 <div class="section-title animated" data-animation="fadeInUp" data-animation-delay="700">*/
/*                     <h2><span class="extrabold">{{ pagBemVindo.retranca }}</span> {{ pagBemVindo.titulo }}</h2>*/
/*                     <div class="section-title-line animated" data-animation="fadeInUp" data-animation-delay="900">*/
/*                         <div class="section-title-icon">*/
/*                             <img src="{{ asset('frangonorte/images/shrimp_icon.png' ) }}" alt="">*/
/*                         </div>*/
/*                         <hr>*/
/*                     </div>*/
/*                     <p>*/
/*                         {{ pagBemVindo.resumo }}*/
/*                     </p>*/
/*                 </div><!-- end .section-title -->*/
/*                 <div class="welcome-image text-center">*/
/*                     <img src="{{ asset('uploads/paginas/' ~ pagBemVindo.imageName) }}" alt="">*/
/*                 </div>*/
/*                 <br />*/
/*                 <div class="col-md-6">*/
/*                     <h3 class="without-margin">*/
/*                         <span class="extrabold">*/
/*                             {{ pagSecundaria.retranca }}*/
/*                         </span>*/
/*                     </h3>*/
/*                     <h3>{{ pagSecundaria.titulo }}</h3>*/
/*                     <p>*/
/*                         {{ pagSecundaria.resumo }}*/
/*                         <br/>*/
/*                         <a href="fron">Leia mais</a>*/
/*                     </p>*/
/*                 </div>*/
/*                 <div class="col-md-6">*/
/*                     <h3 class="without-margin">*/
/*                         <span class="extrabold">*/
/*                             {{ pagTerciaria.retranca }}*/
/*                         </span>*/
/*                     </h3>*/
/*                     <h3>{{ pagTerciaria.titulo }}</h3>*/
/*                     <p>*/
/*                         {{ pagTerciaria.resumo }}*/
/*                     </p>*/
/*                 </div>*/
/*             </div><!-- end .row -->*/
/*         </div><!-- end .container -->*/
/*     </section>*/
/*     <!-- bem vindo -->*/
/*     <!-- about us -->*/
/*     <section id="about" class="about section grey section-without-pb">*/
/*         <div class="container-fluid">*/
/*             <div class="row">*/
/*                 <div class="section-title animated" data-animation="fadeInUp" data-animation-delay="700">*/
/*                     <h2><span class="extrabold">{{ pagSobre.retranca }}</h2>*/
/*                     <div class="section-title-line">*/
/*                         <div class="section-title-icon">*/
/*                             <img src="{{ asset('frangonorte/images/shrimp_icon.png') }}" alt="">*/
/*                         </div>*/
/*                         <hr>*/
/*                     </div>*/
/*                     <p>*/
/*                         {{ pagSobre.resumo }}*/
/*                     </p>*/
/*                 </div><!-- end .section-title -->*/
/*                 <div class="col-md-6 side-image-left">*/
/*                     <div class="image-slider owl-carousel animated" data-animation="fadeInLeft" data-animation-delay="1000">*/
/*                         <div class="swiper-slide"><img src="{{ asset('uploads/paginas/' ~ pagSobre.imageName) }}" alt=""></div>*/
/*                     </div><!-- end .image-slider -->*/
/*                 </div>*/
/*                 <div class="col-md-6">*/
/*                     <div class="tab-set animated" data-animation="fadeInRight" data-animation-delay="1000">*/
/*                         <ul class="tabs-titles">*/
/*                             {% for item in pagSobre1 %}*/
/*                             <li>{{ item.titulo }}</li>*/
/*                             {% endfor %}*/
/*                         </ul>*/
/*                         {% for item in pagSobre1 %}*/
/*                         <div class="tab-content">*/
/*                             <p>*/
/*                                 {{ item.resumo }}*/
/*                             </p>*/
/*                         </div>*/
/*                         {% endfor %}*/
/*                     </div><!-- end .tab-set -->*/
/* */
/*                     <div class="accordion animated" data-animation="fadeInRight" data-animation-delay="1000">*/
/*                         {% for item in pagSobre2 %}*/
/*                         <div class="acc-item">*/
/*                             <div class="acc-head {% if loop.first %}current{% endif %}">{{ item.titulo }}</div>*/
/*                             <div class="acc-content">*/
/*                                 <p>*/
/*                                     {{ item.resumo }}*/
/*                                 </p>*/
/*                             </div><!-- end .acc-content -->*/
/*                         </div><!-- end .acc-item -->*/
/*                         {% endfor %}*/
/*                     </div><!-- end .accordion -->*/
/*                 </div>*/
/*             </div><!-- end .row -->*/
/*         </div><!-- end .container -->*/
/*     </section>*/
/*     <!-- notices -->*/
/*     <section id="blog" class="blog section">*/
/*         <div class="container">*/
/*             <div class="row">*/
/*                 <div class="section-title animated" data-animation="fadeInUp" data-animation-delay="700">*/
/*                     <h2><span class="extrabold">Nossas</span> Notícias</h2>*/
/*                     <div class="section-title-line">*/
/*                         <div class="section-title-icon">*/
/*                             <img src="{{ asset('frangonorte/images/shrimp_icon.png') }}" alt="">*/
/*                         </div>*/
/*                         <hr>*/
/*                     </div>*/
/*                     {{ pagNossasNoticias.texto|raw }}*/
/*                 </div><!-- end .section-title -->*/
/* */
/*                 {% for item in noticias %}*/
/*                     <article class="post-item">*/
/*                         <div class="row">*/
/*                             <div class="col-md-6">*/
/*                                 <div class="post-image">*/
/*                                     <a href="{{ path('front_noticia', {'id' : item.id, 'slug': item.slug}) }}"><img src="{{ asset('uploads/noticias/' ~ item.imageName) }}" alt=""></a>*/
/*                                 </div><!-- end .post-image -->*/
/*                             </div><!-- end .col -->*/
/*                             <div class="col-md-6">*/
/*                                 <div class="post-content">*/
/*                                     <h3 class="post-title"><a href="{{ path('front_noticia', {'id' : item.id, 'slug': item.slug}) }}"><span class="extrabold">{{ item.titulo }}</span></a></h3>*/
/*                                     <div class="post-meta">*/
/*                                         <span class="post-date">{{ item.created|date('d/m/Y H:i:s') }}</span> por <a href="#">Departamento de Comunicação</a>*/
/*                                     </div><!-- end .post-meta -->*/
/*                                     <div class="post-entry">*/
/*                                         <p>*/
/*                                             {{ item.resumo }}*/
/*                                         </p>*/
/*                                     </div><!-- end .post-entry -->*/
/*                                 </div><!-- end .post-content -->*/
/*                             </div><!-- end .col -->*/
/*                         </div><!-- end .row -->*/
/*                     </article>*/
/*                 {% endfor %}*/
/*             </div><!-- end .row -->*/
/*         </div><!-- end .container -->*/
/*     </section>*/
/* {% endblock %}*/

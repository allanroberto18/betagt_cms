<?php

namespace BetaGT\Bundles\ProdutosBundle\Controller;

use BetaGT\Bundles\CMSBundle\Controller\MainController;
use BetaGT\Bundles\ProdutosBundle\Entity\Produtos;
use BetaGT\Bundles\ProdutosBundle\Entity\Receitas;
use BetaGT\Bundles\ProdutosBundle\Form\ReceitasType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class ReceitasController extends MainController
{
    /**
     * @Route("/produtos/{idProduto}/receitas/novo", name="pro_receitas_novo")
     * @Template("ProdutosBundle:Receitas:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction($idProduto, Request $request)
    {
        $produto = $this->checkParent($idProduto, 'ProdutosBundle', 'Produtos', 'pro_produtos_listar', null);

        $entity = new Receitas();

        $form = $this->createForm(new ReceitasType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setProduto($produto);

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'pro_receitas_novo'
                : 'pro_receitas_listar';

            return $this->redirectToRoute($nextAction, array('idProduto' => $idProduto));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home'));
        $breadcrumbs->addItem('Produtos: Listar Registros', $this->get('router')->generate('pro_produtos_listar', array()));
        $breadcrumbs->addItem($produto->getTitulo(), $this->get('router')->generate('pro_produtos_visualizar', array('id' => $idProduto)));
        $breadcrumbs->addItem('Receitas: Listar Registros', $this->get('router')->generate('pro_receitas_listar', array('idProduto' => $idProduto)));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Receitas', 'descricao' => ''),
            'idProduto' => $idProduto,
        );

    }

    /**
     * @Route("/produtos/{idProduto}/receitas/{id}/atualizar", name="pro_receitas_atualizar")
     * @Template("ProdutosBundle:Receitas:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($idProduto, $id, Request $request)
    {
        $produto = $this->checkParent($idProduto, 'ProdutosBundle', 'Produtos', 'pro_produtos_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ProdutosBundle:Receitas')->find($id);
        if (!$entity instanceof Receitas) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('pro_receitas_listar', array('idProduto' => $idProduto));
        }

        $form = $this->createForm(new ReceitasType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'pro_receitas_listar_novo'
                : 'pro_receitas_listar_listar';

            return $this->redirectToRoute($nextAction, array('idProduto' => $idProduto));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Produtos: Listar Registros', $this->get('router')->generate('pro_produtos_listar', array()));
        $breadcrumbs->addItem($produto->getTitulo(), $this->get('router')->generate('pro_produtos_visualizar', array('id' => $idProduto)));
        $breadcrumbs->addItem('Receitas: Listar Registros', $this->get('router')->generate('pro_receitas_listar', array('idProduto' => $idProduto)));
        $breadcrumbs->addItem('Visualizar: ' . $entity->getTitulo(), $this->get('router')->generate('pro_receitas_visualizar', array('idProduto' => $idProduto, 'id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Receitas', 'descricao' => ''),
            'idProduto' => $idProduto,
        );
    }

    /**
     * @Route("/produtos/{idProduto}/receitas/listar", name="pro_receitas_listar")
     * @Template("ProdutosBundle:Receitas:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction($idProduto, Request $request)
    {
        $produto = $this->checkParent($idProduto, 'ProdutosBundle', 'Produtos', 'pro_produtos_listar', null);

        $repository = $this->getDoctrine()->getRepository('ProdutosBundle:Receitas');
        $queryBuilder = $repository->createQueryBuilder('item')
            ->where('item.status = :status')
            ->andWhere('item.produto = :produto')
            ->setParameter('status', '1')
            ->setParameter('produto', $idProduto)
        ;

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('item.titulo', array('label' => 'Título', 'filterable' => 'true', 'sortable' => true)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Produtos: Listar Registros', $this->get('router')->generate('pro_produtos_listar', array()));
        $breadcrumbs->addItem($produto->getTitulo(), $this->get('router')->generate('pro_produtos_visualizar', array('id' => $idProduto)));
        $breadcrumbs->addItem('Receitas: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('pro_receitas_delete_selecionado', ['idProduto' => $idProduto]),
            'novo' => $this->generateUrl('pro_receitas_novo', ['idProduto' => $idProduto]),
            'modulo' => array('titulo' => 'Receitas', 'descricao' => ''),
            'idProduto' => $idProduto,
        );
    }

    /**
     * @Route("/produtos/{idProduto}/receitas/{id}/visualizar", name="pro_receitas_visualizar")
     * @Template("ProdutosBundle:Receitas:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($idProduto, $id)
    {
        $produto = $this->checkParent($idProduto, 'ProdutosBundle', 'Produtos', 'pro_produtos_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ProdutosBundle:Receitas')->find($id);
        if (!$entity instanceof Receitas) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('pro_receitas_listar', array('idProduto' => $idProduto));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Produtos: Listar Registros', $this->get('router')->generate('pro_produtos_listar', array()));
        $breadcrumbs->addItem($produto->getTitulo(), $this->get('router')->generate('pro_produtos_visualizar', array('id' => $idProduto)));
        $breadcrumbs->addItem('Receitas: Listar Registros', $this->get('router')->generate('pro_receitas_listar', array('idProduto' => $idProduto)));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar: ' . $entity->getTitulo(),
            'entity' => $entity,
            'modulo' => array('titulo' => 'Receitas', 'descricao' => ''),
            'idProduto' => $idProduto,
        );
    }

    /**
     * @Route("/produtos/{idProduto}/receitas/{id}/delete", name="pro_receitas_delete")
     * @Method("GET")
     */
    public function deleteAction($idProduto, $id)
    {
        $this->checkParent($idProduto, 'ProdutosBundle', 'Produtos', 'pro_produtos_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ProdutosBundle:Receitas')->find($id);
        if (!$entity instanceof Receitas) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('pro_receitas_listar', array('idProduto' => $idProduto));
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('pro_receitas_listar', array('idProduto' => $idProduto));
    }

    /**
     * @Route("/produtos/{idProduto}/receitas/delete/selecionados", name="pro_receitas_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction($idProduto, Request $request)
    {
        $this->checkParent($idProduto, 'ProdutosBundle', 'Produtos', 'pro_produtos_listar', null);

        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('pro_receitas_listar', array('idProduto' => $idProduto));
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('ProdutosBundle:Receitas')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('pro_receitas_listar', array('idProduto' => $idProduto));
    }
}

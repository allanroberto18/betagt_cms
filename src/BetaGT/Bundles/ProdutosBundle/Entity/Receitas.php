<?php

namespace BetaGT\Bundles\ProdutosBundle\Entity;

use BetaGT\Bundles\CMSBundle\Entity\EntityMaster;
use BetaGT\Bundles\CMSBundle\Entity\TFoto;
use BetaGT\Bundles\CMSBundle\Entity\TSlug;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Receitas
 * @Vich\Uploadable
 * @ORM\Table(name="receitas", indexes={@ORM\Index(name="fk_produto_idx", columns={"produto_id"})})
 * @ORM\Entity
 */
class Receitas extends EntityMaster
{
    use TFoto, TSlug;
    /**
     * @var string
     *
     * @ORM\Column(name="retranca", type="string", length=75, nullable=true)
     */
    private $retranca;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Título é obrigatório")
     * @ORM\Column(name="titulo", type="string", length=255, nullable=true)
     */
    private $titulo;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Resumo é obrigatório")
     * @ORM\Column(name="resumo", type="string", length=500, nullable=true)
     */
    private $resumo;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Texto é obrigatório")
     * @ORM\Column(name="texto", type="text", nullable=true)
     */
    private $texto;

    /**
     * @var File
     * @Vich\UploadableField(mapping="receita_image", fileNameProperty="imageName")
     */
    private $imageFile;

    /**
     * @var \Produtos
     *
     * @ORM\ManyToOne(targetEntity="Produtos", inversedBy="receitas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="produto_id", referencedColumnName="id")
     * })
     */
    private $produto;

    /**
     * Set retranca
     *
     * @param string $retranca
     * @return Receitas
     */
    public function setRetranca($retranca)
    {
        $this->retranca = $retranca;

        return $this;
    }

    /**
     * Get retranca
     *
     * @return string 
     */
    public function getRetranca()
    {
        return $this->retranca;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Receitas
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set resumo
     *
     * @param string $resumo
     * @return Receitas
     */
    public function setResumo($resumo)
    {
        $this->resumo = $resumo;

        return $this;
    }

    /**
     * Get resumo
     *
     * @return string 
     */
    public function getResumo()
    {
        return $this->resumo;
    }

    /**
     * Set texto
     *
     * @param string $texto
     * @return Receitas
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string 
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updated = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set produto
     *
     * @param \BetaGT\Bundles\ProdutosBundle\Entity\Produtos $produto
     * @return Receitas
     */
    public function setProduto(\BetaGT\Bundles\ProdutosBundle\Entity\Produtos $produto = null)
    {
        $this->produto = $produto;

        return $this;
    }

    /**
     * Get produto
     *
     * @return \BetaGT\Bundles\ProdutosBundle\Entity\Produtos
     */
    public function getProduto()
    {
        return $this->produto;
    }
}

<?php

/* CMSBundle:Home:index.html.twig */
class __TwigTemplate_c1ecffc8cacab1e12b3875ff9d12ec2fa9893860e9e08e7faf3f917737347f09 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CMSBundle:Default:base.html.twig", "CMSBundle:Home:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CMSBundle:Default:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "    <h5>Welcome to the Home:index page</h5>
";
    }

    public function getTemplateName()
    {
        return "CMSBundle:Home:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "CMSBundle:Default:base.html.twig" %}*/
/* {% block body %}*/
/*     <h5>Welcome to the Home:index page</h5>*/
/* {% endblock %}*/
/* */

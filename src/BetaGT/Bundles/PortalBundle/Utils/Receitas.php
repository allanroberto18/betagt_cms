<?php

namespace BetaGT\Bundles\PortalBundle\Utils;

use Doctrine\ORM\EntityManager;

class Receitas
{
    private $entityManager;

    /**
     * ServiceMaster constructor.
     * @param $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array|\BetaGT\Bundles\ProdutosBundle\Entity\Receitas[]
     */
    public function returnAll()
    {
        return $this->entityManager->getRepository("ProdutosBundle:Receitas")->findBy(['status' => 1], ['created' => "DESC"]);
    }

    public function returnRegister($id)
    {
        return $this->entityManager->getRepository("ProdutosBundle:Receitas")->find($id);
    }
}
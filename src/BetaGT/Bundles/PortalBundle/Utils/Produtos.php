<?php
/**
 * Created by PhpStorm.
 * User: allan
 * Date: 08/11/2015
 * Time: 21:54
 */

namespace BetaGT\Bundles\PortalBundle\Utils;

use Doctrine\ORM\EntityManager;

class Produtos
{
    private $entityManager;

    /**
     * ServiceMaster constructor.
     * @param $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function returnRegister($id)
    {
        return $this->entityManager->getRepository('ProdutosBundle:Produtos')->find($id);
    }

    public function returnAll()
    {
        return $this->entityManager->getRepository('ProdutosBundle:Produtos')->findBy(['status' => 1]);
    }
}
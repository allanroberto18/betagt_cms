<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/admin')) {
            if (0 === strpos($pathinfo, '/admin/pessoas')) {
                // fin_contratos_novo
                if (preg_match('#^/admin/pessoas/(?P<idPessoa>[^/]++)/contratos/novo$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fin_contratos_novo;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fin_contratos_novo')), array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\ContratosController::novoAction',));
                }
                not_fin_contratos_novo:

                // fin_contratos_atualizar
                if (preg_match('#^/admin/pessoas/(?P<idPessoa>[^/]++)/contratos/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fin_contratos_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fin_contratos_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\ContratosController::updateAction',));
                }
                not_fin_contratos_atualizar:

                // fin_contratos_listar
                if (preg_match('#^/admin/pessoas/(?P<idPessoa>[^/]++)/contratos/listar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fin_contratos_listar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fin_contratos_listar')), array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\ContratosController::listAction',));
                }
                not_fin_contratos_listar:

                // fin_contratos_visualizar
                if (preg_match('#^/admin/pessoas/(?P<idPessoa>[^/]++)/contratos/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fin_contratos_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fin_contratos_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\ContratosController::viewAction',));
                }
                not_fin_contratos_visualizar:

                // fin_contratos_delete
                if (preg_match('#^/admin/pessoas/(?P<idPessoa>[^/]++)/contratos/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fin_contratos_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fin_contratos_delete')), array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\ContratosController::deleteAction',));
                }
                not_fin_contratos_delete:

                // fin_contratos_delete_selecionado
                if (preg_match('#^/admin/pessoas/(?P<idPessoa>[^/]++)/contratos/delete/selecionados$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fin_contratos_delete_selecionado;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fin_contratos_delete_selecionado')), array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\ContratosController::deleteSelecionadosAction',));
                }
                not_fin_contratos_delete_selecionado:

            }

            if (0 === strpos($pathinfo, '/admin/Empresas')) {
                // fin_empresas_novo
                if ($pathinfo === '/admin/Empresas/novo') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fin_empresas_novo;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\EmpresasController::novoAction',  '_route' => 'fin_empresas_novo',);
                }
                not_fin_empresas_novo:

                // fin_empresas_atualizar
                if (preg_match('#^/admin/Empresas/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fin_empresas_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fin_empresas_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\EmpresasController::updateAction',));
                }
                not_fin_empresas_atualizar:

                // fin_empresas_listar
                if ($pathinfo === '/admin/Empresas/listar') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fin_empresas_listar;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\EmpresasController::listAction',  '_route' => 'fin_empresas_listar',);
                }
                not_fin_empresas_listar:

                // fin_empresas_visualizar
                if (preg_match('#^/admin/Empresas/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fin_empresas_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fin_empresas_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\EmpresasController::viewAction',));
                }
                not_fin_empresas_visualizar:

                // fin_empresas_delete
                if (preg_match('#^/admin/Empresas/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fin_empresas_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fin_empresas_delete')), array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\EmpresasController::deleteAction',));
                }
                not_fin_empresas_delete:

                // fin_empresas_delete_selecionado
                if ($pathinfo === '/admin/Empresas/delete/selecionados') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fin_empresas_delete_selecionado;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\EmpresasController::deleteSelecionadosAction',  '_route' => 'fin_empresas_delete_selecionado',);
                }
                not_fin_empresas_delete_selecionado:

            }

            if (0 === strpos($pathinfo, '/admin/contratos')) {
                // fin_faturas_novo
                if (preg_match('#^/admin/contratos/(?P<idContrato>[^/]++)/faturas/novo$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fin_faturas_novo;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fin_faturas_novo')), array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\FaturasController::novoAction',));
                }
                not_fin_faturas_novo:

                // fin_faturas_atualizar
                if (preg_match('#^/admin/contratos/(?P<idContrato>[^/]++)/faturas/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fin_faturas_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fin_faturas_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\FaturasController::updateAction',));
                }
                not_fin_faturas_atualizar:

                // fin_faturas_listar
                if (preg_match('#^/admin/contratos/(?P<idContrato>[^/]++)/faturas/listar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fin_faturas_listar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fin_faturas_listar')), array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\FaturasController::listAction',));
                }
                not_fin_faturas_listar:

                // fin_faturas_visualizar
                if (preg_match('#^/admin/contratos/(?P<idContrato>[^/]++)/faturas/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fin_faturas_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fin_faturas_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\FaturasController::viewAction',));
                }
                not_fin_faturas_visualizar:

                // fin_faturas_delete
                if (preg_match('#^/admin/contratos/(?P<idContrato>[^/]++)/faturas/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fin_faturas_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fin_faturas_delete')), array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\FaturasController::deleteAction',));
                }
                not_fin_faturas_delete:

                // fin_faturas_delete_selecionado
                if (preg_match('#^/admin/contratos/(?P<idContrato>[^/]++)/faturas/delete/selecionados$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fin_faturas_delete_selecionado;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fin_faturas_delete_selecionado')), array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\FaturasController::deleteSelecionadosAction',));
                }
                not_fin_faturas_delete_selecionado:

            }

            if (0 === strpos($pathinfo, '/admin/Planos')) {
                // fin_planos_novo
                if ($pathinfo === '/admin/Planos/novo') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fin_planos_novo;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\PlanosController::novoAction',  '_route' => 'fin_planos_novo',);
                }
                not_fin_planos_novo:

                // fin_planos_atualizar
                if (preg_match('#^/admin/Planos/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fin_planos_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fin_planos_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\PlanosController::updateAction',));
                }
                not_fin_planos_atualizar:

                // fin_planos_listar
                if ($pathinfo === '/admin/Planos/listar') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fin_planos_listar;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\PlanosController::listAction',  '_route' => 'fin_planos_listar',);
                }
                not_fin_planos_listar:

                // fin_planos_visualizar
                if (preg_match('#^/admin/Planos/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fin_planos_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fin_planos_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\PlanosController::viewAction',));
                }
                not_fin_planos_visualizar:

                // fin_planos_delete
                if (preg_match('#^/admin/Planos/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fin_planos_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fin_planos_delete')), array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\PlanosController::deleteAction',));
                }
                not_fin_planos_delete:

                // fin_planos_delete_selecionado
                if ($pathinfo === '/admin/Planos/delete/selecionados') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fin_planos_delete_selecionado;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\FinanceiroBundle\\Controller\\PlanosController::deleteSelecionadosAction',  '_route' => 'fin_planos_delete_selecionado',);
                }
                not_fin_planos_delete_selecionado:

            }

            if (0 === strpos($pathinfo, '/admin/produtos')) {
                if (0 === strpos($pathinfo, '/admin/produtos/categorias')) {
                    // pro_categorias_produtos_novo
                    if ($pathinfo === '/admin/produtos/categorias/novo') {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_pro_categorias_produtos_novo;
                        }

                        return array (  '_controller' => 'BetaGT\\Bundles\\ProdutosBundle\\Controller\\CategoriasProdutosController::novoAction',  '_route' => 'pro_categorias_produtos_novo',);
                    }
                    not_pro_categorias_produtos_novo:

                    // pro_categorias_produtos_atualizar
                    if (preg_match('#^/admin/produtos/categorias/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_pro_categorias_produtos_atualizar;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pro_categorias_produtos_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\ProdutosBundle\\Controller\\CategoriasProdutosController::updateAction',));
                    }
                    not_pro_categorias_produtos_atualizar:

                    // pro_categorias_produtos_listar
                    if ($pathinfo === '/admin/produtos/categorias/listar') {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_pro_categorias_produtos_listar;
                        }

                        return array (  '_controller' => 'BetaGT\\Bundles\\ProdutosBundle\\Controller\\CategoriasProdutosController::listAction',  '_route' => 'pro_categorias_produtos_listar',);
                    }
                    not_pro_categorias_produtos_listar:

                    // pro_categorias_produtos_visualizar
                    if (preg_match('#^/admin/produtos/categorias/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_pro_categorias_produtos_visualizar;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pro_categorias_produtos_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\ProdutosBundle\\Controller\\CategoriasProdutosController::viewAction',));
                    }
                    not_pro_categorias_produtos_visualizar:

                    // pro_categorias_produtos_delete
                    if (preg_match('#^/admin/produtos/categorias/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_pro_categorias_produtos_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pro_categorias_produtos_delete')), array (  '_controller' => 'BetaGT\\Bundles\\ProdutosBundle\\Controller\\CategoriasProdutosController::deleteAction',));
                    }
                    not_pro_categorias_produtos_delete:

                    // pro_categorias_produtos_delete_selecionado
                    if ($pathinfo === '/admin/produtos/categorias/delete/selecionados') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_pro_categorias_produtos_delete_selecionado;
                        }

                        return array (  '_controller' => 'BetaGT\\Bundles\\ProdutosBundle\\Controller\\CategoriasProdutosController::deleteSelecionadosAction',  '_route' => 'pro_categorias_produtos_delete_selecionado',);
                    }
                    not_pro_categorias_produtos_delete_selecionado:

                }

                // pro_produtos_novo
                if ($pathinfo === '/admin/produtos/novo') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_pro_produtos_novo;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\ProdutosBundle\\Controller\\ProdutosController::novoAction',  '_route' => 'pro_produtos_novo',);
                }
                not_pro_produtos_novo:

                // pro_produtos_atualizar
                if (preg_match('#^/admin/produtos/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_pro_produtos_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'pro_produtos_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\ProdutosBundle\\Controller\\ProdutosController::updateAction',));
                }
                not_pro_produtos_atualizar:

                // pro_produtos_listar
                if ($pathinfo === '/admin/produtos/listar') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_pro_produtos_listar;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\ProdutosBundle\\Controller\\ProdutosController::listAction',  '_route' => 'pro_produtos_listar',);
                }
                not_pro_produtos_listar:

                // pro_produtos_visualizar
                if (preg_match('#^/admin/produtos/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_pro_produtos_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'pro_produtos_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\ProdutosBundle\\Controller\\ProdutosController::viewAction',));
                }
                not_pro_produtos_visualizar:

                // pro_produtos_delete
                if (preg_match('#^/admin/produtos/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_pro_produtos_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'pro_produtos_delete')), array (  '_controller' => 'BetaGT\\Bundles\\ProdutosBundle\\Controller\\ProdutosController::deleteAction',));
                }
                not_pro_produtos_delete:

                // pro_produtos_delete_selecionado
                if ($pathinfo === '/admin/produtos/delete/selecionados') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_pro_produtos_delete_selecionado;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\ProdutosBundle\\Controller\\ProdutosController::deleteSelecionadosAction',  '_route' => 'pro_produtos_delete_selecionado',);
                }
                not_pro_produtos_delete_selecionado:

                // pro_receitas_novo
                if (preg_match('#^/admin/produtos/(?P<idProduto>[^/]++)/receitas/novo$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_pro_receitas_novo;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'pro_receitas_novo')), array (  '_controller' => 'BetaGT\\Bundles\\ProdutosBundle\\Controller\\ReceitasController::novoAction',));
                }
                not_pro_receitas_novo:

                // pro_receitas_atualizar
                if (preg_match('#^/admin/produtos/(?P<idProduto>[^/]++)/receitas/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_pro_receitas_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'pro_receitas_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\ProdutosBundle\\Controller\\ReceitasController::updateAction',));
                }
                not_pro_receitas_atualizar:

                // pro_receitas_listar
                if (preg_match('#^/admin/produtos/(?P<idProduto>[^/]++)/receitas/listar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_pro_receitas_listar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'pro_receitas_listar')), array (  '_controller' => 'BetaGT\\Bundles\\ProdutosBundle\\Controller\\ReceitasController::listAction',));
                }
                not_pro_receitas_listar:

                // pro_receitas_visualizar
                if (preg_match('#^/admin/produtos/(?P<idProduto>[^/]++)/receitas/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_pro_receitas_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'pro_receitas_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\ProdutosBundle\\Controller\\ReceitasController::viewAction',));
                }
                not_pro_receitas_visualizar:

                // pro_receitas_delete
                if (preg_match('#^/admin/produtos/(?P<idProduto>[^/]++)/receitas/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_pro_receitas_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'pro_receitas_delete')), array (  '_controller' => 'BetaGT\\Bundles\\ProdutosBundle\\Controller\\ReceitasController::deleteAction',));
                }
                not_pro_receitas_delete:

                // pro_receitas_delete_selecionado
                if (preg_match('#^/admin/produtos/(?P<idProduto>[^/]++)/receitas/delete/selecionados$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_pro_receitas_delete_selecionado;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'pro_receitas_delete_selecionado')), array (  '_controller' => 'BetaGT\\Bundles\\ProdutosBundle\\Controller\\ReceitasController::deleteSelecionadosAction',));
                }
                not_pro_receitas_delete_selecionado:

            }

        }

        // front_home
        if (rtrim($pathinfo, '/') === '') {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_front_home;
            }

            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'front_home');
            }

            return array (  '_controller' => 'BetaGT\\Bundles\\PortalBundle\\Controller\\HomeController::indexAction',  '_route' => 'front_home',);
        }
        not_front_home:

        // front_pagina
        if (0 === strpos($pathinfo, '/Pagina') && preg_match('#^/Pagina/(?P<id>[^/]++)/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_front_pagina;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'front_pagina')), array (  '_controller' => 'BetaGT\\Bundles\\PortalBundle\\Controller\\HomeController::paginaAction',));
        }
        not_front_pagina:

        // front_noticias
        if (rtrim($pathinfo, '/') === '/Noticias') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_front_noticias;
            }

            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'front_noticias');
            }

            return array (  '_controller' => 'BetaGT\\Bundles\\PortalBundle\\Controller\\HomeController::noticiasAction',  '_route' => 'front_noticias',);
        }
        not_front_noticias:

        if (0 === strpos($pathinfo, '/Receita')) {
            // front_receitas
            if (rtrim($pathinfo, '/') === '/Receitas') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_front_receitas;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'front_receitas');
                }

                return array (  '_controller' => 'BetaGT\\Bundles\\PortalBundle\\Controller\\HomeController::receitasAction',  '_route' => 'front_receitas',);
            }
            not_front_receitas:

            // front_receita
            if (preg_match('#^/Receita/(?P<id>[^/]++)/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_front_receita;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'front_receita')), array (  '_controller' => 'BetaGT\\Bundles\\PortalBundle\\Controller\\HomeController::receitaAction',));
            }
            not_front_receita:

        }

        // front_noticia
        if (0 === strpos($pathinfo, '/Noticias') && preg_match('#^/Noticias/(?P<id>[^/]++)/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_front_noticia;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'front_noticia')), array (  '_controller' => 'BetaGT\\Bundles\\PortalBundle\\Controller\\HomeController::noticiaAction',));
        }
        not_front_noticia:

        if (0 === strpos($pathinfo, '/Albuns')) {
            // betagt_bundles_portal_home_galerias
            if (rtrim($pathinfo, '/') === '/Albuns') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_betagt_bundles_portal_home_galerias;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'betagt_bundles_portal_home_galerias');
                }

                return array (  '_controller' => 'BetaGT\\Bundles\\PortalBundle\\Controller\\HomeController::galeriasAction',  '_route' => 'betagt_bundles_portal_home_galerias',);
            }
            not_betagt_bundles_portal_home_galerias:

            // betagt_bundles_portal_home_galeria
            if (preg_match('#^/Albuns/(?P<id>[^/]++)/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_betagt_bundles_portal_home_galeria;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'betagt_bundles_portal_home_galeria')), array (  '_controller' => 'BetaGT\\Bundles\\PortalBundle\\Controller\\HomeController::galeriaAction',));
            }
            not_betagt_bundles_portal_home_galeria:

        }

        if (0 === strpos($pathinfo, '/Co')) {
            // betagt_bundles_portal_home_contato
            if ($pathinfo === '/Contato/salvar') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_betagt_bundles_portal_home_contato;
                }

                return array (  '_controller' => 'BetaGT\\Bundles\\PortalBundle\\Controller\\HomeController::contatoAction',  '_route' => 'betagt_bundles_portal_home_contato',);
            }
            not_betagt_bundles_portal_home_contato:

            // betagt_bundles_portal_home_comentario
            if ($pathinfo === '/Comentario/salvar') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_betagt_bundles_portal_home_comentario;
                }

                return array (  '_controller' => 'BetaGT\\Bundles\\PortalBundle\\Controller\\HomeController::comentarioAction',  '_route' => 'betagt_bundles_portal_home_comentario',);
            }
            not_betagt_bundles_portal_home_comentario:

        }

        if (0 === strpos($pathinfo, '/Enquete')) {
            // betagt_bundles_portal_home_enquete
            if ($pathinfo === '/Enquete/salvar') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_betagt_bundles_portal_home_enquete;
                }

                return array (  '_controller' => 'BetaGT\\Bundles\\PortalBundle\\Controller\\HomeController::enqueteAction',  '_route' => 'betagt_bundles_portal_home_enquete',);
            }
            not_betagt_bundles_portal_home_enquete:

            // betagt_bundles_portal_home_enqueteshow
            if (preg_match('#^/Enquete/(?P<id>[^/]++)/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_betagt_bundles_portal_home_enqueteshow;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'betagt_bundles_portal_home_enqueteshow')), array (  '_controller' => 'BetaGT\\Bundles\\PortalBundle\\Controller\\HomeController::enqueteShowAction',));
            }
            not_betagt_bundles_portal_home_enqueteshow:

        }

        if (0 === strpos($pathinfo, '/admin')) {
            if (0 === strpos($pathinfo, '/admin/assuntos')) {
                // cms_assuntos_novo
                if ($pathinfo === '/admin/assuntos/novo') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_assuntos_novo;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\AssuntosController::novoAction',  '_route' => 'cms_assuntos_novo',);
                }
                not_cms_assuntos_novo:

                // cms_assuntos_atualizar
                if (preg_match('#^/admin/assuntos/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_assuntos_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_assuntos_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\AssuntosController::updateAction',));
                }
                not_cms_assuntos_atualizar:

                // cms_assuntos_listar
                if ($pathinfo === '/admin/assuntos/listar') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_assuntos_listar;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\AssuntosController::listAction',  '_route' => 'cms_assuntos_listar',);
                }
                not_cms_assuntos_listar:

                // cms_assuntos_visualizar
                if (preg_match('#^/admin/assuntos/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_assuntos_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_assuntos_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\AssuntosController::viewAction',));
                }
                not_cms_assuntos_visualizar:

                // cms_assuntos_delete
                if (preg_match('#^/admin/assuntos/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_assuntos_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_assuntos_delete')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\AssuntosController::deleteAction',));
                }
                not_cms_assuntos_delete:

                // cms_assuntos_delete_selecionado
                if ($pathinfo === '/admin/assuntos/delete/selecionados') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_cms_assuntos_delete_selecionado;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\AssuntosController::deleteSelecionadosAction',  '_route' => 'cms_assuntos_delete_selecionado',);
                }
                not_cms_assuntos_delete_selecionado:

            }

            if (0 === strpos($pathinfo, '/admin/categorias')) {
                // cms_categorias_novo
                if ($pathinfo === '/admin/categorias/novo') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_categorias_novo;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\CategoriasController::novoAction',  '_route' => 'cms_categorias_novo',);
                }
                not_cms_categorias_novo:

                // cms_categorias_update
                if (preg_match('#^/admin/categorias/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_categorias_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_categorias_update')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\CategoriasController::updateAction',));
                }
                not_cms_categorias_update:

                // cms_categorias_listar
                if ($pathinfo === '/admin/categorias/listar') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_categorias_listar;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\CategoriasController::listAction',  '_route' => 'cms_categorias_listar',);
                }
                not_cms_categorias_listar:

                // cms_categorias_view
                if (preg_match('#^/admin/categorias/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_categorias_view;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_categorias_view')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\CategoriasController::viewAction',));
                }
                not_cms_categorias_view:

                // cms_categorias_delete
                if (preg_match('#^/admin/categorias/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_categorias_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_categorias_delete')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\CategoriasController::deleteAction',));
                }
                not_cms_categorias_delete:

                // cms_categorias_delete_selecionado
                if ($pathinfo === '/admin/categorias/delete/selecionados') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_cms_categorias_delete_selecionado;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\CategoriasController::deleteSelecionadosAction',  '_route' => 'cms_categorias_delete_selecionado',);
                }
                not_cms_categorias_delete_selecionado:

            }

            if (0 === strpos($pathinfo, '/admin/noticia')) {
                // cms_comentarios_novo
                if (preg_match('#^/admin/noticia/(?P<idNoticia>[^/]++)/Comentarios/novo$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_comentarios_novo;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_comentarios_novo')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\ComentariosController::novoAction',));
                }
                not_cms_comentarios_novo:

                // cms_comentarios_atualizar
                if (preg_match('#^/admin/noticia/(?P<idNoticia>[^/]++)/Comentarios/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_comentarios_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_comentarios_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\ComentariosController::updateAction',));
                }
                not_cms_comentarios_atualizar:

                // cms_comentarios_listar
                if (preg_match('#^/admin/noticia/(?P<idNoticia>[^/]++)/Comentarios/listar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_comentarios_listar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_comentarios_listar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\ComentariosController::listAction',));
                }
                not_cms_comentarios_listar:

                // cms_comentarios_visualizar
                if (preg_match('#^/admin/noticia/(?P<idNoticia>[^/]++)/Comentarios/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_comentarios_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_comentarios_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\ComentariosController::viewAction',));
                }
                not_cms_comentarios_visualizar:

                // cms_comentarios_delete
                if (preg_match('#^/admin/noticia/(?P<idNoticia>[^/]++)/Comentarios/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_comentarios_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_comentarios_delete')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\ComentariosController::deleteAction',));
                }
                not_cms_comentarios_delete:

                // cms_comentarios_delete_selecionado
                if (preg_match('#^/admin/noticia/(?P<idNoticia>[^/]++)/Comentarios/delete/selecionados$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_cms_comentarios_delete_selecionado;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_comentarios_delete_selecionado')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\ComentariosController::deleteSelecionadosAction',));
                }
                not_cms_comentarios_delete_selecionado:

            }

            if (0 === strpos($pathinfo, '/admin/contatos')) {
                // cms_contatos_novo
                if ($pathinfo === '/admin/contatos/novo') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_contatos_novo;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\ContatosController::novoAction',  '_route' => 'cms_contatos_novo',);
                }
                not_cms_contatos_novo:

                // cms_contatos_atualizar
                if (preg_match('#^/admin/contatos/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_contatos_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_contatos_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\ContatosController::updateAction',));
                }
                not_cms_contatos_atualizar:

                // cms_contatos_listar
                if ($pathinfo === '/admin/contatos/listar') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_contatos_listar;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\ContatosController::listAction',  '_route' => 'cms_contatos_listar',);
                }
                not_cms_contatos_listar:

                // cms_contatos_visualizar
                if (preg_match('#^/admin/contatos/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_contatos_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_contatos_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\ContatosController::viewAction',));
                }
                not_cms_contatos_visualizar:

                // cms_contatos_delete
                if (preg_match('#^/admin/contatos/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_contatos_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_contatos_delete')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\ContatosController::deleteAction',));
                }
                not_cms_contatos_delete:

                // cms_contatos_delete_selecionado
                if ($pathinfo === '/admin/contatos/delete/selecionados') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_cms_contatos_delete_selecionado;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\ContatosController::deleteSelecionadosAction',  '_route' => 'cms_contatos_delete_selecionado',);
                }
                not_cms_contatos_delete_selecionado:

            }

            if (0 === strpos($pathinfo, '/admin/enquetes')) {
                // cms_enquetes_novo
                if ($pathinfo === '/admin/enquetes/novo') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_enquetes_novo;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesController::novoAction',  '_route' => 'cms_enquetes_novo',);
                }
                not_cms_enquetes_novo:

                // cms_enquetes_atualizar
                if (preg_match('#^/admin/enquetes/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_enquetes_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesController::updateAction',));
                }
                not_cms_enquetes_atualizar:

                // cms_enquetes_listar
                if ($pathinfo === '/admin/enquetes/listar') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_enquetes_listar;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesController::listAction',  '_route' => 'cms_enquetes_listar',);
                }
                not_cms_enquetes_listar:

                // cms_enquetes_visualizar
                if (preg_match('#^/admin/enquetes/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_enquetes_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesController::viewAction',));
                }
                not_cms_enquetes_visualizar:

                // cms_enquetes_delete
                if (preg_match('#^/admin/enquetes/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_enquetes_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_delete')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesController::deleteAction',));
                }
                not_cms_enquetes_delete:

                // cms_enquetes_delete_selecionado
                if ($pathinfo === '/admin/enquetes/delete/selecionados') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_cms_enquetes_delete_selecionado;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesController::deleteSelecionadosAction',  '_route' => 'cms_enquetes_delete_selecionado',);
                }
                not_cms_enquetes_delete_selecionado:

                // cms_enquetes_itens_novo
                if (preg_match('#^/admin/enquetes/(?P<idEnquete>[^/]++)/itens/novo$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_enquetes_itens_novo;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_itens_novo')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesItensController::novoAction',));
                }
                not_cms_enquetes_itens_novo:

                // cms_enquetes_itens_atualizar
                if (preg_match('#^/admin/enquetes/(?P<idEnquete>[^/]++)/itens/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_enquetes_itens_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_itens_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesItensController::updateAction',));
                }
                not_cms_enquetes_itens_atualizar:

                // cms_enquetes_itens_listar
                if (preg_match('#^/admin/enquetes/(?P<idEnquete>[^/]++)/itens/listar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_enquetes_itens_listar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_itens_listar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesItensController::listAction',));
                }
                not_cms_enquetes_itens_listar:

                // cms_enquetes_itens_visualizar
                if (preg_match('#^/admin/enquetes/(?P<idEnquete>[^/]++)/itens/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_enquetes_itens_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_itens_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesItensController::viewAction',));
                }
                not_cms_enquetes_itens_visualizar:

                // cms_enquetes_itens_delete
                if (preg_match('#^/admin/enquetes/(?P<idEnquete>[^/]++)/itens/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_enquetes_itens_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_itens_delete')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesItensController::deleteAction',));
                }
                not_cms_enquetes_itens_delete:

                // cms_enquetes_itens_delete_selecionado
                if (preg_match('#^/admin/enquetes/(?P<idEnquete>[^/]++)/itens/delete/selecionados$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_cms_enquetes_itens_delete_selecionado;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_itens_delete_selecionado')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesItensController::deleteSelecionadosAction',));
                }
                not_cms_enquetes_itens_delete_selecionado:

            }

            if (0 === strpos($pathinfo, '/admin/noticias')) {
                // cms_enquetes_noticias_novo
                if (preg_match('#^/admin/noticias/(?P<idNoticia>[^/]++)/enquetes/novo$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_enquetes_noticias_novo;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_noticias_novo')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesNoticiasController::novoAction',));
                }
                not_cms_enquetes_noticias_novo:

                // cms_enquetes_noticias_atualizar
                if (preg_match('#^/admin/noticias/(?P<idNoticia>[^/]++)/enquetes/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_enquetes_noticias_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_noticias_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesNoticiasController::updateAction',));
                }
                not_cms_enquetes_noticias_atualizar:

                // cms_enquetes_noticias_listar
                if (preg_match('#^/admin/noticias/(?P<idNoticia>[^/]++)/enquetes/listar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_enquetes_noticias_listar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_noticias_listar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesNoticiasController::listAction',));
                }
                not_cms_enquetes_noticias_listar:

                // cms_enquetes_noticias_visualizar
                if (preg_match('#^/admin/noticias/(?P<idNoticia>[^/]++)/enquetes/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_enquetes_noticias_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_noticias_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesNoticiasController::viewAction',));
                }
                not_cms_enquetes_noticias_visualizar:

                // cms_enquetes_noticias_delete
                if (preg_match('#^/admin/noticias/(?P<idNoticia>[^/]++)/enquetes/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_enquetes_noticias_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_noticias_delete')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesNoticiasController::deleteAction',));
                }
                not_cms_enquetes_noticias_delete:

                // cms_enquetes_noticias_delete_selecionado
                if (preg_match('#^/admin/noticias/(?P<idNoticia>[^/]++)/enquetes/delete/selecionados$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_cms_enquetes_noticias_delete_selecionado;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_noticias_delete_selecionado')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesNoticiasController::deleteSelecionadosAction',));
                }
                not_cms_enquetes_noticias_delete_selecionado:

            }

            if (0 === strpos($pathinfo, '/admin/enquetes')) {
                // cms_enquetes_resultados_novo
                if (preg_match('#^/admin/enquetes/(?P<idEnquete>[^/]++)/resultados/novo$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_enquetes_resultados_novo;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_resultados_novo')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesResultadosController::novoAction',));
                }
                not_cms_enquetes_resultados_novo:

                // cms_enquetes_resultados_atualizar
                if (preg_match('#^/admin/enquetes/(?P<idEnquete>[^/]++)/resultados/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_enquetes_resultados_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_resultados_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesResultadosController::updateAction',));
                }
                not_cms_enquetes_resultados_atualizar:

                // cms_enquetes_resultados_listar
                if (preg_match('#^/admin/enquetes/(?P<idEnquete>[^/]++)/resultados/listar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_enquetes_resultados_listar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_resultados_listar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesResultadosController::listAction',));
                }
                not_cms_enquetes_resultados_listar:

                // cms_enquetes_resultados_visualizar
                if (preg_match('#^/admin/enquetes/(?P<idEnquete>[^/]++)/resultados/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_enquetes_resultados_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_resultados_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesResultadosController::viewAction',));
                }
                not_cms_enquetes_resultados_visualizar:

                // cms_enquetes_resultados_delete
                if (preg_match('#^/admin/enquetes/(?P<idEnquete>[^/]++)/resultados/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_enquetes_resultados_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_resultados_delete')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesResultadosController::deleteAction',));
                }
                not_cms_enquetes_resultados_delete:

                // cms_enquetes_resultados_delete_selecionado
                if (preg_match('#^/admin/enquetes/(?P<idEnquete>[^/]++)/resultados/delete/selecionados$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_cms_enquetes_resultados_delete_selecionado;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_enquetes_resultados_delete_selecionado')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\EnquetesResultadosController::deleteSelecionadosAction',));
                }
                not_cms_enquetes_resultados_delete_selecionado:

            }

            if (0 === strpos($pathinfo, '/admin/galerias')) {
                // cms_galerias_novo
                if ($pathinfo === '/admin/galerias/novo') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_galerias_novo;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasController::novoAction',  '_route' => 'cms_galerias_novo',);
                }
                not_cms_galerias_novo:

                // cms_galerias_atualizar
                if (preg_match('#^/admin/galerias/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_galerias_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_galerias_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasController::updateAction',));
                }
                not_cms_galerias_atualizar:

                // cms_galerias_listar
                if ($pathinfo === '/admin/galerias/listar') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_galerias_listar;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasController::listAction',  '_route' => 'cms_galerias_listar',);
                }
                not_cms_galerias_listar:

                // cms_galerias_visualizar
                if (preg_match('#^/admin/galerias/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_galerias_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_galerias_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasController::viewAction',));
                }
                not_cms_galerias_visualizar:

                // cms_galerias_delete
                if (preg_match('#^/admin/galerias/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_galerias_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_galerias_delete')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasController::deleteAction',));
                }
                not_cms_galerias_delete:

                // cms_galerias_delete_selecionado
                if ($pathinfo === '/admin/galerias/delete/selecionados') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_cms_galerias_delete_selecionado;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasController::deleteSelecionadosAction',  '_route' => 'cms_galerias_delete_selecionado',);
                }
                not_cms_galerias_delete_selecionado:

                // cms_galerias_itens_novo
                if (preg_match('#^/admin/galerias/(?P<idGaleria>[^/]++)/itens/novo$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_galerias_itens_novo;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_galerias_itens_novo')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasItensController::novoAction',));
                }
                not_cms_galerias_itens_novo:

                // cms_galerias_itens_atualizar
                if (preg_match('#^/admin/galerias/(?P<idGaleria>[^/]++)/itens/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_galerias_itens_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_galerias_itens_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasItensController::updateAction',));
                }
                not_cms_galerias_itens_atualizar:

                // cms_galerias_itens_listar
                if (preg_match('#^/admin/galerias/(?P<idGaleria>[^/]++)/itens/listar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_galerias_itens_listar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_galerias_itens_listar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasItensController::listAction',));
                }
                not_cms_galerias_itens_listar:

                // cms_galerias_itens_visualizar
                if (preg_match('#^/admin/galerias/(?P<idGaleria>[^/]++)/itens/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_galerias_itens_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_galerias_itens_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasItensController::viewAction',));
                }
                not_cms_galerias_itens_visualizar:

                // cms_galerias_itens_delete
                if (preg_match('#^/admin/galerias/(?P<idGaleria>[^/]++)/itens/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_galerias_itens_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_galerias_itens_delete')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasItensController::deleteAction',));
                }
                not_cms_galerias_itens_delete:

                // cms_galerias_itens_delete_selecionado
                if (preg_match('#^/admin/galerias/(?P<idGaleria>[^/]++)/itens/delete/selecionados$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_cms_galerias_itens_delete_selecionado;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_galerias_itens_delete_selecionado')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasItensController::deleteSelecionadosAction',));
                }
                not_cms_galerias_itens_delete_selecionado:

            }

            if (0 === strpos($pathinfo, '/admin/noticias')) {
                // cms_noticias_galerias_novo
                if (preg_match('#^/admin/noticias/(?P<idNoticia>[^/]++)/galerias/novo$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_noticias_galerias_novo;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_noticias_galerias_novo')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasNoticiasController::novoAction',));
                }
                not_cms_noticias_galerias_novo:

                // cms_noticias_galerias_atualizar
                if (preg_match('#^/admin/noticias/(?P<idNoticia>[^/]++)/galerias/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_noticias_galerias_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_noticias_galerias_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasNoticiasController::updateAction',));
                }
                not_cms_noticias_galerias_atualizar:

                // cms_noticias_galerias_listar
                if (preg_match('#^/admin/noticias/(?P<idNoticia>[^/]++)/galerias/listar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_noticias_galerias_listar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_noticias_galerias_listar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasNoticiasController::listAction',));
                }
                not_cms_noticias_galerias_listar:

                // cms_noticias_galerias_visualizar
                if (preg_match('#^/admin/noticias/(?P<idNoticia>[^/]++)/galerias/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_noticias_galerias_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_noticias_galerias_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasNoticiasController::viewAction',));
                }
                not_cms_noticias_galerias_visualizar:

                // cms_noticias_galerias_delete
                if (preg_match('#^/admin/noticias/(?P<idNoticia>[^/]++)/galerias/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_noticias_galerias_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_noticias_galerias_delete')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasNoticiasController::deleteAction',));
                }
                not_cms_noticias_galerias_delete:

                // cms_noticias_galerias_delete_selecionado
                if (preg_match('#^/admin/noticias/(?P<idNoticia>[^/]++)/galerias/delete/selecionados$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_cms_noticias_galerias_delete_selecionado;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_noticias_galerias_delete_selecionado')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasNoticiasController::deleteSelecionadosAction',));
                }
                not_cms_noticias_galerias_delete_selecionado:

            }

            if (0 === strpos($pathinfo, '/admin/paginas')) {
                // cms_galerias_paginas_novo
                if (preg_match('#^/admin/paginas/(?P<idPagina>[^/]++)/galerias/novo$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_galerias_paginas_novo;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_galerias_paginas_novo')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasPaginasController::novoAction',));
                }
                not_cms_galerias_paginas_novo:

                // cms_galerias_paginas_atualizar
                if (preg_match('#^/admin/paginas/(?P<idPagina>[^/]++)/galerias/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_galerias_paginas_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_galerias_paginas_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasPaginasController::updateAction',));
                }
                not_cms_galerias_paginas_atualizar:

                // cms_galerias_paginas_listar
                if (preg_match('#^/admin/paginas/(?P<idPagina>[^/]++)/galerias/listar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_galerias_paginas_listar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_galerias_paginas_listar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasPaginasController::listAction',));
                }
                not_cms_galerias_paginas_listar:

                // cms_galerias_paginas_visualizar
                if (preg_match('#^/admin/paginas/(?P<idPagina>[^/]++)/galerias/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_galerias_paginas_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_galerias_paginas_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasPaginasController::viewAction',));
                }
                not_cms_galerias_paginas_visualizar:

                // cms_galerias_paginas_delete
                if (preg_match('#^/admin/paginas/(?P<idPagina>[^/]++)/galerias/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_galerias_paginas_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_galerias_paginas_delete')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasPaginasController::deleteAction',));
                }
                not_cms_galerias_paginas_delete:

                // cms_galerias_paginas_delete_selecionado
                if (preg_match('#^/admin/paginas/(?P<idPagina>[^/]++)/galerias/delete/selecionados$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_cms_galerias_paginas_delete_selecionado;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_galerias_paginas_delete_selecionado')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\GaleriasPaginasController::deleteSelecionadosAction',));
                }
                not_cms_galerias_paginas_delete_selecionado:

            }

            // cms_home
            if (rtrim($pathinfo, '/') === '/admin') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'cms_home');
                }

                return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\HomeController::indexAction',  '_route' => 'cms_home',);
            }

            if (0 === strpos($pathinfo, '/admin/noticias')) {
                // cms_noticias_novo
                if ($pathinfo === '/admin/noticias/novo') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_noticias_novo;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\NoticiasController::novoAction',  '_route' => 'cms_noticias_novo',);
                }
                not_cms_noticias_novo:

                // cms_noticias_atualizar
                if (preg_match('#^/admin/noticias/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_noticias_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_noticias_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\NoticiasController::updateAction',));
                }
                not_cms_noticias_atualizar:

                // cms_noticias_listar
                if ($pathinfo === '/admin/noticias/listar') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_cms_noticias_listar;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\NoticiasController::listAction',  '_route' => 'cms_noticias_listar',);
                }
                not_cms_noticias_listar:

                // cms_noticias_visualizar
                if (preg_match('#^/admin/noticias/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_noticias_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_noticias_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\NoticiasController::viewAction',));
                }
                not_cms_noticias_visualizar:

                // cms_noticias_delete
                if (preg_match('#^/admin/noticias/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_cms_noticias_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_noticias_delete')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\NoticiasController::deleteAction',));
                }
                not_cms_noticias_delete:

                // cms_noticias_delete_selecionado
                if ($pathinfo === '/admin/noticias/delete/selecionados') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_cms_noticias_delete_selecionado;
                    }

                    return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\NoticiasController::deleteSelecionadosAction',  '_route' => 'cms_noticias_delete_selecionado',);
                }
                not_cms_noticias_delete_selecionado:

            }

            if (0 === strpos($pathinfo, '/admin/p')) {
                if (0 === strpos($pathinfo, '/admin/paginas')) {
                    // cms_paginas_novo
                    if ($pathinfo === '/admin/paginas/novo') {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_cms_paginas_novo;
                        }

                        return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\PaginasController::novoAction',  '_route' => 'cms_paginas_novo',);
                    }
                    not_cms_paginas_novo:

                    // cms_paginas_atualizar
                    if (preg_match('#^/admin/paginas/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_cms_paginas_atualizar;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_paginas_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\PaginasController::updateAction',));
                    }
                    not_cms_paginas_atualizar:

                    // cms_paginas_listar
                    if ($pathinfo === '/admin/paginas/listar') {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_cms_paginas_listar;
                        }

                        return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\PaginasController::listAction',  '_route' => 'cms_paginas_listar',);
                    }
                    not_cms_paginas_listar:

                    // cms_paginas_visualizar
                    if (preg_match('#^/admin/paginas/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_cms_paginas_visualizar;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_paginas_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\PaginasController::viewAction',));
                    }
                    not_cms_paginas_visualizar:

                    // cms_paginas_delete
                    if (preg_match('#^/admin/paginas/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_cms_paginas_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_paginas_delete')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\PaginasController::deleteAction',));
                    }
                    not_cms_paginas_delete:

                    // cms_paginas_delete_selecionado
                    if ($pathinfo === '/admin/paginas/delete/selecionados') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_cms_paginas_delete_selecionado;
                        }

                        return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\PaginasController::deleteSelecionadosAction',  '_route' => 'cms_paginas_delete_selecionado',);
                    }
                    not_cms_paginas_delete_selecionado:

                }

                if (0 === strpos($pathinfo, '/admin/pessoas')) {
                    // cms_pessoas_atualizar
                    if (preg_match('#^/admin/pessoas/(?P<id>[^/]++)/atualizar/tipo/(?P<tipo>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_cms_pessoas_atualizar;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_pessoas_atualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\PessoasController::updateAction',));
                    }
                    not_cms_pessoas_atualizar:

                    // cms_pessoas_atualizar_foto
                    if (preg_match('#^/admin/pessoas/(?P<id>[^/]++)/atualizar/foto$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_cms_pessoas_atualizar_foto;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_pessoas_atualizar_foto')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\PessoasController::updateFotoAction',));
                    }
                    not_cms_pessoas_atualizar_foto:

                    // cms_pessoas_atualizar_perfil
                    if (preg_match('#^/admin/pessoas/(?P<id>[^/]++)/atualizar/perfil$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_cms_pessoas_atualizar_perfil;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_pessoas_atualizar_perfil')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\PessoasController::updatePerfilAction',));
                    }
                    not_cms_pessoas_atualizar_perfil:

                    // cms_pessoas_listar
                    if ($pathinfo === '/admin/pessoas/listar') {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_cms_pessoas_listar;
                        }

                        return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\PessoasController::listAction',  '_route' => 'cms_pessoas_listar',);
                    }
                    not_cms_pessoas_listar:

                    // cms_pessoas_visualizar
                    if (preg_match('#^/admin/pessoas/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_cms_pessoas_visualizar;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_pessoas_visualizar')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\PessoasController::viewAction',));
                    }
                    not_cms_pessoas_visualizar:

                    // cms_pessoas_delete
                    if (preg_match('#^/admin/pessoas/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_cms_pessoas_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'cms_pessoas_delete')), array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\PessoasController::deleteAction',));
                    }
                    not_cms_pessoas_delete:

                    // cms_pessoas_delete_selecionado
                    if ($pathinfo === '/admin/pessoas/delete/selecionados') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_cms_pessoas_delete_selecionado;
                        }

                        return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\PessoasController::deleteSelecionadosAction',  '_route' => 'cms_pessoas_delete_selecionado',);
                    }
                    not_cms_pessoas_delete_selecionado:

                }

            }

            // cms_contatos_send
            if ($pathinfo === '/admin/contatos/send') {
                return array (  '_controller' => 'BetaGT\\Bundles\\CMSBundle\\Controller\\ContatosController::sendContatoAction',  '_route' => 'cms_contatos_send',);
            }

        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ($pathinfo === '/login') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }
                not_fos_user_security_login:

                // fos_user_security_check
                if ($pathinfo === '/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ($pathinfo === '/logout') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_security_logout;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }
            not_fos_user_security_logout:

        }

        if (0 === strpos($pathinfo, '/admin/Usuarios')) {
            if (0 === strpos($pathinfo, '/admin/Usuarios/perfil')) {
                // fos_user_profile_show
                if (rtrim($pathinfo, '/') === '/admin/Usuarios/perfil') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_profile_show;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
                }
                not_fos_user_profile_show:

                // fos_user_profile_edit
                if ($pathinfo === '/admin/Usuarios/perfil/atualizar') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_profile_edit;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
                }
                not_fos_user_profile_edit:

            }

            if (0 === strpos($pathinfo, '/admin/Usuarios/novo')) {
                // fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/admin/Usuarios/novo') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_registration_register;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
                }
                not_fos_user_registration_register:

                if (0 === strpos($pathinfo, '/admin/Usuarios/novo/c')) {
                    // fos_user_registration_check_email
                    if ($pathinfo === '/admin/Usuarios/novo/checar-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/admin/Usuarios/novo/confirma')) {
                        // fos_user_registration_confirm
                        if (0 === strpos($pathinfo, '/admin/Usuarios/novo/confirmar') && preg_match('#^/admin/Usuarios/novo/confirmar/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ($pathinfo === '/admin/Usuarios/novo/confirmado') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/admin/Usuarios/redefinir')) {
                // fos_user_resetting_request
                if ($pathinfo === '/admin/Usuarios/redefinir/recuperar-senha') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_request;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
                }
                not_fos_user_resetting_request:

                // fos_user_resetting_send_email
                if ($pathinfo === '/admin/Usuarios/redefinir/enviar-email') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_resetting_send_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                }
                not_fos_user_resetting_send_email:

                // fos_user_resetting_check_email
                if ($pathinfo === '/admin/Usuarios/redefinir/checar-email') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_check_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                }
                not_fos_user_resetting_check_email:

                // fos_user_resetting_reset
                if (0 === strpos($pathinfo, '/admin/Usuarios/redefinir/redefinir') && preg_match('#^/admin/Usuarios/redefinir/redefinir/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_resetting_reset;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
                }
                not_fos_user_resetting_reset:

            }

            // fos_user_change_password
            if ($pathinfo === '/admin/Usuarios/perfil/atualizar/senha') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_change_password;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
            }
            not_fos_user_change_password:

        }

        // fos_js_routing_js
        if (0 === strpos($pathinfo, '/js/routing') && preg_match('#^/js/routing(?:\\.(?P<_format>js|json))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_js_routing_js')), array (  '_controller' => 'fos_js_routing.controller:indexAction',  '_format' => 'js',));
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}

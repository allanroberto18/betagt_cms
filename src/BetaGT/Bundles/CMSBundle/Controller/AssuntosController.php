<?php

namespace BetaGT\Bundles\CMSBundle\Controller;

use BetaGT\Bundles\CMSBundle\Entity\Assuntos;
use BetaGT\Bundles\CMSBundle\Form\AssuntosType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class AssuntosController extends MainController
{
    /**
     * @Route("/assuntos/novo", name="cms_assuntos_novo")
     * @Template("CMSBundle:Assuntos:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction(Request $request)
    {
        $entity = new Assuntos();

        $form = $this->createForm(new AssuntosType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get("actions")->get('salvarNovo')->isClicked()
                ? 'cms_assuntos_novo'
                : 'cms_assuntos_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home'));
        $breadcrumbs->addItem('Assunto: Listar Registros', $this->get('router')->generate('cms_assuntos_listar'));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Assuntos', 'descricao' => 'Separação de mensagens via contato do site'),
        );
    }

    /**
     * @Route("/assuntos/{id}/atualizar", name="cms_assuntos_atualizar")
     * @Template("CMSBundle:Assuntos:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Assuntos')->find($id);
        if (!$entity instanceof Assuntos)
        {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_assuntos_listar', array());
        }

        $form = $this->createForm(new AssuntosType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get("actions")->get('salvarNovo')->isClicked()
                ? 'cms_assuntos_novo'
                : 'cms_assuntos_listar';

            return $this->redirectToRoute($nextAction, array());
        }
        
        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Assunto: Listar Registros', $this->get('router')->generate('cms_assuntos_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $entity->getTitulo(), $this->get('router')->generate('cms_assuntos_visualizar', array('id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Assuntos', 'descricao' => 'Separação de mensagens via contato do site'),
        );
    }

    /**
     * @Route("/assuntos/listar", name="cms_assuntos_listar")
     * @Template("CMSBundle:Assuntos:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('CMSBundle:Assuntos');
        $queryBuilder = $repository->createQueryBuilder('item')
            ->where('item.status = :status')
            ->setParameter('status', '1')
        ;

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('item.titulo', array('label' => 'Título', 'filterable' => 'true', 'sortable' => true)))
            ->addField(new Field('item.email', array('label' => 'E-mail', 'filterable' => 'true', 'sortable' => true)))
        ;

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Assunto: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('cms_assuntos_delete_selecionado'),
            'novo' => $this->generateUrl('cms_assuntos_novo'),
            'modulo' => array('titulo' => 'Assuntos', 'descricao' => 'Separação de mensagens via contato do site'),
        );
    }

    /**
     * @Route("/assuntos/{id}/visualizar", name="cms_assuntos_visualizar")
     * @Template("CMSBundle:Assuntos:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Assuntos')->find($id);
        if (!$entity instanceof Assuntos)
        {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_assuntos_listar', array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Listar Registros', $this->get('router')->generate('cms_assuntos_listar', array()));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar: ' . $entity->getTitulo(),
            'entity' => $entity,
            'modulo' => array('titulo' => 'Assuntos', 'descricao' => 'Separação de mensagens via contato do site'),
        );
    }

    /**
     * @Route("/assuntos/{id}/delete", name="cms_assuntos_delete")
     * @Method("GET")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Assuntos')->find($id);
        if (!$entity instanceof Assuntos)
        {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_assuntos_listar', array());
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('cms_assuntos_listar', array());
    }

    /**
     * @Route("/assuntos/delete/selecionados", name="cms_assuntos_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction(Request $request)
    {
        $data = $request->request->all();

        if (empty($data))
        {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('cms_assuntos_listar', array());
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++)
        {
            $entity = $em->getRepository('CMSBundle:Assuntos')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('cms_assuntos_listar', array());
    }
}

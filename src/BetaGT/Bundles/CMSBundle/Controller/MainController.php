<?php
/**
 * Created by PhpStorm.
 * User: allan
 * Date: 04/09/2015
 * Time: 15:46
 */

namespace BetaGT\Bundles\CMSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{

    /**
     * @param string $route
     */
    private function goToPage($route)
    {
        header("location:" . $route);
        exit();
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager|object
     */
    protected function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @param $id
     * @param $bundleName
     * @param $entityName
     * @param $route
     * @param $routeParams
     * @return Retornar entitidade ou redirecionar para rota
     */
    protected function checkParent($id, $bundleName, $entityName, $route, $routeParams)
    {
        $entity = $this->getEntityManager()->getRepository($bundleName . ":" . $entityName)->find($id);
        if (empty($entity)) {
            $this->addFlash('error', "Relacionamento não localizado");

            $route = $this->generateUrl($route, array($routeParams));
            $this->goToPage($route);
        }
        return $entity;
    }

}
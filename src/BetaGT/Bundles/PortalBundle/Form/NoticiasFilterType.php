<?php

namespace BetaGT\Bundles\PortalBundle\Form;

use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NoticiasFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('titulo', 'filter_text', [ 'label' => 'Pesquisar Notícias', 'condition_pattern' => FilterOperands::STRING_BOTH, 'attr' => [ 'class' => 'form-group'] ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'validation_groups' => ['filtering'],
        ]);
    }

    public function getName()
    {
        return 'noticias_filter';
    }
}

<?php

namespace BetaGT\Bundles\CMSBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PessoasType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', [ 'label' => 'Razão Social'])
            ->add('nomeFantasia')
            ->add('cnpj', 'text', [ 'label' => 'CNPJ', 'attr' => [ 'class' => 'cnpj' ]])
            ->add('inscricaoEstadual')
            ->add('inscricaoMunicipal')
            ->add('endereco')
            ->add('complemento')
            ->add('bairro')
            ->add('cep', 'text', [ 'label' => 'CEP', 'attr' => [ 'class' => 'cep']])
            ->add('estado')
            ->add('cidade')
            ->add('actions', 'form_actions', [
                'buttons' => [
                    'salvar' => ['type' => 'submit', 'options' => ['label' => 'Salvar', 'attr' => ['class' => 'btn btn-primary']]],
                    'salvarNovo' => ['type' => 'submit', 'options' => ['label' => 'Salvar e novo', 'attr' => ['class' => 'btn btn-primary']]],
                    'cancelar' => ['type' => 'reset', 'options' => ['label' => 'Cancelar', 'attr' => ['class' => 'btn btn-warning']]],
                ]
            ])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BetaGT\Bundles\CMSBundle\Entity\Pessoas'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form';
    }
}

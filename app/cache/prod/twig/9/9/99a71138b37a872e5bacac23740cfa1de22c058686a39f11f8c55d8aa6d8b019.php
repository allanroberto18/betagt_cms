<?php

/* CMSBundle:Default:base.html.twig */
class __TwigTemplate_b03903f5981faf10a26f69f2a3457929c0f1c005d27336e8c82bc48bc164a571 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'nav' => array($this, 'block_nav'),
            'message' => array($this, 'block_message'),
            'body_header' => array($this, 'block_body_header'),
            'modal' => array($this, 'block_modal'),
            'body' => array($this, 'block_body'),
            'body_footer' => array($this, 'block_body_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("CMSBundle:Default:header.html.twig", "CMSBundle:Default:base.html.twig", 1)->display($context);
        // line 2
        echo "    <!-- Content Wrapper. Contains page content -->
    <div class=\"content-wrapper\">
        <!-- Content Header (Page header) -->
        <section class=\"content-header\">
            <h1>
                ";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["modulo"]) ? $context["modulo"] : null), "titulo", array()), "html", null, true);
        echo "
                <small>";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["modulo"]) ? $context["modulo"] : null), "descricao", array()), "html", null, true);
        echo "</small>
            </h1>
            ";
        // line 10
        $this->displayBlock('nav', $context, $blocks);
        // line 13
        echo "        </section>
        <!-- Main content -->
        <section class=\"content\">
            <!-- Default box -->
            <div class=\"box\">
                <div class=\"box-header with-border\">
                    <h3 class=\"box-title\">";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["titulo"]) ? $context["titulo"] : null), "html", null, true);
        echo "</h3>
                    <div class=\"box-tools pull-right\">
                        <button class=\"btn btn-box-tool\" data-widget=\"collapse\" data-toggle=\"tooltip\" title=\"Collapse\">
                            <i class=\"fa fa-minus\"></i>
                        </button>
                        <button class=\"btn btn-box-tool\" data-widget=\"remove\" data-toggle=\"tooltip\" title=\"Remove\">
                            <i class=\"fa fa-times\"></i>
                        </button>
                    </div>
                </div>
                <div class=\"box-body\">
                    ";
        // line 30
        $this->displayBlock('message', $context, $blocks);
        // line 53
        echo "                    ";
        $this->displayBlock('body_header', $context, $blocks);
        // line 56
        echo "                    ";
        $this->displayBlock('modal', $context, $blocks);
        // line 59
        echo "                    ";
        $this->displayBlock('body', $context, $blocks);
        // line 62
        echo "                </div>
                <!-- /.box-body -->
                <div class=\"box-footer\">
                    ";
        // line 65
        $this->displayBlock('body_footer', $context, $blocks);
        // line 68
        echo "                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
    <footer class=\"main-footer\">
        <div class=\"pull-right hidden-xs\">
            <b>Versão</b> 0.1
        </div>
        <strong>Copyright &copy; 2015 <a href=\"http://betagt.com.br\">BetaGT Desenvolvimento de Software</a>.</strong>
        Todos os direitos reservados.
    </footer>
</div>
";
        // line 83
        $this->displayBlock('javascripts', $context, $blocks);
        // line 111
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/dist/js/app.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 112
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/dist/js/pages/dashboard.js"), "html", null, true);
        echo "\" ></script>
</body>
</html>
";
    }

    // line 10
    public function block_nav($context, array $blocks = array())
    {
        // line 11
        echo "                ";
        echo $this->env->getExtension('breadcrumbs')->renderBreadcrumbs();
        echo "
            ";
    }

    // line 30
    public function block_message($context, array $blocks = array())
    {
        // line 31
        echo "                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 32
            echo "                            <div class=\"alert alert-danger alert-dismissable\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                                <h4><i class=\"icon fa fa-remove\"></i> Atenção</h4>
                                ";
            // line 35
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo " <br/>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "info"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 39
            echo "                            <div class=\"alert alert-info alert-dismissable\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                                <h4><i class=\"icon fa fa-exclamation\"></i> Atenção</h4>
                                ";
            // line 42
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo " <br/>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 46
            echo "                            <div class=\"alert alert-success alert-dismissable\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                                <h4><i class=\"icon fa fa-check\"></i> Parabéns!</h4>
                                ";
            // line 49
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo " <br/>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "                    ";
    }

    // line 53
    public function block_body_header($context, array $blocks = array())
    {
        // line 54
        echo "
                    ";
    }

    // line 56
    public function block_modal($context, array $blocks = array())
    {
        // line 57
        echo "
                    ";
    }

    // line 59
    public function block_body($context, array $blocks = array())
    {
        // line 60
        echo "
                    ";
    }

    // line 65
    public function block_body_footer($context, array $blocks = array())
    {
        // line 66
        echo "
                    ";
    }

    // line 83
    public function block_javascripts($context, array $blocks = array())
    {
        // line 84
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/jQuery/jQuery-2.1.4.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"https://code.jquery.com/ui/1.11.4/jquery-ui.min.js\"></script>
    <script type=\"text/javascript\">
        \$.widget.bridge('uibutton', \$.ui.button);
    </script>
    <script type=\"text/javascript\" src=\"";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 91
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/morris/morris.min.js"), "html", null, true);
        echo "\"></script>
    <!-- Sparkline -->
    <script type=\"text/javascript\" src=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/sparkline/jquery.sparkline.min.js"), "html", null, true);
        echo "\"></script>
    <!-- jvectormap -->
    <script type=\"text/javascript\" src=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"), "html", null, true);
        echo "\"></script>
    <!-- jQuery Knob Chart -->
    <script type=\"text/javascript\" src=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/knob/jquery.knob.js"), "html", null, true);
        echo "\"></script>
    <!-- daterangepicker -->
    <script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 101
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/daterangepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>
    <!-- datepicker -->
    <script type=\"text/javascript\" src=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/datepicker/bootstrap-datepicker.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script type=\"text/javascript\" src=\"";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"), "html", null, true);
        echo "\" ></script>

    <script type=\"text/javascript\" src=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/slimScroll/jquery.slimscroll.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 108
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/fastclick/fastclick.js"), "html", null, true);
        echo "\"></script>

";
    }

    public function getTemplateName()
    {
        return "CMSBundle:Default:base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  285 => 108,  281 => 107,  276 => 105,  271 => 103,  266 => 101,  260 => 98,  255 => 96,  251 => 95,  246 => 93,  241 => 91,  236 => 89,  227 => 84,  224 => 83,  219 => 66,  216 => 65,  211 => 60,  208 => 59,  203 => 57,  200 => 56,  195 => 54,  192 => 53,  188 => 52,  179 => 49,  174 => 46,  169 => 45,  160 => 42,  155 => 39,  150 => 38,  141 => 35,  136 => 32,  131 => 31,  128 => 30,  121 => 11,  118 => 10,  110 => 112,  105 => 111,  103 => 83,  86 => 68,  84 => 65,  79 => 62,  76 => 59,  73 => 56,  70 => 53,  68 => 30,  54 => 19,  46 => 13,  44 => 10,  39 => 8,  35 => 7,  28 => 2,  26 => 1,);
    }
}
/* {% include 'CMSBundle:Default:header.html.twig' %}*/
/*     <!-- Content Wrapper. Contains page content -->*/
/*     <div class="content-wrapper">*/
/*         <!-- Content Header (Page header) -->*/
/*         <section class="content-header">*/
/*             <h1>*/
/*                 {{ modulo.titulo }}*/
/*                 <small>{{ modulo.descricao }}</small>*/
/*             </h1>*/
/*             {% block nav %}*/
/*                 {{ wo_render_breadcrumbs() }}*/
/*             {% endblock %}*/
/*         </section>*/
/*         <!-- Main content -->*/
/*         <section class="content">*/
/*             <!-- Default box -->*/
/*             <div class="box">*/
/*                 <div class="box-header with-border">*/
/*                     <h3 class="box-title">{{ titulo }}</h3>*/
/*                     <div class="box-tools pull-right">*/
/*                         <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">*/
/*                             <i class="fa fa-minus"></i>*/
/*                         </button>*/
/*                         <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">*/
/*                             <i class="fa fa-times"></i>*/
/*                         </button>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="box-body">*/
/*                     {% block message %}*/
/*                         {% for flashMessage in app.session.flashbag.get('error') %}*/
/*                             <div class="alert alert-danger alert-dismissable">*/
/*                                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>*/
/*                                 <h4><i class="icon fa fa-remove"></i> Atenção</h4>*/
/*                                 {{ flashMessage }} <br/>*/
/*                             </div>*/
/*                         {% endfor %}*/
/*                         {% for flashMessage in app.session.flashbag.get('info') %}*/
/*                             <div class="alert alert-info alert-dismissable">*/
/*                                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>*/
/*                                 <h4><i class="icon fa fa-exclamation"></i> Atenção</h4>*/
/*                                 {{ flashMessage }} <br/>*/
/*                             </div>*/
/*                         {% endfor %}*/
/*                         {% for flashMessage in app.session.flashbag.get('success') %}*/
/*                             <div class="alert alert-success alert-dismissable">*/
/*                                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>*/
/*                                 <h4><i class="icon fa fa-check"></i> Parabéns!</h4>*/
/*                                 {{ flashMessage }} <br/>*/
/*                             </div>*/
/*                         {% endfor %}*/
/*                     {% endblock %}*/
/*                     {% block body_header %}*/
/* */
/*                     {% endblock %}*/
/*                     {% block modal %}*/
/* */
/*                     {% endblock %}*/
/*                     {% block body %}*/
/* */
/*                     {% endblock %}*/
/*                 </div>*/
/*                 <!-- /.box-body -->*/
/*                 <div class="box-footer">*/
/*                     {% block body_footer %}*/
/* */
/*                     {% endblock %}*/
/*                 </div>*/
/*                 <!-- /.box-footer-->*/
/*             </div>*/
/*             <!-- /.box -->*/
/*         </section>*/
/*         <!-- /.content -->*/
/*     </div><!-- /.content-wrapper -->*/
/*     <footer class="main-footer">*/
/*         <div class="pull-right hidden-xs">*/
/*             <b>Versão</b> 0.1*/
/*         </div>*/
/*         <strong>Copyright &copy; 2015 <a href="http://betagt.com.br">BetaGT Desenvolvimento de Software</a>.</strong>*/
/*         Todos os direitos reservados.*/
/*     </footer>*/
/* </div>*/
/* {% block javascripts %}*/
/*     <script type="text/javascript" src="{{ asset('admin/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>*/
/*     <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>*/
/*     <script type="text/javascript">*/
/*         $.widget.bridge('uibutton', $.ui.button);*/
/*     </script>*/
/*     <script type="text/javascript" src="{{ asset('admin/bootstrap/js/bootstrap.min.js') }}"></script>*/
/*     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>*/
/*     <script type="text/javascript" src="{{ asset('admin/plugins/morris/morris.min.js') }}"></script>*/
/*     <!-- Sparkline -->*/
/*     <script type="text/javascript" src="{{ asset('admin/plugins/sparkline/jquery.sparkline.min.js') }}"></script>*/
/*     <!-- jvectormap -->*/
/*     <script type="text/javascript" src="{{ asset('admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>*/
/*     <script type="text/javascript" src="{{ asset('admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>*/
/*     <!-- jQuery Knob Chart -->*/
/*     <script type="text/javascript" src="{{ asset('admin/plugins/knob/jquery.knob.js') }}"></script>*/
/*     <!-- daterangepicker -->*/
/*     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>*/
/*     <script type="text/javascript" src="{{ asset('admin/plugins/daterangepicker/daterangepicker.js') }}"></script>*/
/*     <!-- datepicker -->*/
/*     <script type="text/javascript" src="{{ asset('admin/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>*/
/*     <!-- Bootstrap WYSIHTML5 -->*/
/*     <script type="text/javascript" src="{{ asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" ></script>*/
/* */
/*     <script type="text/javascript" src="{{ asset('admin/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>*/
/*     <script type="text/javascript" src="{{ asset('admin/plugins/fastclick/fastclick.js') }}"></script>*/
/* */
/* {% endblock %}*/
/*     <script type="text/javascript" src="{{ asset('admin/dist/js/app.min.js') }}"></script>*/
/*     <script type="text/javascript" src="{{ asset('admin/dist/js/pages/dashboard.js') }}" ></script>*/
/* </body>*/
/* </html>*/
/* */

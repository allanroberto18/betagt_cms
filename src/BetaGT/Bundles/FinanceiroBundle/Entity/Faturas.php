<?php

namespace BetaGT\Bundles\FinanceiroBundle\Entity;

use BetaGT\Bundles\CMSBundle\Entity\EntityMaster;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Faturas
 *
 * @ORM\Table(name="faturas", indexes={@ORM\Index(name="fk_faturas_contratos_idx", columns={"contrato_id"})})
 * @ORM\Entity
 */
class Faturas extends EntityMaster
{
    /**
     * @var \DateTime
     * @Assert\NotBlank(message="O campo Vencimento é obrigatório")
     * @ORM\Column(name="vencimento", type="datetime", nullable=true)
     */
    private $vencimento;

    /**
     * @var float
     * @Assert\NotBlank(message="O campo Valor é obrigatório")
     * @ORM\Column(name="valor", type="float", precision=10, scale=2, nullable=true)
     */
    private $valor;

    /**
     * @var float
     * @Assert\NotBlank(message="O campo Valor Pago é obrigatório", groups={"update"} )
     * @ORM\Column(name="valor_pago", type="float", precision=10, scale=2, nullable=true)
     */
    private $valorPago;

    /**
     * @var \DateTime
     * @Assert\NotBlank(message="O campo Data de Pagamento é obrigatório", groups={"update"} )
     * @ORM\Column(name="data_pagamento", type="date", nullable=true)
     */
    private $dataPagamento;

    /**
     * @var string
     *
     * @ORM\Column(name="observacao", type="text", nullable=true)
     */
    private $observacao;

    /**
     * @var \Contratos
     *
     * @ORM\ManyToOne(targetEntity="Contratos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contrato_id", referencedColumnName="id")
     * })
     */
    private $contrato;

    /**
     * Set vencimento
     *
     * @param \DateTime $vencimento
     * @return Faturas
     */
    public function setVencimento($vencimento)
    {
        $this->vencimento = $vencimento;

        return $this;
    }

    /**
     * Get vencimento
     *
     * @return \DateTime 
     */
    public function getVencimento()
    {
        return $this->vencimento;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return Faturas
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set valorPago
     *
     * @param float $valorPago
     * @return Faturas
     */
    public function setValorPago($valorPago)
    {
        $this->valorPago = $valorPago;

        return $this;
    }

    /**
     * Get valorPago
     *
     * @return float 
     */
    public function getValorPago()
    {
        return $this->valorPago;
    }

    /**
     * Set dataPagamento
     *
     * @param \DateTime $dataPagamento
     * @return Faturas
     */
    public function setDataPagamento($dataPagamento)
    {
        $this->dataPagamento = $dataPagamento;

        return $this;
    }

    /**
     * Get dataPagamento
     *
     * @return \DateTime 
     */
    public function getDataPagamento()
    {
        return $this->dataPagamento;
    }

    /**
     * Set observacao
     *
     * @param string $observacao
     * @return Faturas
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;

        return $this;
    }

    /**
     * Get observacao
     *
     * @return string 
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * Set contrato
     *
     * @param \BetaGT\Bundles\FinanceiroBundle\Entity\Contratos $contrato
     * @return Faturas
     */
    public function setContrato(\BetaGT\Bundles\FinanceiroBundle\Entity\Contratos $contrato = null)
    {
        $this->contrato = $contrato;

        return $this;
    }

    /**
     * Get contrato
     *
     * @return \BetaGT\Bundles\FinanceiroBundle\Entity\Contratos
     */
    public function getContrato()
    {
        return $this->contrato;
    }

    public function returnStatus()
    {
        switch ($this->status)
        {
            case 1 :
                return "<span class='label label-danger'><span class='fa fa-close'></span> Em Aberto</span>";
                break;
            case 2 :
                return "<span class='label label-success'><span class='fa fa-check'></span> Pago</span>";
                break;
        }
    }
}

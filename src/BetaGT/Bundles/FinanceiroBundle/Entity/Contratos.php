<?php

namespace BetaGT\Bundles\FinanceiroBundle\Entity;

use BetaGT\Bundles\CMSBundle\Entity\EntityMaster;
use Doctrine\ORM\Mapping as ORM;

/**
 * Contratos
 *
 * @ORM\Table(name="contratos", indexes={@ORM\Index(name="fk_contratos_clientes_idx", columns={"pessoa_id"}), @ORM\Index(name="fk_contratos_planos_idx", columns={"plano_id"})})
 * @ORM\Entity
 */
class Contratos extends EntityMaster
{
    /**
     * @var \Planos
     *
     * @ORM\ManyToOne(targetEntity="Planos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plano_id", referencedColumnName="id")
     * })
     */
    private $plano;

    /**
     * @var \Pessoas
     *
     * @ORM\ManyToOne(targetEntity="\BetaGT\Bundles\CMSBundle\Entity\Pessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pessoa_id", referencedColumnName="id")
     * })
     */
    private $pessoa;

    /**
     * Set plano
     *
     * @param \BetaGT\Bundles\FinanceiroBundle\Entity\Planos $plano
     * @return Contratos
     */
    public function setPlano(\BetaGT\Bundles\FinanceiroBundle\Entity\Planos $plano = null)
    {
        $this->plano = $plano;

        return $this;
    }

    /**
     * Get plano
     *
     * @return \BetaGT\Bundles\FinanceiroBundle\Entity\Planos
     */
    public function getPlano()
    {
        return $this->plano;
    }

    /**
     * Set pessoa
     *
     * @param \BetaGT\Bundles\CMSBundle\Entity\Pessoas $pessoa
     * @return Contratos
     */
    public function setPessoa(\BetaGT\Bundles\CMSBundle\Entity\Pessoas $pessoa = null)
    {
        $this->pessoa = $pessoa;

        return $this;
    }

    /**
     * Get pessoa
     *
     * @return \BetaGT\Bundles\CMSBundle\Entity\Pessoas
     */
    public function getPessoa()
    {
        return $this->pessoa;
    }

    public function returnStatus()
    {
        switch ($this->status)
        {
            case 1 :
                return "<span class='label label-danger'><span class='fa fa-lock'></span> Bloqueado</span>";
                break;
            case 2 :
                return "<span class='label label-success'><span class='fa fa-check'></span> Liberado</span>";
                break;
        }
    }


}

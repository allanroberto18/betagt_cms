<?php

namespace BetaGT\Bundles\FinanceiroBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FaturasType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('contrato')
            ->add('vencimento')
            ->add('valor')
            ->add('valorPago')
            ->add('dataPagamento')
            ->add('observacao')
            ->add('actions', 'form_actions', [
                'buttons' => [
                    'salvar' => ['type' => 'submit', 'options' => ['label' => 'Salvar', 'attr' => ['class' => 'btn btn-primary']]],
                    'salvarNovo' => ['type' => 'submit', 'options' => ['label' => 'Salvar e novo', 'attr' => ['class' => 'btn btn-primary']]],
                    'cancelar' => ['type' => 'reset', 'options' => ['label' => 'Cancelar', 'attr' => ['class' => 'btn btn-warning']]],
                ]
            ])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BetaGT\Bundles\FinanceiroBundle\Entity\Faturas'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form';
    }
}

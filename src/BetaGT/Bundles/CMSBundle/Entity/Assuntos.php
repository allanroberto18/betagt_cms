<?php

namespace BetaGT\Bundles\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Assuntos
 *
 * @ORM\Table(name="assuntos")
 * @ORM\Entity
 */
class Assuntos extends EntityMaster
{
    /**
     * @var string
     * @Assert\NotBlank(message="O campo Título é obrigatório")
     * @ORM\Column(name="titulo", type="string", length=255, nullable=true)
     */
    private $titulo;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo E-mail é obrigatório")
     * @Assert\Email(message="O E-mail informado é inválido")
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    public function __toString()
    {
        return $this->getTitulo();
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Assuntos
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Assuntos
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
}

<?php

namespace BetaGT\Bundles\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Banners
 * @Vich\Uploadable
 * @ORM\Table(name="banners")
 * @ORM\Entity
 */
class Banners extends EntityMaster
{
    use TImageName;
    /**
     * @var string
     * @Assert\NotBlank(message="O campo Título é obrigatório")
     * @ORM\Column(name="titulo", type="string", length=255, nullable=true)
     */
    private $titulo;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Link é obrigatório")
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Data Final é obrigatório")
     * @ORM\Column(name="data_final", type="date", length=255, nullable=true)
     */
    private $dataFinal;

    /**
     * @var File
     * @Vich\UploadableField(mapping="banner_image", fileNameProperty="imageName")
     */
    private $imageFile;

    public function __toString()
    {
        return $this->getTitulo();
    }

    /**
 * Set titulo
 *
 * @param string $titulo
 * @return Banners
 */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return Banners
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set dataFinal
     *
     * @param date $dataFinal
     * @return Banners
     */
    public function setDataFinal($dataFinal)
    {
        $this->dataFinal = $dataFinal;

        return $this;
    }

    /**
     * Get dataFinal
     *
     * @return date 
     */
    public function getDataFinal()
    {
        return $this->dataFinal;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updated = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function returnStatus()
    {
        switch ($this->status)
        {
            case 1:
                return "Sim";
                break;
            case 2:
                return "Não";
                break;
        }
    }


}

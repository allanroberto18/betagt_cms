<?php
/**
 * Created by PhpStorm.
 * User: allan
 * Date: 31/10/2015
 * Time: 17:14
 */

namespace BetaGT\Bundles\CMSBundle\Utils;

use Doctrine\ORM\EntityManager;

class BannerTest
{
    private $entityManager;

    /**
     * BannerTest constructor.
     * @param $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getAll()
    {
        return $this->entityManager->getRepository('\BetaGT\Bundles\CMSBundle\Entity\Banner')->findAll();
    }
}
<?php

namespace BetaGT\Bundles\PortalBundle\Utils;

use Doctrine\ORM\EntityManager;

class Assuntos
{
    private $entityManager;

    /**
     * ServiceMaster constructor.
     * @param $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array|\BetaGT\Bundles\CMSBundle\Entity\Assuntos[]
     */
    public function returnAll()
    {
        return $this->entityManager->getRepository("CMSBundle:Assuntos")->findBy(['status' => 1], ['titulo' => 'ASC']);
    }
}
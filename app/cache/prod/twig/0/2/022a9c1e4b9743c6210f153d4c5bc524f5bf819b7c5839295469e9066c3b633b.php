<?php

/* CMSBundle:Default:header.html.twig */
class __TwigTemplate_f392ac80864cf1651beb9b23e1fce20f92f190d4c7d256809fa98070b94137c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'styles' => array($this, 'block_styles'),
            'title' => array($this, 'block_title'),
            'header' => array($this, 'block_header'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
    ";
        // line 7
        $this->displayBlock('styles', $context, $blocks);
        // line 16
        echo "    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
    <title>";
        // line 22
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    <style>

    </style>
</head>
<body class=\"skin-blue sidebar-mini\">
<div class=\"wrapper\">
    ";
        // line 29
        $this->displayBlock('header', $context, $blocks);
        // line 181
        echo twig_include($this->env, $context, "@CMS/Default/menu.html.twig");
    }

    // line 7
    public function block_styles($context, array $blocks = array())
    {
        // line 8
        echo "        <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />
        <link href=\"https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css\" rel=\"stylesheet\" type=\"text/css\" />
        <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/dist/css/AdminLTE.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
        <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/dist/css/skins/skin-blue.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
        <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/picedit/css/picedit.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
        <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
    ";
    }

    // line 22
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (isset($context["titulo"]) ? $context["titulo"] : null), "html", null, true);
    }

    // line 29
    public function block_header($context, array $blocks = array())
    {
        // line 30
        echo "    <header class=\"main-header\">
        <!-- Logo -->
        <a href=\"";
        // line 32
        echo $this->env->getExtension('routing')->getPath("cms_home");
        echo "\" class=\"logo\">
            <span class=\"logo-mini\"><b>CMS</b>Portal</span>
            <span class=\"logo-lg\"><b>BetaGT</b> CMSPortal</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class=\"navbar navbar-static-top\" role=\"navigation\">
            <!-- Sidebar toggle button-->
            <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </a>
            <div class=\"navbar-custom-menu\">
                <ul class=\"nav navbar-nav\">
                    <!-- Messages: style can be found in dropdown.less-->
                    ";
        // line 49
        echo "                        ";
        // line 50
        echo "                            ";
        // line 51
        echo "                            ";
        // line 52
        echo "                        ";
        // line 53
        echo "                        ";
        // line 54
        echo "                            ";
        // line 55
        echo "                            ";
        // line 56
        echo "                                ";
        // line 57
        echo "                                ";
        // line 58
        echo "                                    ";
        // line 59
        echo "                                        ";
        // line 60
        echo "                                            ";
        // line 61
        echo "                                                ";
        // line 62
        echo "                                            ";
        // line 63
        echo "                                            ";
        // line 64
        echo "                                                ";
        // line 65
        echo "                                                ";
        // line 66
        echo "                                            ";
        // line 67
        echo "                                            ";
        // line 68
        echo "                                        ";
        // line 69
        echo "                                    ";
        // line 70
        echo "                                ";
        // line 71
        echo "                            ";
        // line 72
        echo "                            ";
        // line 73
        echo "                        ";
        // line 74
        echo "                    ";
        // line 75
        echo "                    <!-- Notifications: style can be found in dropdown.less -->
                    ";
        // line 77
        echo "                        ";
        // line 78
        echo "                            ";
        // line 79
        echo "                            ";
        // line 80
        echo "                        ";
        // line 81
        echo "                        ";
        // line 82
        echo "                            ";
        // line 83
        echo "                            ";
        // line 84
        echo "                                ";
        // line 85
        echo "                                ";
        // line 86
        echo "                                    ";
        // line 87
        echo "                                        ";
        // line 88
        echo "                                            ";
        // line 89
        echo "                                        ";
        // line 90
        echo "                                    ";
        // line 91
        echo "                                ";
        // line 92
        echo "                            ";
        // line 93
        echo "                            ";
        // line 94
        echo "                        ";
        // line 95
        echo "                    ";
        // line 96
        echo "                    <!-- Tasks: style can be found in dropdown.less -->
                    ";
        // line 98
        echo "                        ";
        // line 99
        echo "                            ";
        // line 100
        echo "                            ";
        // line 101
        echo "                        ";
        // line 102
        echo "                        ";
        // line 103
        echo "                            ";
        // line 104
        echo "                            ";
        // line 105
        echo "                                ";
        // line 106
        echo "                                ";
        // line 107
        echo "                                    ";
        // line 108
        echo "                                        ";
        // line 109
        echo "                                            ";
        // line 110
        echo "                                                ";
        // line 111
        echo "                                                ";
        // line 112
        echo "                                            ";
        // line 113
        echo "                                            ";
        // line 114
        echo "                                                ";
        // line 115
        echo "                                                    ";
        // line 116
        echo "                                                ";
        // line 117
        echo "                                            ";
        // line 118
        echo "                                        ";
        // line 119
        echo "                                    ";
        // line 120
        echo "                                ";
        // line 121
        echo "                            ";
        // line 122
        echo "                            ";
        // line 123
        echo "                                ";
        // line 124
        echo "                            ";
        // line 125
        echo "                        ";
        // line 126
        echo "                    ";
        // line 127
        echo "                    <!-- User Account: style can be found in dropdown.less -->
                    <li class=\"dropdown user user-menu\">
                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                            ";
        // line 130
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "imageFile", array())) {
            // line 131
            echo "                                <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl(("uploads/galerias/img/" . $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "imageFile", array()))), "html", null, true);
            echo "\" class=\"user-image\" alt=\"User Image\" />
                            ";
        } else {
            // line 133
            echo "                                <i class=\"fa fa-user\"></i>
                            ";
        }
        // line 135
        echo "                            <span class=\"hidden-xs\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
        echo "</span>
                        </a>
                        <ul class=\"dropdown-menu\">
                            <!-- User image -->
                            <li class=\"user-header\">
                                ";
        // line 140
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "imageFile", array())) {
            // line 141
            echo "                                    <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl(("uploads/galerias/img/" . $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "imageFile", array()))), "html", null, true);
            echo "\" class=\"img-circle\" alt=\"User Image\" />
                                ";
        } else {
            // line 143
            echo "                                <span class=\"fa-stack fa-4x\">
                                    <i class=\"fa fa-circle fa-stack-2x\"></i>
                                    <i class=\"fa fa-user fa-stack-1x fa-inverse\"></i>
                                </span>
                                ";
        }
        // line 148
        echo "                                <p>
                                    ";
        // line 149
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
        echo "
                                    <small>";
        // line 150
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "email", array()), "html", null, true);
        echo "</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            ";
        // line 155
        echo "                                ";
        // line 156
        echo "                                    ";
        // line 157
        echo "                                ";
        // line 158
        echo "                                ";
        // line 159
        echo "                                    ";
        // line 160
        echo "                                ";
        // line 161
        echo "                                ";
        // line 162
        echo "                                    ";
        // line 163
        echo "                                ";
        // line 164
        echo "                            ";
        // line 165
        echo "                            <!-- Menu Footer-->
                            <li class=\"user-footer\">
                                <div class=\"pull-left\">
                                    <a href=\"";
        // line 168
        echo $this->env->getExtension('routing')->getPath("fos_user_profile_show");
        echo "\" class=\"btn btn-default btn-flat\"><i class=\"fa fa-search\"></i> Perfil</a>
                                </div>
                                <div class=\"pull-right\">
                                    <a href=\"";
        // line 171
        echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
        echo "\" class=\"btn btn-default btn-flat\"><i class=\"fa fa-sign-out\"></i> Sair</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
";
    }

    public function getTemplateName()
    {
        return "CMSBundle:Default:header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  356 => 171,  350 => 168,  345 => 165,  343 => 164,  341 => 163,  339 => 162,  337 => 161,  335 => 160,  333 => 159,  331 => 158,  329 => 157,  327 => 156,  325 => 155,  318 => 150,  314 => 149,  311 => 148,  304 => 143,  298 => 141,  296 => 140,  287 => 135,  283 => 133,  277 => 131,  275 => 130,  270 => 127,  268 => 126,  266 => 125,  264 => 124,  262 => 123,  260 => 122,  258 => 121,  256 => 120,  254 => 119,  252 => 118,  250 => 117,  248 => 116,  246 => 115,  244 => 114,  242 => 113,  240 => 112,  238 => 111,  236 => 110,  234 => 109,  232 => 108,  230 => 107,  228 => 106,  226 => 105,  224 => 104,  222 => 103,  220 => 102,  218 => 101,  216 => 100,  214 => 99,  212 => 98,  209 => 96,  207 => 95,  205 => 94,  203 => 93,  201 => 92,  199 => 91,  197 => 90,  195 => 89,  193 => 88,  191 => 87,  189 => 86,  187 => 85,  185 => 84,  183 => 83,  181 => 82,  179 => 81,  177 => 80,  175 => 79,  173 => 78,  171 => 77,  168 => 75,  166 => 74,  164 => 73,  162 => 72,  160 => 71,  158 => 70,  156 => 69,  154 => 68,  152 => 67,  150 => 66,  148 => 65,  146 => 64,  144 => 63,  142 => 62,  140 => 61,  138 => 60,  136 => 59,  134 => 58,  132 => 57,  130 => 56,  128 => 55,  126 => 54,  124 => 53,  122 => 52,  120 => 51,  118 => 50,  116 => 49,  97 => 32,  93 => 30,  90 => 29,  84 => 22,  78 => 14,  74 => 13,  70 => 12,  66 => 11,  59 => 8,  56 => 7,  52 => 181,  50 => 29,  40 => 22,  32 => 16,  30 => 7,  22 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/* <head>*/
/*     <meta charset="utf-8">*/
/*     <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*     <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">*/
/*     {% block styles %}*/
/*         <link href="{{ asset('admin/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />*/
/*         <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />*/
/*         <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />*/
/*         <link href="{{ asset('admin/dist/css/AdminLTE.css') }}" rel="stylesheet" type="text/css" />*/
/*         <link href="{{ asset('admin/dist/css/skins/skin-blue.css') }}" rel="stylesheet" type="text/css" />*/
/*         <link href="{{ asset('admin/plugins/picedit/css/picedit.css') }}" rel="stylesheet" type="text/css" />*/
/*         <link href="{{ asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css" />*/
/*     {% endblock %}*/
/*     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->*/
/*     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->*/
/*     <!--[if lt IE 9]>*/
/*     <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>*/
/*     <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>*/
/*     <![endif]-->*/
/*     <title>{% block title %}{{ titulo }}{% endblock %}</title>*/
/*     <style>*/
/* */
/*     </style>*/
/* </head>*/
/* <body class="skin-blue sidebar-mini">*/
/* <div class="wrapper">*/
/*     {% block header %}*/
/*     <header class="main-header">*/
/*         <!-- Logo -->*/
/*         <a href="{{ path('cms_home') }}" class="logo">*/
/*             <span class="logo-mini"><b>CMS</b>Portal</span>*/
/*             <span class="logo-lg"><b>BetaGT</b> CMSPortal</span>*/
/*         </a>*/
/*         <!-- Header Navbar: style can be found in header.less -->*/
/*         <nav class="navbar navbar-static-top" role="navigation">*/
/*             <!-- Sidebar toggle button-->*/
/*             <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">*/
/*                 <span class="sr-only">Toggle navigation</span>*/
/*                 <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </a>*/
/*             <div class="navbar-custom-menu">*/
/*                 <ul class="nav navbar-nav">*/
/*                     <!-- Messages: style can be found in dropdown.less-->*/
/*                     {#<li class="dropdown messages-menu">#}*/
/*                         {#<a href="#" class="dropdown-toggle" data-toggle="dropdown">#}*/
/*                             {#<i class="fa fa-envelope-o"></i>#}*/
/*                             {#<span class="label label-success">4</span>#}*/
/*                         {#</a>#}*/
/*                         {#<ul class="dropdown-menu">#}*/
/*                             {#<li class="header">You have 4 messages</li>#}*/
/*                             {#<li>#}*/
/*                                 {#<!-- inner menu: contains the actual data -->#}*/
/*                                 {#<ul class="menu">#}*/
/*                                     {#<li><!-- start message -->#}*/
/*                                         {#<a href="#">#}*/
/*                                             {#<div class="pull-left">#}*/
/*                                                 {#<img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />#}*/
/*                                             {#</div>#}*/
/*                                             {#<h4>#}*/
/*                                                 {#Support Team#}*/
/*                                                 {#<small><i class="fa fa-clock-o"></i> 5 mins</small>#}*/
/*                                             {#</h4>#}*/
/*                                             {#<p>Why not buy a new awesome theme?</p>#}*/
/*                                         {#</a>#}*/
/*                                     {#</li><!-- end message -->#}*/
/*                                 {#</ul>#}*/
/*                             {#</li>#}*/
/*                             {#<li class="footer"><a href="#">See All Messages</a></li>#}*/
/*                         {#</ul>#}*/
/*                     {#</li>#}*/
/*                     <!-- Notifications: style can be found in dropdown.less -->*/
/*                     {#<li class="dropdown notifications-menu">#}*/
/*                         {#<a href="#" class="dropdown-toggle" data-toggle="dropdown">#}*/
/*                             {#<i class="fa fa-bell-o"></i>#}*/
/*                             {#<span class="label label-warning">10</span>#}*/
/*                         {#</a>#}*/
/*                         {#<ul class="dropdown-menu">#}*/
/*                             {#<li class="header">You have 10 notifications</li>#}*/
/*                             {#<li>#}*/
/*                                 {#<!-- inner menu: contains the actual data -->#}*/
/*                                 {#<ul class="menu">#}*/
/*                                     {#<li>#}*/
/*                                         {#<a href="#">#}*/
/*                                             {#<i class="fa fa-users text-aqua"></i> 5 new members joined today#}*/
/*                                         {#</a>#}*/
/*                                     {#</li>#}*/
/*                                 {#</ul>#}*/
/*                             {#</li>#}*/
/*                             {#<li class="footer"><a href="#">View all</a></li>#}*/
/*                         {#</ul>#}*/
/*                     {#</li>#}*/
/*                     <!-- Tasks: style can be found in dropdown.less -->*/
/*                     {#<li class="dropdown tasks-menu">#}*/
/*                         {#<a href="#" class="dropdown-toggle" data-toggle="dropdown">#}*/
/*                             {#<i class="fa fa-flag-o"></i>#}*/
/*                             {#<span class="label label-danger">9</span>#}*/
/*                         {#</a>#}*/
/*                         {#<ul class="dropdown-menu">#}*/
/*                             {#<li class="header">You have 9 tasks</li>#}*/
/*                             {#<li>#}*/
/*                                 {#<!-- inner menu: contains the actual data -->#}*/
/*                                 {#<ul class="menu">#}*/
/*                                     {#<li><!-- Task item -->#}*/
/*                                         {#<a href="#">#}*/
/*                                             {#<h3>#}*/
/*                                                 {#Design some buttons#}*/
/*                                                 {#<small class="pull-right">20%</small>#}*/
/*                                             {#</h3>#}*/
/*                                             {#<div class="progress xs">#}*/
/*                                                 {#<div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">#}*/
/*                                                     {#<span class="sr-only">20% Complete</span>#}*/
/*                                                 {#</div>#}*/
/*                                             {#</div>#}*/
/*                                         {#</a>#}*/
/*                                     {#</li><!-- end task item -->#}*/
/*                                 {#</ul>#}*/
/*                             {#</li>#}*/
/*                             {#<li class="footer">#}*/
/*                                 {#<a href="#">View all tasks</a>#}*/
/*                             {#</li>#}*/
/*                         {#</ul>#}*/
/*                     {#</li>#}*/
/*                     <!-- User Account: style can be found in dropdown.less -->*/
/*                     <li class="dropdown user user-menu">*/
/*                         <a href="#" class="dropdown-toggle" data-toggle="dropdown">*/
/*                             {% if app.user.imageFile %}*/
/*                                 <img src="{{ asset('uploads/galerias/img/' ~ app.user.imageFile) }}" class="user-image" alt="User Image" />*/
/*                             {% else %}*/
/*                                 <i class="fa fa-user"></i>*/
/*                             {% endif %}*/
/*                             <span class="hidden-xs">{{ app.user.username }}</span>*/
/*                         </a>*/
/*                         <ul class="dropdown-menu">*/
/*                             <!-- User image -->*/
/*                             <li class="user-header">*/
/*                                 {% if app.user.imageFile %}*/
/*                                     <img src="{{ asset('uploads/galerias/img/' ~ app.user.imageFile) }}" class="img-circle" alt="User Image" />*/
/*                                 {% else %}*/
/*                                 <span class="fa-stack fa-4x">*/
/*                                     <i class="fa fa-circle fa-stack-2x"></i>*/
/*                                     <i class="fa fa-user fa-stack-1x fa-inverse"></i>*/
/*                                 </span>*/
/*                                 {% endif %}*/
/*                                 <p>*/
/*                                     {{ app.user.username }}*/
/*                                     <small>{{ app.user.email }}</small>*/
/*                                 </p>*/
/*                             </li>*/
/*                             <!-- Menu Body -->*/
/*                             {#<li class="user-body">#}*/
/*                                 {#<div class="col-xs-4 text-center">#}*/
/*                                     {#<a href="#">Followers</a>#}*/
/*                                 {#</div>#}*/
/*                                 {#<div class="col-xs-4 text-center">#}*/
/*                                     {#<a href="#">Sales</a>#}*/
/*                                 {#</div>#}*/
/*                                 {#<div class="col-xs-4 text-center">#}*/
/*                                     {#<a href="#">Friends</a>#}*/
/*                                 {#</div>#}*/
/*                             {#</li>#}*/
/*                             <!-- Menu Footer-->*/
/*                             <li class="user-footer">*/
/*                                 <div class="pull-left">*/
/*                                     <a href="{{ path('fos_user_profile_show') }}" class="btn btn-default btn-flat"><i class="fa fa-search"></i> Perfil</a>*/
/*                                 </div>*/
/*                                 <div class="pull-right">*/
/*                                     <a href="{{ path('fos_user_security_logout') }}" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sair</a>*/
/*                                 </div>*/
/*                             </li>*/
/*                         </ul>*/
/*                     </li>*/
/*                 </ul>*/
/*             </div>*/
/*         </nav>*/
/*     </header>*/
/* {% endblock %}*/
/* {{ include('@CMS/Default/menu.html.twig') }}*/

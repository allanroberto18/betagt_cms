<?php

namespace BetaGT\Bundles\CMSBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends MainController
{
    /**
     * @Route("/", name="cms_home")
     * @Template("CMSBundle:Home:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        return array(
            'titulo' => 'Administração',
            'modulo' => array(
                'titulo' => 'Página Principal',
                'descricao' => 'Gerenciar Portal de Conteúdo'
            ),
        );
    }

}

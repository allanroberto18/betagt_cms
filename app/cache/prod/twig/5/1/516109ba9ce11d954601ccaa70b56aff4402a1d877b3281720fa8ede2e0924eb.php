<?php

/* PortalBundle:Default:default.html.twig */
class __TwigTemplate_4f3a8693eb6aa0890ac1d9431e862c0fa53c700db5033e75ef72bb1436842b14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'styles' => array($this, 'block_styles'),
            'header' => array($this, 'block_header'),
            'nav' => array($this, 'block_nav'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE 7 ]><html class=\"ie7\" lang=\"en\"><![endif]-->
<!--[if IE 8 ]><html class=\"ie8\" lang=\"en\"><![endif]-->
<!--[if IE 9 ]><html class=\"ie9\" lang=\"en\"><![endif]-->
<!--[if (gte IE 10)|!(IE)]><!--><html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en-US\"><!--<![endif]-->
<head>

    <title>";
        // line 8
        $this->displayBlock('titulo', $context, $blocks);
        echo "</title>
    <meta name=\"format-detection\" content=\"telephone=no\">
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\" />

    <!-- Seo Meta -->
    <meta name=\"description\" content=\"\">
    <meta name=\"keywords\" content=\"\">

    <!-- Styles -->
    ";
        // line 18
        $this->displayBlock('styles', $context, $blocks);
        // line 27
        echo "    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,300,200,100,500' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800' rel='stylesheet' type='text/css'>

    <!-- Favicon -->
    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/images/favicon.png"), "html", null, true);
        echo "\">

    <script type=\"text/javascript\" src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/bower_components/angular/angular.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/bower_components/ngMask/dist/ngMask.min.js"), "html", null, true);
        echo "\"></script>
</head>

<body>
    ";
        // line 39
        $this->displayBlock('header', $context, $blocks);
        // line 97
        echo "    ";
        $this->displayBlock('nav', $context, $blocks);
        // line 100
        echo "    ";
        $this->displayBlock('body', $context, $blocks);
        // line 103
        echo "    <!-- Contato -->
    ";
        // line 104
        $this->loadTemplate("@Portal/home/contato.html.twig", "PortalBundle:Default:default.html.twig", 104)->display($context);
        // line 105
        echo "    <!-- Contato -->
    <!-- Google Maps -->
    <script type=\"text/javascript\" src=\"http://maps.google.com/maps/api/js?sensor=false\"></script>
    <script>
        var latlng=new google.maps.LatLng(-10.25925553154425,-48.883763551712036);
        function initialize() {
            var mapProp = {
                center:latlng,
                scrollwheel: false,
                zoom:15,
                zoomControl:true,
                mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
                navigationControl: true,
                navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
                panControl:true,
                //mapTypeId:google.maps.MapTypeId.ROADMAP
                mapTypeId:google.maps.MapTypeId.HYBRID
            };
            var map=new google.maps.Map(document.getElementById(\"google-map-id\"),mapProp);
            var marker=new google.maps.Marker({
                position:latlng,
                map: map,
                title: 'Frango Norte'
            });
            marker.setMap(map);
            var infowindow = new google.maps.InfoWindow({
                content:\"Paraíso, Tocantins\"
            });
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map,marker);
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <section id=\"google-map\">
        <div class=\"googlemap-container\">
            <div id=\"google-map-id\" class=\"google-map\"></div>
        </div><!-- end .googlemap-container -->
    </section>
    <!-- Google Maps -->
    ";
        // line 145
        $this->displayBlock('footer', $context, $blocks);
        // line 166
        echo "    ";
        $this->displayBlock('javascript', $context, $blocks);
        // line 189
        echo "</body>
</html>";
    }

    // line 8
    public function block_titulo($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (isset($context["titulo"]) ? $context["titulo"] : null), "html", null, true);
    }

    // line 18
    public function block_styles($context, array $blocks = array())
    {
        // line 19
        echo "        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/styles/font-awesome.min.css"), "html", null, true);
        echo "\" media=\"screen\" />
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/styles/prettyPhoto.css"), "html", null, true);
        echo "\" media=\"screen\" />
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/styles/bootstrap.min.css"), "html", null, true);
        echo "\" media=\"screen\" />
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/styles/owl.carousel.css"), "html", null, true);
        echo "\" media=\"screen\" />
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/styles/owl.theme.css"), "html", null, true);
        echo "\" media=\"screen\" />
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/styles/animate.min.css"), "html", null, true);
        echo "\" media=\"screen\" />
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/style.css"), "html", null, true);
        echo "\" media=\"screen\" />
    ";
    }

    // line 39
    public function block_header($context, array $blocks = array())
    {
        // line 40
        echo "        <header id=\"header\">
            <div id=\"logo\" class=\"text-center animated\" data-animation=\"fadeInUp\" data-animation-delay=\"400\" >
                <a href=\"index.html\"><img src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/images/frangonorte_logo.png"), "html", null, true);
        echo "\" alt=\"Logo\"></a>
            </div><!-- end logo -->
            <div id=\"slideshow\">
                <ul class=\"rslides\">
                    ";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["receitas"]) ? $context["receitas"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 47
            echo "                    <li style=\"background-image: url(";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl(("uploads/receitas/" . $this->getAttribute($context["item"], "imageName", array()))), "html", null, true);
            echo ");\">
                        <img src=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl(("uploads/receitas/" . $this->getAttribute($context["item"], "imageName", array()))), "html", null, true);
            echo "\" alt=\"\">
                        <div class=\"slideshow-caption\">
                            <h1>";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "titulo", array()), "html", null, true);
            echo "</h1>
                            <div class=\"button\">
                                <a href=\"";
            // line 52
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("front_receita", array("id" => $this->getAttribute($context["item"], "id", array()), "slug" => $this->getAttribute($context["item"], "slug", array()))), "html", null, true);
            echo "\">Confira a receita</a>
                            </div>
                        </div><!-- end .slideshow-caption -->
                    </li><!-- end .slideshow item -->
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "                </ul>
            </div><!-- end slideshow -->
        </header>

        <nav id=\"navigation\">
            <div class=\"nav-container\">
                <ul>
                    <li><a class=\"active\" href=\"#welcome\">Bem Vindo</a></li>
                    <li><a href=\"#about\">Sobre <span class=\"extrabold\">Nós</span></a></li>
                    ";
        // line 67
        echo "                    ";
        // line 68
        echo "                    ";
        // line 69
        echo "                    ";
        // line 70
        echo "                    <li><a href=\"#blog\">Nossas <span class=\"extrabold\">Notícias</span></a></li>
                    <li><a href=\"#contact\">Fale <span class=\"extrabold\">Conosco</span></a></li>
                </ul>
            </div><!-- end .nav-container -->
        </nav>

        <nav id=\"mobile-navigation\">
            <div class=\"mobile-nav-container\">
                <div id=\"menu-toggle\">
                    <i class=\"fa fa-bars\"></i>
                </div>
                <ul class=\"inactive\">
                    <li><a class=\"active\" href=\"#welcome\">Bem vindo</a></li>
                    <li><a href=\"#about\">Sobre <span class=\"extrabold\">Nós</span></a></li>
                    ";
        // line 85
        echo "                    ";
        // line 86
        echo "                    ";
        // line 87
        echo "                    ";
        // line 88
        echo "                    <li><a href=\"#blog\">Nossas <span class=\"extrabold\">Notícias</span></a></li>
                    <li><a href=\"#contact\">Fale <span class=\"extrabold\">Conosco</span></a></li>
                </ul>
            </div><!-- end .mobile-nav-container -->
        </nav>
        <a href=\"#\" class=\"scrollup\">
            <i class=\"fa fa-angle-double-up\"></i>
        </a><!-- end .scrollup -->
    ";
    }

    // line 97
    public function block_nav($context, array $blocks = array())
    {
        // line 98
        echo "
    ";
    }

    // line 100
    public function block_body($context, array $blocks = array())
    {
        // line 101
        echo "
    ";
    }

    // line 145
    public function block_footer($context, array $blocks = array())
    {
        // line 146
        echo "        <footer id=\"footer\">
            <div class=\"footer-container\">
                <ul class=\"socials\">
                    <li class=\"facebook\"><a href=\"#\" class=\"circle-icon\"><i class=\"fa fa-facebook\"></i></a></li>
                    <li class=\"twitter\"><a href=\"#\" class=\"circle-icon\"><i class=\"fa fa-twitter\"></i></a></li>
                    <li class=\"flickr\"><a href=\"#\" class=\"circle-icon\"><i class=\"fa fa-flickr\"></i></a></li>
                    <li class=\"youtube\"><a href=\"#\" class=\"circle-icon\"><i class=\"fa fa-youtube\"></i></a></li>
                    <li class=\"rss\"><a href=\"#\" class=\"circle-icon\"><i class=\"fa fa-rss\"></i></a></li>
                    <li class=\"share-alt\"><a href=\"#\" class=\"circle-icon\"><i class=\"fa fa-share-alt\"></i></a></li>
                    <!--<li class=\"pinterest\"><a href=\"#\" class=\"circle-icon\"><i class=\"fa fa-pinterest\"></i></a></li>-->
                    <!--<li class=\"github\"><a href=\"#\" class=\"circle-icon\"><i class=\"fa fa-github\"></i></a></li>-->
                    <!--<li class=\"google-plus\"><a href=\"#\" class=\"circle-icon\"><i class=\"fa fa-google-plus\"></i></a></li>-->
                    <!--<li class=\"linkedin\"><a href=\"#\" class=\"circle-icon\"><i class=\"fa fa-linkedin\"></i></a></li>-->
                    <!--<li class=\"skype\"><a href=\"#\" class=\"circle-icon\"><i class=\"fa fa-skype\"></i></a></li>-->
                    <!--<li class=\"tumblr\"><a href=\"#\" class=\"circle-icon\"><i class=\"fa fa-tumblr\"></i></a></li>-->
                </ul>
                <h5 class=\"footer-copyright\">Frango Norte @ Todos os Direitos Reservados - <a href=\"http://betagt.com.br\">BetaGT Desenvolvimento de Software</a>.</h5>
            </div>
        </footer>
    ";
    }

    // line 166
    public function block_javascript($context, array $blocks = array())
    {
        // line 167
        echo "        <!-- Scripts -->
        <!--[if lt IE 9]>
        <script type=\"text/javascript\" src=\"";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/scripts/jquery-1.11.0.min.js"), "html", null, true);
        echo "?ver=1\"></script>
        <![endif]-->
        <!--[if (gte IE 9) | (!IE)]><!-->
        <script type=\"text/javascript\" src=\"";
        // line 172
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/scripts/jquery-2.1.0.min.js"), "html", null, true);
        echo "?ver=1\"></script>
        <!--<![endif]-->
        <script type=\"text/javascript\" src=\"";
        // line 174
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/scripts/jquery.easing.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/scripts/jquery.prettyPhoto.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/scripts/jquery.tools.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/scripts/owl.carousel.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/scripts/jquery.nav.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/scripts/jquery.scrollTo.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 180
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/scripts/jquery.sticky.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 181
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/scripts/jquery.appear.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/scripts/responsiveslides.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 183
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/scripts/verge.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 184
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("frangonorte/scripts/custom.js"), "html", null, true);
        echo "\"></script>

        <script src=\"";
        // line 186
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 187
        echo $this->env->getExtension('routing')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData"));
        echo "\"></script>
    ";
    }

    public function getTemplateName()
    {
        return "PortalBundle:Default:default.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  382 => 187,  378 => 186,  373 => 184,  369 => 183,  365 => 182,  361 => 181,  357 => 180,  353 => 179,  349 => 178,  345 => 177,  341 => 176,  337 => 175,  333 => 174,  328 => 172,  322 => 169,  318 => 167,  315 => 166,  292 => 146,  289 => 145,  284 => 101,  281 => 100,  276 => 98,  273 => 97,  261 => 88,  259 => 87,  257 => 86,  255 => 85,  239 => 70,  237 => 69,  235 => 68,  233 => 67,  222 => 57,  211 => 52,  206 => 50,  201 => 48,  196 => 47,  192 => 46,  185 => 42,  181 => 40,  178 => 39,  172 => 25,  168 => 24,  164 => 23,  160 => 22,  156 => 21,  152 => 20,  147 => 19,  144 => 18,  138 => 8,  133 => 189,  130 => 166,  128 => 145,  86 => 105,  84 => 104,  81 => 103,  78 => 100,  75 => 97,  73 => 39,  66 => 35,  62 => 34,  57 => 32,  50 => 27,  48 => 18,  35 => 8,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <!--[if IE 7 ]><html class="ie7" lang="en"><![endif]-->*/
/* <!--[if IE 8 ]><html class="ie8" lang="en"><![endif]-->*/
/* <!--[if IE 9 ]><html class="ie9" lang="en"><![endif]-->*/
/* <!--[if (gte IE 10)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" lang="en-US"><!--<![endif]-->*/
/* <head>*/
/* */
/*     <title>{% block titulo %}{{ titulo }}{% endblock %}</title>*/
/*     <meta name="format-detection" content="telephone=no">*/
/*     <meta http-equiv="Content-Type" content="text/html; charset=utf-8">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />*/
/* */
/*     <!-- Seo Meta -->*/
/*     <meta name="description" content="">*/
/*     <meta name="keywords" content="">*/
/* */
/*     <!-- Styles -->*/
/*     {% block styles %}*/
/*         <link rel="stylesheet" type="text/css" href="{{ asset('frangonorte/styles/font-awesome.min.css') }}" media="screen" />*/
/*         <link rel="stylesheet" type="text/css" href="{{ asset('frangonorte/styles/prettyPhoto.css') }}" media="screen" />*/
/*         <link rel="stylesheet" type="text/css" href="{{ asset('frangonorte/styles/bootstrap.min.css') }}" media="screen" />*/
/*         <link rel="stylesheet" type="text/css" href="{{ asset('frangonorte/styles/owl.carousel.css') }}" media="screen" />*/
/*         <link rel="stylesheet" type="text/css" href="{{ asset('frangonorte/styles/owl.theme.css') }}" media="screen" />*/
/*         <link rel="stylesheet" type="text/css" href="{{ asset('frangonorte/styles/animate.min.css') }}" media="screen" />*/
/*         <link rel="stylesheet" type="text/css" href="{{ asset('frangonorte/style.css') }}" media="screen" />*/
/*     {% endblock %}*/
/*     <!-- Fonts -->*/
/*     <link href='http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,300,200,100,500' rel='stylesheet' type='text/css'>*/
/*     <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800' rel='stylesheet' type='text/css'>*/
/* */
/*     <!-- Favicon -->*/
/*     <link rel="shortcut icon" type="image/x-icon" href="{{ asset('frangonorte/images/favicon.png') }}">*/
/* */
/*     <script type="text/javascript" src="{{ asset('js/bower_components/angular/angular.min.js') }}"></script>*/
/*     <script type="text/javascript" src="{{ asset('js/bower_components/ngMask/dist/ngMask.min.js') }}"></script>*/
/* </head>*/
/* */
/* <body>*/
/*     {% block header%}*/
/*         <header id="header">*/
/*             <div id="logo" class="text-center animated" data-animation="fadeInUp" data-animation-delay="400" >*/
/*                 <a href="index.html"><img src="{{ asset('frangonorte/images/frangonorte_logo.png') }}" alt="Logo"></a>*/
/*             </div><!-- end logo -->*/
/*             <div id="slideshow">*/
/*                 <ul class="rslides">*/
/*                     {% for item in receitas %}*/
/*                     <li style="background-image: url({{ asset('uploads/receitas/' ~ item.imageName) }});">*/
/*                         <img src="{{ asset('uploads/receitas/' ~ item.imageName) }}" alt="">*/
/*                         <div class="slideshow-caption">*/
/*                             <h1>{{ item.titulo }}</h1>*/
/*                             <div class="button">*/
/*                                 <a href="{{ path('front_receita', {'id': item.id, 'slug': item.slug }) }}">Confira a receita</a>*/
/*                             </div>*/
/*                         </div><!-- end .slideshow-caption -->*/
/*                     </li><!-- end .slideshow item -->*/
/*                     {% endfor %}*/
/*                 </ul>*/
/*             </div><!-- end slideshow -->*/
/*         </header>*/
/* */
/*         <nav id="navigation">*/
/*             <div class="nav-container">*/
/*                 <ul>*/
/*                     <li><a class="active" href="#welcome">Bem Vindo</a></li>*/
/*                     <li><a href="#about">Sobre <span class="extrabold">Nós</span></a></li>*/
/*                     {#<li><a href="#special-gallery">Daily <span class="extrabold">Specialties</span></a></li>#}*/
/*                     {#<li><a href="#gallery">Seafood <span class="extrabold">Dishes</span></a></li>#}*/
/*                     {#<li><a href="#big-menu">Our <span class="extrabold">Menu</span></a></li>#}*/
/*                     {#<li><a href="#cooks">Our <span class="extrabold">Cooks</span></a></li>#}*/
/*                     <li><a href="#blog">Nossas <span class="extrabold">Notícias</span></a></li>*/
/*                     <li><a href="#contact">Fale <span class="extrabold">Conosco</span></a></li>*/
/*                 </ul>*/
/*             </div><!-- end .nav-container -->*/
/*         </nav>*/
/* */
/*         <nav id="mobile-navigation">*/
/*             <div class="mobile-nav-container">*/
/*                 <div id="menu-toggle">*/
/*                     <i class="fa fa-bars"></i>*/
/*                 </div>*/
/*                 <ul class="inactive">*/
/*                     <li><a class="active" href="#welcome">Bem vindo</a></li>*/
/*                     <li><a href="#about">Sobre <span class="extrabold">Nós</span></a></li>*/
/*                     {#<li><a href="#special-gallery">Daily <span class="extrabold">Specialties</span></a></li>#}*/
/*                     {#<li><a href="#gallery">Seafood <span class="extrabold">Dishes</span></a></li>#}*/
/*                     {#<li><a href="#big-menu">Our <span class="extrabold">Menu</span></a></li>#}*/
/*                     {#<li><a href="#cooks">Our <span class="extrabold">Cooks</span></a></li>#}*/
/*                     <li><a href="#blog">Nossas <span class="extrabold">Notícias</span></a></li>*/
/*                     <li><a href="#contact">Fale <span class="extrabold">Conosco</span></a></li>*/
/*                 </ul>*/
/*             </div><!-- end .mobile-nav-container -->*/
/*         </nav>*/
/*         <a href="#" class="scrollup">*/
/*             <i class="fa fa-angle-double-up"></i>*/
/*         </a><!-- end .scrollup -->*/
/*     {% endblock %}*/
/*     {% block nav %}*/
/* */
/*     {% endblock %}*/
/*     {% block body %}*/
/* */
/*     {% endblock %}*/
/*     <!-- Contato -->*/
/*     {% include('@Portal/home/contato.html.twig') %}*/
/*     <!-- Contato -->*/
/*     <!-- Google Maps -->*/
/*     <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>*/
/*     <script>*/
/*         var latlng=new google.maps.LatLng(-10.25925553154425,-48.883763551712036);*/
/*         function initialize() {*/
/*             var mapProp = {*/
/*                 center:latlng,*/
/*                 scrollwheel: false,*/
/*                 zoom:15,*/
/*                 zoomControl:true,*/
/*                 mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},*/
/*                 navigationControl: true,*/
/*                 navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},*/
/*                 panControl:true,*/
/*                 //mapTypeId:google.maps.MapTypeId.ROADMAP*/
/*                 mapTypeId:google.maps.MapTypeId.HYBRID*/
/*             };*/
/*             var map=new google.maps.Map(document.getElementById("google-map-id"),mapProp);*/
/*             var marker=new google.maps.Marker({*/
/*                 position:latlng,*/
/*                 map: map,*/
/*                 title: 'Frango Norte'*/
/*             });*/
/*             marker.setMap(map);*/
/*             var infowindow = new google.maps.InfoWindow({*/
/*                 content:"Paraíso, Tocantins"*/
/*             });*/
/*             google.maps.event.addListener(marker, 'click', function() {*/
/*                 infowindow.open(map,marker);*/
/*             });*/
/*         }*/
/*         google.maps.event.addDomListener(window, 'load', initialize);*/
/*     </script>*/
/*     <section id="google-map">*/
/*         <div class="googlemap-container">*/
/*             <div id="google-map-id" class="google-map"></div>*/
/*         </div><!-- end .googlemap-container -->*/
/*     </section>*/
/*     <!-- Google Maps -->*/
/*     {% block footer %}*/
/*         <footer id="footer">*/
/*             <div class="footer-container">*/
/*                 <ul class="socials">*/
/*                     <li class="facebook"><a href="#" class="circle-icon"><i class="fa fa-facebook"></i></a></li>*/
/*                     <li class="twitter"><a href="#" class="circle-icon"><i class="fa fa-twitter"></i></a></li>*/
/*                     <li class="flickr"><a href="#" class="circle-icon"><i class="fa fa-flickr"></i></a></li>*/
/*                     <li class="youtube"><a href="#" class="circle-icon"><i class="fa fa-youtube"></i></a></li>*/
/*                     <li class="rss"><a href="#" class="circle-icon"><i class="fa fa-rss"></i></a></li>*/
/*                     <li class="share-alt"><a href="#" class="circle-icon"><i class="fa fa-share-alt"></i></a></li>*/
/*                     <!--<li class="pinterest"><a href="#" class="circle-icon"><i class="fa fa-pinterest"></i></a></li>-->*/
/*                     <!--<li class="github"><a href="#" class="circle-icon"><i class="fa fa-github"></i></a></li>-->*/
/*                     <!--<li class="google-plus"><a href="#" class="circle-icon"><i class="fa fa-google-plus"></i></a></li>-->*/
/*                     <!--<li class="linkedin"><a href="#" class="circle-icon"><i class="fa fa-linkedin"></i></a></li>-->*/
/*                     <!--<li class="skype"><a href="#" class="circle-icon"><i class="fa fa-skype"></i></a></li>-->*/
/*                     <!--<li class="tumblr"><a href="#" class="circle-icon"><i class="fa fa-tumblr"></i></a></li>-->*/
/*                 </ul>*/
/*                 <h5 class="footer-copyright">Frango Norte @ Todos os Direitos Reservados - <a href="http://betagt.com.br">BetaGT Desenvolvimento de Software</a>.</h5>*/
/*             </div>*/
/*         </footer>*/
/*     {% endblock %}*/
/*     {% block javascript %}*/
/*         <!-- Scripts -->*/
/*         <!--[if lt IE 9]>*/
/*         <script type="text/javascript" src="{{ asset('frangonorte/scripts/jquery-1.11.0.min.js') }}?ver=1"></script>*/
/*         <![endif]-->*/
/*         <!--[if (gte IE 9) | (!IE)]><!-->*/
/*         <script type="text/javascript" src="{{ asset('frangonorte/scripts/jquery-2.1.0.min.js') }}?ver=1"></script>*/
/*         <!--<![endif]-->*/
/*         <script type="text/javascript" src="{{ asset('frangonorte/scripts/jquery.easing.js') }}"></script>*/
/*         <script type="text/javascript" src="{{ asset('frangonorte/scripts/jquery.prettyPhoto.js') }}"></script>*/
/*         <script type="text/javascript" src="{{ asset('frangonorte/scripts/jquery.tools.min.js') }}"></script>*/
/*         <script type="text/javascript" src="{{ asset('frangonorte/scripts/owl.carousel.min.js') }}"></script>*/
/*         <script type="text/javascript" src="{{ asset('frangonorte/scripts/jquery.nav.js') }}"></script>*/
/*         <script type="text/javascript" src="{{ asset('frangonorte/scripts/jquery.scrollTo.js') }}"></script>*/
/*         <script type="text/javascript" src="{{ asset('frangonorte/scripts/jquery.sticky.js') }}"></script>*/
/*         <script type="text/javascript" src="{{ asset('frangonorte/scripts/jquery.appear.js') }}"></script>*/
/*         <script type="text/javascript" src="{{ asset('frangonorte/scripts/responsiveslides.min.js') }}"></script>*/
/*         <script type="text/javascript" src="{{ asset('frangonorte/scripts/verge.min.js') }}"></script>*/
/*         <script type="text/javascript" src="{{ asset('frangonorte/scripts/custom.js') }}"></script>*/
/* */
/*         <script src="{{ asset('bundles/fosjsrouting/js/router.js') }}"></script>*/
/*         <script src="{{ path('fos_js_routing_js', {'callback': 'fos.Router.setData'}) }}"></script>*/
/*     {% endblock %}*/
/* </body>*/
/* </html>*/

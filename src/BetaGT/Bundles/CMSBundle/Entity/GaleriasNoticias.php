<?php

namespace BetaGT\Bundles\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * GaleriasNoticias
 *
 * @ORM\Table(name="galerias_noticias", indexes={@ORM\Index(name="fk_galerias_solucoes_pagina_id_idx", columns={"noticia_id"}), @ORM\Index(name="fk_galerias_solucoes_galeria_id_idx", columns={"galeria_id"})})
 * @ORM\Entity
 */
class GaleriasNoticias extends EntityMaster
{
    /**
     * @var \Galerias
     *
     * @ORM\ManyToOne(targetEntity="Galerias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="galeria_id", referencedColumnName="id")
     * })
     */
    private $galeria;

    /**
     * @var \Noticias
     *
     * @ORM\ManyToOne(targetEntity="Noticias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="noticia_id", referencedColumnName="id")
     * })
     */
    private $noticia;

    /**
     * Set galeria
     *
     * @param \BetaGT\Bundles\CMSBundle\Entity\Galerias $galeria
     * @return GaleriasNoticias
     */
    public function setGaleria(\BetaGT\Bundles\CMSBundle\Entity\Galerias $galeria = null)
    {
        $this->galeria = $galeria;

        return $this;
    }

    /**
     * Get galeria
     *
     * @return \BetaGT\Bundles\CMSBundle\Entity\Galerias
     */
    public function getGaleria()
    {
        return $this->galeria;
    }

    /**
     * Set noticia
     *
     * @param \BetaGT\Bundles\CMSBundle\Entity\Noticias $noticia
     * @return GaleriasNoticias
     */
    public function setNoticia(\BetaGT\Bundles\CMSBundle\Entity\Noticias $noticia = null)
    {
        $this->noticia = $noticia;

        return $this;
    }

    /**
     * Get noticia
     *
     * @return \BetaGT\Bundles\CMSBundle\Entity\Noticias
     */
    public function getNoticia()
    {
        return $this->noticia;
    }
}

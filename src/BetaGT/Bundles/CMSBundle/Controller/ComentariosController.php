<?php

namespace BetaGT\Bundles\CMSBundle\Controller;

use BetaGT\Bundles\CMSBundle\Entity\Comentarios;
use BetaGT\Bundles\CMSBundle\Entity\Noticias;
use BetaGT\Bundles\CMSBundle\Form\ComentariosType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class ComentariosController extends MainController
{
    /**
     * @Route("/noticia/{idNoticia}/Comentarios/novo", name="cms_comentarios_novo")
     * @Template("CMSBundle:Comentarios:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction($idNoticia, Request $request)
    {
        $noticia = $this->checkParent($idNoticia, 'CMSBundle', 'Noticias', 'cms_noticias_listar', null);

        $entity = new Comentarios();

        $form = $this->createForm(new ComentariosType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setNoticia($noticia);

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_comentarios_novo'
                : 'cms_comentarios_listar';

            return $this->redirectToRoute($nextAction, array('idNoticia' => $idNoticia));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home'));
        $breadcrumbs->addItem('Notícias: Listar Registros', $this->get('router')->generate('cms_noticias_listar', array()));
        $breadcrumbs->addItem($noticia->getTitulo(), $this->get('router')->generate('cms_noticias_visualizar', array('id' => $idNoticia)));
        $breadcrumbs->addItem('Comentários: Listar Registros', $this->get('router')->generate('cms_comentarios_listar', array('idNoticia' => $idNoticia)));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Comentários', 'descricao' => ''),
        );
    }

    /**
     * @Route("/noticia/{idNoticia}/Comentarios/{id}/atualizar", name="cms_comentarios_atualizar")
     * @Template("CMSBundle:Comentarios:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($idNoticia, $id, Request $request)
    {
        $noticia = $this->checkParent($idNoticia, 'CMSBundle', 'Noticias', 'cms_noticias_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Comentarios')->find($id);
        if (!$entity instanceof Comentarios) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_comentarios_listar', array('idNoticia' => $idNoticia));
        }

        $form = $this->createForm(new ComentariosType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_comentarios_listar_novo'
                : 'cms_comentarios_listar_listar';

            return $this->redirectToRoute($nextAction, array('idNoticia' => $idNoticia));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Notícias: Listar Registros', $this->get('router')->generate('cms_noticias_listar', array()));
        $breadcrumbs->addItem($noticia->getTitulo(), $this->get('router')->generate('cms_noticias_visualizar', array('id' => $idNoticia)));
        $breadcrumbs->addItem('Comentários: Listar Registros', $this->get('router')->generate('cms_comentarios_listar', array('idNoticia' => $idNoticia)));
        $breadcrumbs->addItem('Visualizar', $this->get('router')->generate('cms_comentarios_listar_visualizar', array('idNoticia' => $idNoticia, 'id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Comentários', 'descricao' => ''),
        );
    }

    /**
     * @Route("/noticia/{idNoticia}/Comentarios/listar", name="cms_comentarios_listar")
     * @Template("CMSBundle:Comentarios:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction($idNoticia, Request $request)
    {
        $noticia = $this->checkParent($idNoticia, 'CMSBundle', 'Noticias', 'cms_noticias_listar', null);

        $repository = $this->getDoctrine()->getRepository('CMSBundle:Comentarios');
        $queryBuilder = $repository->createQueryBuilder('item')
            ->where('item.status = :status')
            ->andWhere('item.noticia = :noticia')
            ->setParameter('status', '1')
            ->setParameter('noticia', $idNoticia)
        ;

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('item.texto', array('label' => 'Comentário', 'filterable' => 'true', 'sortable' => true)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Notícias: Listar Registros', $this->get('router')->generate('cms_noticias_listar', array()));
        $breadcrumbs->addItem($noticia->getTitulo(), $this->get('router')->generate('cms_noticias_visualizar', array('id' => $idNoticia)));
        $breadcrumbs->addItem('Comentários: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('cms_comentarios_delete_selecionado', ['idNoticia' => $idNoticia]),
            'novo' => $this->generateUrl('cms_comentarios_novo', ['idNoticia' => $idNoticia]),
            'modulo' => array('titulo' => 'Comentários', 'descricao' => ''),
        );
    }

    /**
     * @Route("/noticia/{idNoticia}/Comentarios/{id}/visualizar", name="cms_comentarios_visualizar")
     * @Template("CMSBundle:Comentarios:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($idNoticia, $id)
    {
        $noticia = $this->checkParent($idNoticia, 'CMSBundle', 'Noticias', 'cms_noticias_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Comentarios')->find($id);
        if (!$entity instanceof Comentarios) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_comentarios_listar', array('idNoticia' => $idNoticia));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Notícias: Listar Registros', $this->get('router')->generate('cms_noticias_listar', array()));
        $breadcrumbs->addItem($noticia->getTitulo(), $this->get('router')->generate('cms_noticias_visualizar', array('id' => $idNoticia)));
        $breadcrumbs->addItem('Comentários: Listar Registros', $this->get('router')->generate('cms_comentarios_listar', array('idNoticia' => $idNoticia)));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar: ' . $noticia->getTitulo(),
            'entity' => $entity,
            'modulo' => array('titulo' => 'Comentários', 'descricao' => ''),
        );
    }

    /**
     * @Route("/noticia/{idNoticia}/Comentarios/{id}/delete", name="cms_comentarios_delete")
     * @Method("GET")
     */
    public function deleteAction($idNoticia, $id)
    {
        $this->checkParent($idNoticia, 'CMSBundle', 'Noticias', 'cms_noticias_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Comentarios')->find($id);
        if (!$entity instanceof Comentarios) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_comentarios_listar', array('idNoticia' => $idNoticia));
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('cms_comentarios_listar', array('idNoticia' => $idNoticia));
    }

    /**
     * @Route("/noticia/{idNoticia}/Comentarios/delete/selecionados", name="cms_comentarios_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction($idNoticia, Request $request)
    {
        $this->checkParent($idNoticia, 'CMSBundle', 'Noticias', 'cms_noticias_listar', null);

        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('cms_comentarios_listar', array('idNoticia' => $idNoticia));
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('CMSBundle:Comentarios')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('cms_comentarios_listar', array('idNoticia' => $idNoticia));
    }
}

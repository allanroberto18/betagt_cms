<?php

namespace BetaGT\Bundles\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EnquetesResultados
 *
 * @ORM\Table(name="enquetes_resultados", indexes={@ORM\Index(name="fk_enquete_resultado_enquete_idx", columns={"enquete_id"}), @ORM\Index(name="fk_enquete_resultado_item_idx", columns={"enquete_item_id"})})
 * @ORM\Entity
 */
class EnquetesResultados extends EntityMaster
{
    /**
     * @var \Enquetes
     *
     * @ORM\ManyToOne(targetEntity="Enquetes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="enquete_id", referencedColumnName="id")
     * })
     */
    private $enquete;

    /**
     * @var \EnquetesItens
     *
     * @ORM\ManyToOne(targetEntity="EnquetesItens")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="enquete_item_id", referencedColumnName="id")
     * })
     */
    private $enqueteItem;

    /**
     * Set enquete
     *
     * @param \BetaGT\Bundles\CMSBundle\Entity\Enquetes $enquete
     * @return EnquetesResultados
     */
    public function setEnquete(\BetaGT\Bundles\CMSBundle\Entity\Enquetes $enquete = null)
    {
        $this->enquete = $enquete;

        return $this;
    }

    /**
     * Get enquete
     *
     * @return \BetaGT\Bundles\CMSBundle\Entity\Enquetes
     */
    public function getEnquete()
    {
        return $this->enquete;
    }

    /**
     * Set enqueteItem
     *
     * @param \BetaGT\Bundles\CMSBundle\Entity\EnquetesItens $enqueteItem
     * @return EnquetesResultados
     */
    public function setEnqueteItem(\BetaGT\Bundles\CMSBundle\Entity\EnquetesItens $enqueteItem = null)
    {
        $this->enqueteItem = $enqueteItem;

        return $this;
    }

    /**
     * Get enqueteItem
     *
     * @return \BetaGT\Bundles\CMSBundle\Entity\EnquetesItens
     */
    public function getEnqueteItem()
    {
        return $this->enqueteItem;
    }
}

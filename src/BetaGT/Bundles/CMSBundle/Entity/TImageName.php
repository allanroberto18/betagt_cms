<?php
/**
 * Created by PhpStorm.
 * User: allan
 * Date: 11/09/2015
 * Time: 13:15
 */

namespace BetaGT\Bundles\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

trait TImageName
{

    /**
     * @var string
     *
     * @ORM\Column(name="image_name", type="string", length=255, nullable=true)
     */
    protected $imageName;

    /**
     * Set imageName
     *
     * @param string $imageName
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * Get imageName
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }
}
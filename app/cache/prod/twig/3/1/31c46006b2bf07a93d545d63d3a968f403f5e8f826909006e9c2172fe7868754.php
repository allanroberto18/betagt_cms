<?php

/* @CMS/Default/menu.html.twig */
class __TwigTemplate_6cba8a047770b438d305691f60469a219c9e19f7419966d28799e5bb7ae7a1c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'menu' => array($this, 'block_menu'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('menu', $context, $blocks);
    }

    public function block_menu($context, array $blocks = array())
    {
        // line 2
        echo "    <aside class=\"main-sidebar\">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class=\"sidebar\">
            <!-- Sidebar user panel -->
            <div class=\"user-panel\">
                <div class=\"pull-left image\">
                    ";
        // line 8
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "imageFile", array())) {
            // line 9
            echo "                    <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl(("uploads/galerias/img/" . $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "imageFile", array()))), "html", null, true);
            echo "\" class=\"img-circle\" alt=\"User Image\" />
                    ";
        } else {
            // line 11
            echo "                        <span class=\"fa fa-user fa-3x fa-inverse\"></span>
                    ";
        }
        // line 13
        echo "                </div>
                <div class=\"pull-left info\">
                    <p>";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
        echo "</p>
                    <a href=\"#\"><i class=\"fa fa-circle text-success\"></i> Online</a>
                </div>
            </div>
            <!-- search form -->
            ";
        // line 21
        echo "                ";
        // line 22
        echo "                    ";
        // line 23
        echo "                      ";
        // line 24
        echo "                          ";
        // line 25
        echo "                      ";
        // line 26
        echo "                ";
        // line 27
        echo "            ";
        // line 28
        echo "            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            ";
        // line 30
        echo $this->env->getExtension('knp_menu')->render("CMSBundle:Builder:mainMenu", array("currentClass" => "active", "ancestorClass" => "active", "firstClass" => "", "lastClass " => ""));
        // line 37
        echo "
        </section>
        <!-- /.sidebar -->
    </aside>
";
    }

    public function getTemplateName()
    {
        return "@CMS/Default/menu.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  78 => 37,  76 => 30,  72 => 28,  70 => 27,  68 => 26,  66 => 25,  64 => 24,  62 => 23,  60 => 22,  58 => 21,  50 => 15,  46 => 13,  42 => 11,  36 => 9,  34 => 8,  26 => 2,  20 => 1,);
    }
}
/* {% block menu %}*/
/*     <aside class="main-sidebar">*/
/*         <!-- sidebar: style can be found in sidebar.less -->*/
/*         <section class="sidebar">*/
/*             <!-- Sidebar user panel -->*/
/*             <div class="user-panel">*/
/*                 <div class="pull-left image">*/
/*                     {% if app.user.imageFile %}*/
/*                     <img src="{{ asset('uploads/galerias/img/' ~ app.user.imageFile) }}" class="img-circle" alt="User Image" />*/
/*                     {% else %}*/
/*                         <span class="fa fa-user fa-3x fa-inverse"></span>*/
/*                     {% endif %}*/
/*                 </div>*/
/*                 <div class="pull-left info">*/
/*                     <p>{{ app.user.username }}</p>*/
/*                     <a href="#"><i class="fa fa-circle text-success"></i> Online</a>*/
/*                 </div>*/
/*             </div>*/
/*             <!-- search form -->*/
/*             {#<form action="#" method="get" class="sidebar-form">#}*/
/*                 {#<div class="input-group">#}*/
/*                     {#<input type="text" name="q" class="form-control" placeholder="Pesquisar..." />#}*/
/*                       {#<span class="input-group-btn">#}*/
/*                           {#<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>#}*/
/*                       {#</span>#}*/
/*                 {#</div>#}*/
/*             {#</form>#}*/
/*             <!-- /.search form -->*/
/*             <!-- sidebar menu: : style can be found in sidebar.less -->*/
/*             {{*/
/*                 knp_menu_render('CMSBundle:Builder:mainMenu', {*/
/*                     'currentClass': 'active',*/
/*                     'ancestorClass' : 'active',*/
/*                     'firstClass' : '',*/
/*                     'lastClass ' : '',*/
/*                 })*/
/*             }}*/
/*         </section>*/
/*         <!-- /.sidebar -->*/
/*     </aside>*/
/* {% endblock %}*/

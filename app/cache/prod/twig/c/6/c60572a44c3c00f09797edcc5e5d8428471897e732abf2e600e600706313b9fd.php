<?php

/* CMSBundle:Default:form.html.twig */
class __TwigTemplate_1253886b6b8bc9128d07402af9ac648c756c64befe115d2ad839440445b4b05e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@CMS/Default/base.html.twig", "CMSBundle:Default:form.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@CMS/Default/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
    ";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'widget');
        echo "
    ";
        // line 5
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
";
    }

    // line 7
    public function block_javascripts($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/input-mask/jquery.inputmask.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/input-mask/jquery.inputmask.date.extensions.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
    <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/input-mask/jquery.inputmask.numeric.extensions.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
    <script src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/input-mask/jquery.inputmask.extensions.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
    <script type=\"text/javascript\">
        \$(document).ready(function(){
            \$(\".cpf\").inputmask(\"999.999.999-99\");
            \$(\".cnpj\").inputmask(\"99.999.999/9999-99\");
            \$(\".cep\").inputmask(\"99.999-999\");
            \$('.telefone').inputmask({mask: \"(99) 9999-9999[9]\", greedy: true});
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "CMSBundle:Default:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 12,  63 => 11,  59 => 10,  55 => 9,  50 => 8,  47 => 7,  41 => 5,  37 => 4,  32 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends('@CMS/Default/base.html.twig') %}*/
/* {% block body %}*/
/*     {{ form_start(form) }}*/
/*     {{ form_widget(form) }}*/
/*     {{ form_end(form) }}*/
/* {% endblock %}*/
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     <script src="{{ asset('admin/plugins/input-mask/jquery.inputmask.js') }}" type="text/javascript"></script>*/
/*     <script src="{{ asset('admin/plugins/input-mask/jquery.inputmask.date.extensions.js') }}" type="text/javascript"></script>*/
/*     <script src="{{ asset('admin/plugins/input-mask/jquery.inputmask.numeric.extensions.js') }}" type="text/javascript"></script>*/
/*     <script src="{{ asset('admin/plugins/input-mask/jquery.inputmask.extensions.js') }}" type="text/javascript"></script>*/
/*     <script type="text/javascript">*/
/*         $(document).ready(function(){*/
/*             $(".cpf").inputmask("999.999.999-99");*/
/*             $(".cnpj").inputmask("99.999.999/9999-99");*/
/*             $(".cep").inputmask("99.999-999");*/
/*             $('.telefone').inputmask({mask: "(99) 9999-9999[9]", greedy: true});*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/

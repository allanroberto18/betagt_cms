<?php

namespace BetaGT\Bundles\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EnquetesNoticias
 *
 * @ORM\Table(name="enquetes_noticias", indexes={@ORM\Index(name="fk_enquete_noticia_noticia_idx", columns={"noticia_id"}), @ORM\Index(name="fk_enqute_noticia_enquete_idx", columns={"enquete_id"})})
 * @ORM\Entity
 */
class EnquetesNoticias extends EntityMaster
{
    /**
     * @var \Noticias
     *
     * @ORM\ManyToOne(targetEntity="Noticias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="noticia_id", referencedColumnName="id")
     * })
     */
    private $noticia;

    /**
     * @var \Enquetes
     *
     * @ORM\ManyToOne(targetEntity="Enquetes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="enquete_id", referencedColumnName="id")
     * })
     */
    private $enquete;

    /**
     * Set noticia
     *
     * @param \BetaGT\Bundles\CMSBundle\Entity\Noticias $noticia
     * @return EnquetesNoticias
     */
    public function setNoticia(\BetaGT\Bundles\CMSBundle\Entity\Noticias $noticia = null)
    {
        $this->noticia = $noticia;

        return $this;
    }

    /**
     * Get noticia
     *
     * @return \BetaGT\Bundles\CMSBundle\Entity\Noticias
     */
    public function getNoticia()
    {
        return $this->noticia;
    }

    /**
     * Set enquete
     *
     * @param \BetaGT\Bundles\CMSBundle\Entity\Enquetes $enquete
     * @return EnquetesNoticias
     */
    public function setEnquete(\BetaGT\Bundles\CMSBundle\Entity\Enquetes $enquete = null)
    {
        $this->enquete = $enquete;

        return $this;
    }

    /**
     * Get enquete
     *
     * @return \BetaGT\Bundles\CMSBundle\Entity\Enquetes
     */
    public function getEnquete()
    {
        return $this->enquete;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: allan
 * Date: 08/11/2015
 * Time: 21:29
 */

namespace BetaGT\Bundles\PortalBundle\Utils;

use Doctrine\ORM\EntityManager;

class Banners
{
    private $entityManager;

    /**
     * ServiceMaster constructor.
     * @param $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array|\BetaGT\Bundles\CMSBundle\Entity\Galerias[]
     */
    public function returnAll()
    {
        return $this->entityManager->getRepository("CMSBundle:Banners")->findBy([ 'status' => 1 ]);
    }

    public function returnRegister($id)
    {
        return $this->entityManager->getRepository("CMSBundle:Banners")->find($id);
    }
}
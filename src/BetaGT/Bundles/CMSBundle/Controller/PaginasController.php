<?php

namespace BetaGT\Bundles\CMSBundle\Controller;

use BetaGT\Bundles\CMSBundle\Entity\Paginas;
use BetaGT\Bundles\CMSBundle\Form\PaginasType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class PaginasController extends MainController
{
    /**
     * @Route("/paginas/novo", name="cms_paginas_novo")
     * @Template("CMSBundle:Paginas:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction(Request $request)
    {
        $entity = new Paginas();

        $form = $this->createForm(new PaginasType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_paginas_novo'
                : 'cms_paginas_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home'));
        $breadcrumbs->addItem('Páginas: Listar Registros', $this->get('router')->generate('cms_paginas_listar'));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Paginas', 'descricao' => 'Conteúdo institucional'),
        );
    }

    /**
     * @Route("/paginas/{id}/atualizar", name="cms_paginas_atualizar")
     * @Template("CMSBundle:Paginas:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Paginas')->find($id);
        if (!$entity instanceof Paginas) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_paginas_listar', array());
        }

        $form = $this->createForm(new PaginasType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_paginas_novo'
                : 'cms_paginas_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Paginas: Listar Registros', $this->get('router')->generate('cms_paginas_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $entity->getTitulo(), $this->get('router')->generate('cms_paginas_visualizar', array('id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Paginas', 'descricao' => 'Conteúdo institucional'),
        );
    }

    /**
     * @Route("/paginas/listar", name="cms_paginas_listar")
     * @Template("CMSBundle:Paginas:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('CMSBundle:Paginas');
        $queryBuilder = $repository->createQueryBuilder('item')
            ->where('item.status = :status')
            ->setParameter('status', '1');

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('item.retranca', array('label' => 'Retranca', 'filterable' => 'true', 'sortable' => true)))
            ->addField(new Field('item.titulo', array('label' => 'Título', 'filterable' => 'true', 'sortable' => true)))
        ;

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Páginas: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('cms_paginas_delete_selecionado'),
            'novo' => $this->generateUrl('cms_paginas_novo'),
            'modulo' => array('titulo' => 'Páginas', 'descricao' => 'Conteúdo institucional'),
        );
    }

    /**
     * @Route("/paginas/{id}/visualizar", name="cms_paginas_visualizar")
     * @Template("CMSBundle:Paginas:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Paginas')->find($id);
        if (!$entity instanceof Paginas) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_paginas_listar', array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Listar Registros', $this->get('router')->generate('cms_paginas_listar', array()));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar: ' . $entity->getTitulo(),
            'entity' => $entity,
            'modulo' => array('titulo' => 'Paginas', 'descricao' => 'Conteúdo institucional'),
        );
    }

    /**
     * @Route("/paginas/{id}/delete", name="cms_paginas_delete")
     * @Method("GET")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Paginas')->find($id);
        if (!$entity instanceof Paginas) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_paginas_listar', array());
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('cms_paginas_listar', array());
    }

    /**
     * @Route("/paginas/delete/selecionados", name="cms_paginas_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction(Request $request)
    {
        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('cms_paginas_listar', array());
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('CMSBundle:Paginas')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('cms_paginas_listar', array());
    }
}

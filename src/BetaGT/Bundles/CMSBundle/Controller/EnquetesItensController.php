<?php

namespace BetaGT\Bundles\CMSBundle\Controller;

use BetaGT\Bundles\CMSBundle\Entity\Enquetes;
use BetaGT\Bundles\CMSBundle\Entity\EnquetesItens;
use BetaGT\Bundles\CMSBundle\Form\EnquetesItensType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class EnquetesItensController extends MainController
{
    /**
     * @Route("/enquetes/{idEnquete}/itens/novo", name="cms_enquetes_itens_novo")
     * @Template("CMSBundle:EnquetesItens:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction($idEnquete, Request $request)
    {
        $enquete = $this->checkParent($idEnquete, 'CMSBundle', 'Enquetes', 'cms_enquetes_listar', null);

        $entity = new EnquetesItens();

        $form = $this->createForm(new EnquetesItensType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setEnquete($enquete);

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_enquetes_itens_novo'
                : 'cms_enquetes_itens_listar';

            return $this->redirectToRoute($nextAction, array('idEnquete' => $idEnquete));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home'));
        $breadcrumbs->addItem('Enquetes: Listar Registros', $this->get('router')->generate('cms_enquetes_listar'));
        $breadcrumbs->addItem($enquete->getTitulo(), $this->get('router')->generate('cms_enquetes_visualizar', array('id' => $idEnquete)));
        $breadcrumbs->addItem('Ítens da Enquete: Listar Registros', $this->get('router')->generate('cms_enquetes_itens_listar', array('idEnquete' => $idEnquete)));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Ítens da Enquete', 'descricao' => ''),
        );
    }

    /**
     * @Route("/enquetes/{idEnquete}/itens/{id}/atualizar", name="cms_enquetes_itens_atualizar")
     * @Template("CMSBundle:EnquetesItens:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($idEnquete, $id, Request $request)
    {
        $enquete = $this->checkParent($idEnquete, 'CMSBundle', 'Enquetes', 'cms_enquetes_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:EnquetesItens')->find($id);
        if (!$entity instanceof EnquetesItens) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_enquetes_itens_listar', array('idEnquete' => $idEnquete));
        }

        $form = $this->createForm(new EnquetesItensType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_enquetes_itens_novo'
                : 'cms_enquetes_itens_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Enquetes: Listar Registros', $this->get('router')->generate('cms_enquetes_listar'));
        $breadcrumbs->addItem($enquete->getTitulo(), $this->get('router')->generate('cms_enquetes_visualizar', array('id' => $idEnquete)));
        $breadcrumbs->addItem('Ítens da Enquete: Listar Registros', $this->get('router')->generate('cms_enquetes_itens_listar', array('idEnquete' => $idEnquete)));
        $breadcrumbs->addItem('Visualizar: '. $entity->getTitulo(), $this->get('router')->generate('cms_enquetes_itens_listar', array('idEnquete' => $idEnquete, 'id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Ítens da Enquete', 'descricao' => ''),
        );
    }

    /**
     * @Route("/enquetes/{idEnquete}/itens/listar", name="cms_enquetes_itens_listar")
     * @Template("CMSBundle:EnquetesItens:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction($idEnquete, Request $request)
    {
        $enquete = $this->checkParent($idEnquete, 'CMSBundle', 'Enquetes', 'cms_enquetes_listar', null);

        $repository = $this->getDoctrine()->getRepository('CMSBundle:EnquetesItens');
        $queryBuilder = $repository->createQueryBuilder('item')
            ->where('item.status = :status')
            ->andWhere('item.enquete = :enquete')
            ->setParameter('status', '1')
            ->setParameter('enquete', $idEnquete)
        ;

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('item.titulo', array('label' => 'Título', 'filterable' => 'true', 'sortable' => true)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Enquetes: Listar Registros', $this->get('router')->generate('cms_enquetes_listar'));
        $breadcrumbs->addItem($enquete->getTitulo(), $this->get('router')->generate('cms_enquetes_visualizar', array('id' => $idEnquete)));
        $breadcrumbs->addItem('Ítens da Enquete: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('cms_enquetes_itens_delete_selecionado', ['idEnquete' => $idEnquete]),
            'novo' => $this->generateUrl('cms_enquetes_itens_novo', ['idEnquete' => $idEnquete]),
            'modulo' => array('titulo' => 'Ítens da Enquete', 'descricao' => ''),
        );
    }

    /**
     * @Route("/enquetes/{idEnquete}/itens/{id}/visualizar", name="cms_enquetes_itens_visualizar")
     * @Template("CMSBundle:EnquetesItens:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($idEnquete, $id)
    {
        $enquete = $this->checkParent($idEnquete, 'CMSBundle', 'Enquetes', 'cms_enquetes_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:EnquetesItens')->find($id);
        if (!$entity instanceof EnquetesItens) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_enquetes_itens_listar', array('idEnquete' => $idEnquete));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Enquetes: Listar Registros', $this->get('router')->generate('cms_enquetes_listar'));
        $breadcrumbs->addItem($enquete->getTitulo(), $this->get('router')->generate('cms_enquetes_visualizar', array('id' => $idEnquete)));
        $breadcrumbs->addItem('Ítens da Enquete: Listar Registros', $this->get('router')->generate('cms_enquetes_itens_listar', array('idEnquete' => $idEnquete)));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar: ' . $entity->getTitulo(),
            'entity' => $entity,
            'modulo' => array('titulo' => 'Ítens da Enquete', 'descricao' => ''),
        );
    }

    /**
     * @Route("/enquetes/{idEnquete}/itens/{id}/delete", name="cms_enquetes_itens_delete")
     * @Method("GET")
     */
    public function deleteAction($idEnquete, $id)
    {
        $this->checkParent($idEnquete, 'CMSBundle', 'Enquetes', 'cms_enquetes_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:EnquetesItens')->find($id);
        if (!$entity instanceof EnquetesItens) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_enquetes_itens_listar', array('idEnquete' => $idEnquete));
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('cms_enquetes_itens_listar', array('idEnquete' => $idEnquete));
    }

    /**
     * @Route("/enquetes/{idEnquete}/itens/delete/selecionados", name="cms_enquetes_itens_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction($idEnquete, Request $request)
    {
        $this->checkParent($idEnquete, 'CMSBundle', 'Enquetes', 'cms_enquetes_listar', null);

        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('cms_enquetes_itens_listar', array('idEnquete' => $idEnquete));
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('CMSBundle:EnquetesItens')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('cms_enquetes_itens_listar', array('idEnquete' => $idEnquete));
    }
}

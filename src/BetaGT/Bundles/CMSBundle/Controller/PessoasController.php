<?php

namespace BetaGT\Bundles\CMSBundle\Controller;

use BetaGT\Bundles\CMSBundle\Entity\Pessoas;
use BetaGT\Bundles\CMSBundle\Form\PessoasFisicaType;
use BetaGT\Bundles\CMSBundle\Form\PessoasFotoType;
use BetaGT\Bundles\CMSBundle\Form\PessoasPerfilType;
use BetaGT\Bundles\CMSBundle\Form\PessoasType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class PessoasController extends MainController
{
    /**
     * @Route("/pessoas/{id}/atualizar/tipo/{tipo}", name="cms_pessoas_atualizar")
     * @Template("CMSBundle:Pessoas:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($id, $tipo, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Pessoas')->find($id);
        if (!$entity instanceof Pessoas) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_pessoas_listar', array());
        }
        if ($tipo == 1)
        {
            $form = $this->createForm(new PessoasType(), $entity, [ 'validation_groups' => ['juridica'], 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        } else {
            $form = $this->createForm(new PessoasFisicaType(), $entity, [ 'validation_groups' => ['fisica'], 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        }
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'fos_user_registration_register'
                : 'cms_pessoas_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Pessoas: Listar Registros', $this->get('router')->generate('cms_pessoas_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $entity->getUsername(), $this->get('router')->generate('cms_pessoas_visualizar', array('id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Pessoas', 'descricao' => 'Cadastro de Usuários'),
        );
    }

    /**
     * @Route("/pessoas/{id}/atualizar/foto", name="cms_pessoas_atualizar_foto")
     * @Template("CMSBundle:Pessoas:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateFotoAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Pessoas')->find($id);
        if (!$entity instanceof Pessoas) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_pessoas_listar', array());
        }
        $form = $this->createForm(new PessoasFotoType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'fos_user_registration_register'
                : 'cms_pessoas_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Pessoas: Listar Registros', $this->get('router')->generate('cms_pessoas_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $entity->getUsername(), $this->get('router')->generate('cms_pessoas_visualizar', array('id' => $id)));
        $breadcrumbs->addItem('Atualizar Foto');

        return array(
            'titulo' => 'Alterar Foto',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Pessoas', 'descricao' => 'Cadastro de Usuários'),
        );
    }

    /**
     * @Route("/pessoas/{id}/atualizar/perfil", name="cms_pessoas_atualizar_perfil")
     * @Template("CMSBundle:Pessoas:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updatePerfilAction($id,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Pessoas')->find($id);
        if (!$entity instanceof Pessoas) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_pessoas_listar', array());
        }
        $form = $this->createForm(new PessoasPerfilType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'fos_user_registration_register'
                : 'cms_pessoas_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Pessoas: Listar Registros', $this->get('router')->generate('cms_pessoas_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $entity->getUsername(), $this->get('router')->generate('cms_pessoas_visualizar', array('id' => $id)));
        $breadcrumbs->addItem('Liberar Acessos');

        return array(
            'titulo' => 'Alterar Foto',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Pessoas', 'descricao' => 'Cadastro de Usuários'),
        );
    }

    /**
     * @Route("/pessoas/listar", name="cms_pessoas_listar")
     * @Template("CMSBundle:Pessoas:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('CMSBundle:Pessoas');
        $queryBuilder = $repository->createQueryBuilder('item');

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('item.username', array('label' => 'Nome', 'filterable' => 'true', 'sortable' => true)))
            ->addField(new Field('item.email', array('label' => 'Nome', 'filterable' => 'true', 'sortable' => true)))
        ;

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Pessoas: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('cms_pessoas_delete_selecionado'),
            'novo' => $this->generateUrl('fos_user_registration_register'),
            'modulo' => array('titulo' => 'Pessoas', 'descricao' => 'Cadastro de Usuários'),
        );
    }

    /**
     * @Route("/pessoas/{id}/visualizar", name="cms_pessoas_visualizar")
     * @Template("CMSBundle:Pessoas:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Pessoas')->find($id);
        if (!$entity instanceof Pessoas) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_pessoas_listar', array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Listar Registros', $this->get('router')->generate('cms_pessoas_listar', array()));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar: ' . $entity->getUsername(),
            'entity' => $entity,
            'modulo' => array('titulo' => 'Pessoas', 'descricao' => 'Cadastro de Usuários'),
        );
    }

    /**
     * @Route("/pessoas/{id}/delete", name="cms_pessoas_delete")
     * @Method("GET")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Pessoas')->find($id);
        if (!$entity instanceof Pessoas) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_pessoas_listar', array());
        }
        $entity->setEnabled(0);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('cms_pessoas_listar', array());
    }

    /**
     * @Route("/pessoas/delete/selecionados", name="cms_pessoas_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction(Request $request)
    {
        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('cms_pessoas_listar', array());
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('CMSBundle:Pessoas')->find($data['check'][$i]);
            $entity->setEnabled(0);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('cms_pessoas_listar', array());
    }
}

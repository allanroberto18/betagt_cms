<?php

namespace BetaGT\Bundles\FinanceiroBundle\Controller;

use BetaGT\Bundles\CMSBundle\Controller\MainController;

use BetaGT\Bundles\CMSBundle\Entity\Pessoas;
use BetaGT\Bundles\FinanceiroBundle\Entity\Contratos;
use BetaGT\Bundles\FinanceiroBundle\Form\ContratosType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class ContratosController extends MainController
{
    /**
     * @Route("/pessoas/{idPessoa}/contratos/novo", name="fin_contratos_novo")
     * @Template("FinanceiroBundle:Contratos:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction($idPessoa, Request $request)
    {
        $pessoa = $this->checkParent($idPessoa, 'CMSBundle', 'Pessoas', 'cms_pessoas_listar', null);

        $entity = new Contratos();

        $form = $this->createForm(new ContratosType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setPessoa($pessoa);

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'fin_contratos_novo'
                : 'fin_contratos_listar';

            return $this->redirectToRoute($nextAction, array('idPessoa' => $idPessoa));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home'));
        $breadcrumbs->addItem('Pessoas: Listar Registros', $this->get('router')->generate('cms_pessoas_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $pessoa->getUsername(), $this->get('router')->generate('cms_pessoas_visualizar', array('id' => $idPessoa)));
        $breadcrumbs->addItem('Contratos: Listar Registros', $this->get('router')->generate('fin_contratos_listar', array('idPessoa' => $idPessoa)));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Contratos', 'descricao' => 'Prestação de Serviços'),
        );
    }

    /**
     * @Route("/pessoas/{idPessoa}/contratos/{id}/atualizar", name="fin_contratos_atualizar")
     * @Template("CMSBundle:Contratos:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($idPessoa, $id, Request $request)
    {
        $pessoa = $this->checkParent($idPessoa, 'CMSBundle', 'Pessoas', 'cms_pessoas_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('FinanceiroBundle:Contratos')->find($id);
        if (!$entity instanceof Contratos) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('fin_contratos_listar', array('idPessoa' => $idPessoa));
        }

        $form = $this->createForm(new ContratosType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'fin_contratos_novo'
                : 'fin_contratos_listar';

            return $this->redirectToRoute($nextAction, array('idPessoa' => $idPessoa));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Pessoas: Listar Registros', $this->get('router')->generate('cms_pessoas_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $pessoa->getUsername(), $this->get('router')->generate('cms_pessoas_visualizar', array('id' => $idPessoa)));
        $breadcrumbs->addItem('Contratos: Listar Registros', $this->get('router')->generate('fin_contratos_listar', array('idPessoa' => $idPessoa)));
        $breadcrumbs->addItem('Visualizar: ' . $entity->getTitulo(), $this->get('router')->generate('fin_contratos_visualizar', array('idPessoa' => $idPessoa, 'id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Contratos', 'descricao' => 'Prestação de Serviços'),
        );
    }

    /**
     * @Route("/pessoas/{idPessoa}/contratos/listar", name="fin_contratos_listar")
     * @Template("CMSBundle:Contratos:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction($idPessoa, Request $request)
    {
        $pessoa = $this->checkParent($idPessoa, 'CMSBundle', 'Pessoas', 'cms_pessoas_listar', null);

        $repository = $this->getDoctrine()->getRepository('FinanceiroBundle:Contratos');
        $queryBuilder = $repository->createQueryBuilder()
            ->select('item, plano')
            ->from('FinanceiroBundle:Contratos', 'item')
            ->leftJoin('item.plano', 'plano')
            ->where('item.status = :status')
            ->andWhere('item.pessoaId = :pessoa')
            ->setParameter('status', '1')
            ->setParameter('pessoa', $idPessoa)
        ;

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('plano.nome', array('label' => 'Serviço Contratado', 'filterable' => 'true', 'sortable' => true)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Pessoas: Listar Registros', $this->get('router')->generate('cms_pessoas_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $pessoa->getUsername(), $this->get('router')->generate('cms_pessoas_visualizar', array('id' => $idPessoa)));
        $breadcrumbs->addItem('Contratos: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('fin_contratos_delete_selecionado', ['idPessoa' => $idPessoa]),
            'novo' => $this->generateUrl('fin_contratos_novo', ['idPessoa' => $idPessoa]),
            'modulo' => array('titulo' => 'Contratos', 'descricao' => 'Prestação de Serviços'),
        );
    }

    /**
     * @Route("/pessoas/{idPessoa}/contratos/{id}/visualizar", name="fin_contratos_visualizar")
     * @Template("CMSBundle:Contratos:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($idPessoa, $id)
    {
        $pessoa = $this->checkParent($idPessoa, 'CMSBundle', 'Pessoas', 'cms_pessoas_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('FinanceiroBundle:Contratos')->find($id);
        if (!$entity instanceof Contratos) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('fin_contratos_listar', array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Pessoas: Listar Registros', $this->get('router')->generate('cms_pessoas_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $pessoa->getUsername(), $this->get('router')->generate('cms_pessoas_visualizar', array('id' => $idPessoa)));
        $breadcrumbs->addItem('Contratos: Listar Registros', $this->get('router')->generate('fin_contratos_listar', array('idPessoa' => $idPessoa)));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar: ' . $entity->getPlano()->getNome(),
            'entity' => $entity,
            'modulo' => array('titulo' => 'Contratos', 'descricao' => 'Prestação de Serviço'),
        );
    }

    /**
     * @Route("/pessoas/{idPessoa}/contratos/{id}/delete", name="fin_contratos_delete")
     * @Method("GET")
     */
    public function deleteAction($idPessoa, $id)
    {
        $pessoa = $this->checkParent($idPessoa, 'CMSBundle', 'Pessoas', 'cms_pessoas_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('FinanceiroBundle:Contratos')->find($id);
        if (!$entity instanceof Contratos) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('fin_contratos_listar', array('idPessoa' => $idPessoa));
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('fin_contratos_listar', array('idPessoa' => $idPessoa));
    }

    /**
     * @Route("/pessoas/{idPessoa}/contratos/delete/selecionados", name="fin_contratos_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction($idPessoa, Request $request)
    {
        $pessoa = $this->checkParent($idPessoa, 'CMSBundle', 'Pessoas', 'cms_pessoas_listar', null);

        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('fin_contratos_listar', array('idPessoa' => $idPessoa));
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('FinanceiroBundle:Contratos')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('fin_contratos_listar', array('idPessoa' => $idPessoa));
    }
}

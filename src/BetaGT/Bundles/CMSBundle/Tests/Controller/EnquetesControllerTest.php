<?php

namespace BetaGT\Bundles\CMSBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EnquetesControllerTest extends WebTestCase
{
    public function testNovo()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Enquetes/novo');
    }

    public function testUpdate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Enquetes/{id}/atualizar');
    }

    public function testList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Enquetes/listar');
    }

    public function testView()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Enquetes/{id}/visualizar');
    }

    public function testDelete()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Enquetes/{id}/delete');
    }

}

<?php

namespace BetaGT\Bundles\ProdutosBundle\Entity;

use BetaGT\Bundles\CMSBundle\Entity\EntityMaster;
use BetaGT\Bundles\CMSBundle\Entity\TImageName;
use BetaGT\Bundles\CMSBundle\Entity\TSlug;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Produtos
 * @Vich\Uploadable
 * @ORM\Table(name="produtos", indexes={@ORM\Index(name="fk_produto_categoria_idx", columns={"categoria_produto_id"})})
 * @ORM\Entity
 */
class Produtos extends EntityMaster
{
    use TSlug, TImageName;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Título é obrigatório")
     * @ORM\Column(name="titulo", type="string", length=250, nullable=false)
     */
    private $titulo;
    
    /**
     * @var string
     * @Assert\NotBlank(message="O campo Resumo é obrigatório")
     * @ORM\Column(name="resumo", type="string", length=500, nullable=true)
     */
    private $resumo;
    
    /**
     * @var string
     * @Assert\NotBlank(message="O campo Texto é obrigatório")
     * @ORM\Column(name="texto", type="text", nullable=true)
     */
    private $texto;

    /**
     * @var File
     * @Vich\UploadableField(mapping="produto_image", fileNameProperty="imageName")
     */
    private $imageFile;

    /**
     * @var \CategoriasProdutos
     *
     * @ORM\ManyToOne(targetEntity="CategoriasProdutos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoria_produto_id", referencedColumnName="id")
     * })
     */
    private $categoriaProduto;
    
    /**
     * @var \Receitas
     * @ORM\OneToMany(targetEntity="Receitas", mappedBy="produto", fetch="EXTRA_LAZY")
     */
    private $receitas;

    function __construct()
    {
    	$this->receitas = ArrayCollection();
    }
    
    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Produtos
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set resumo
     *
     * @param string $resumo
     * @return Produtos
     */
    public function setResumo($resumo)
    {
    	$this->resumo = $resumo;
    
    	return $this;
    }
    
    /**
     * Get resumo
     *
     * @return string
     */
    public function getResumo()
    {
    	return $this->resumo;
    }
    
    /**
     * Set texto
     *
     * @param string $texto
     * @return Produtos
     */
    public function setTexto($texto)
    {
    	$this->texto = $texto;
    
    	return $this;
    }
    
    /**
     * Get texto
     *
     * @return string
     */
    public function getTexto()
    {
    	return $this->texto;
    }
    
    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updated = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set categoriaProduto
     *
     * @param \BetaGT\Bundles\ProdutosBundle\Entity\CategoriasProdutos $categoriaProduto
     * @return Produtos
     */
    public function setCategoriaProduto(\BetaGT\Bundles\ProdutosBundle\Entity\CategoriasProdutos $categoriaProduto = null)
    {
        $this->categoriaProduto = $categoriaProduto;

        return $this;
    }

    /**
     * Get categoriaProduto
     *
     * @return \BetaGT\Bundles\ProdutosBundle\Entity\CategoriasProdutos
     */
    public function getCategoriaProduto()
    {
        return $this->categoriaProduto;
    }
    
	public function getReceitas() {
		return $this->receitas;
	}
	
	public function setReceitas(\BetaGT\Bundles\ProdutosBundle\Entity\Receitas $receitas = null) {
		$this->receitas = $receitas;
		return $this;
	}
	
    
    
}

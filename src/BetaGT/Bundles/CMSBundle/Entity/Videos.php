<?php

namespace BetaGT\Bundles\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Videos
 * @ORM\Table(name="videos")
 * @ORM\Entity
 */
class Videos extends EntityMaster
{
    use TSlug;
    /**
     * @var string
     *
     * @ORM\Column(name="retranca", type="string", length=75, nullable=true)
     */
    private $retranca;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Título é obrigatório")
     * @ORM\Column(name="titulo", type="string", length=255, nullable=true)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="resumo", type="string", length=500, nullable=true)
     */
    private $resumo;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Link é obrigatório")
     * @ORM\Column(name="link", type="string", nullable=true)
     */
    private $link;

    /**
     * Set retranca
     *
     * @param string $retranca
     * @return Videos
     */
    public function setRetranca($retranca)
    {
        $this->retranca = $retranca;

        return $this;
    }

    /**
     * Get retranca
     *
     * @return string 
     */
    public function getRetranca()
    {
        return $this->retranca;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Videos
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set resumo
     *
     * @param string $resumo
     * @return Videos
     */
    public function setResumo($resumo)
    {
        $this->resumo = $resumo;

        return $this;
    }

    /**
     * Get resumo
     *
     * @return string 
     */
    public function getResumo()
    {
        return $this->resumo;
    }

    /**
     * Set link
     *
     * @param string $texto
     * @return Videos
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }
}

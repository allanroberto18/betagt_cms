<?php

namespace BetaGT\Bundles\CMSBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ComentariosControllerTest extends WebTestCase
{
    public function testNovo()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Noticias/{id}/Comentarios/novo');
    }

    public function testUpdate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Noticias/{id}/Comentarios/{idComentario}/atualizar');
    }

    public function testList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Noticias/{id}/Comentarios/listar');
    }

    public function testView()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Noticias/{id}/Comentarios/{idComentario}/visualizar');
    }

    public function testDelete()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Noticias/{id}/Comentarios/{idComentario}/delete');
    }

}

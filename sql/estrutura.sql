-- MySQL dump 10.13  Distrib 5.6.26, for Win32 (x86)
--
-- Host: localhost    Database: betagt_portal
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `areas_publicacoes`
--

DROP TABLE IF EXISTS `areas_publicacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areas_publicacoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `areas_publicacoes`
--

LOCK TABLES `areas_publicacoes` WRITE;
/*!40000 ALTER TABLE `areas_publicacoes` DISABLE KEYS */;
INSERT INTO `areas_publicacoes` VALUES (1,'Destaque Principal',3,'2015-11-02 04:22:22','2015-11-02 04:22:22',1),(2,'Sem Destaque',0,'2015-11-02 04:22:43','2015-11-02 04:22:43',1);
/*!40000 ALTER TABLE `areas_publicacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assuntos`
--

DROP TABLE IF EXISTS `assuntos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assuntos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assuntos`
--

LOCK TABLES `assuntos` WRITE;
/*!40000 ALTER TABLE `assuntos` DISABLE KEYS */;
INSERT INTO `assuntos` VALUES (1,'Departamento Comercial','comercial@gmail.com','2015-09-17 11:17:42','2015-09-17 11:17:42',1),(2,'Departamento Financeiro','finaceiro@gmail.com','2015-09-17 11:20:03','2015-09-17 11:20:03',1),(3,'Departamento de Marketing','marketing@gmail.com','2015-09-17 11:23:56','2015-09-17 11:23:56',1);
/*!40000 ALTER TABLE `assuntos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `data_final` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,'Banner teste 1','563711b228bbc.png','#','2015-10-31','2015-10-27 20:38:51','2015-11-02 04:33:06',1),(2,'Banner teste 12','563711b98829c.png','#','2015-10-31','2015-10-27 20:39:14','2015-11-02 04:33:13',1);
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorias`
--

DROP TABLE IF EXISTS `categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorias`
--

LOCK TABLES `categorias` WRITE;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` VALUES (1,'Notícias','noticias','55faf6b5e80be.png','2015-09-17 14:21:57','2015-09-17 14:21:57',1);
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorias_produtos`
--

DROP TABLE IF EXISTS `categorias_produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorias_produtos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(250) NOT NULL,
  `titulo` varchar(250) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorias_produtos`
--

LOCK TABLES `categorias_produtos` WRITE;
/*!40000 ALTER TABLE `categorias_produtos` DISABLE KEYS */;
INSERT INTO `categorias_produtos` VALUES (1,'cortes_congelados','Cortes Congelados','2015-09-28 07:37:55','2015-09-28 07:37:55',1);
/*!40000 ALTER TABLE `categorias_produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comentarios`
--

DROP TABLE IF EXISTS `comentarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comentarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `noticia_id` int(11) DEFAULT NULL,
  `nome` longtext,
  `email` longtext,
  `texto` text,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_comentario_noticia_idx` (`noticia_id`),
  CONSTRAINT `FK_F54B3FC099926010` FOREIGN KEY (`noticia_id`) REFERENCES `noticias` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comentarios`
--

LOCK TABLES `comentarios` WRITE;
/*!40000 ALTER TABLE `comentarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `comentarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos`
--

DROP TABLE IF EXISTS `contatos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assunto_id` int(11) DEFAULT NULL,
  `texto` text,
  `nome` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_contatos_assunto_id_idx` (`assunto_id`),
  CONSTRAINT `FK_831F6B1C4CE74285` FOREIGN KEY (`assunto_id`) REFERENCES `assuntos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos`
--

LOCK TABLES `contatos` WRITE;
/*!40000 ALTER TABLE `contatos` DISABLE KEYS */;
INSERT INTO `contatos` VALUES (1,1,'<p>Testando a primeira mensagem</p>','Allan Roberto','allanroberto18@gmail.com','(63) 8465-0300','2015-09-17 14:10:17','2015-09-17 14:10:17',1),(2,1,'Teste','Allan Roberto','allanroberto18@gmail.com','(63)8465-0300','2015-10-08 20:11:56','2015-10-08 20:11:56',1),(3,1,'Teste','Allan Roberto','allanroberto18@gmail.com','(63)8465-0300','2015-10-08 20:16:13','2015-10-08 20:16:13',1),(4,1,'Teste','Allan Roberto','allanroberto18@gmail.com','(63)8465-0300','2015-10-08 20:16:42','2015-10-08 20:16:42',1),(5,1,'Teste','Allan Roberto','allanroberto18@gmail.com','(63)8465-0300','2015-10-08 20:28:01','2015-10-08 20:28:01',1),(6,3,'Teste','Allan Roberto','allanroberto18@gmail.com','(63)8465-0300','2015-10-08 20:30:44','2015-10-08 20:30:44',1),(7,1,'Teste','Allan Roberto','allanroberto18@gmail.com','(63)8465-0300','2015-10-08 20:35:06','2015-10-08 20:35:06',1),(8,1,'Teste','Allan Roberto','allanroberto18@gmail.com','(63)8465-0300','2015-10-08 20:42:23','2015-10-08 20:42:23',1),(9,2,'Teste','Allan Roberto','allanroberto18@gmail.com','(63)8465-0300','2015-10-08 21:03:11','2015-10-08 21:03:11',1),(10,3,'Teste','Allan Roberto','allanroberto18@gmail.com','(63)8465-0300','2015-10-08 21:04:09','2015-10-08 21:04:09',1),(11,1,'e\\asd\\adasd','Allan Roberto','allanroberto18@gmail.com','(63)8465-0300','2015-10-08 21:07:24','2015-10-08 21:07:24',1),(12,1,'Teste','Allan Roberto','allanroberto18@gmail.com','(63)8465-0300','2015-10-08 21:08:57','2015-10-08 21:08:57',1),(13,1,'Teste','Allan Roberto','allanroberto18@gmail.com','(63)8465-0300','2015-10-08 21:13:15','2015-10-08 21:13:15',1),(14,1,'Teste','Allan Roberto','allanroberto18@gmail.com','(55)6384-6503','2015-10-08 21:14:34','2015-10-08 21:14:34',1),(15,1,'Teste','Allan Roberto','allanroberto18@gmail.com','(+5)56384-6503','2015-10-08 21:33:06','2015-10-08 21:33:06',1),(16,1,'Teste','Allan Roberto','allanroberto18@gmail.com','(63)8465-0300','2015-10-08 22:23:51','2015-10-08 22:23:51',1),(17,1,'Teste','Allan Roberto','allanroberto18@gmail.com','(55)6384-6503','2015-10-08 22:25:00','2015-10-08 22:25:00',1),(18,1,'Teste','Allan Roberto','allanroberto18@gmail.com','(63)8465-0300','2015-10-08 22:28:56','2015-10-08 22:28:56',1),(19,1,'Teste','Allan Roberto','allanroberto18@gmail.com','(63)8465-0300','2015-10-08 22:29:59','2015-10-08 22:29:59',1);
/*!40000 ALTER TABLE `contatos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contratos`
--

DROP TABLE IF EXISTS `contratos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contratos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pessoa_id` int(11) DEFAULT NULL,
  `plano_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_contratos_clientes_idx` (`pessoa_id`),
  KEY `fk_contratos_planos_idx` (`plano_id`),
  CONSTRAINT `FK_B90FD71C9A8B86CC` FOREIGN KEY (`plano_id`) REFERENCES `planos` (`id`),
  CONSTRAINT `FK_B90FD71CDE734E51` FOREIGN KEY (`pessoa_id`) REFERENCES `pessoas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contratos`
--

LOCK TABLES `contratos` WRITE;
/*!40000 ALTER TABLE `contratos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contratos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresas`
--

DROP TABLE IF EXISTS `empresas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razao_social` varchar(255) DEFAULT NULL,
  `nome_fantasia` varchar(255) DEFAULT NULL,
  `cnpj` varchar(18) DEFAULT NULL,
  `inscricao_estadual` varchar(45) DEFAULT NULL,
  `inscricao_municiapl` varchar(45) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresas`
--

LOCK TABLES `empresas` WRITE;
/*!40000 ALTER TABLE `empresas` DISABLE KEYS */;
/*!40000 ALTER TABLE `empresas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enquetes`
--

DROP TABLE IF EXISTS `enquetes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enquetes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `inicio` date DEFAULT NULL,
  `fim` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enquetes`
--

LOCK TABLES `enquetes` WRITE;
/*!40000 ALTER TABLE `enquetes` DISABLE KEYS */;
/*!40000 ALTER TABLE `enquetes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enquetes_itens`
--

DROP TABLE IF EXISTS `enquetes_itens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enquetes_itens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enquete_id` int(11) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_enquete_item_enquete_idx` (`enquete_id`),
  CONSTRAINT `fk_enquete_item_enquete` FOREIGN KEY (`enquete_id`) REFERENCES `enquetes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enquetes_itens`
--

LOCK TABLES `enquetes_itens` WRITE;
/*!40000 ALTER TABLE `enquetes_itens` DISABLE KEYS */;
/*!40000 ALTER TABLE `enquetes_itens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enquetes_noticias`
--

DROP TABLE IF EXISTS `enquetes_noticias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enquetes_noticias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `noticia_id` int(11) DEFAULT NULL,
  `enquete_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_enquete_noticia_noticia_idx` (`noticia_id`),
  KEY `fk_enqute_noticia_enquete_idx` (`enquete_id`),
  CONSTRAINT `fk_enquete_noticia_noticia` FOREIGN KEY (`noticia_id`) REFERENCES `noticias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_enqute_noticia_enquete` FOREIGN KEY (`enquete_id`) REFERENCES `enquetes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enquetes_noticias`
--

LOCK TABLES `enquetes_noticias` WRITE;
/*!40000 ALTER TABLE `enquetes_noticias` DISABLE KEYS */;
/*!40000 ALTER TABLE `enquetes_noticias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enquetes_resultados`
--

DROP TABLE IF EXISTS `enquetes_resultados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enquetes_resultados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enquete_id` int(11) DEFAULT NULL,
  `enquete_item_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_enquete_resultado_enquete_idx` (`enquete_id`),
  KEY `fk_enquete_resultado_item_idx` (`enquete_item_id`),
  CONSTRAINT `FK_F27F6E795FDC5003` FOREIGN KEY (`enquete_id`) REFERENCES `enquetes` (`id`),
  CONSTRAINT `FK_F27F6E79F177C6FA` FOREIGN KEY (`enquete_item_id`) REFERENCES `enquetes_itens` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enquetes_resultados`
--

LOCK TABLES `enquetes_resultados` WRITE;
/*!40000 ALTER TABLE `enquetes_resultados` DISABLE KEYS */;
/*!40000 ALTER TABLE `enquetes_resultados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ext_log_entries`
--

DROP TABLE IF EXISTS `ext_log_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ext_log_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `logged_at` datetime NOT NULL,
  `object_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `object_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `version` int(11) NOT NULL,
  `data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:array)',
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `log_class_lookup_idx` (`object_class`),
  KEY `log_date_lookup_idx` (`logged_at`),
  KEY `log_user_lookup_idx` (`username`),
  KEY `log_version_lookup_idx` (`object_id`,`object_class`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ext_log_entries`
--

LOCK TABLES `ext_log_entries` WRITE;
/*!40000 ALTER TABLE `ext_log_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `ext_log_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ext_translations`
--

DROP TABLE IF EXISTS `ext_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ext_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `object_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `field` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `foreign_key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lookup_unique_idx` (`locale`,`object_class`,`field`,`foreign_key`),
  KEY `translations_lookup_idx` (`locale`,`object_class`,`foreign_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ext_translations`
--

LOCK TABLES `ext_translations` WRITE;
/*!40000 ALTER TABLE `ext_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `ext_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faturas`
--

DROP TABLE IF EXISTS `faturas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faturas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contrato_id` int(11) DEFAULT NULL,
  `vencimento` datetime DEFAULT NULL,
  `valor` float(10,2) DEFAULT NULL,
  `valor_pago` float(10,2) DEFAULT NULL,
  `data_pagamento` date DEFAULT NULL,
  `observacao` text,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '2',
  PRIMARY KEY (`id`),
  KEY `fk_faturas_contratos_idx` (`contrato_id`),
  CONSTRAINT `FK_48EAAA170AE7BF1` FOREIGN KEY (`contrato_id`) REFERENCES `contratos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faturas`
--

LOCK TABLES `faturas` WRITE;
/*!40000 ALTER TABLE `faturas` DISABLE KEYS */;
/*!40000 ALTER TABLE `faturas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galerias`
--

DROP TABLE IF EXISTS `galerias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galerias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galerias`
--

LOCK TABLES `galerias` WRITE;
/*!40000 ALTER TABLE `galerias` DISABLE KEYS */;
INSERT INTO `galerias` VALUES (1,'Primeira Galeria de Fotos','primeira_galeria_de_fotos','55fb03ec16b3b.jpeg','2015-09-17 15:18:20','2015-09-17 15:18:20',1),(2,'Principal','principal','5605a2eca5cfa.jpeg','2015-09-25 16:39:24','2015-09-25 16:39:24',1);
/*!40000 ALTER TABLE `galerias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galerias_itens`
--

DROP TABLE IF EXISTS `galerias_itens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galerias_itens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `galeria_id` int(11) DEFAULT NULL,
  `credito` varchar(75) DEFAULT NULL,
  `legenda` varchar(255) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_galeria_item_galeria_id_idx` (`galeria_id`),
  CONSTRAINT `fk_galeria_item_galeria_id` FOREIGN KEY (`galeria_id`) REFERENCES `galerias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galerias_itens`
--

LOCK TABLES `galerias_itens` WRITE;
/*!40000 ALTER TABLE `galerias_itens` DISABLE KEYS */;
INSERT INTO `galerias_itens` VALUES (1,1,'Divulgação','Foto 1','55fb0c9f4cf42.png','2015-09-17 15:55:27','2015-09-17 15:55:27',1),(2,1,'Divulgação','Foto 2','55fb0d82b1a43.png','2015-09-17 15:59:14','2015-09-17 15:59:14',1),(3,1,'Divulgação','Foto 3','55fb0d9ea1db6.jpeg','2015-09-17 15:59:42','2015-09-17 15:59:42',1),(4,2,NULL,NULL,'5605a369bf60f.png','2015-09-25 16:40:19','2015-09-25 16:41:29',1),(5,2,NULL,NULL,'5605a3493b694.png','2015-09-25 16:40:57','2015-09-25 16:40:57',1),(6,2,NULL,NULL,'5605a39975ed9.png','2015-09-25 16:42:17','2015-09-25 16:42:17',1);
/*!40000 ALTER TABLE `galerias_itens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galerias_noticias`
--

DROP TABLE IF EXISTS `galerias_noticias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galerias_noticias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `galeria_id` int(11) DEFAULT NULL,
  `noticia_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_galerias_solucoes_pagina_id_idx` (`noticia_id`),
  KEY `fk_galerias_solucoes_galeria_id_idx` (`galeria_id`),
  CONSTRAINT `fk_galerias_solucoes_galeria_id` FOREIGN KEY (`galeria_id`) REFERENCES `galerias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_galerias_solucoes_solucao_id` FOREIGN KEY (`noticia_id`) REFERENCES `noticias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galerias_noticias`
--

LOCK TABLES `galerias_noticias` WRITE;
/*!40000 ALTER TABLE `galerias_noticias` DISABLE KEYS */;
/*!40000 ALTER TABLE `galerias_noticias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galerias_paginas`
--

DROP TABLE IF EXISTS `galerias_paginas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galerias_paginas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `galeria_id` int(11) DEFAULT NULL,
  `pagina_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_galerias_paginas_galeria_id_idx` (`galeria_id`),
  KEY `fk_galerias_paginas_pagina_id_idx` (`pagina_id`),
  CONSTRAINT `fk_galerias_paginas_galeria_id` FOREIGN KEY (`galeria_id`) REFERENCES `galerias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_galerias_paginas_pagina_id` FOREIGN KEY (`pagina_id`) REFERENCES `paginas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galerias_paginas`
--

LOCK TABLES `galerias_paginas` WRITE;
/*!40000 ALTER TABLE `galerias_paginas` DISABLE KEYS */;
/*!40000 ALTER TABLE `galerias_paginas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticias`
--

DROP TABLE IF EXISTS `noticias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noticias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria_id` int(11) DEFAULT NULL,
  `area_publicacao_id` int(11) DEFAULT NULL,
  `retranca` varchar(75) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `resumo` varchar(500) DEFAULT NULL,
  `texto` text,
  `credito` varchar(75) DEFAULT NULL,
  `legenda` varchar(255) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_solucoes_categoria_id_idx` (`categoria_id`),
  KEY `fk_solucoes_areas_publicacoes_id_idx` (`area_publicacao_id`),
  CONSTRAINT `fk_solucoes_areas_publicacoes_id` FOREIGN KEY (`area_publicacao_id`) REFERENCES `areas_publicacoes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_solucoes_categoria_id` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticias`
--

LOCK TABLES `noticias` WRITE;
/*!40000 ALTER TABLE `noticias` DISABLE KEYS */;
INSERT INTO `noticias` VALUES (1,1,1,'Lorem ipsum','Nam nec faucibus elit','nam_nec_faucibus_elit','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse a maximus velit. Fusce commodo ex et nulla convallis tempus. Sed finibus vel ante ac dictum. Nam nec faucibus elit. Aliquam eu libero a sem accumsan pharetra. Etiam turpis ipsum, blandit et sapien nec, gravida rutrum libero','<p>\r\n\r\n</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse a maximus velit. Fusce commodo ex et nulla convallis tempus. Sed finibus vel ante ac dictum. Nam nec faucibus elit. Aliquam eu libero a sem accumsan pharetra. Etiam turpis ipsum, blandit et sapien nec, gravida rutrum libero. Nam non consectetur nibh. Nam in mattis urna. Nunc pretium pellentesque accumsan. In a diam a neque euismod volutpat vitae eget ex. Donec non libero ultricies, bibendum orci elementum, posuere risus.</p><p>Maecenas sed ultricies ante. Mauris eu est ac tortor vehicula consequat. Duis in efficitur tellus, ac condimentum neque. Cras iaculis accumsan erat, ac viverra ante mollis nec. Donec fringilla orci quis velit pulvinar, vitae vulputate risus molestie. Praesent hendrerit massa ac leo ultrices vulputate. Nam turpis erat, bibendum eget libero non, tempor iaculis turpis. Praesent quis mi massa. Integer condimentum orci eu placerat sagittis. Praesent viverra semper massa, quis porttitor dui vestibulum sed. Pellentesque ullamcorper justo ut cursus finibus. Nunc facilisis vehicula metus, eu lacinia felis. Donec sit amet euismod tortor, et accumsan neque. Ut et tincidunt lacus, in imperdiet libero. Nulla facilisi.</p><p>Cras vulputate ultricies erat eu egestas. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean mauris nunc, tincidunt ac neque ut, dapibus ornare augue. Morbi sodales libero sed sollicitudin tristique. Maecenas ac ipsum vulputate, dictum ipsum sed, facilisis ligula. Vestibulum eu vestibulum magna. In elementum vitae ipsum ac blandit. Nulla ultrices nibh non nibh porttitor, ut tincidunt odio vulputate. Mauris congue vitae dolor vel semper. Integer elementum tempor viverra.</p><p>In hac habitasse platea dictumst. Integer id ornare ipsum. Etiam quam eros, porttitor vitae nibh vel, volutpat suscipit nibh. Vestibulum blandit lobortis congue. Integer a felis purus. Vestibulum egestas, ante eget maximus faucibus, felis turpis malesuada arcu, at euismod quam diam id arcu. Mauris non venenatis mi, ut consequat enim. Nulla vitae tellus nec purus blandit fringilla vel nec dui.</p><p>Nunc semper, risus ut commodo auctor, orci orci fringilla magna, sed accumsan risus leo eu felis. Ut ac tempor justo, nec fermentum ante. Maecenas ligula nisl, fringilla quis viverra vitae, auctor lobortis enim. Pellentesque imperdiet et turpis at consequat. Mauris in tincidunt lacus. Cras tortor velit, vestibulum ac massa eget, bibendum pulvinar sapien. Integer nisi arcu, dictum a tempor in, luctus nec dolor. Donec libero est, efficitur a sollicitudin quis, interdum quis elit. Aenean porta varius magna id suscipit. Donec ullamcorper, libero sit amet ullamcorper blandit, nulla justo tincidunt nulla, in convallis risus diam ut massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n\r\n<br><p></p>','Divulgação','Lorem ipsum dolor sit amet, consectetur adipiscing elit','563710cf5eb80.jpg','2015-11-02 04:29:19','2015-11-02 04:29:19',1),(2,1,1,'Lorem ipsum','Nam nec faucibus elit','nam_nec_faucibus_elit_1','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse a maximus velit. Fusce commodo ex et nulla convallis tempus. Sed finibus vel ante ac dictum. Nam nec faucibus elit. Aliquam eu libero a sem accumsan pharetra. Etiam turpis ipsum, blandit et sapien nec, gravida rutrum libero','<p>\r\n\r\n</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse a maximus velit. Fusce commodo ex et nulla convallis tempus. Sed finibus vel ante ac dictum. Nam nec faucibus elit. Aliquam eu libero a sem accumsan pharetra. Etiam turpis ipsum, blandit et sapien nec, gravida rutrum libero. Nam non consectetur nibh. Nam in mattis urna. Nunc pretium pellentesque accumsan. In a diam a neque euismod volutpat vitae eget ex. Donec non libero ultricies, bibendum orci elementum, posuere risus.</p><p>Maecenas sed ultricies ante. Mauris eu est ac tortor vehicula consequat. Duis in efficitur tellus, ac condimentum neque. Cras iaculis accumsan erat, ac viverra ante mollis nec. Donec fringilla orci quis velit pulvinar, vitae vulputate risus molestie. Praesent hendrerit massa ac leo ultrices vulputate. Nam turpis erat, bibendum eget libero non, tempor iaculis turpis. Praesent quis mi massa. Integer condimentum orci eu placerat sagittis. Praesent viverra semper massa, quis porttitor dui vestibulum sed. Pellentesque ullamcorper justo ut cursus finibus. Nunc facilisis vehicula metus, eu lacinia felis. Donec sit amet euismod tortor, et accumsan neque. Ut et tincidunt lacus, in imperdiet libero. Nulla facilisi.</p><p>Cras vulputate ultricies erat eu egestas. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean mauris nunc, tincidunt ac neque ut, dapibus ornare augue. Morbi sodales libero sed sollicitudin tristique. Maecenas ac ipsum vulputate, dictum ipsum sed, facilisis ligula. Vestibulum eu vestibulum magna. In elementum vitae ipsum ac blandit. Nulla ultrices nibh non nibh porttitor, ut tincidunt odio vulputate. Mauris congue vitae dolor vel semper. Integer elementum tempor viverra.</p><p>In hac habitasse platea dictumst. Integer id ornare ipsum. Etiam quam eros, porttitor vitae nibh vel, volutpat suscipit nibh. Vestibulum blandit lobortis congue. Integer a felis purus. Vestibulum egestas, ante eget maximus faucibus, felis turpis malesuada arcu, at euismod quam diam id arcu. Mauris non venenatis mi, ut consequat enim. Nulla vitae tellus nec purus blandit fringilla vel nec dui.</p><p>Nunc semper, risus ut commodo auctor, orci orci fringilla magna, sed accumsan risus leo eu felis. Ut ac tempor justo, nec fermentum ante. Maecenas ligula nisl, fringilla quis viverra vitae, auctor lobortis enim. Pellentesque imperdiet et turpis at consequat. Mauris in tincidunt lacus. Cras tortor velit, vestibulum ac massa eget, bibendum pulvinar sapien. Integer nisi arcu, dictum a tempor in, luctus nec dolor. Donec libero est, efficitur a sollicitudin quis, interdum quis elit. Aenean porta varius magna id suscipit. Donec ullamcorper, libero sit amet ullamcorper blandit, nulla justo tincidunt nulla, in convallis risus diam ut massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n\r\n<br><p></p>','Divulgação','Lorem ipsum dolor sit amet, consectetur adipiscing elit','563710d6de642.jpg','2015-11-02 04:29:26','2015-11-02 04:29:26',1),(3,1,1,'Lorem ipsum','Nam nec faucibus elit','nam_nec_faucibus_elit_2','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse a maximus velit. Fusce commodo ex et nulla convallis tempus. Sed finibus vel ante ac dictum. Nam nec faucibus elit. Aliquam eu libero a sem accumsan pharetra. Etiam turpis ipsum, blandit et sapien nec, gravida rutrum libero','<p>\r\n\r\n</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse a maximus velit. Fusce commodo ex et nulla convallis tempus. Sed finibus vel ante ac dictum. Nam nec faucibus elit. Aliquam eu libero a sem accumsan pharetra. Etiam turpis ipsum, blandit et sapien nec, gravida rutrum libero. Nam non consectetur nibh. Nam in mattis urna. Nunc pretium pellentesque accumsan. In a diam a neque euismod volutpat vitae eget ex. Donec non libero ultricies, bibendum orci elementum, posuere risus.</p><p>Maecenas sed ultricies ante. Mauris eu est ac tortor vehicula consequat. Duis in efficitur tellus, ac condimentum neque. Cras iaculis accumsan erat, ac viverra ante mollis nec. Donec fringilla orci quis velit pulvinar, vitae vulputate risus molestie. Praesent hendrerit massa ac leo ultrices vulputate. Nam turpis erat, bibendum eget libero non, tempor iaculis turpis. Praesent quis mi massa. Integer condimentum orci eu placerat sagittis. Praesent viverra semper massa, quis porttitor dui vestibulum sed. Pellentesque ullamcorper justo ut cursus finibus. Nunc facilisis vehicula metus, eu lacinia felis. Donec sit amet euismod tortor, et accumsan neque. Ut et tincidunt lacus, in imperdiet libero. Nulla facilisi.</p><p>Cras vulputate ultricies erat eu egestas. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean mauris nunc, tincidunt ac neque ut, dapibus ornare augue. Morbi sodales libero sed sollicitudin tristique. Maecenas ac ipsum vulputate, dictum ipsum sed, facilisis ligula. Vestibulum eu vestibulum magna. In elementum vitae ipsum ac blandit. Nulla ultrices nibh non nibh porttitor, ut tincidunt odio vulputate. Mauris congue vitae dolor vel semper. Integer elementum tempor viverra.</p><p>In hac habitasse platea dictumst. Integer id ornare ipsum. Etiam quam eros, porttitor vitae nibh vel, volutpat suscipit nibh. Vestibulum blandit lobortis congue. Integer a felis purus. Vestibulum egestas, ante eget maximus faucibus, felis turpis malesuada arcu, at euismod quam diam id arcu. Mauris non venenatis mi, ut consequat enim. Nulla vitae tellus nec purus blandit fringilla vel nec dui.</p><p>Nunc semper, risus ut commodo auctor, orci orci fringilla magna, sed accumsan risus leo eu felis. Ut ac tempor justo, nec fermentum ante. Maecenas ligula nisl, fringilla quis viverra vitae, auctor lobortis enim. Pellentesque imperdiet et turpis at consequat. Mauris in tincidunt lacus. Cras tortor velit, vestibulum ac massa eget, bibendum pulvinar sapien. Integer nisi arcu, dictum a tempor in, luctus nec dolor. Donec libero est, efficitur a sollicitudin quis, interdum quis elit. Aenean porta varius magna id suscipit. Donec ullamcorper, libero sit amet ullamcorper blandit, nulla justo tincidunt nulla, in convallis risus diam ut massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n\r\n<br><p></p>','Divulgação','Lorem ipsum dolor sit amet, consectetur adipiscing elit','563710daf2235.jpg','2015-11-02 04:29:30','2015-11-02 04:29:30',1);
/*!40000 ALTER TABLE `noticias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paginas`
--

DROP TABLE IF EXISTS `paginas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paginas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `retranca` varchar(75) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `resumo` varchar(500) DEFAULT NULL,
  `texto` text,
  `credito` varchar(75) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `legenda` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paginas`
--

LOCK TABLES `paginas` WRITE;
/*!40000 ALTER TABLE `paginas` DISABLE KEYS */;
INSERT INTO `paginas` VALUES (1,'Retranca','Fale Conosoco','fale_conosoco',NULL,'<p></p><p>\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;</p><p>Vivamus facilisis urna ipsum, ut porta mauris dignissim in.&nbsp;</p><p>In hac habitasse platea dictumst. Nullam et tortor iaculis, posuere mauris nec, aliquet elit.&nbsp;</p><p>Curabitur condimentum porttitor nisl eget ullamcorper\r\n\r\n<br></p><p></p>',NULL,'56014c9d484b5.jpeg',NULL,'2015-09-17 14:51:08','2015-09-22 09:44:19',1),(2,NULL,'Nossas Notícias','nossas_noticias',NULL,'<p>\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur placerat eros eu volutpat sodales. Morbi non elit quis turpis suscipit sollicitudin. Nam a nisi est. Nam eu egestas nulla. Aliquam erat volutpat. Nam purus nisl, tempor sed volutpat vel, lacinia ac eros. Integer aliquam neque a lacus venenatis, non sagittis nisl suscipit. Nulla a enim ac risus imperdiet maximus pretium vel ex\r\n\r\n<br></p>',NULL,NULL,NULL,'2015-09-22 06:27:13','2015-09-22 06:27:13',1),(3,'Bem vindo','à Frango Norte','a_frango_norte','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis elementum cursus ante eu maximus. \r\nMauris commodo porttitor magna, ut molestie tellus luctus vitae. Sed suscipit, risus non consequat \r\nbibendum, massa dui cursus eros, sit amet sodales ligula enim eu nisi','<p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam commodo lorem iaculis consequat bibendum. Phasellus faucibus augue id eleifend scelerisque. Etiam in lacinia ligula. Sed convallis, nibh nec tempus vehicula, tellus odio varius est, sed euismod ipsum sem sed libero. Donec tempor finibus nibh a malesuada. Aliquam egestas dolor vitae ligula finibus, nec dictum orci lacinia. Donec ut est vehicula, fringilla nunc congue, consectetur mi. Integer pretium efficitur ultrices. Donec vulputate eu massa ac dignissim. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin imperdiet, ligula quis dapibus suscipit, leo risus tristique orci, at vestibulum diam erat ut elit. Vivamus erat urna, iaculis at sapien non, dignissim pellentesque lectus.</p><p>Praesent ullamcorper nulla at urna molestie tempor. Sed fringilla magna in nisi sollicitudin, id interdum justo dapibus. Vivamus nec eros placerat turpis tincidunt posuere. Quisque vitae tincidunt sem, et egestas sem. Pellentesque at nisi erat. Maecenas mollis, lorem a scelerisque mollis, mauris nulla ornare mi, vel cursus tortor orci a sem. Maecenas nibh ipsum, lacinia ornare vestibulum sed, luctus eu ligula. Curabitur porttitor nibh sapien, ut placerat justo aliquet vel. Sed semper orci orci, in malesuada neque auctor eu.</p><p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer leo mi, rhoncus a interdum sit amet, pellentesque quis erat. Integer tempus eu eros a interdum. Nam eleifend neque rutrum massa molestie egestas. Nullam molestie porttitor laoreet. Praesent sed neque et diam faucibus efficitur in sed erat. Pellentesque ullamcorper luctus nisl, nec euismod ipsum condimentum ut. Mauris pellentesque ac felis id lacinia. Sed tristique ut nisl ut sagittis. Fusce velit orci, congue quis ultricies et, faucibus iaculis justo. Nam dapibus, tellus ac finibus semper, erat mauris sagittis lacus, euismod tristique metus justo laoreet ipsum. Ut nec dolor ac leo maximus congue vitae nec mi. Nulla varius mauris velit, id gravida ipsum facilisis in. Fusce non accumsan quam, sit amet egestas leo. Suspendisse in lectus magna.</p><p>Nullam ultrices, ipsum non gravida tristique, purus odio commodo leo, at gravida mi lorem ac neque. Curabitur odio dolor, tincidunt ut tempor sit amet, semper et metus. Fusce bibendum velit ac ex venenatis pharetra. Praesent eget neque est. Maecenas et turpis ut neque porttitor rhoncus eu hendrerit risus. Fusce pulvinar massa a diam sollicitudin, vel porta turpis dapibus. Mauris lacinia elit accumsan tortor faucibus, nec posuere arcu consectetur. Donec vestibulum enim arcu, quis condimentum ante eleifend at. Cras tempor vitae tellus pretium egestas. Maecenas eget lorem in nisl semper dapibus. Pellentesque finibus porttitor nisi, et lacinia ligula. Vivamus ac elit ut sapien aliquam ullamcorper quis a lectus. Aenean gravida tincidunt libero a aliquet.</p><p>Donec efficitur orci euismod, aliquet sem in, venenatis quam. Integer at venenatis metus. Nam pellentesque rhoncus arcu, malesuada vulputate dolor euismod nec. Morbi scelerisque pellentesque maximus. Suspendisse non justo euismod, hendrerit magna ac, molestie metus. Maecenas eu ex at urna gravida congue. Nam id orci scelerisque, fringilla lorem nec, dictum purus.</p>\r\n\r\n</p>','Divulgação','56043710b8f3d.jpeg','Nullam ultrices, ipsum non gravida tristique, purus odio commodo leo, at gravida mi lorem ac neque','2015-09-24 14:46:56','2015-09-24 14:46:56',1),(4,'Título de Destaque','Título secundário','titulo_secundario','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eget imperdiet odio. Quisque tincidunt neque lobortis, lacinia nibh eu, ornare risus. Sed eleifend, metus non tincidunt blandit','<p>\r\n\r\n</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eget imperdiet odio. Quisque tincidunt neque lobortis, lacinia nibh eu, ornare risus. Sed eleifend, metus non tincidunt blandit, orci diam egestas sapien, sed lacinia arcu massa quis urna. Donec pretium vel ligula nec scelerisque. Praesent pretium porttitor dui, ac tempor augue euismod in. Vestibulum volutpat lectus vel viverra dapibus. Phasellus non neque tincidunt, mollis tellus a, semper leo. Praesent rhoncus enim ac cursus tempor. Ut gravida at ligula non fringilla. Quisque sed elit sed diam gravida suscipit quis et odio. Pellentesque tortor velit, facilisis nec eleifend lobortis, sodales eget est. Aliquam tellus sapien, sodales vel convallis efficitur, sagittis ut massa.</p><p>Sed nec ante ac arcu ullamcorper consectetur et in felis. Suspendisse varius enim eu orci suscipit egestas. Nunc nisl urna, placerat eu blandit gravida, sodales suscipit libero. Fusce efficitur, mi vel finibus tincidunt, dolor mauris porta est, a gravida ipsum odio quis nibh. Pellentesque fringilla vulputate nisl, sit amet semper arcu faucibus in. Etiam accumsan ipsum ac nulla rhoncus ornare. Nam venenatis quam quis viverra efficitur. Maecenas elementum, enim vel tincidunt gravida, odio arcu rhoncus enim, malesuada accumsan lacus justo id augue. Etiam at lacinia velit, iaculis imperdiet nisi. Aliquam et pellentesque erat. In ut lacinia ligula. Aliquam erat volutpat. Aenean semper cursus placerat.</p><p>Vestibulum gravida, sem vel facilisis auctor, enim felis dapibus nisi, molestie fringilla nunc erat at nisi. Ut porttitor nibh in leo placerat, non aliquet ex consequat. Vestibulum bibendum, urna ut posuere iaculis, augue purus pretium tortor, eget euismod diam erat quis magna. Nam placerat suscipit semper. Duis dictum, tellus sed mollis lacinia, neque arcu malesuada lorem, vel finibus sapien nibh sed sem. In varius, turpis vel elementum suscipit, metus nulla congue lectus, at blandit ex ipsum vulputate lectus. Phasellus egestas, ligula ut efficitur facilisis, leo dolor tincidunt velit, ac finibus lacus purus ut justo. Donec luctus velit in aliquam dignissim. Fusce tincidunt lectus quis ipsum rutrum, vel faucibus augue semper. Cras elementum fermentum massa, sit amet dictum ante blandit a. Etiam commodo enim quis enim hendrerit molestie. Morbi ac neque eget leo aliquet eleifend vel sit amet lacus.</p><p>Nullam condimentum neque erat, vitae pulvinar sem vestibulum vitae. In malesuada at ante at mattis. Suspendisse non mattis ante. Phasellus eget egestas dolor. In non justo lobortis dui venenatis ornare. Curabitur risus enim, vestibulum a lectus sit amet, vehicula venenatis elit. Duis cursus, eros non molestie aliquet, neque magna vulputate leo, et iaculis ipsum nisl vel ex. Nullam ut vehicula enim. Vestibulum blandit purus at erat pulvinar, quis porta lorem rutrum. Ut vitae feugiat quam. Aliquam egestas libero turpis, sit amet facilisis lacus placerat eget. Duis id odio eget ligula congue fermentum sed at odio. Morbi porta massa vel dolor rutrum, a lobortis tortor iaculis. Ut fringilla volutpat purus, eget facilisis quam rhoncus eu. Sed mollis commodo turpis, ac lacinia dui auctor a.</p><p>In pellentesque est velit, sit amet dapibus ligula congue eu. Nulla facilisi. Donec et ex nisi. In vestibulum posuere metus, eget maximus velit fermentum vel. Donec placerat sem eget metus aliquam, nec euismod augue sodales. Phasellus feugiat enim libero, eu ornare tortor tempus quis. Aliquam erat massa, maximus eu velit vel, maximus dapibus velit. Donec luctus id ex id elementum.</p>\r\n\r\n<br><p></p>','Divulgação','5605475b9cbdd.jpeg','Lorem ipsum dolor sit amet, consectetur adipiscing elit','2015-09-25 10:08:43','2015-09-25 10:08:43',1),(5,'Título de Destaque','Título secundário','titulo_secundario_1','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eget imperdiet odio. Quisque tincidunt neque lobortis, lacinia nibh eu, ornare risus. Sed eleifend, metus non tincidunt blandit','<p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eget imperdiet odio. Quisque tincidunt neque lobortis, lacinia nibh eu, ornare risus. Sed eleifend, metus non tincidunt blandit, orci diam egestas sapien, sed lacinia arcu massa quis urna. Donec pretium vel ligula nec scelerisque. Praesent pretium porttitor dui, ac tempor augue euismod in. Vestibulum volutpat lectus vel viverra dapibus. Phasellus non neque tincidunt, mollis tellus a, semper leo. Praesent rhoncus enim ac cursus tempor. Ut gravida at ligula non fringilla. Quisque sed elit sed diam gravida suscipit quis et odio. Pellentesque tortor velit, facilisis nec eleifend lobortis, sodales eget est. Aliquam tellus sapien, sodales vel convallis efficitur, sagittis ut massa.</p><p>Sed nec ante ac arcu ullamcorper consectetur et in felis. Suspendisse varius enim eu orci suscipit egestas. Nunc nisl urna, placerat eu blandit gravida, sodales suscipit libero. Fusce efficitur, mi vel finibus tincidunt, dolor mauris porta est, a gravida ipsum odio quis nibh. Pellentesque fringilla vulputate nisl, sit amet semper arcu faucibus in. Etiam accumsan ipsum ac nulla rhoncus ornare. Nam venenatis quam quis viverra efficitur. Maecenas elementum, enim vel tincidunt gravida, odio arcu rhoncus enim, malesuada accumsan lacus justo id augue. Etiam at lacinia velit, iaculis imperdiet nisi. Aliquam et pellentesque erat. In ut lacinia ligula. Aliquam erat volutpat. Aenean semper cursus placerat.</p><p>Vestibulum gravida, sem vel facilisis auctor, enim felis dapibus nisi, molestie fringilla nunc erat at nisi. Ut porttitor nibh in leo placerat, non aliquet ex consequat. Vestibulum bibendum, urna ut posuere iaculis, augue purus pretium tortor, eget euismod diam erat quis magna. Nam placerat suscipit semper. Duis dictum, tellus sed mollis lacinia, neque arcu malesuada lorem, vel finibus sapien nibh sed sem. In varius, turpis vel elementum suscipit, metus nulla congue lectus, at blandit ex ipsum vulputate lectus. Phasellus egestas, ligula ut efficitur facilisis, leo dolor tincidunt velit, ac finibus lacus purus ut justo. Donec luctus velit in aliquam dignissim. Fusce tincidunt lectus quis ipsum rutrum, vel faucibus augue semper. Cras elementum fermentum massa, sit amet dictum ante blandit a. Etiam commodo enim quis enim hendrerit molestie. Morbi ac neque eget leo aliquet eleifend vel sit amet lacus.</p><p>Nullam condimentum neque erat, vitae pulvinar sem vestibulum vitae. In malesuada at ante at mattis. Suspendisse non mattis ante. Phasellus eget egestas dolor. In non justo lobortis dui venenatis ornare. Curabitur risus enim, vestibulum a lectus sit amet, vehicula venenatis elit. Duis cursus, eros non molestie aliquet, neque magna vulputate leo, et iaculis ipsum nisl vel ex. Nullam ut vehicula enim. Vestibulum blandit purus at erat pulvinar, quis porta lorem rutrum. Ut vitae feugiat quam. Aliquam egestas libero turpis, sit amet facilisis lacus placerat eget. Duis id odio eget ligula congue fermentum sed at odio. Morbi porta massa vel dolor rutrum, a lobortis tortor iaculis. Ut fringilla volutpat purus, eget facilisis quam rhoncus eu. Sed mollis commodo turpis, ac lacinia dui auctor a.</p><p>In pellentesque est velit, sit amet dapibus ligula congue eu. Nulla facilisi. Donec et ex nisi. In vestibulum posuere metus, eget maximus velit fermentum vel. Donec placerat sem eget metus aliquam, nec euismod augue sodales. Phasellus feugiat enim libero, eu ornare tortor tempus quis. Aliquam erat massa, maximus eu velit vel, maximus dapibus velit. Donec luctus id ex id elementum.</p>\r\n\r\n<br></p>',NULL,NULL,NULL,'2015-09-25 10:09:46','2015-09-25 10:09:46',1),(6,'Sobre nós','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nec augue ipsum','lorem_ipsum_dolor_sit_amet_consectetur_adipiscing_elit_nullam_nec_augue_ipsum','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nec augue ipsum. In hac habitasse platea dictumst. Quisque pellentesque risus id ligula tempor, sit amet dapibus justo consectetur. Aenean eget congue elit. Sed ut nisi non purus semper convallis vel id leo. Suspendisse a sollicitudin dolor','<p>\r\n\r\n</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nec augue ipsum. In hac habitasse platea dictumst. Quisque pellentesque risus id ligula tempor, sit amet dapibus justo consectetur. Aenean eget congue elit. Sed ut nisi non purus semper convallis vel id leo. Suspendisse a sollicitudin dolor. Suspendisse et ultricies est, a lobortis augue. Aliquam non efficitur ex. Praesent suscipit ante ligula, vel accumsan justo cursus nec. Integer non lectus sit amet dui condimentum consequat dictum a velit. Ut euismod risus leo, in porttitor massa suscipit eget. Vestibulum vel tortor nec arcu fringilla imperdiet ac faucibus urna. Nunc lacinia, sem sit amet rutrum fringilla, orci tortor luctus lacus, sit amet mattis ex elit euismod tellus. Vestibulum sed ante eget lacus ullamcorper finibus et eu mauris.</p><p>Vestibulum volutpat sapien turpis, sollicitudin ultricies dolor vulputate id. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras sodales tincidunt libero, sed suscipit arcu commodo non. Nulla volutpat felis id blandit tristique. Nulla facilisi. Etiam tincidunt, odio vitae congue elementum, risus elit malesuada turpis, ac consequat erat enim vel ex. Morbi varius arcu eu sapien aliquet maximus. Fusce ac quam tempus sapien varius vulputate mattis nec nisi. Donec quis nisi vel ante fringilla condimentum eget id nisl. Fusce nec condimentum justo. Proin at lorem ornare, hendrerit ligula eget, suscipit leo. Quisque quis sagittis enim. Pellentesque et felis at tellus lacinia pulvinar non quis nibh.</p><p>Aenean volutpat auctor dui, ut mattis dolor dapibus vel. In hac habitasse platea dictumst. Praesent congue pretium lectus, eu rhoncus odio imperdiet ac. Donec varius semper euismod. Pellentesque tellus magna, rhoncus at tellus vel, dapibus varius urna. Nulla cursus tellus et sapien faucibus rhoncus. Mauris sed risus scelerisque, tincidunt quam a, pellentesque nunc. Sed tincidunt mauris a facilisis pharetra.</p><p>Donec ultrices dui augue, eu tristique purus accumsan id. Mauris non turpis at nisi ultrices varius tincidunt tincidunt metus. Nam non cursus ex. Nunc tempus massa convallis odio eleifend auctor. Praesent auctor lectus vel tempor porttitor. Integer sed nisl ultrices, lobortis justo vel, ornare eros. Cras luctus rhoncus ante, ut hendrerit enim pulvinar eu. Etiam consectetur dui vitae volutpat rhoncus. Fusce ac orci a quam tincidunt volutpat. Nullam facilisis in arcu sit amet lobortis. Praesent sagittis quis quam sed consequat.</p><p>Aenean ante nulla, imperdiet pulvinar aliquam et, porta eu metus. Duis sodales ac tortor eu vehicula. Nulla at aliquet lacus. Quisque dignissim magna vitae leo varius, eget sollicitudin enim convallis. Fusce at mollis leo. Integer bibendum ex dolor, non vehicula magna scelerisque sit amet. Suspendisse vitae sem sed erat finibus imperdiet. Ut cursus lectus non nunc tempor luctus. Sed ultrices finibus urna in sagittis. Maecenas eget suscipit nulla. Integer vulputate lectus facilisis semper condimentum. Nulla facilisi. Suspendisse felis libero, scelerisque eget ornare eu, faucibus in felis. Nunc nec nunc dolor. Suspendisse vehicula ante enim, eget consequat purus porta non.</p>\r\n\r\n<br><p></p>',NULL,'5605558fe928d.jpeg',NULL,'2015-09-25 10:23:40','2015-09-25 11:09:19',1),(7,NULL,'Nossa história','nossa_historia','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id massa tellus. Sed massa sapien, auctor ac ipsum eget, dignissim molestie mi. Vestibulum in augue lectus. Morbi iaculis nec mauris vel dapibus. Suspendisse potenti','<p>\r\n\r\n</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin commodo, odio non elementum mollis, ex sem sagittis risus, vitae gravida felis orci in arcu. Nam iaculis fermentum purus. Maecenas porttitor dapibus odio, at maximus dui dapibus quis. Vivamus pretium iaculis vehicula. Nullam tincidunt, erat ac ornare lacinia, mi tellus commodo mauris, vitae consequat lectus ligula sed mi. Donec pellentesque pellentesque ligula, sit amet ultrices sem auctor vel. Maecenas et fringilla turpis, at faucibus ipsum. Proin orci elit, faucibus non pellentesque et, placerat at ante. Maecenas et pulvinar mauris. Nunc tellus sapien, congue nec tincidunt vel, rhoncus sit amet nulla. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed efficitur scelerisque nulla, ut dictum nunc convallis non. Vestibulum finibus diam justo, ac malesuada libero aliquam ac.</p><p>Donec volutpat fermentum efficitur. Maecenas at ipsum vel purus ultricies bibendum. Mauris consequat, tellus nec iaculis pretium, lacus nisi vestibulum lacus, faucibus auctor ex tortor sed purus. Fusce et elementum augue. Nulla sollicitudin placerat est quis fermentum. Nulla non enim nunc. Pellentesque ut eros at justo aliquam elementum et non lectus. Aenean sit amet dignissim libero, vitae elementum sapien. Vivamus venenatis faucibus magna, id dapibus massa aliquet nec. Vestibulum in accumsan purus, in auctor augue. Nulla condimentum vitae purus vitae placerat. Aenean nisl lacus, bibendum quis vehicula a, volutpat ac est. Praesent in ultrices tortor. Phasellus ut erat non leo interdum finibus.</p><p>Nam vitae urna eu lectus volutpat malesuada. Quisque viverra neque quis tellus tincidunt, ac dapibus ante tristique. Sed quam lacus, tincidunt lobortis massa at, hendrerit fermentum diam. Vivamus ut augue mi. Quisque non fermentum quam. Aliquam semper tortor eget dui lacinia porta. Nullam bibendum nunc justo, nec tincidunt leo scelerisque vel. Nam et feugiat sem, eget tristique libero. Vestibulum purus enim, tincidunt in vulputate id, vehicula id leo.</p><p>Vivamus sodales sit amet elit et volutpat. Pellentesque urna ante, posuere id pharetra ut, sollicitudin eget massa. Duis accumsan eleifend dolor nec blandit. Morbi ac est sed dui ultricies mollis et nec est. Phasellus at eros ipsum. Vivamus sodales turpis non cursus dictum. Etiam sed metus congue, lobortis tortor ac, molestie orci. Nulla feugiat pulvinar lorem, vitae pulvinar mi accumsan ut. Maecenas lobortis dapibus lorem sed pretium. Integer pharetra posuere tempor. Proin in mauris nunc. Curabitur ullamcorper luctus lectus. Morbi quam nisi, luctus in semper eu, pharetra sit amet neque. Vestibulum quis est commodo, maximus magna sed, suscipit nisi. Morbi ultricies, quam in viverra ullamcorper, nisi lorem ullamcorper quam, nec scelerisque lacus dolor sit amet elit.</p><p>Donec semper nulla vel nunc eleifend commodo. Donec lobortis sem eget diam gravida, nec consectetur massa pharetra. Aenean blandit interdum mauris, non feugiat orci iaculis ac. Nulla quis blandit elit. Curabitur tortor sem, cursus nec dui id, ultrices elementum est. Cras accumsan felis felis, ut feugiat magna vehicula at. Pellentesque neque justo, hendrerit vel nisl id, malesuada dictum nisi. Aenean rutrum sem ut urna semper tempor et vel sapien. Praesent in risus diam. Nulla volutpat elementum laoreet. In molestie neque volutpat, aliquam turpis vitae, ornare dui. In eleifend euismod massa.</p>\r\n\r\n<br><p></p>',NULL,NULL,NULL,'2015-09-25 16:22:30','2015-09-25 16:22:30',1),(8,NULL,'Nossos valores','nossos_valores','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id massa tellus. Sed massa sapien, auctor ac ipsum eget, dignissim molestie mi. Vestibulum in augue lectus. Morbi iaculis nec mauris vel dapibus. Suspendisse potenti','<p>\r\n\r\n</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin commodo, odio non elementum mollis, ex sem sagittis risus, vitae gravida felis orci in arcu. Nam iaculis fermentum purus. Maecenas porttitor dapibus odio, at maximus dui dapibus quis. Vivamus pretium iaculis vehicula. Nullam tincidunt, erat ac ornare lacinia, mi tellus commodo mauris, vitae consequat lectus ligula sed mi. Donec pellentesque pellentesque ligula, sit amet ultrices sem auctor vel. Maecenas et fringilla turpis, at faucibus ipsum. Proin orci elit, faucibus non pellentesque et, placerat at ante. Maecenas et pulvinar mauris. Nunc tellus sapien, congue nec tincidunt vel, rhoncus sit amet nulla. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed efficitur scelerisque nulla, ut dictum nunc convallis non. Vestibulum finibus diam justo, ac malesuada libero aliquam ac.</p><p>Donec volutpat fermentum efficitur. Maecenas at ipsum vel purus ultricies bibendum. Mauris consequat, tellus nec iaculis pretium, lacus nisi vestibulum lacus, faucibus auctor ex tortor sed purus. Fusce et elementum augue. Nulla sollicitudin placerat est quis fermentum. Nulla non enim nunc. Pellentesque ut eros at justo aliquam elementum et non lectus. Aenean sit amet dignissim libero, vitae elementum sapien. Vivamus venenatis faucibus magna, id dapibus massa aliquet nec. Vestibulum in accumsan purus, in auctor augue. Nulla condimentum vitae purus vitae placerat. Aenean nisl lacus, bibendum quis vehicula a, volutpat ac est. Praesent in ultrices tortor. Phasellus ut erat non leo interdum finibus.</p><p>Nam vitae urna eu lectus volutpat malesuada. Quisque viverra neque quis tellus tincidunt, ac dapibus ante tristique. Sed quam lacus, tincidunt lobortis massa at, hendrerit fermentum diam. Vivamus ut augue mi. Quisque non fermentum quam. Aliquam semper tortor eget dui lacinia porta. Nullam bibendum nunc justo, nec tincidunt leo scelerisque vel. Nam et feugiat sem, eget tristique libero. Vestibulum purus enim, tincidunt in vulputate id, vehicula id leo.</p><p>Vivamus sodales sit amet elit et volutpat. Pellentesque urna ante, posuere id pharetra ut, sollicitudin eget massa. Duis accumsan eleifend dolor nec blandit. Morbi ac est sed dui ultricies mollis et nec est. Phasellus at eros ipsum. Vivamus sodales turpis non cursus dictum. Etiam sed metus congue, lobortis tortor ac, molestie orci. Nulla feugiat pulvinar lorem, vitae pulvinar mi accumsan ut. Maecenas lobortis dapibus lorem sed pretium. Integer pharetra posuere tempor. Proin in mauris nunc. Curabitur ullamcorper luctus lectus. Morbi quam nisi, luctus in semper eu, pharetra sit amet neque. Vestibulum quis est commodo, maximus magna sed, suscipit nisi. Morbi ultricies, quam in viverra ullamcorper, nisi lorem ullamcorper quam, nec scelerisque lacus dolor sit amet elit.</p><p>Donec semper nulla vel nunc eleifend commodo. Donec lobortis sem eget diam gravida, nec consectetur massa pharetra. Aenean blandit interdum mauris, non feugiat orci iaculis ac. Nulla quis blandit elit. Curabitur tortor sem, cursus nec dui id, ultrices elementum est. Cras accumsan felis felis, ut feugiat magna vehicula at. Pellentesque neque justo, hendrerit vel nisl id, malesuada dictum nisi. Aenean rutrum sem ut urna semper tempor et vel sapien. Praesent in risus diam. Nulla volutpat elementum laoreet. In molestie neque volutpat, aliquam turpis vitae, ornare dui. In eleifend euismod massa.</p>\r\n\r\n<br><p></p>',NULL,NULL,NULL,'2015-09-25 16:22:42','2015-09-25 16:22:42',1),(9,NULL,'Nosso futuro','nosso_futuro','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id massa tellus. Sed massa sapien, auctor ac ipsum eget, dignissim molestie mi. Vestibulum in augue lectus. Morbi iaculis nec mauris vel dapibus. Suspendisse potenti','<p>\r\n\r\n</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin commodo, odio non elementum mollis, ex sem sagittis risus, vitae gravida felis orci in arcu. Nam iaculis fermentum purus. Maecenas porttitor dapibus odio, at maximus dui dapibus quis. Vivamus pretium iaculis vehicula. Nullam tincidunt, erat ac ornare lacinia, mi tellus commodo mauris, vitae consequat lectus ligula sed mi. Donec pellentesque pellentesque ligula, sit amet ultrices sem auctor vel. Maecenas et fringilla turpis, at faucibus ipsum. Proin orci elit, faucibus non pellentesque et, placerat at ante. Maecenas et pulvinar mauris. Nunc tellus sapien, congue nec tincidunt vel, rhoncus sit amet nulla. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed efficitur scelerisque nulla, ut dictum nunc convallis non. Vestibulum finibus diam justo, ac malesuada libero aliquam ac.</p><p>Donec volutpat fermentum efficitur. Maecenas at ipsum vel purus ultricies bibendum. Mauris consequat, tellus nec iaculis pretium, lacus nisi vestibulum lacus, faucibus auctor ex tortor sed purus. Fusce et elementum augue. Nulla sollicitudin placerat est quis fermentum. Nulla non enim nunc. Pellentesque ut eros at justo aliquam elementum et non lectus. Aenean sit amet dignissim libero, vitae elementum sapien. Vivamus venenatis faucibus magna, id dapibus massa aliquet nec. Vestibulum in accumsan purus, in auctor augue. Nulla condimentum vitae purus vitae placerat. Aenean nisl lacus, bibendum quis vehicula a, volutpat ac est. Praesent in ultrices tortor. Phasellus ut erat non leo interdum finibus.</p><p>Nam vitae urna eu lectus volutpat malesuada. Quisque viverra neque quis tellus tincidunt, ac dapibus ante tristique. Sed quam lacus, tincidunt lobortis massa at, hendrerit fermentum diam. Vivamus ut augue mi. Quisque non fermentum quam. Aliquam semper tortor eget dui lacinia porta. Nullam bibendum nunc justo, nec tincidunt leo scelerisque vel. Nam et feugiat sem, eget tristique libero. Vestibulum purus enim, tincidunt in vulputate id, vehicula id leo.</p><p>Vivamus sodales sit amet elit et volutpat. Pellentesque urna ante, posuere id pharetra ut, sollicitudin eget massa. Duis accumsan eleifend dolor nec blandit. Morbi ac est sed dui ultricies mollis et nec est. Phasellus at eros ipsum. Vivamus sodales turpis non cursus dictum. Etiam sed metus congue, lobortis tortor ac, molestie orci. Nulla feugiat pulvinar lorem, vitae pulvinar mi accumsan ut. Maecenas lobortis dapibus lorem sed pretium. Integer pharetra posuere tempor. Proin in mauris nunc. Curabitur ullamcorper luctus lectus. Morbi quam nisi, luctus in semper eu, pharetra sit amet neque. Vestibulum quis est commodo, maximus magna sed, suscipit nisi. Morbi ultricies, quam in viverra ullamcorper, nisi lorem ullamcorper quam, nec scelerisque lacus dolor sit amet elit.</p><p>Donec semper nulla vel nunc eleifend commodo. Donec lobortis sem eget diam gravida, nec consectetur massa pharetra. Aenean blandit interdum mauris, non feugiat orci iaculis ac. Nulla quis blandit elit. Curabitur tortor sem, cursus nec dui id, ultrices elementum est. Cras accumsan felis felis, ut feugiat magna vehicula at. Pellentesque neque justo, hendrerit vel nisl id, malesuada dictum nisi. Aenean rutrum sem ut urna semper tempor et vel sapien. Praesent in risus diam. Nulla volutpat elementum laoreet. In molestie neque volutpat, aliquam turpis vitae, ornare dui. In eleifend euismod massa.</p>\r\n\r\n<br><p></p>',NULL,NULL,NULL,'2015-09-25 16:22:53','2015-09-25 16:22:53',1),(10,NULL,'Nossa empresa','nossa_empresa','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id massa tellus. Sed massa sapien, auctor ac ipsum eget, dignissim molestie mi. Vestibulum in augue lectus. Morbi iaculis nec mauris vel dapibus. Suspendisse potenti','<p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin commodo, odio non elementum mollis, ex sem sagittis risus, vitae gravida felis orci in arcu. Nam iaculis fermentum purus. Maecenas porttitor dapibus odio, at maximus dui dapibus quis. Vivamus pretium iaculis vehicula. Nullam tincidunt, erat ac ornare lacinia, mi tellus commodo mauris, vitae consequat lectus ligula sed mi. Donec pellentesque pellentesque ligula, sit amet ultrices sem auctor vel. Maecenas et fringilla turpis, at faucibus ipsum. Proin orci elit, faucibus non pellentesque et, placerat at ante. Maecenas et pulvinar mauris. Nunc tellus sapien, congue nec tincidunt vel, rhoncus sit amet nulla. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed efficitur scelerisque nulla, ut dictum nunc convallis non. Vestibulum finibus diam justo, ac malesuada libero aliquam ac.</p><p>Donec volutpat fermentum efficitur. Maecenas at ipsum vel purus ultricies bibendum. Mauris consequat, tellus nec iaculis pretium, lacus nisi vestibulum lacus, faucibus auctor ex tortor sed purus. Fusce et elementum augue. Nulla sollicitudin placerat est quis fermentum. Nulla non enim nunc. Pellentesque ut eros at justo aliquam elementum et non lectus. Aenean sit amet dignissim libero, vitae elementum sapien. Vivamus venenatis faucibus magna, id dapibus massa aliquet nec. Vestibulum in accumsan purus, in auctor augue. Nulla condimentum vitae purus vitae placerat. Aenean nisl lacus, bibendum quis vehicula a, volutpat ac est. Praesent in ultrices tortor. Phasellus ut erat non leo interdum finibus.</p><p>Nam vitae urna eu lectus volutpat malesuada. Quisque viverra neque quis tellus tincidunt, ac dapibus ante tristique. Sed quam lacus, tincidunt lobortis massa at, hendrerit fermentum diam. Vivamus ut augue mi. Quisque non fermentum quam. Aliquam semper tortor eget dui lacinia porta. Nullam bibendum nunc justo, nec tincidunt leo scelerisque vel. Nam et feugiat sem, eget tristique libero. Vestibulum purus enim, tincidunt in vulputate id, vehicula id leo.</p><p>Vivamus sodales sit amet elit et volutpat. Pellentesque urna ante, posuere id pharetra ut, sollicitudin eget massa. Duis accumsan eleifend dolor nec blandit. Morbi ac est sed dui ultricies mollis et nec est. Phasellus at eros ipsum. Vivamus sodales turpis non cursus dictum. Etiam sed metus congue, lobortis tortor ac, molestie orci. Nulla feugiat pulvinar lorem, vitae pulvinar mi accumsan ut. Maecenas lobortis dapibus lorem sed pretium. Integer pharetra posuere tempor. Proin in mauris nunc. Curabitur ullamcorper luctus lectus. Morbi quam nisi, luctus in semper eu, pharetra sit amet neque. Vestibulum quis est commodo, maximus magna sed, suscipit nisi. Morbi ultricies, quam in viverra ullamcorper, nisi lorem ullamcorper quam, nec scelerisque lacus dolor sit amet elit.</p><p>Donec semper nulla vel nunc eleifend commodo. Donec lobortis sem eget diam gravida, nec consectetur massa pharetra. Aenean blandit interdum mauris, non feugiat orci iaculis ac. Nulla quis blandit elit. Curabitur tortor sem, cursus nec dui id, ultrices elementum est. Cras accumsan felis felis, ut feugiat magna vehicula at. Pellentesque neque justo, hendrerit vel nisl id, malesuada dictum nisi. Aenean rutrum sem ut urna semper tempor et vel sapien. Praesent in risus diam. Nulla volutpat elementum laoreet. In molestie neque volutpat, aliquam turpis vitae, ornare dui. In eleifend euismod massa.</p>\r\n\r\n<br></p>',NULL,NULL,NULL,'2015-09-25 16:23:18','2015-09-25 16:23:18',1),(11,NULL,'Nossos funcionários','nossos_funcionarios','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id massa tellus. Sed massa sapien, auctor ac ipsum eget, dignissim molestie mi. Vestibulum in augue lectus. Morbi iaculis nec mauris vel dapibus. Suspendisse potenti','<p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin commodo, odio non elementum mollis, ex sem sagittis risus, vitae gravida felis orci in arcu. Nam iaculis fermentum purus. Maecenas porttitor dapibus odio, at maximus dui dapibus quis. Vivamus pretium iaculis vehicula. Nullam tincidunt, erat ac ornare lacinia, mi tellus commodo mauris, vitae consequat lectus ligula sed mi. Donec pellentesque pellentesque ligula, sit amet ultrices sem auctor vel. Maecenas et fringilla turpis, at faucibus ipsum. Proin orci elit, faucibus non pellentesque et, placerat at ante. Maecenas et pulvinar mauris. Nunc tellus sapien, congue nec tincidunt vel, rhoncus sit amet nulla. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed efficitur scelerisque nulla, ut dictum nunc convallis non. Vestibulum finibus diam justo, ac malesuada libero aliquam ac.</p><p>Donec volutpat fermentum efficitur. Maecenas at ipsum vel purus ultricies bibendum. Mauris consequat, tellus nec iaculis pretium, lacus nisi vestibulum lacus, faucibus auctor ex tortor sed purus. Fusce et elementum augue. Nulla sollicitudin placerat est quis fermentum. Nulla non enim nunc. Pellentesque ut eros at justo aliquam elementum et non lectus. Aenean sit amet dignissim libero, vitae elementum sapien. Vivamus venenatis faucibus magna, id dapibus massa aliquet nec. Vestibulum in accumsan purus, in auctor augue. Nulla condimentum vitae purus vitae placerat. Aenean nisl lacus, bibendum quis vehicula a, volutpat ac est. Praesent in ultrices tortor. Phasellus ut erat non leo interdum finibus.</p><p>Nam vitae urna eu lectus volutpat malesuada. Quisque viverra neque quis tellus tincidunt, ac dapibus ante tristique. Sed quam lacus, tincidunt lobortis massa at, hendrerit fermentum diam. Vivamus ut augue mi. Quisque non fermentum quam. Aliquam semper tortor eget dui lacinia porta. Nullam bibendum nunc justo, nec tincidunt leo scelerisque vel. Nam et feugiat sem, eget tristique libero. Vestibulum purus enim, tincidunt in vulputate id, vehicula id leo.</p><p>Vivamus sodales sit amet elit et volutpat. Pellentesque urna ante, posuere id pharetra ut, sollicitudin eget massa. Duis accumsan eleifend dolor nec blandit. Morbi ac est sed dui ultricies mollis et nec est. Phasellus at eros ipsum. Vivamus sodales turpis non cursus dictum. Etiam sed metus congue, lobortis tortor ac, molestie orci. Nulla feugiat pulvinar lorem, vitae pulvinar mi accumsan ut. Maecenas lobortis dapibus lorem sed pretium. Integer pharetra posuere tempor. Proin in mauris nunc. Curabitur ullamcorper luctus lectus. Morbi quam nisi, luctus in semper eu, pharetra sit amet neque. Vestibulum quis est commodo, maximus magna sed, suscipit nisi. Morbi ultricies, quam in viverra ullamcorper, nisi lorem ullamcorper quam, nec scelerisque lacus dolor sit amet elit.</p><p>Donec semper nulla vel nunc eleifend commodo. Donec lobortis sem eget diam gravida, nec consectetur massa pharetra. Aenean blandit interdum mauris, non feugiat orci iaculis ac. Nulla quis blandit elit. Curabitur tortor sem, cursus nec dui id, ultrices elementum est. Cras accumsan felis felis, ut feugiat magna vehicula at. Pellentesque neque justo, hendrerit vel nisl id, malesuada dictum nisi. Aenean rutrum sem ut urna semper tempor et vel sapien. Praesent in risus diam. Nulla volutpat elementum laoreet. In molestie neque volutpat, aliquam turpis vitae, ornare dui. In eleifend euismod massa.</p>\r\n\r\n<br></p>',NULL,NULL,NULL,'2015-09-25 16:23:25','2015-09-25 16:23:25',1),(12,NULL,'Nossos produtos','nossos_produtos','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id massa tellus. Sed massa sapien, auctor ac ipsum eget, dignissim molestie mi. Vestibulum in augue lectus','<p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin commodo, odio non elementum mollis, ex sem sagittis risus, vitae gravida felis orci in arcu. Nam iaculis fermentum purus. Maecenas porttitor dapibus odio, at maximus dui dapibus quis. Vivamus pretium iaculis vehicula. Nullam tincidunt, erat ac ornare lacinia, mi tellus commodo mauris, vitae consequat lectus ligula sed mi. Donec pellentesque pellentesque ligula, sit amet ultrices sem auctor vel. Maecenas et fringilla turpis, at faucibus ipsum. Proin orci elit, faucibus non pellentesque et, placerat at ante. Maecenas et pulvinar mauris. Nunc tellus sapien, congue nec tincidunt vel, rhoncus sit amet nulla. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed efficitur scelerisque nulla, ut dictum nunc convallis non. Vestibulum finibus diam justo, ac malesuada libero aliquam ac.</p><p>Donec volutpat fermentum efficitur. Maecenas at ipsum vel purus ultricies bibendum. Mauris consequat, tellus nec iaculis pretium, lacus nisi vestibulum lacus, faucibus auctor ex tortor sed purus. Fusce et elementum augue. Nulla sollicitudin placerat est quis fermentum. Nulla non enim nunc. Pellentesque ut eros at justo aliquam elementum et non lectus. Aenean sit amet dignissim libero, vitae elementum sapien. Vivamus venenatis faucibus magna, id dapibus massa aliquet nec. Vestibulum in accumsan purus, in auctor augue. Nulla condimentum vitae purus vitae placerat. Aenean nisl lacus, bibendum quis vehicula a, volutpat ac est. Praesent in ultrices tortor. Phasellus ut erat non leo interdum finibus.</p><p>Nam vitae urna eu lectus volutpat malesuada. Quisque viverra neque quis tellus tincidunt, ac dapibus ante tristique. Sed quam lacus, tincidunt lobortis massa at, hendrerit fermentum diam. Vivamus ut augue mi. Quisque non fermentum quam. Aliquam semper tortor eget dui lacinia porta. Nullam bibendum nunc justo, nec tincidunt leo scelerisque vel. Nam et feugiat sem, eget tristique libero. Vestibulum purus enim, tincidunt in vulputate id, vehicula id leo.</p><p>Vivamus sodales sit amet elit et volutpat. Pellentesque urna ante, posuere id pharetra ut, sollicitudin eget massa. Duis accumsan eleifend dolor nec blandit. Morbi ac est sed dui ultricies mollis et nec est. Phasellus at eros ipsum. Vivamus sodales turpis non cursus dictum. Etiam sed metus congue, lobortis tortor ac, molestie orci. Nulla feugiat pulvinar lorem, vitae pulvinar mi accumsan ut. Maecenas lobortis dapibus lorem sed pretium. Integer pharetra posuere tempor. Proin in mauris nunc. Curabitur ullamcorper luctus lectus. Morbi quam nisi, luctus in semper eu, pharetra sit amet neque. Vestibulum quis est commodo, maximus magna sed, suscipit nisi. Morbi ultricies, quam in viverra ullamcorper, nisi lorem ullamcorper quam, nec scelerisque lacus dolor sit amet elit.</p><p>Donec semper nulla vel nunc eleifend commodo. Donec lobortis sem eget diam gravida, nec consectetur massa pharetra. Aenean blandit interdum mauris, non feugiat orci iaculis ac. Nulla quis blandit elit. Curabitur tortor sem, cursus nec dui id, ultrices elementum est. Cras accumsan felis felis, ut feugiat magna vehicula at. Pellentesque neque justo, hendrerit vel nisl id, malesuada dictum nisi. Aenean rutrum sem ut urna semper tempor et vel sapien. Praesent in risus diam. Nulla volutpat elementum laoreet. In molestie neque volutpat, aliquam turpis vitae, ornare dui. In eleifend euismod massa.</p>\r\n\r\n<br></p>',NULL,NULL,NULL,'2015-09-25 16:23:57','2015-09-25 16:23:57',1),(13,'Nossos','Produtos','produtos','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nibh metus, tincidunt at risus eu, sollicitudin vehicula purus. Ut tempor nisi arcu, eu congue diam pellentesque vel. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Interdum et malesuada fames ac ante ipsum primis in faucibus','<p>\r\n\r\n</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nibh metus, tincidunt at risus eu, sollicitudin vehicula purus. Ut tempor nisi arcu, eu congue diam pellentesque vel. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Phasellus scelerisque elit risus, et placerat lacus feugiat ut. Donec eu porta turpis. Aenean vehicula ante quis ante faucibus ornare. Nullam placerat suscipit lectus, ut sagittis tellus fringilla a.</p><p>Nunc felis erat, lobortis et luctus sed, blandit eget enim. Nunc volutpat vestibulum purus non blandit. Suspendisse sollicitudin blandit ultricies. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sit amet nisi eu diam suscipit consequat. Duis lorem elit, aliquet quis leo in, egestas bibendum risus. Ut finibus maximus massa, non suscipit massa sodales id. Integer vehicula, enim a vehicula fermentum, enim quam faucibus erat, in sodales purus purus in leo. Ut scelerisque felis et lorem efficitur tincidunt. Sed iaculis at quam eget tincidunt. Vestibulum ut blandit nunc, id ornare sapien. Etiam varius ex ac magna sodales consectetur. Maecenas dolor nisi, suscipit vel sagittis ac, facilisis et nisi. Phasellus feugiat enim nec nulla molestie, a rhoncus nisi sollicitudin. Nullam vel malesuada velit, nec ullamcorper tellus. Nullam ac nunc nisi.</p><p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla maximus vel ex id tincidunt. Integer venenatis egestas feugiat. Ut et lorem dapibus, aliquet nibh et, iaculis ligula. Etiam sed iaculis justo, a viverra leo. Donec lacinia porta turpis, tempus pharetra justo vestibulum in. Proin non nunc velit. In hac habitasse platea dictumst. Integer iaculis dolor ac molestie venenatis.</p><p>Donec quis risus sit amet magna luctus pretium in et purus. Nullam id interdum quam. Aliquam posuere justo augue, non sagittis mauris mollis sed. Nulla eget augue elit. Quisque dui turpis, dapibus at ex varius, tincidunt dapibus nulla. Praesent eget velit eu diam mattis congue nec sit amet mauris. Curabitur purus metus, rutrum in tellus vitae, ornare tincidunt augue. Integer non faucibus lacus. Aliquam non dui metus. Nam eu erat at sapien venenatis ornare non at mauris. Integer sagittis dolor quam, in feugiat leo sagittis id. Maecenas id laoreet nibh. Curabitur tempor a odio sit amet egestas.</p><p>Aenean ut nibh in leo faucibus condimentum. Vestibulum efficitur massa id ex semper, eget hendrerit leo mattis. Phasellus vel mauris sed turpis cursus laoreet. Proin augue felis, dignissim sit amet maximus eu, lobortis eget lorem. Nulla sed turpis tempus, venenatis nulla eget, mattis turpis. Integer iaculis urna quam, vitae volutpat nulla sollicitudin ut. Etiam cursus et mi eget mollis. Cras nec varius urna, non ullamcorper nulla. Mauris luctus in est a luctus. Pellentesque imperdiet augue ut auctor eleifend. Quisque rhoncus augue vel magna dignissim fringilla. Vestibulum posuere ullamcorper lorem, eu congue elit suscipit non.</p>\r\n\r\n<br><p></p>','Divulgação','5630180906513.jpeg','Lorem ipsum dolor sit amet, consectetur adipiscing elit','2015-10-27 21:34:17','2015-10-27 21:34:17',1),(14,'Nossas','Receitas','receitas','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis cursus ex nulla, ut pellentesque sem ultrices sit amet. Phasellus vehicula arcu arcu, eget pretium sapien malesuada et. Integer et ligula id metus condimentum accumsan. Integer imperdiet magna vitae bibendum tempor','<p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis cursus ex nulla, ut pellentesque sem ultrices sit amet. Phasellus vehicula arcu arcu, eget pretium sapien malesuada et. Integer et ligula id metus condimentum accumsan. Integer imperdiet magna vitae bibendum tempor. Mauris in accumsan augue, non suscipit turpis. Pellentesque cursus augue sit amet leo volutpat, ut volutpat erat volutpat. Aliquam sit amet nulla vel diam imperdiet varius in id dui. Aliquam suscipit consectetur nunc eu pharetra.</p><p>Fusce vulputate augue id enim mollis, rutrum commodo libero dapibus. Aliquam quis volutpat nunc. Donec aliquam ligula vel nisi elementum, a egestas purus ornare. Vivamus rhoncus ornare fermentum. In ac augue quis eros consectetur tincidunt. Duis volutpat pulvinar gravida. Donec id metus nisi.</p><p>Curabitur vestibulum sagittis nisl, eu elementum tortor eleifend ut. Suspendisse aliquet nisl quis velit dignissim, quis dignissim sem accumsan. Pellentesque laoreet est non odio suscipit, in viverra nulla laoreet. Suspendisse ornare tincidunt justo, quis tincidunt quam ullamcorper sit amet. Praesent ultricies ligula et orci congue, eget tincidunt sapien tincidunt. Aenean ornare, nibh ac consectetur rhoncus, sapien risus tristique enim, id auctor tortor dui eget sem. Donec ut tempor dolor, nec ornare enim. Nullam rhoncus porta risus eu ornare. Sed elementum ipsum et rutrum placerat. In ut sapien non justo imperdiet pellentesque a vel ex. Nam urna est, maximus eu euismod ac, lobortis a ipsum. Sed mauris purus, consectetur non efficitur eu, blandit a ipsum.</p><p>Vestibulum non condimentum mauris, a cursus massa. Curabitur viverra interdum sem. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc iaculis tellus leo, et posuere dolor tincidunt vitae. Sed scelerisque elit et diam aliquet eleifend. Ut ut porttitor enim. Nulla efficitur suscipit gravida. In nisl lorem, commodo quis interdum quis, interdum in justo. Cras volutpat dignissim tortor quis fringilla. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras quis bibendum quam. Morbi libero elit, dictum a rutrum quis, gravida sed metus. Mauris a sem condimentum, aliquam tortor et, varius felis. Sed feugiat venenatis imperdiet. Cras condimentum eu enim sed interdum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p><p>Sed euismod ac arcu ac tristique. Vestibulum ut convallis libero, eget cursus ipsum. Nullam fringilla ante nec lobortis porttitor. Pellentesque volutpat lacinia metus, quis tincidunt lacus pharetra vitae. Duis felis orci, accumsan sed felis at, semper posuere nibh. Cras id dolor sit amet nisl tempus tempus ut non lorem. Proin sagittis erat sit amet vestibulum scelerisque. Nulla erat ex, finibus non egestas ut, auctor ac odio. Nullam condimentum dui vitae varius accumsan. Phasellus vel arcu et augue blandit efficitur in ut turpis. Ut placerat dictum ultricies. Donec tincidunt faucibus lectus eu imperdiet. Aliquam tristique, magna at tincidunt rhoncus, erat lectus fermentum massa, vel sagittis nulla elit fringilla magna. Sed laoreet nibh dui, ac egestas quam euismod in. Fusce vel elementum tortor, vel feugiat sem. Integer erat velit, elementum varius posuere sit amet, bibendum sit amet metus.</p>\r\n\r\n<br></p>','Divulgação','5630395d80fb2.jpeg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis cursus ex nulla','2015-10-27 23:56:29','2015-10-27 23:56:29',1);
/*!40000 ALTER TABLE `paginas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoas`
--

DROP TABLE IF EXISTS `pessoas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `tipo_cliente` int(11) DEFAULT NULL,
  `nome_fantasia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnpj` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cpf` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inscricao_estadual` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inscricao_municipal` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `endereco` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `complemento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bairro` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cep` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cidade` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_18A4F2AC92FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_18A4F2ACA0D96FBF` (`email_canonical`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoas`
--

LOCK TABLES `pessoas` WRITE;
/*!40000 ALTER TABLE `pessoas` DISABLE KEYS */;
INSERT INTO `pessoas` VALUES (1,'Allan Roberto','allan roberto','allanroberto18@gmail.com','allanroberto18@gmail.com',1,'supnvahay4gwgksw4o4ccc44ck0k4w4','$2y$13$supnvahay4gwgksw4o4ccOGxGKrNnqAHnpRzSCcGyb47au.cZIcYa','2015-11-02 04:19:51',0,0,NULL,'tdrCOteN6J9s1-hgVodFSPxGgOOVgDOVV12YWRxR0oQ',NULL,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}',0,NULL,2,NULL,NULL,NULL,'929.692.081-72',NULL,NULL,'405 Sul, Alameda 32, Lote 01, Bloco 02, Apartamento 24',NULL,'Plano Diretor Sul','77.015-648','TO','Palmas'),(2,'Bruce Ambrosio','bruce ambrosio','contato@agencialumia.com.br','contato@agencialumia.com.br',1,'85ue8uln2944ck0occs0s0gg044kwg4','$2y$13$85ue8uln2944ck0occs0suZ4CqJepUYSrQGNrBtX0lAFmLX9bH1RO','2015-10-13 20:40:33',0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'usuario teste','usuario teste','usuario@teste.com.br','usuario@teste.com.br',0,'nzw7525b9dwggwocskgkos0s80owsgs','$2y$13$nzw7525b9dwggwocskgkoeHHWLJgiHr0YaGsLCpWAK702U99dBUS6',NULL,0,0,NULL,'vgIsWQrIaE3Bkfn8wKqO_ZGyPhVH2zwnjOt8brFetto',NULL,'a:0:{}',0,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'usuario1 teste','usuario1 teste','usuario1@teste.com.br','usuario1@teste.com.br',0,'oi22g8dmctcko4kcwwgcc8kokk4cs0c','$2y$13$oi22g8dmctcko4kcwwgccuqAnQsJ9571xZeCP6HcXUUBS5ytk4pwK',NULL,0,0,NULL,'-M820HOh9HZOZijP_Yqc49G-5wRADvOwQL-Yn_53nZg',NULL,'a:0:{}',0,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'usuario2 teste','usuario2 teste','usuario2@teste.com.br','usuario2@teste.com.br',1,'tfwhe2odzuow48848ogc48gccsowo4w','$2y$13$tfwhe2odzuow48848ogc4uWkYYY7xQOZ9Tp1TK8kDF.JSY75Vzuja','2015-10-14 21:12:21',0,0,NULL,'XDU9d7m0oa43PINuHtdmv0yAvloO-0gQ5fNbXHZ0YuI',NULL,'a:0:{}',0,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `pessoas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `planos`
--

DROP TABLE IF EXISTS `planos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `planos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `parcelas` int(11) NOT NULL,
  `valor` float(10,2) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `planos`
--

LOCK TABLES `planos` WRITE;
/*!40000 ALTER TABLE `planos` DISABLE KEYS */;
/*!40000 ALTER TABLE `planos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtos`
--

DROP TABLE IF EXISTS `produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria_produto_id` int(11) DEFAULT NULL,
  `slug` varchar(250) NOT NULL,
  `titulo` varchar(250) NOT NULL,
  `resumo` varchar(500) DEFAULT NULL,
  `texto` text,
  `image_name` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_produto_categoria_idx_1` (`categoria_produto_id`),
  CONSTRAINT `FK_produto_categoria_1` FOREIGN KEY (`categoria_produto_id`) REFERENCES `categorias_produtos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtos`
--

LOCK TABLES `produtos` WRITE;
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;
INSERT INTO `produtos` VALUES (1,1,'coxa_e_sobre_coxa','Coxa e Sobre Coxa',NULL,NULL,'56091909349f4.jpeg','2015-09-28 07:39:27','2015-09-28 07:40:09',1);
/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receitas`
--

DROP TABLE IF EXISTS `receitas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receitas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produto_id` int(11) DEFAULT NULL,
  `retranca` varchar(75) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `resumo` varchar(500) DEFAULT NULL,
  `texto` text,
  `credito` varchar(75) DEFAULT NULL,
  `legenda` varchar(255) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_produto_idx_1` (`produto_id`),
  CONSTRAINT `fk_produto_id_1` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receitas`
--

LOCK TABLES `receitas` WRITE;
/*!40000 ALTER TABLE `receitas` DISABLE KEYS */;
INSERT INTO `receitas` VALUES (1,1,'Lorem ipsum','Lorem ipsum dolor sit amet, consectetur adipiscing elit','lorem_ipsum_dolor_sit_amet_consectetur_adipiscing_elit','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut placerat bibendum mollis. Phasellus finibus elit nulla, non aliquam urna fermentum nec. Maecenas tincidunt pulvinar dapibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas','<p>\r\n\r\n</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut placerat bibendum mollis. Phasellus finibus elit nulla, non aliquam urna fermentum nec. Maecenas tincidunt pulvinar dapibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque velit lorem, convallis quis vestibulum nec, tincidunt in turpis. Quisque nec mollis turpis. Aenean vulputate a lorem ut porttitor. Mauris eu fringilla urna. Nam in libero purus. Curabitur dui orci, pulvinar vitae erat eget, bibendum mattis magna. Praesent nec leo pretium, viverra nibh molestie, pellentesque sapien. Etiam eleifend elementum orci eget rutrum. Duis ultrices, ante eu iaculis fermentum, risus metus vulputate metus, ac lacinia purus urna sit amet orci. Mauris blandit massa ac dignissim facilisis. Vestibulum vel ex posuere, pretium enim sit amet, varius diam. Morbi elit massa, euismod fermentum lobortis id, mollis eget dui.</p><p>Morbi lobortis eget metus at lacinia. Ut sed pellentesque metus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean porta nulla sit amet dapibus tempor. Pellentesque nibh ex, lobortis quis lacus in, convallis malesuada mauris. Phasellus vulputate turpis ac metus pulvinar faucibus. Nunc id sem et mauris elementum vestibulum. Mauris consequat in tortor eu auctor. Phasellus quis dictum libero. Donec lectus felis, rutrum sed viverra nec, cursus non tellus. Nam pellentesque nunc nec rhoncus consectetur. Donec varius felis vitae turpis sollicitudin, ut tristique nisl malesuada.</p><p>Praesent lorem nunc, luctus sed ex ac, fermentum sollicitudin est. Mauris id semper risus. Duis pharetra sed urna quis rhoncus. Aliquam ut metus ultricies, bibendum massa sed, volutpat sapien. Sed vel feugiat lacus. Duis porta iaculis commodo. Nullam at placerat massa. Nunc at ornare lorem, quis sodales metus. Cras auctor libero nulla, nec posuere dolor sodales a. Etiam fringilla felis ligula, laoreet vehicula lacus dapibus pulvinar. Maecenas nec ex sit amet justo placerat dapibus. Sed finibus vestibulum magna nec aliquet. Donec ex sem, sagittis ac nisi eu, consectetur egestas purus.</p><p>Nulla lobortis lacus ut orci eleifend, nec scelerisque urna ornare. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris at nibh porta, vulputate nibh ut, rutrum eros. Integer massa erat, elementum nec vehicula vitae, efficitur commodo eros. Integer laoreet, est ut ornare tempor, neque justo aliquam libero, quis venenatis nulla felis et nulla. Nam non convallis felis. Mauris non sodales lectus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque hendrerit dictum mattis. Nulla sit amet dolor nec mi molestie vestibulum eget ut urna. Nam tortor dolor, elementum vel posuere sed, dictum a tellus. Proin sodales erat sed quam lacinia laoreet. Nulla facilisi. Praesent quis nibh et justo pellentesque pretium sed quis mi.</p><p>Morbi dapibus vel ante in bibendum. Vestibulum pulvinar bibendum libero in vehicula. Integer non quam sit amet dui sagittis dignissim. Proin ut ornare libero. Donec vel orci vel mi consectetur venenatis ac sed libero. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc ante dui, blandit eget condimentum et, accumsan vel erat. Sed non scelerisque mauris. Aenean pharetra purus sed ipsum finibus vestibulum. Suspendisse convallis sed lectus vitae convallis. Donec vestibulum justo sed mi finibus pretium.</p>\r\n\r\n<br><p></p>','Divulgação','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut placerat bibendum mollis. Phasellus finibus elit nulla, non aliquam urna fermentum nec. Maecenas tincidunt pulvinar dapibus. Pellentesque habitant morbi tristique senectus et netus et malesuada f','56091b4e969ac.png','2015-09-28 07:49:50','2015-09-28 07:49:50',1),(2,1,'Lorem ipsum','Lorem ipsum dolor sit amet','lorem_ipsum_dolor_sit_amet','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut placerat bibendum mollis. Phasellus finibus elit nulla, non aliquam urna fermentum nec. Maecenas tincidunt pulvinar dapibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas','<p>\r\n\r\n</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut placerat bibendum mollis. Phasellus finibus elit nulla, non aliquam urna fermentum nec. Maecenas tincidunt pulvinar dapibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque velit lorem, convallis quis vestibulum nec, tincidunt in turpis. Quisque nec mollis turpis. Aenean vulputate a lorem ut porttitor. Mauris eu fringilla urna. Nam in libero purus. Curabitur dui orci, pulvinar vitae erat eget, bibendum mattis magna. Praesent nec leo pretium, viverra nibh molestie, pellentesque sapien. Etiam eleifend elementum orci eget rutrum. Duis ultrices, ante eu iaculis fermentum, risus metus vulputate metus, ac lacinia purus urna sit amet orci. Mauris blandit massa ac dignissim facilisis. Vestibulum vel ex posuere, pretium enim sit amet, varius diam. Morbi elit massa, euismod fermentum lobortis id, mollis eget dui.</p><p>Morbi lobortis eget metus at lacinia. Ut sed pellentesque metus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean porta nulla sit amet dapibus tempor. Pellentesque nibh ex, lobortis quis lacus in, convallis malesuada mauris. Phasellus vulputate turpis ac metus pulvinar faucibus. Nunc id sem et mauris elementum vestibulum. Mauris consequat in tortor eu auctor. Phasellus quis dictum libero. Donec lectus felis, rutrum sed viverra nec, cursus non tellus. Nam pellentesque nunc nec rhoncus consectetur. Donec varius felis vitae turpis sollicitudin, ut tristique nisl malesuada.</p><p>Praesent lorem nunc, luctus sed ex ac, fermentum sollicitudin est. Mauris id semper risus. Duis pharetra sed urna quis rhoncus. Aliquam ut metus ultricies, bibendum massa sed, volutpat sapien. Sed vel feugiat lacus. Duis porta iaculis commodo. Nullam at placerat massa. Nunc at ornare lorem, quis sodales metus. Cras auctor libero nulla, nec posuere dolor sodales a. Etiam fringilla felis ligula, laoreet vehicula lacus dapibus pulvinar. Maecenas nec ex sit amet justo placerat dapibus. Sed finibus vestibulum magna nec aliquet. Donec ex sem, sagittis ac nisi eu, consectetur egestas purus.</p><p>Nulla lobortis lacus ut orci eleifend, nec scelerisque urna ornare. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris at nibh porta, vulputate nibh ut, rutrum eros. Integer massa erat, elementum nec vehicula vitae, efficitur commodo eros. Integer laoreet, est ut ornare tempor, neque justo aliquam libero, quis venenatis nulla felis et nulla. Nam non convallis felis. Mauris non sodales lectus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque hendrerit dictum mattis. Nulla sit amet dolor nec mi molestie vestibulum eget ut urna. Nam tortor dolor, elementum vel posuere sed, dictum a tellus. Proin sodales erat sed quam lacinia laoreet. Nulla facilisi. Praesent quis nibh et justo pellentesque pretium sed quis mi.</p><p>Morbi dapibus vel ante in bibendum. Vestibulum pulvinar bibendum libero in vehicula. Integer non quam sit amet dui sagittis dignissim. Proin ut ornare libero. Donec vel orci vel mi consectetur venenatis ac sed libero. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc ante dui, blandit eget condimentum et, accumsan vel erat. Sed non scelerisque mauris. Aenean pharetra purus sed ipsum finibus vestibulum. Suspendisse convallis sed lectus vitae convallis. Donec vestibulum justo sed mi finibus pretium.</p>\r\n\r\n<br><p></p>','Divulgação','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut placerat bibendum mollis. Phasellus finibus elit nulla, non aliquam urna fermentum nec. Maecenas tincidunt pulvinar dapibus. Pellentesque habitant morbi tristique senectus et netus et malesuada f','56092a890c280.png','2015-09-28 08:54:49','2015-09-28 08:54:49',1);
/*!40000 ALTER TABLE `receitas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `retranca` varchar(255) DEFAULT NULL,
  `titulo` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `resumo` varchar(500) NOT NULL,
  `link` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-09  0:03:06

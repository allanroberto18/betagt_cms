<?php

namespace BetaGT\Bundles\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * GaleriasItens
 * @Vich\Uploadable
 * @ORM\Table(name="galerias_itens", indexes={@ORM\Index(name="fk_galeria_item_galeria_id_idx", columns={"galeria_id"})})
 * @ORM\Entity
 */
class GaleriasItens extends EntityMaster
{
    use TFoto;

    /**
     * @var File
     * @Vich\UploadableField(mapping="galeria_item_image", fileNameProperty="imageName")
     */
    private $imageFile;

    /**
     * @var \Galerias
     *
     * @ORM\ManyToOne(targetEntity="Galerias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="galeria_id", referencedColumnName="id")
     * })
     */
    private $galeria;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updated = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set galeria
     *
     * @param \BetaGT\Bundles\CMSBundle\Entity\Galerias $galeria
     * @return GaleriasItens
     */
    public function setGaleria(\BetaGT\Bundles\CMSBundle\Entity\Galerias $galeria = null)
    {
        $this->galeria = $galeria;

        return $this;
    }

    /**
     * Get galeria
     *
     * @return \BetaGT\Bundles\CMSBundle\Entity\Galerias
     */
    public function getGaleria()
    {
        return $this->galeria;
    }
}

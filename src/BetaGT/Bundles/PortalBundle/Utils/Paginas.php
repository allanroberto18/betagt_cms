<?php
/**
 * Created by PhpStorm.
 * User: allan
 * Date: 08/11/2015
 * Time: 21:21
 */

namespace BetaGT\Bundles\PortalBundle\Utils;

use Doctrine\ORM\EntityManager;

class Paginas
{
    private $entityManager;

    /**
     * ServiceMaster constructor.
     * @param $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function returnRegister($id)
    {
        return $this->entityManager->getRepository('CMSBundle:Paginas')->find($id);
    }
}
<?php

namespace BetaGT\Bundles\CMSBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CategoriasControllerTest extends WebTestCase
{
    public function testNovo()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Categorias/novo');
    }

    public function testUpdate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Categorias/{id}/atualizar');
    }

    public function testList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Categorias/listar');
    }

    public function testView()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Categorias/{id}/visualizar');
    }

    public function testDelete()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Categorias/{id}delete');
    }

}

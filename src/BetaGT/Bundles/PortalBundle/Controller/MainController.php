<?php

namespace BetaGT\Bundles\PortalBundle\Controller;

use BetaGT\Bundles\CMSBundle\Entity\Contatos;
use BetaGT\Bundles\CMSBundle\Form\ContatosFrontType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{
    protected function formContato()
    {
        $entity = new Contatos();

        $form = $this->createForm(new ContatosFrontType(), $entity, [ 'attr' => [ 'class' => 'form-horizontal', 'novalidate' => 'novalidate' ] ]);

        return $form->createView();
    }
}

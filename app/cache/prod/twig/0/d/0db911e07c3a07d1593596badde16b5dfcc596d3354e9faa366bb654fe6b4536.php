<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_1bd9a025d59a758ff68cc5dc299991172f3784030631f322922b318d2c507347 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@CMS/Assuntos/form.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'body_footer' => array($this, 'block_body_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@CMS/Assuntos/form.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["titulo"] = "Novo Usuário";
        // line 4
        $context["modulo"] = array();
        // line 5
        $context["modulo"] = twig_array_merge((isset($context["modulo"]) ? $context["modulo"] : null), array("titulo" => "Novo usuário", "descricao" => "Dados do Usuário"));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        // line 8
        $this->loadTemplate("FOSUserBundle:Registration:register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 8)->display($context);
    }

    // line 11
    public function block_body_footer($context, array $blocks = array())
    {
        // line 12
        echo "    <a href=\"";
        echo $this->env->getExtension('routing')->getPath("cms_pessoas_listar");
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-list-ul\"></i> Listar Usuários</a>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 12,  43 => 11,  39 => 8,  36 => 7,  32 => 1,  30 => 5,  28 => 4,  26 => 3,  11 => 1,);
    }
}
/* {% extends "@CMS/Assuntos/form.html.twig" %}*/
/* */
/* {% set titulo = 'Novo Usuário' %}*/
/* {% set modulo = [] %}*/
/* {% set modulo = modulo|merge({'titulo': 'Novo usuário', 'descricao' : 'Dados do Usuário'}) %}*/
/* */
/* {% block body %}*/
/* {% include "FOSUserBundle:Registration:register_content.html.twig" %}*/
/* {% endblock %}*/
/* */
/* {% block body_footer %}*/
/*     <a href="{{ path('cms_pessoas_listar') }}" class="btn btn-default"><i class="fa fa-list-ul"></i> Listar Usuários</a>*/
/* {% endblock %}*/
/* */

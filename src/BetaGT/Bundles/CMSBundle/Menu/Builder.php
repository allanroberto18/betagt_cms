<?php
/**
 * Created by PhpStorm.
 * User: allan
 * Date: 27/08/2015
 * Time: 12:50
 */

namespace BetaGT\Bundles\CMSBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array$options)
    {
        $menu = $factory->createItem('root', array(
                'childrenAttributes' => array(
                    'class' => 'sidebar-menu',
                ),
            )
        );

        $menu->addChild('Header', array('label' => 'Menu Principal'))->setAttribute('class', 'header');

        $menu->addChild('Home', array('label' => 'Home', 'route' => 'cms_home'))->setAttributes(array('class' => '','icon' => 'fa fa-dashboard'));

        $menu->addChild('Header2', array('label' => 'Contatos'))->setAttribute('class', 'header');
        $menu->addChild('Assuntos', array('label' => 'Assuntos'))->setAttribute('class', 'treeview');
        $menu['Assuntos']->setUri('#');
        $menu['Assuntos']->setChildrenAttribute('class', "treeview-menu")->setAttributes(array('class' => 'treeview' , 'icon' => 'fa fa-bookmark'));
        $menu['Assuntos']->addChild('cms_assuntos_listar', array('route' => 'cms_assuntos_listar', 'label' => 'Listar Registros'))->setAttribute('icon', 'fa fa-list-alt');
        $menu['Assuntos']->addChild('cms_assuntos_novo', array('route' => 'cms_assuntos_novo', 'label' => 'Novo Registro'))->setAttribute('icon', 'fa fa-plus');

        $menu->addChild('Contatos', array('label' => 'Contatos'))->setAttribute('class', 'treeview');
        $menu['Contatos']->setUri('#');
        $menu['Contatos']->setChildrenAttribute('class', "treeview-menu")->setAttributes(array('class' => 'treeview' , 'icon' => 'fa fa-bullhorn'));
        $menu['Contatos']->addChild('cms_contatos_listar', array('route' => 'cms_contatos_listar', 'label' => 'Listar Registros'))->setAttribute('icon', 'fa fa-list-alt');
        $menu['Contatos']->addChild('cms_contatos_novo', array('route' => 'cms_contatos_novo', 'label' => 'Novo Registro'))->setAttribute('icon', 'fa fa-plus');

        $menu->addChild('Header3', array('label' => 'Conteúdo'))->setAttribute('class', 'header');
        $menu->addChild('Banners', array('label' => 'Banners'))->setAttribute('class', 'treeview');
        $menu['Banners']->setUri('#');
        $menu['Banners']->setChildrenAttribute('class', "treeview-menu")->setAttributes(array('class' => 'treeview' , 'icon' => 'fa fa-tag'));
        $menu['Banners']->addChild('cms_banners_listar', array('route' => 'cms_banners_listar', 'label' => 'Listar Registros'))->setAttribute('icon', 'fa fa-list-alt');
        $menu['Banners']->addChild('cms_banners_novo', array('route' => 'cms_banners_novo', 'label' => 'Novo Registro'))->setAttribute('icon', 'fa fa-plus');

        $menu->addChild('Categorias', array('label' => 'Categorias'));
        $menu['Categorias']->setUri('#');
        $menu['Categorias']->setChildrenAttribute('class', "treeview-menu")->setAttributes(array('class' => 'treeview', 'icon' => 'fa fa-bookmark'));
        $menu['Categorias']->addChild('cms_categorias_listar', array('route' => 'cms_categorias_listar', 'label' => 'Listar Registros'))->setAttribute('icon', 'fa fa-list-alt');
        $menu['Categorias']->addChild('cms_categorias_novo', array('route' => 'cms_categorias_novo', 'label' => 'Novo Registro'))->setAttribute('icon', 'fa fa-plus');

        $menu->addChild('Galerias', array('label' => 'Galerias de Fotos'));
        $menu['Galerias']->setUri('#');
        $menu['Galerias']->setChildrenAttribute('class', "treeview-menu")->setAttributes(array('class' => 'treeview', 'icon' => 'fa fa-photo'));
        $menu['Galerias']->addChild('cms_galerias_listar', array('route' => 'cms_galerias_listar', 'label' => 'Listar Registros'))->setAttribute('icon', 'fa fa-list-alt');
        $menu['Galerias']->addChild('cms_galerias_novo', array('route' => 'cms_galerias_novo', 'label' => 'Novo Registro'))->setAttribute('icon', 'fa fa-plus');

        $menu->addChild('AreasPublicacoes', array('label' => 'Áreas de Publicações'));
        $menu['AreasPublicacoes']->setUri('#');
        $menu['AreasPublicacoes']->setChildrenAttribute('class', "treeview-menu")->setAttributes(array('class' => 'treeview', 'icon' => 'fa fa-tags'));
        $menu['AreasPublicacoes']->addChild('cms_areas_publicacoes_listar', array('route' => 'cms_areas_publicacoes_listar', 'label' => 'Listar Registros'))->setAttribute('icon', 'fa fa-list-alt');
        $menu['AreasPublicacoes']->addChild('cms_areas_publicacoes_novo', array('route' => 'cms_areas_publicacoes_novo', 'label' => 'Novo Registro'))->setAttribute('icon', 'fa fa-plus');

        $menu->addChild('Noticias', array('label' => 'Notícias'));
        $menu['Noticias']->setUri('#');
        $menu['Noticias']->setChildrenAttribute('class', "treeview-menu")->setAttributes(array('class' => 'treeview', 'icon' => 'fa fa-book'));
        $menu['Noticias']->addChild('cms_noticias_listar', array('route' => 'cms_noticias_listar', 'label' => 'Listar Registros'))->setAttribute('icon', 'fa fa-list-alt');
        $menu['Noticias']->addChild('cms_noticias_novo', array('route' => 'cms_noticias_novo', 'label' => 'Novo Registro'))->setAttribute('icon', 'fa fa-plus');

        $menu->addChild('Videos', array('label' => 'Videos'));
        $menu['Videos']->setUri('#');
        $menu['Videos']->setChildrenAttribute('class', "treeview-menu")->setAttributes(array('class' => 'treeview', 'icon' => 'fa fa-video-camera'));
        $menu['Videos']->addChild('cms_videos_listar', array('route' => 'cms_videos_listar', 'label' => 'Listar Registros'))->setAttribute('icon', 'fa fa-list-alt');
        $menu['Videos']->addChild('cms_videos_novo', array('route' => 'cms_videos_novo', 'label' => 'Novo Registro'))->setAttribute('icon', 'fa fa-plus');

        $menu->addChild('Paginas', array('label' => 'Páginas'));
        $menu['Paginas']->setUri('#');
        $menu['Paginas']->setChildrenAttribute('class', "treeview-menu")->setAttributes(array('class' => 'treeview', 'icon' => 'fa fa-briefcase'));
        $menu['Paginas']->addChild('cms_paginas_listar', array('route' => 'cms_paginas_listar', 'label' => 'Listar Registros'))->setAttribute('icon', 'fa fa-list-alt');
        $menu['Paginas']->addChild('cms_paginas_novo', array('route' => 'cms_paginas_novo', 'label' => 'Novo Registro'))->setAttribute('icon', 'fa fa-plus');

        $menu->addChild('Header4', array('label' => 'Frango Norte'))->setAttribute('class', 'header');

        $menu->addChild('CategoriasProdutos', array('label' => 'Categorias/Produtos'));
        $menu['CategoriasProdutos']->setUri('#');
        $menu['CategoriasProdutos']->setChildrenAttribute('class', "treeview-menu")->setAttributes(array('class' => 'treeview', 'icon' => 'fa fa-bookmark'));
        $menu['CategoriasProdutos']->addChild('pro_categorias_produtos_listar', array('route' => 'pro_categorias_produtos_listar', 'label' => 'Listar Registros'))->setAttribute('icon', 'fa fa-list-alt');
        $menu['CategoriasProdutos']->addChild('pro_categorias_produtos_novo', array('route' => 'pro_categorias_produtos_novo', 'label' => 'Novo Registro'))->setAttribute('icon', 'fa fa-plus');

        $menu->addChild('Produto', array('label' => 'Produtos'));
        $menu['Produto']->setUri('#');
        $menu['Produto']->setChildrenAttribute('class', "treeview-menu")->setAttributes(array('class' => 'treeview', 'icon' => 'fa fa-truck'));
        $menu['Produto']->addChild('pro_produtos_listar', array('route' => 'pro_produtos_listar', 'label' => 'Listar Registros'))->setAttribute('icon', 'fa fa-list-alt');
        $menu['Produto']->addChild('pro_produtos_novo', array('route' => 'pro_produtos_novo', 'label' => 'Novo Registro'))->setAttribute('icon', 'fa fa-plus');

        $menu->addChild('Header5', array('label' => 'Financeiro'))->setAttribute('class', 'header');
        $menu->addChild('Planos', array('label' => 'Planos/Preços'));
        $menu['Planos']->setUri('#');
        $menu['Planos']->setChildrenAttribute('class', "treeview-menu")->setAttributes(array('class' => 'treeview', 'icon' => 'fa fa-book'));
        $menu['Planos']->addChild('fin_planos_listar', array('route' => 'fin_planos_listar', 'label' => 'Listar Registros'))->setAttribute('icon', 'fa fa-list-alt');
        $menu['Planos']->addChild('fin_planos_novo', array('route' => 'fin_planos_novo', 'label' => 'Novo Registro'))->setAttribute('icon', 'fa fa-plus');

        $menu->addChild('Empresas', array('label' => 'Dados da Empresa'));
        $menu['Empresas']->setUri('#');
        $menu['Empresas']->setChildrenAttribute('class', "treeview-menu")->setAttributes(array('class' => 'treeview', 'icon' => 'fa fa-bank'));
        $menu['Empresas']->addChild('fin_empresas_listar', array('route' => 'fin_empresas_listar', 'label' => 'Listar Registros'))->setAttribute('icon', 'fa fa-list-alt');
        $menu['Empresas']->addChild('fin_empresas_novo', array('route' => 'fin_empresas_novo', 'label' => 'Novo Registro'))->setAttribute('icon', 'fa fa-plus');

        $menu->addChild('Header9', array('label' => 'Segurança'))->setAttribute('class', 'header');
        $menu->addChild('Usuarios', array('label' => 'Usuários'));
        $menu['Usuarios']->setUri('#');
        $menu['Usuarios']->setChildrenAttribute('class', "treeview-menu")->setAttributes(array('class' => 'treeview', 'icon' => 'fa fa-users'));
        $menu['Usuarios']->addChild('cms_pessoas_listar', array('route' => 'cms_pessoas_listar', 'label' => 'Listar Usuários'))->setAttribute('icon', 'fa fa-list-alt');
        $menu['Usuarios']->addChild('fos_user_profile_show', array('route' => 'fos_user_profile_show', 'label' => 'Perfil'))->setAttribute('icon', 'fa fa-user');
        $menu['Usuarios']->addChild('fos_user_profile_edit', array('route' => 'fos_user_profile_edit', 'label' => 'Atualizar Perfil'))->setAttribute('icon', 'fa fa-pencil');
        $menu['Usuarios']->addChild('fos_user_change_password', array('route' => 'fos_user_change_password', 'label' => 'Atualizar Senha'))->setAttribute('icon', 'fa fa-unlock-alt');

        $menu->addChild('Sair', array('route' => 'fos_user_security_logout'))->setAttributes(array('class' => '', 'icon' => 'fa fa-sign-out'));

        return $menu;
    }

}
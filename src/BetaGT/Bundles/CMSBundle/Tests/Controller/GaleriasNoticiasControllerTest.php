<?php

namespace BetaGT\Bundles\CMSBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GaleriasNoticiasControllerTest extends WebTestCase
{
    public function testNovo()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Noticias/{id}/Galerias/novo');
    }

    public function testUpdate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Noticias/{id}/Galerias/{idGaleria}/atualizar');
    }

    public function testList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Noticias/{id}/Galerias/listar');
    }

    public function testView()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Noticias/{id}/Galerias/{idGaleria}/visualizar');
    }

    public function testDelete()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Noticias/{id}/Galerias/{idGaleria}/delete');
    }

}

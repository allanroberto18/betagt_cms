<?php

namespace BetaGT\Bundles\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Noticias
 * @Vich\Uploadable
 * @ORM\Table(name="noticias", indexes={@ORM\Index(name="fk_solucoes_categoria_id_idx", columns={"categoria_id"})})
 * @ORM\Entity
 */
class Noticias extends EntityMaster
{
    use TSlug, TFoto;
    /**
     * @var string
     *
     * @ORM\Column(name="retranca", type="string", length=75, nullable=true)
     */
    private $retranca;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Título é obrigatório")
     * @ORM\Column(name="titulo", type="string", length=255, nullable=true)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="resumo", type="string", length=500, nullable=true)
     */
    private $resumo;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Texto é obrigatório")
     * @ORM\Column(name="texto", type="text", nullable=true)
     */
    private $texto;

    /**
     * @var File
     * @Vich\UploadableField(mapping="noticia_image", fileNameProperty="imageName")
     */
    private $imageFile;

    /**
     * @var \Categorias
     * @Assert\NotBlank(message="O campo Categoria é obrigatório")
     * @ORM\ManyToOne(targetEntity="Categorias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     * })
     */
    private $categoria;

    /**
     * @var \Categorias
     * @Assert\NotBlank(message="O campo Área de Publicação é obrigatório")
     * @ORM\ManyToOne(targetEntity="AreasPublicacoes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="area_publicacao_id", referencedColumnName="id")
     * })
     */
    private $areaPublicacao;

    /**
     * Set retranca
     *
     * @param string $retranca
     * @return Noticias
     */
    public function setRetranca($retranca)
    {
        $this->retranca = $retranca;

        return $this;
    }

    /**
     * Get retranca
     *
     * @return string 
     */
    public function getRetranca()
    {
        return $this->retranca;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Noticias
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set resumo
     *
     * @param string $resumo
     * @return Noticias
     */
    public function setResumo($resumo)
    {
        $this->resumo = $resumo;

        return $this;
    }

    /**
     * Get resumo
     *
     * @return string 
     */
    public function getResumo()
    {
        return $this->resumo;
    }

    /**
     * Set texto
     *
     * @param string $texto
     * @return Noticias
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string 
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updated = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set categoria
     *
     * @param \BetaGT\Bundles\CMSBundle\Entity\Categorias $categoria
     * @return Noticias
     */
    public function setCategoria(\BetaGT\Bundles\CMSBundle\Entity\Categorias $categoria = null)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \BetaGT\Bundles\CMSBundle\Entity\Categorias
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set areaPublicacao
     *
     * @param \BetaGT\Bundles\CMSBundle\Entity\AreasPublicacoes $areaPublicaco
     * @return Noticias
     */
    public function setAreaPublicacao(\BetaGT\Bundles\CMSBundle\Entity\AreasPublicacoes $areaPublicacao = null)
    {
        $this->areaPublicacao = $areaPublicacao;

        return $this;
    }

    /**
     * Get areaPublicacao
     *
     * @return \BetaGT\Bundles\CMSBundle\Entity\AreasPublicacoes
     */
    public function getAreaPublicacao()
    {
        return $this->areaPublicacao;
    }
}

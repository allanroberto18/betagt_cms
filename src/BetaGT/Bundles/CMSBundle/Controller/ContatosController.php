<?php

namespace BetaGT\Bundles\CMSBundle\Controller;

use BetaGT\Bundles\CMSBundle\Entity\Contatos;
use BetaGT\Bundles\CMSBundle\Form\ContatosType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class ContatosController extends MainController
{
    /**
     * @Route("/contatos/novo", name="cms_contatos_novo")
     * @Template("CMSBundle:Contatos:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction(Request $request)
    {
        $entity = new Contatos();

        $form = $this->createForm(new ContatosType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_contatos_novo'
                : 'cms_contatos_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home'));
        $breadcrumbs->addItem('Contatos: Listar Registros', $this->get('router')->generate('cms_contatos_listar'));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Contatos', 'descricao' => 'Mensagens enviadas dos visitantes do site'),
        );
    }

    /**
     * @Route("/contatos/send", name="cms_contatos_send")
     * @Method("POST")
     */
    public function sendContatoAction(Request $request)
    {
        $data = $request->request->all();

        $em = $this->getDoctrine()->getManager();

        $assunto = $em->getRepository("CMSBundle:Assuntos")->find($data['assunto']);

        $entity = new Contatos();
        $entity->setAssunto($assunto);
        $entity->setTexto($data['texto']);
        $entity->setCelular($data['telefone']);
        $entity->setNome($data['nome']);
        $entity->setEmail($data['email']);

        $em->persist($entity);

        $em->flush();

        if (empty($entity->getId()))
        {
            return new JsonResponse([ 'error' => 'Não foi possível enviar a sua mensagem, contate o suporte']);
        }
        return new JsonResponse([ 'success' => 'Mensagem enviada com sucesso']);
    }

    /**
     * @Route("/contatos/{id}/atualizar", name="cms_contatos_atualizar")
     * @Template("CMSBundle:Contatos:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Contatos')->find($id);
        if (!$entity instanceof Contatos) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_contatos_listar', array());
        }

        $form = $this->createForm(new ContatosType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_contatos_novo'
                : 'cms_contatos_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Contatos: Listar Registros', $this->get('router')->generate('cms_contatos_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $entity->getAssunto()->getTitulo(), $this->get('router')->generate('cms_contatos_listar', array('id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Contatos', 'descricao' => 'Mensagens enviadas dos visitantes do site'),
        );
    }

    /**
     * @Route("/contatos/listar", name="cms_contatos_listar")
     * @Template("CMSBundle:Contatos:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction(Request $request)
    {
        $repository = $this->getDoctrine()->getManager();
        $queryBuilder = $repository->createQueryBuilder()
            ->select('item, assunto')
            ->from('CMSBundle:Contatos', 'item')
            ->leftJoin('item.assunto', 'assunto')
            ->where('item.status = :status')
            ->setParameter('status', '1');

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('assunto.titulo', array('label' => 'Assunto', 'filterable' => 'true', 'sortable' => true)))
            ->addField(new Field('item.nome', array('label' => 'Título', 'filterable' => 'true', 'sortable' => true)))
            ->addField(new Field('item.email', array('label' => 'E-mail', 'filterable' => 'true', 'sortable' => true)))
        ;

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Contatos: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('cms_contatos_delete_selecionado'),
            'novo' => $this->generateUrl('cms_contatos_novo'),
            'modulo' => array('titulo' => 'Contatos', 'descricao' => 'Mensagens enviadas dos visitantes do site'),
        );
    }

    /**
     * @Route("/contatos/{id}/visualizar", name="cms_contatos_visualizar")
     * @Template("CMSBundle:Contatos:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Contatos')->find($id);
        if (!$entity instanceof Contatos) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_contatos_listar', array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Listar Registros', $this->get('router')->generate('cms_contatos_listar', array()));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar: ' . $entity->getAssunto()->getTitulo(),
            'entity' => $entity,
            'modulo' => array('titulo' => 'Contatos', 'descricao' => 'Mensagens enviadas dos visitantes do site'),
        );
    }

    /**
     * @Route("/contatos/{id}/delete", name="cms_contatos_delete")
     * @Method("GET")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Contatos')->find($id);
        if (!$entity instanceof Contatos) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_contatos_listar', array());
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('cms_contatos_listar', array());
    }

    /**
     * @Route("/contatos/delete/selecionados", name="cms_contatos_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction(Request $request)
    {
        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('cms_contatos_listar', array());
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('CMSBundle:Contatos')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('cms_contatos_listar', array());
    }
}

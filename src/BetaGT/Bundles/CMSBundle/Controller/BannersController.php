<?php

namespace BetaGT\Bundles\CMSBundle\Controller;

use BetaGT\Bundles\CMSBundle\Entity\Banners;
use BetaGT\Bundles\CMSBundle\Form\BannersType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;
use Symfony\Component\Validator\Constraints\DateTime;

class BannersController extends MainController
{
    /**
     * @Route("/banners/novo", name="cms_banners_novo")
     * @Template("CMSBundle:Banners:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction(Request $request)
    {
        $entity = new Banners();

        $form = $this->createForm(new BannersType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get("actions")->get('salvarNovo')->isClicked()
                ? 'cms_banners_novo'
                : 'cms_banners_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home'));
        $breadcrumbs->addItem('Banner: Listar Registros', $this->get('router')->generate('cms_banners_listar'));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Banners', 'descricao' => ''),
        );
    }

    /**
     * @Route("/banners/{id}/atualizar", name="cms_banners_atualizar")
     * @Template("CMSBundle:Banners:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Banners')->find($id);
        if (!$entity instanceof Banners)
        {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_banners_listar', array());
        }

        $form = $this->createForm(new BannersType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get("actions")->get('salvarNovo')->isClicked()
                ? 'cms_banners_novo'
                : 'cms_banners_listar';

            return $this->redirectToRoute($nextAction, array());
        }
        
        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Banner: Listar Registros', $this->get('router')->generate('cms_banners_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $entity->getTitulo(), $this->get('router')->generate('cms_banners_visualizar', array('id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Banners', 'descricao' => ''),
        );
    }

    /**
     * @Route("/banners/listar", name="cms_banners_listar")
     * @Template("CMSBundle:Banners:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('CMSBundle:Banners');
        $queryBuilder = $repository->createQueryBuilder('item')
            ->where('item.status = :status')
            ->setParameter('status', '1')
        ;

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('item.titulo', array('label' => 'Título', 'filterable' => 'true', 'sortable' => true)))
            ->addField(new Field('item.imageName', [
                'label' => 'Foto', 'sortable' => true,
                'formatValueCallback' => function($value) {
                    print "<img src='/uploads/banners/{$value}' class='img-responsive img-thumbnail' width='140px' />";
                }
            ]))
            ->addField(new Field('item.link', ['label' => 'E-mail', 'filterable' => 'true', 'sortable' => true]))
            ->addField(new Field('item.dataFinal', ['label' => 'E-mail', 'filterable' => 'true', 'sortable' => true,
                'formatValueCallback' => function($value)
                {
                    return $value->format('d/m/Y');
                }
            ]))
            ->addField(new Field('item.status', ['label' => 'E-mail', 'filterable' => 'true', 'sortable' => true,
                'formatValueCallback' => function($value)
                {
                    switch($value)
                    {
                        case 1:
                            return "Sim";
                        break;
                        case 2:
                            return "Não";
                        break;
                    }
                }
            ]))
        ;

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Banner: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('cms_banners_delete_selecionado'),
            'novo' => $this->generateUrl('cms_banners_novo'),
            'modulo' => array('titulo' => 'Banners', 'descricao' => ''),
        );
    }

    /**
     * @Route("/banners/{id}/visualizar", name="cms_banners_visualizar")
     * @Template("CMSBundle:Banners:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Banners')->find($id);
        if (!$entity instanceof Banners)
        {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_banners_listar', array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Listar Registros', $this->get('router')->generate('cms_banners_listar', array()));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar: ' . $entity->getTitulo(),
            'entity' => $entity,
            'modulo' => array('titulo' => 'Banners', 'descricao' => ''),
        );
    }

    /**
     * @Route("/banners/{id}/delete", name="cms_banners_delete")
     * @Method("GET")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Banners')->find($id);
        if (!$entity instanceof Banners)
        {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_banners_listar', array());
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('cms_banners_listar', array());
    }

    /**
     * @Route("/banners/delete/selecionados", name="cms_banners_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction(Request $request)
    {
        $data = $request->request->all();

        if (empty($data))
        {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('cms_banners_listar', array());
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++)
        {
            $entity = $em->getRepository('CMSBundle:Banners')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('cms_banners_listar', array());
    }
}

<?php

namespace BetaGT\Bundles\PortalBundle\Utils;

use Doctrine\ORM\EntityManager;

class Galerias
{
    private $entityManager;

    /**
     * ServiceMaster constructor.
     * @param $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array|\BetaGT\Bundles\CMSBundle\Entity\Galerias[]
     */
    public function returnAll()
    {
        return $this->entityManager->getRepository("CMSBundle:Galerias")->findAll();
    }

    public function returnRegister($id)
    {
        return $this->entityManager->getRepository("CMSBundle:Galerias")->find($id);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: allan
 * Date: 11/09/2015
 * Time: 13:16
 */

namespace BetaGT\Bundles\CMSBundle\Entity;

trait TFoto
{
    use TImageName;

    /**
     * @var string
     *
     * @ORM\Column(name="credito", type="string", length=75, nullable=true)
     */
    protected $credito;

    /**
     * @var string
     *
     * @ORM\Column(name="legenda", type="string", length=255, nullable=true)
     */
    protected $legenda;

    /**
     * Set credito
     * @param string $credito
     */
    public function setCredito($credito)
    {
        $this->credito = $credito;

        return $this;
    }

    /**
     * Get credito
     *
     * @return string
     */
    public function getCredito()
    {
        return $this->credito;
    }

    /**
     * Set legenda
     *
     * @param string $legenda
     * @return Paginas
     */
    public function setLegenda($legenda)
    {
        $this->legenda = $legenda;

        return $this;
    }

    /**
     * Get legenda
     *
     * @return string
     */
    public function getLegenda()
    {
        return $this->legenda;
    }

}
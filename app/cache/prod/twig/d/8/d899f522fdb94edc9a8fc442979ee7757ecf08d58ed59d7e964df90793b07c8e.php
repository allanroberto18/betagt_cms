<?php

/* CMSBundle:Pessoas:form.html.twig */
class __TwigTemplate_f0788cc7460331af548fb27a5c28fef016c0d9df1ebe0e34b68afe79b464e06b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CMSBundle:Default:form.html.twig", "CMSBundle:Pessoas:form.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body_footer' => array($this, 'block_body_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CMSBundle:Default:form.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (isset($context["titulo"]) ? $context["titulo"] : null), "html", null, true);
    }

    // line 3
    public function block_body_footer($context, array $blocks = array())
    {
        // line 4
        echo "    <a class=\"btn btn-default\" href=\"";
        echo $this->env->getExtension('routing')->getPath("cms_pessoas_listar");
        echo "\"><span class=\"fa fa-list-ul\"></span> Listar Registros</a>
";
    }

    // line 6
    public function block_javascripts($context, array $blocks = array())
    {
        // line 7
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script type=\"text/javascript\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/picedit/js/picedit.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\">
        \$(function() {
            \$('#form_imageFile_file').picEdit({
                maxWidth: 400,
                redirectUrl: '";
        // line 13
        echo $this->env->getExtension('routing')->getPath("cms_pessoas_listar");
        echo "',
                maxHeight: 'auto',
                aspectRatio: true,
            });
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "CMSBundle:Pessoas:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 13,  54 => 8,  49 => 7,  46 => 6,  39 => 4,  36 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends "CMSBundle:Default:form.html.twig" %}*/
/* {% block title %}{{ titulo }}{% endblock %}*/
/* {% block body_footer %}*/
/*     <a class="btn btn-default" href="{{ path('cms_pessoas_listar') }}"><span class="fa fa-list-ul"></span> Listar Registros</a>*/
/* {% endblock %}*/
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     <script type="text/javascript" src="{{ asset('admin/plugins/picedit/js/picedit.min.js') }}"></script>*/
/*     <script type="text/javascript">*/
/*         $(function() {*/
/*             $('#form_imageFile_file').picEdit({*/
/*                 maxWidth: 400,*/
/*                 redirectUrl: '{{ path('cms_pessoas_listar') }}',*/
/*                 maxHeight: 'auto',*/
/*                 aspectRatio: true,*/
/*             });*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
/* */

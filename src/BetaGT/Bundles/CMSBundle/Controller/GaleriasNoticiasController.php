<?php

namespace BetaGT\Bundles\CMSBundle\Controller;

use BetaGT\Bundles\CMSBundle\Entity\GaleriasNoticias;
use BetaGT\Bundles\CMSBundle\Entity\Noticias;
use BetaGT\Bundles\CMSBundle\Form\GaleriasNoticiasType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class GaleriasNoticiasController extends MainController
{
    /**
     * @Route("/noticias/{idNoticia}/galerias/novo", name="cms_noticias_galerias_novo")
     * @Template("CMSBundle:GaleriasNoticias:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction($idNoticia, Request $request)
    {
        $noticia = $this->checkParent($idNoticia, 'CMSBundle', 'Noticias', 'cms_noticias_listar', null);

        $entity = new GaleriasNoticias();

        $form = $this->createForm(new GaleriasNoticiasType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setNoticia($noticia);

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_galerias_noticias_novo'
                : 'cms_galerias_noticias_listar';

            return $this->redirectToRoute($nextAction, array('idNoticia' => $idNoticia));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home'));
        $breadcrumbs->addItem('Noticias: Listar Registros', $this->get('router')->generate('cms_noticias_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $noticia->getTitulo(), $this->get('router')->generate('cms_noticias_visualizar', array('id' => $idNoticia)));
        $breadcrumbs->addItem('Álbum da Notícia: Listar Registros', $this->get('router')->generate('cms_galerias_noticias_listar', array('idNoticia' => $idNoticia)));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Álbum da Notícia', 'descricao' => ''),
        );
    }

    /**
     * @Route("/noticias/{idNoticia}/galerias/{id}/atualizar", name="cms_noticias_galerias_atualizar")
     * @Template("CMSBundle:GaleriasNoticias:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($idNoticia, $id, Request $request)
    {
        $noticia = $this->checkParent($idNoticia, 'CMSBundle', 'Noticias', 'cms_noticias_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:GaleriasNoticias')->find($id);
        if (!$entity instanceof GaleriasNoticias) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_galerias_noticias_listar', array('idNoticia' => $idNoticia));
        }

        $form = $this->createForm(new GaleriasNoticiasType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_galerias_noticias_novo'
                : 'cms_galerias_noticias_listar';

            return $this->redirectToRoute($nextAction, array('idNoticia' => $idNoticia));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Noticias: Listar Registros', $this->get('router')->generate('cms_noticias_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $noticia->getTitulo(), $this->get('router')->generate('cms_noticias_visualizar', array('id' => $idNoticia)));
        $breadcrumbs->addItem('Álbum da Notícia: Listar Registros', $this->get('router')->generate('cms_galerias_noticias_listar', array('idNoticia' => $idNoticia)));
        $breadcrumbs->addItem('Visualizar', $this->get('router')->generate('cms_galerias_noticias_visualizar', array('idNoticia' => $idNoticia, 'id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Álbum da Notícia', 'descricao' => ''),
        );
    }

    /**
     * @Route("/noticias/{idNoticia}/galerias/listar", name="cms_noticias_galerias_listar")
     * @Template("CMSBundle:GaleriasNoticias:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction($idNoticia, Request $request)
    {
        $noticia = $this->checkParent($idNoticia, 'CMSBundle', 'Noticias', 'cms_noticias_listar', null);

        $repository = $this->getDoctrine()->getRepository('CMSBundle:GaleriasNoticias');
        $queryBuilder = $repository->createQueryBuilder()
            ->select('item, galeria')
            ->from('CMSBundle:GaleriasNoticias', 'item')
            ->leftJoin('item.galeria', 'galeria')
            ->where('item.status = :status')
            ->andWhere('item.noticia = :noticia')
            ->setParameter('status', '1')
            ->setParameter('noticia', $idNoticia)
        ;

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('galeria.titulo', array('label' => 'Galería de Fotos', 'filterable' => 'true', 'sortable' => true)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Noticias: Listar Registros', $this->get('router')->generate('cms_noticias_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $noticia->getTitulo(), $this->get('router')->generate('cms_noticias_visualizar', array('id' => $idNoticia)));
        $breadcrumbs->addItem('Álbum da Notícia: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('cms_galerias_noticias_delete_selecionado', ['idNoticia' => $idNoticia]),
            'novo' => $this->generateUrl('cms_galerias_noticias_novo', ['idNoticia' => $idNoticia]),
            'modulo' => array('titulo' => 'Álbum da Notícia', 'descricao' => ''),
        );
    }

    /**
     * @Route("/noticias/{idNoticia}/galerias/{id}/visualizar", name="cms_noticias_galerias_visualizar")
     * @Template("CMSBundle:GaleriasNoticias:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($idNoticia, $id)
    {
        $noticia = $this->checkParent($idNoticia, 'CMSBundle', 'Noticias', 'cms_noticias_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:GaleriasNoticias')->find($id);
        if (!$entity instanceof GaleriasNoticias) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_galerias_noticias_listar', array('idNoticia' => $idNoticia));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Noticias: Listar Registros', $this->get('router')->generate('cms_noticias_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $noticia->getTitulo(), $this->get('router')->generate('cms_noticias_visualizar', array('id' => $idNoticia)));
        $breadcrumbs->addItem('Álbum da Notícia: Listar Registros', $this->get('router')->generate('cms_galerias_noticias_listar', array('idNoticia' => $idNoticia)));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar',
            'entity' => $entity,
            'modulo' => array('titulo' => 'Álbum da Notícia', 'descricao' => ''),
        );
    }

    /**
     * @Route("/noticias/{idNoticia}/galerias/{id}/delete", name="cms_noticias_galerias_delete")
     * @Method("GET")
     */
    public function deleteAction($idNoticia, $id)
    {
        $this->checkParent($idNoticia, 'CMSBundle', 'Noticias', 'cms_noticias_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:GaleriasNoticias')->find($id);
        if (!$entity instanceof GaleriasNoticias) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_galerias_noticias_listar', array('idNoticia' => $idNoticia));
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('cms_galerias_noticias_listar', array('idNoticia' => $idNoticia));
    }

    /**
     * @Route("/noticias/{idNoticia}/galerias/delete/selecionados", name="cms_noticias_galerias_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction($idNoticia, Request $request)
    {
        $this->checkParent($idNoticia, 'CMSBundle', 'Noticias', 'cms_noticias_listar', null);

        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('cms_galerias_noticias_listar', array('idNoticia' => $idNoticia));
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('CMSBundle:GaleriasNoticias')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('cms_galerias_noticias_listar', array('idNoticia' => $idNoticia));
    }
}

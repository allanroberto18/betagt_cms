<?php

namespace BetaGT\Bundles\FinanceiroBundle\Controller;

use BetaGT\Bundles\CMSBundle\Controller\MainController;
use BetaGT\Bundles\FinanceiroBundle\Entity\Contratos;
use BetaGT\Bundles\FinanceiroBundle\Entity\Faturas;
use BetaGT\Bundles\FinanceiroBundle\Form\FaturasType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class FaturasController extends MainController
{
    /**
     * @Route("/contratos/{idContrato}/faturas/novo", name="fin_faturas_novo")
     * @Template("FinanceiroBundle:Faturas:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction($idContrato, Request $request)
    {
        $contrato = $this->checkParent($idContrato, 'FinanceiroBundle', 'Contratos', 'fin_contratos_listar', null);

        $pessoa = $contrato->getPessoa();
        $entity = new Faturas();

        $form = $this->createForm(new FaturasType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setContrato($contrato);

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'fin_faturas_novo'
                : 'fin_faturas_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home'));
        $breadcrumbs->addItem('Pessoas: Listar Registros', $this->get('router')->generate('cms_pessoas_listar', array()));
        $breadcrumbs->addItem('Visualizar', $this->get('router')->generate('cms_pessoas_visualizar', array('id' => $pessoa->getId())));
        $breadcrumbs->addItem('Contratos: Listar Registros', $this->get('router')->generate('fin_contratos_listar', array('idPessoa' => $pessoa->getId())));
        $breadcrumbs->addItem('Visualizar', $this->get('router')->generate('fin_contratos_visualizar', array('idPessoa' => $pessoa->getId(), 'id' => $idContrato)));
        $breadcrumbs->addItem('Faturas: Listar Registros', $this->get('router')->generate('fin_faturas_listar', array('idContrato' => $idContrato)));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Faturas', 'descricao' => ''),
        );
    }

    /**
     * @Route("/contratos/{idContrato}/faturas/{id}/atualizar", name="fin_faturas_atualizar")
     * @Template("CMSBundle:Faturas:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($idContrato, $id, Request $request)
    {
        $contrato = $this->checkParent($idContrato, 'FinanceiroBundle', 'Contratos', 'fin_contratos_listar', null);

        $pessoa = $contrato->getPessoa();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('FinanceiroBundle:Faturas')->find($id);
        if (!$entity instanceof Faturas) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('fin_faturas_listar', array('idContrato' => $idContrato));
        }

        $form = $this->createForm(new FaturasType(), $entity, [ 'validation_groups' => ['update'], 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ] );
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'fin_faturas_novo'
                : 'fin_faturas_listar';

            return $this->redirectToRoute($nextAction, array('idContrato' => $idContrato));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Pessoas: Listar Registros', $this->get('router')->generate('cms_pessoas_listar', array()));
        $breadcrumbs->addItem('Visualizar', $this->get('router')->generate('cms_pessoas_visualizar', array('id' => $pessoa->getId())));
        $breadcrumbs->addItem('Contratos: Listar Registros', $this->get('router')->generate('fin_contratos_listar', array('idPessoa' => $pessoa->getId())));
        $breadcrumbs->addItem('Visualizar', $this->get('router')->generate('fin_contratos_visualizar', array('idPessoa' => $pessoa->getId(), 'id' => $idContrato)));
        $breadcrumbs->addItem('Faturas: Listar Registros', $this->get('router')->generate('fin_faturas_listar', array('idContrato' => $idContrato)));
        $breadcrumbs->addItem('Visualizar', $this->get('router')->generate('fin_faturas_visualizar', array('idContrato' => $idContrato, 'id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Faturas', 'descricao' => ''),
        );
    }

    /**
     * @Route("/contratos/{idContrato}/faturas/listar", name="fin_faturas_listar")
     * @Template("CMSBundle:Faturas:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction($idContrato, Request $request)
    {
        $contrato = $this->checkParent($idContrato, 'FinanceiroBundle', 'Contratos', 'fin_contratos_listar', null);

        $pessoa = $contrato->getPessoa();
        $repository = $this->getDoctrine()->getRepository('FinanceiroBundle:Faturas');
        $queryBuilder = $repository->createQueryBuilder('item')
            ->where('item.status = :status')
            ->andWhere('item.idContrato = :contrato')
            ->setParameter('status', '1')
            ->setParameter('contrato', $idContrato)
        ;

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('item.valor', array('label' => 'Valor', 'filterable' => 'true', 'sortable' => true)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Pessoas: Listar Registros', $this->get('router')->generate('cms_pessoas_listar', array()));
        $breadcrumbs->addItem('Visualizar', $this->get('router')->generate('cms_pessoas_visualizar', array('id' => $pessoa->getId())));
        $breadcrumbs->addItem('Contratos: Listar Registros', $this->get('router')->generate('fin_contratos_listar', array('idPessoa' => $pessoa->getId())));
        $breadcrumbs->addItem('Visualizar', $this->get('router')->generate('fin_contratos_visualizar', array('idPessoa' => $pessoa->getId(), 'id' => $idContrato)));
        $breadcrumbs->addItem('Faturas: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('fin_faturas_delete_selecionado', ['idContrato' => $idContrato]),
            'novo' => $this->generateUrl('fin_faturas_novo', ['idContrato' => $idContrato]),
            'modulo' => array('titulo' => 'Faturas', 'descricao' => ''),
        );
    }

    /**
     * @Route("/contratos/{idContrato}/faturas/{id}/visualizar", name="fin_faturas_visualizar")
     * @Template("CMSBundle:Faturas:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($idContrato, $id)
    {
        $contrato = $this->checkParent($idContrato, 'FinanceiroBundle', 'Contratos', 'fin_contratos_listar', null);

        $pessoa = $contrato->getPessoa();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('FinanceiroBundle:Faturas')->find($id);
        if (!$entity instanceof Faturas) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('fin_faturas_listar', array('idContrato' => $idContrato));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Pessoas: Listar Registros', $this->get('router')->generate('cms_pessoas_listar', array()));
        $breadcrumbs->addItem('Visualizar', $this->get('router')->generate('cms_pessoas_visualizar', array('id' => $pessoa->getId())));
        $breadcrumbs->addItem('Contratos: Listar Registros', $this->get('router')->generate('fin_contratos_listar', array('idPessoa' => $pessoa->getId())));
        $breadcrumbs->addItem('Visualizar', $this->get('router')->generate('fin_contratos_visualizar', array('idPessoa' => $pessoa->getId(), 'id' => $idContrato)));
        $breadcrumbs->addItem('Faturas: Listar Registros', $this->get('router')->generate('fin_faturas_listar', array('idContrato' => $idContrato)));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar',
            'entity' => $entity,
            'modulo' => array('titulo' => 'Faturas', 'descricao' => ''),
        );
    }

    /**
     * @Route("/contratos/{idContrato}/faturas/{id}/delete", name="fin_faturas_delete")
     * @Method("GET")
     */
    public function deleteAction($idContrato, $id)
    {
        $contrato = $this->checkParent($idContrato, 'FinanceiroBundle', 'Contratos', 'fin_contratos_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('FinanceiroBundle:Faturas')->find($id);
        if (!$entity instanceof Faturas) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('fin_faturas_listar', array('idContrato' => $idContrato));
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('fin_faturas_listar', array('idContrato' => $idContrato));
    }

    /**
     * @Route("/contratos/{idContrato}/faturas/delete/selecionados", name="fin_faturas_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction($idContrato, Request $request)
    {
        $contrato = $this->checkParent($idContrato, 'FinanceiroBundle', 'Contratos', 'fin_contratos_listar', null);

        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('fin_faturas_listar', array('idContrato' => $idContrato));
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('FinanceiroBundle:Faturas')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('fin_faturas_listar', array('idContrato' => $idContrato));
    }
}

<?php

namespace BetaGT\Bundles\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * GaleriasPaginas
 *
 * @ORM\Table(name="galerias_paginas", indexes={@ORM\Index(name="fk_galerias_paginas_galeria_id_idx", columns={"galeria_id"}), @ORM\Index(name="fk_galerias_paginas_pagina_id_idx", columns={"pagina_id"})})
 * @ORM\Entity
 */
class GaleriasPaginas extends EntityMaster
{
    /**
     * @var \Galerias
     *
     * @ORM\ManyToOne(targetEntity="Galerias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="galeria_id", referencedColumnName="id")
     * })
     */
    private $galeria;

    /**
     * @var \Paginas
     *
     * @ORM\ManyToOne(targetEntity="Paginas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pagina_id", referencedColumnName="id")
     * })
     */
    private $pagina;

    /**
     * Set galeria
     *
     * @param \BetaGT\Bundles\CMSBundle\Entity\Galerias $galeria
     * @return GaleriasPaginas
     */
    public function setGaleria(\BetaGT\Bundles\CMSBundle\Entity\Galerias $galeria = null)
    {
        $this->galeria = $galeria;

        return $this;
    }

    /**
     * Get galeria
     *
     * @return \BetaGT\Bundles\CMSBundle\Entity\Galerias
     */
    public function getGaleria()
    {
        return $this->galeria;
    }

    /**
     * Set pagina
     *
     * @param \BetaGT\Bundles\CMSBundle\Entity\Paginas $pagina
     * @return GaleriasPaginas
     */
    public function setPagina(\BetaGT\Bundles\CMSBundle\Entity\Paginas $pagina = null)
    {
        $this->pagina = $pagina;

        return $this;
    }

    /**
     * Get pagina
     *
     * @return \BetaGT\Bundles\CMSBundle\Entity\Paginas
     */
    public function getPagina()
    {
        return $this->pagina;
    }
}

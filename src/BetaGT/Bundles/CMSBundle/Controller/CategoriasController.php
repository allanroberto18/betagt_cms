<?php

namespace BetaGT\Bundles\CMSBundle\Controller;

use BetaGT\Bundles\CMSBundle\Entity\Categorias;
use BetaGT\Bundles\CMSBundle\Form\CategoriasType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class CategoriasController extends MainController
{
    /**
     * @Route("/categorias/novo", name="cms_categorias_novo")
     * @Template("CMSBundle:Categorias:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction(Request $request)
    {
        $entity = new Categorias();

        $form = $this->createForm(new CategoriasType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_categorias_novo'
                : 'cms_categorias_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home'));
        $breadcrumbs->addItem('Categorias: Listar Registros', $this->get('router')->generate('cms_categorias_listar'));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Categorias', 'descricao' => 'Categorias de Notícias e Galerias de Fotos'),
        );
    }

    /**
     * @Route("/categorias/{id}/atualizar", name="cms_categorias_update")
     * @Template("CMSBundle:Categorias:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Categorias')->find($id);
        if (!$entity instanceof Categorias) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_categorias_listar', array());
        }

        $form = $this->createForm(new CategoriasType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_categorias_novo'
                : 'cms_categorias_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Assunto: Listar Registros', $this->get('router')->generate('cms_categorias_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $entity->getTitulo(), $this->get('router')->generate('cms_categorias_view', array('id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Categorias', 'descricao' => 'Categorias de Notícias e Galerias de Fotos'),
        );
    }

    /**
     * @Route("/categorias/listar", name="cms_categorias_listar")
     * @Template("CMSBundle:Categorias:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('CMSBundle:Categorias');
        $queryBuilder = $repository->createQueryBuilder('item')
            ->where('item.status = :status')
            ->setParameter('status', '1');

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('item.titulo', array('label' => 'Título', 'filterable' => 'true', 'sortable' => true)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Categorias: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('cms_categorias_delete_selecionado'),
            'novo' => $this->generateUrl('cms_categorias_novo'),
            'modulo' => array('titulo' => 'Categorias', 'descricao' => 'Categorias de Notícias e Galerias de Fotos'),
        );
    }

    /**
     * @Route("/categorias/{id}/visualizar", name="cms_categorias_view")
     * @Template("CMSBundle:Categorias:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Categorias')->find($id);
        if (!$entity instanceof Categorias) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_categorias_listar', array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Listar Registros', $this->get('router')->generate('cms_categorias_listar', array()));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar: ' . $entity->getTitulo(),
            'entity' => $entity,
            'modulo' => array('titulo' => 'Categorias', 'descricao' => 'Categorias de Notícias e Galerias de Fotos'),
        );
    }

    /**
     * @Route("/categorias/{id}/delete", name="cms_categorias_delete")
     * @Method("GET")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Categorias')->find($id);
        if (!$entity instanceof Categorias) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_categorias_listar', array());
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('cms_categorias_listar', array());
    }

    /**
     * @Route("/categorias/delete/selecionados", name="cms_categorias_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction(Request $request)
    {
        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('cms_categorias_listar', array());
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('CMSBundle:Categorias')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('cms_categorias_listar', array());
    }
}

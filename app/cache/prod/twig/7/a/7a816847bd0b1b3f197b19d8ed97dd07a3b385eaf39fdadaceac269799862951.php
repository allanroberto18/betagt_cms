<?php

/* @CMS/Assuntos/form.html.twig */
class __TwigTemplate_67136d71983f4eb35c7315d84098a071a737ac2e558b8e24acb324e75d39902c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CMSBundle:Default:form.html.twig", "@CMS/Assuntos/form.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body_footer' => array($this, 'block_body_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CMSBundle:Default:form.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (isset($context["titulo"]) ? $context["titulo"] : null), "html", null, true);
    }

    // line 3
    public function block_body_footer($context, array $blocks = array())
    {
        // line 4
        echo "    <a class=\"btn btn-default\" href=\"";
        echo $this->env->getExtension('routing')->getPath("cms_assuntos_listar");
        echo "\"><span class=\"fa fa-list-ul\"></span> Listar Registros</a>
";
    }

    public function getTemplateName()
    {
        return "@CMS/Assuntos/form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 4,  35 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends "CMSBundle:Default:form.html.twig" %}*/
/* {% block title %}{{ titulo }}{% endblock %}*/
/* {% block body_footer %}*/
/*     <a class="btn btn-default" href="{{ path('cms_assuntos_listar') }}"><span class="fa fa-list-ul"></span> Listar Registros</a>*/
/* {% endblock %}*/

<?php

namespace BetaGT\Bundles\PortalBundle\Controller;

use BetaGT\Bundles\ProdutosBundle\Entity\Receitas;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/Receitas")
 */
class ReceitasController extends MainController
{
    /**
     * @Route("/", name="front_receitas")
     * @Template("PortalBundle:receitas:index.html.twig")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $filterBuilder = $this->get('doctrine.orm.entity_manager')
            ->getRepository('ProdutosBundle:Receitas')
            ->createQueryBuilder('e');

        $form = $this->get('form.factory')->create(new ReceitasFilterType());

        if($request->query->has($form->getName()))
        {
            $form->submit($request->query->get($form->getName()));

            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);
        }

        $query = $filterBuilder->getQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query, $request->query->get('page', 1), 10);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('front_home'));
        $breadcrumbs->addItem('Receitas');

        return array(
            "titulo" => "Receitas",
            "form" => $this->formContato(),
            "filter" => $form->createView(),
            "pagination" => $pagination,
            "pagFaleConosco" => $this->container->get('portal.paginas')->returnRegister(1),
            "assuntos" => $this->container->get('portal.assuntos')->returnAll(),
            "galerias" => $this->container->get('portal.galerias')->returnAll(),
            "receitas" => $this->container->get('portal.receitas')->returnAll(),
            "banners" => $this->container->get('portal.banners')->returnAll(),
            "produtos" => $this->container->get('portal.produtos')->returnAll(),
        );
    }

    /**
     * @Route("/{id}/{slug}", name="front_receita")
     * @Template("PortalBundle:receitas:page.html.twig")
     * @Method("GET")
     */
    public function pageAction($id, $slug)
    {
        $entity = $this->container->get('portal.receitas')->returnRegister($id);
        if (!$entity instanceof Receitas)
        {
            return $this->redirect("/");
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('front_home'));
        $breadcrumbs->addItem('Receitas' , $this->get('router')->generate('front_receitas'));
        $breadcrumbs->addItem($entity->getTitulo());

        return array(
            "titulo" => $entity->getTitulo(),
            "item" => $entity,
            "form" => $this->formContato(),
            "pagFaleConosco" => $this->container->get('portal.paginas')->returnRegister(1),
            "assuntos" => $this->container->get('portal.assuntos')->returnAll(),
            "galerias" => $this->container->get('portal.galerias')->returnAll(),
            "receitas" => $this->container->get('portal.receitas')->returnAll(),
            "banners" => $this->container->get('portal.banners')->returnAll(),
            "produtos" => $this->container->get('portal.produtos')->returnAll(),
        );
    }
}

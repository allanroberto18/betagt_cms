<?php

namespace BetaGT\Bundles\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Videos
 * @ORM\Table(name="areas_publicacoes")
 * @ORM\Entity
 */
class AreasPublicacoes extends EntityMaster
{
    /**
     * @var string
     * @Assert\NotBlank(message="O campo Título é obrigatório")
     * @ORM\Column(name="titulo", type="string", length=255, nullable=true)
     */
    private $titulo;

    /**
     * @var integer
     * @Assert\NotBlank(message="O campo Quantidade é obrigatório")
     * @ORM\Column(name="quantidade", type="integer", nullable=true)
     */
    private $quantidade;

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return AreasPublicacoes
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set quantidade
     *
     * @param string $quantidade
     * @return AreasPublicacoes
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;

        return $this;
    }

    /**
     * Get quantidade
     *
     * @return integer
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    function __toString()
    {
        return $this->titulo . "[quantidade: " . $this->getQuantidade() . "]";
    }


}

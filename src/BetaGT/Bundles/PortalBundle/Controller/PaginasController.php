<?php

namespace BetaGT\Bundles\PortalBundle\Controller;

use BetaGT\Bundles\CMSBundle\Entity\Paginas;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/Paginas")
 */
class PaginasController extends MainController
{
    /**
     * @Route("/{id}/{slug}", name="front_pagina")
     * @Template("PortalBundle:paginas:pagina.html.twig")
     * @Method("GET")
     */
    public function pageAction($id, $slug)
    {
        $entity = $this->container->get('portal.paginas')->returnRegister($id);
        if (!$entity instanceof Paginas)
        {
            return $this->redirect("/");
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('front_home'));
        $breadcrumbs->addItem($entity->getTitulo());

        return array(
            "titulo" => "Portal",
            "form" => $this->formContato(),
            "item" => $entity,
            "pagFaleConosco" => $this->container->get('portal.paginas')->returnRegister(1),
            "assuntos" => $this->container->get('portal.assuntos')->returnAll(),
            "galerias" => $this->container->get('portal.galerias')->returnAll(),
            "receitas" => $this->container->get('portal.receitas')->returnAll(),
            "banners" => $this->container->get('portal.banners')->returnAll(),
            "produtos" => $this->container->get('portal.produtos')->returnAll(),
        );
    }
}

<?php

namespace BetaGT\Bundles\FinanceiroBundle\Entity;

use BetaGT\Bundles\CMSBundle\Entity\EntityMaster;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Planos
 *
 * @ORM\Table(name="planos")
 * @ORM\Entity
 */
class Planos extends EntityMaster
{
    /**
     * @var string
     * @Assert\NotBlank(message="O campo Nome do Serviço Oferecido é obrigatório")
     * @ORM\Column(name="nome", type="string", length=100, nullable=false)
     */
    private $nome;

    /**
     * @var integer
     * @Assert\NotBlank(message="O campo Quantidade de Parcelas é obrigatório")
     * @ORM\Column(name="parcelas", type="integer", nullable=false)
     */
    private $parcelas;

    /**
     * @var float
     * @Assert\NotBlank(message="O campo Valor é obrigatório")
     * @ORM\Column(name="valor", type="float", precision=10, scale=2, nullable=false)
     */
    private $valor;

    /**
     * Set nome
     *
     * @param string $nome
     * @return Planos
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string 
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set parcelas
     *
     * @param integer $parcelas
     * @return Planos
     */
    public function setParcelas($parcelas)
    {
        $this->parcelas = $parcelas;

        return $this;
    }

    /**
     * Get parcelas
     *
     * @return integer 
     */
    public function getParcelas()
    {
        return $this->parcelas;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return Planos
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor()
    {
        return $this->valor;
    }
}

<?php

namespace BetaGT\Bundles\CMSBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PessoasPerfilType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('roles', 'choice', array(
                'choices' => [
                    'ROLE_USER' => 'Ambiente do Usuário',
                    'ROLE_ADMIN' => 'Administração'
                ],
                'label' => 'Regras',
                'expanded' => true,
                'multiple' => true,
                'mapped' => true,
            ))
            ->add('enabled', 'choice', array(
                'choices' => [
                    '0' => 'Não',
                    '1' => 'Sim'
                ],
                'label' => 'Ativar Usuário para acesso',
                'expanded' => true,
                'multiple' => false,
                'mapped' => true,
            ))
            ->add('actions', 'form_actions', [
                'buttons' => [
                    'salvar' => ['type' => 'submit', 'options' => ['label' => 'Salvar', 'attr' => ['class' => 'btn btn-primary']]],
                    'salvarNovo' => ['type' => 'submit', 'options' => ['label' => 'Salvar e novo', 'attr' => ['class' => 'btn btn-primary']]],
                    'cancelar' => ['type' => 'reset', 'options' => ['label' => 'Cancelar', 'attr' => ['class' => 'btn btn-warning']]],
                ]
            ])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BetaGT\Bundles\CMSBundle\Entity\Pessoas'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form';
    }
}

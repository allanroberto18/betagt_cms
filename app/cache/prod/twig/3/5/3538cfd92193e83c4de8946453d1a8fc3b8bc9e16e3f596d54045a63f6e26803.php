<?php

/* FOSUserBundle:Registration:checkEmail.html.twig */
class __TwigTemplate_3dc026ba4d6b6f72af45760ad1aecdf33c97f2bd3ab57397ff165acd16c4b802 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CMSBundle:Default:security.html.twig", "FOSUserBundle:Registration:checkEmail.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CMSBundle:Default:security.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "    <h3>Usuário criado com sucesso</h3>
    Para que o mesmo possa a ter acesso as funcionalidades do sistema
    é necessário ativar seu usário e liberar os respectivos acessos. <br />
    <a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("cms_pessoas_listar");
        echo "\">clique aqui</a> e vá para listagem de usuários.
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:checkEmail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 9,  31 => 6,  28 => 5,  11 => 1,);
    }
}
/* {% extends 'CMSBundle:Default:security.html.twig' %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block body %}*/
/*     <h3>Usuário criado com sucesso</h3>*/
/*     Para que o mesmo possa a ter acesso as funcionalidades do sistema*/
/*     é necessário ativar seu usário e liberar os respectivos acessos. <br />*/
/*     <a href="{{ path('cms_pessoas_listar') }}">clique aqui</a> e vá para listagem de usuários.*/
/* {% endblock %}*/
/* */

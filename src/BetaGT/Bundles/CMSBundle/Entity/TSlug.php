<?php
/**
 * Created by PhpStorm.
 * User: allan
 * Date: 11/09/2015
 * Time: 10:46
 */

namespace BetaGT\Bundles\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

trait TSlug
{
    /**
     * @var string
     * @Gedmo\Slug(fields={"titulo"}, updatable=true, separator="_")
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    protected $slug;

    /**
     * Set slug
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
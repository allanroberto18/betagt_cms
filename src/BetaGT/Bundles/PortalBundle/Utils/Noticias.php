<?php
/**
 * Created by PhpStorm.
 * User: allan
 * Date: 08/11/2015
 * Time: 21:14
 */

namespace BetaGT\Bundles\PortalBundle\Utils;

use Doctrine\ORM\EntityManager;

class Noticias
{
    private $entityManager;

    /**
     * ServiceMaster constructor.
     * @param $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $quantidade
     * @param null $areaPublicacao
     * @return array|\BetaGT\Bundles\CMSBundle\Entity\Noticias[]
     */
    public function returnNoticias($quantidade=3, $areaPublicacao=null)
    {
        if (empty($areaPublicacao))
        {
            return $this->entityManager->getRepository("CMSBundle:Noticias")->findBy(['status' => 1], ['created' => 'DESC'], $quantidade);
        }
        return $this->entityManager->getRepository("CMSBundle:Noticias")->findBy([ 'status' => 1, 'areaPublicacao' => $areaPublicacao ], [ 'created' => 'DESC'  ] , $quantidade);
    }

    public function returnRegister($id)
    {
        return $this->entityManager->getRepository("CMSBundle:Noticias")->find($id);
    }
}
<?php

/* CMSBundle:Default:list.html.twig */
class __TwigTemplate_f01821522ddd2bd8265b8b5a9ed81883b193de35d16a016d10517f38ee42fb33 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CMSBundle:Default:base.html.twig", "CMSBundle:Default:list.html.twig", 1);
        $this->blocks = array(
            'body_header' => array($this, 'block_body_header'),
            'modal' => array($this, 'block_modal'),
            'body' => array($this, 'block_body'),
            'kit_grid_selector' => array($this, 'block_kit_grid_selector'),
            'kit_grid_filter' => array($this, 'block_kit_grid_filter'),
            'kit_grid_debug' => array($this, 'block_kit_grid_debug'),
            'kit_grid_before_table' => array($this, 'block_kit_grid_before_table'),
            'kit_grid_thead' => array($this, 'block_kit_grid_thead'),
            'kit_grid_thead_before_column' => array($this, 'block_kit_grid_thead_before_column'),
            'kit_grid_tbody' => array($this, 'block_kit_grid_tbody'),
            'kit_grid_tbody_before_column' => array($this, 'block_kit_grid_tbody_before_column'),
            'body_footer' => array($this, 'block_body_footer'),
            'kit_grid_after_table' => array($this, 'block_kit_grid_after_table'),
            'kit_grid_paginator' => array($this, 'block_kit_grid_paginator'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CMSBundle:Default:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body_header($context, array $blocks = array())
    {
        // line 3
        echo "    <a class=\"btn btn-success\" href=\"";
        echo twig_escape_filter($this->env, (isset($context["novo"]) ? $context["novo"] : null), "html", null, true);
        echo "\">
        <span class=\"glyphicon glyphicon-plus\"></span> Novo Registro
    </a>
";
    }

    // line 7
    public function block_modal($context, array $blocks = array())
    {
        // line 8
        echo "    <div id=\"modal-dialog\" class=\"modal\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h3>Atenção</h3>
                </div>
                <div class=\"modal-body\">
                    <div class=\"mensagem_body\">
                        <p>Você deseja remover os itens selecionados</p>
                    </div>
                    <div class=\"progress\" style=\"display: none\">
                        <div class=\"progress-bar progress-bar-striped active\" role=\"progressbar\" aria-valuenow=\"100\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 100%\">
                            <span class=\"sr-only\">Aguarde...</span>
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <a href=\"#\" id=\"btnYes\" class=\"btn btn-danger confirm\"><span class=\"fa fa-remove\"></span> Remover Selecionados</a>
                    <a href=\"#\" data-dismiss=\"modal\" aria-hidden=\"true\" class=\"btn btn-default secondary\"><span class=\"fa fa-sign-out\"></span> Cancelar</a>
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 32
    public function block_body($context, array $blocks = array())
    {
        // line 33
        echo "    <div class=\"kit-grid ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "getGridCssName", array()), "html", null, true);
        echo "\">
        ";
        // line 34
        $this->displayBlock('kit_grid_selector', $context, $blocks);
        // line 47
        echo "        ";
        $this->displayBlock('kit_grid_filter', $context, $blocks);
        // line 76
        echo "        ";
        $this->displayBlock('kit_grid_debug', $context, $blocks);
        // line 81
        echo "        ";
        $this->displayBlock("kit_grid_before_table", $context, $blocks);
        echo "
        ";
        // line 82
        $this->displayBlock('kit_grid_before_table', $context, $blocks);
        // line 85
        echo "        <div class=\"mb\">
            <table class=\"table table-striped table-hover\">
                ";
        // line 87
        $this->displayBlock('kit_grid_thead', $context, $blocks);
        // line 110
        echo "                ";
        $this->displayBlock('kit_grid_tbody', $context, $blocks);
        // line 138
        echo "            </table>
        </div>
    </div>
";
    }

    // line 34
    public function block_kit_grid_selector($context, array $blocks = array())
    {
        // line 35
        echo "            ";
        if (($this->getAttribute($this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "gridConfig", array()), "selectorList", array()) != null)) {
            // line 36
            echo "                <div class=\"kit-grid-selector\">
                    <ul class=\"nav nav-pills\">
                        ";
            // line 38
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "gridConfig", array()), "selectorList", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["selector"]) {
                // line 39
                echo "                            <li class=\"";
                if ($this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "isSelectorSelected", array(0 => $this->getAttribute($context["selector"], "field", array()), 1 => $this->getAttribute($context["selector"], "value", array())), "method")) {
                    echo "active";
                }
                echo "\">
                                <a href=\"";
                // line 40
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "getSelectorUrl", array(0 => $this->getAttribute($context["selector"], "field", array()), 1 => $this->getAttribute($context["selector"], "value", array())), "method"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["selector"], "label", array()), "html", null, true);
                echo "</a>
                            </li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['selector'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 43
            echo "                    </ul>
                </div>
            ";
        }
        // line 46
        echo "        ";
    }

    // line 47
    public function block_kit_grid_filter($context, array $blocks = array())
    {
        // line 48
        echo "            <div class=\"kit-grid-filter\">
                <form class=\"form-inline\"
                      action=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "requestUri", array()), "html", null, true);
        echo "\"
                      id=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "filterFormName", array()), "html", null, true);
        echo "_form\"
                      method=\"GET\">
                    <div class=\"form-group mt mb\">
                        <div class=\"input-group\">
                            <label class=\"sr-only\"
                                   for=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "filterFormName", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("kitpages_data_grid.Filter"), "html", null, true);
        echo "</label>
                            <input type=\"text\" class=\"form-control\" id=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "filterFormName", array()), "html", null, true);
        echo "\"
                                   value=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "filterValue", array()), "html", null, true);
        echo "\" name=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "filterFormName", array()), "html", null, true);
        echo "\"
                                   placeholder=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Pesquisar Registros"), "html", null, true);
        echo "\"/>

                            <div class=\"input-group-btn\">
                                <button type=\"submit\" class=\"btn btn-default\">
                                    <span class=\"fa fa-search\"></span> ";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Pesquisar Registros"), "html", null, true);
        echo "
                                </button>
                                <a class=\"btn btn-default\"
                                   href=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "requestCurrentRoute", array()), $this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "requestCurrentRouteParams", array())), "html", null, true);
        echo "\"
                                   id=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "filterFormName", array()), "html", null, true);
        echo "_reset_button\">
                                    <span class=\"fa fa-close\"></span> ";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Limpar Pesquisa"), "html", null, true);
        echo "
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        ";
    }

    // line 76
    public function block_kit_grid_debug($context, array $blocks = array())
    {
        // line 77
        echo "            ";
        if ($this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "debugMode", array())) {
            // line 78
            echo "                ";
            echo $this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "dump", array(), "method");
            echo "
            ";
        }
        // line 80
        echo "        ";
    }

    // line 82
    public function block_kit_grid_before_table($context, array $blocks = array())
    {
        // line 83
        echo "        <form action=\"";
        echo twig_escape_filter($this->env, (isset($context["delete"]) ? $context["delete"] : null), "html", null, true);
        echo "\" id=\"form_delete\" method=\"POST\">
        ";
    }

    // line 87
    public function block_kit_grid_thead($context, array $blocks = array())
    {
        // line 88
        echo "                    <thead>
                    <tr>
                        ";
        // line 90
        $this->displayBlock('kit_grid_thead_before_column', $context, $blocks);
        // line 95
        echo "                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "gridConfig", array()), "fieldList", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 96
            echo "                            ";
            if ($this->getAttribute($context["field"], "visible", array())) {
                // line 97
                echo "                                <th class=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "getSortCssClass", array(0 => $this->getAttribute($context["field"], "fieldName", array())), "method"), "html", null, true);
                echo "\">
                                    ";
                // line 98
                if ($this->getAttribute($context["field"], "sortable", array())) {
                    // line 99
                    echo "                                        <a href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "getSortUrl", array(0 => $this->getAttribute($context["field"], "fieldName", array())), "method"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($context["field"], "label", array())), "html", null, true);
                    echo "</a>
                                    ";
                } else {
                    // line 101
                    echo "                                        ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($context["field"], "label", array())), "html", null, true);
                    echo "
                                    ";
                }
                // line 103
                echo "                                </th>
                            ";
            }
            // line 105
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 106
        echo "                        ";
        $this->displayBlock("kit_grid_thead_column", $context, $blocks);
        echo "
                    </tr>
                    </thead>
                ";
    }

    // line 90
    public function block_kit_grid_thead_before_column($context, array $blocks = array())
    {
        // line 91
        echo "                            <th>
                                <input type=\"checkbox\" name=\"todos\" id=\"select\" value=\"todos\" /> <br/>
                            </th>
                        ";
    }

    // line 110
    public function block_kit_grid_tbody($context, array $blocks = array())
    {
        // line 111
        echo "                    <tbody>
                    ";
        // line 112
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "itemList", array()));
        $context['_iterated'] = false;
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 113
            echo "                        <tr class=\"";
            if ((($this->getAttribute($context["loop"], "index", array()) % 2) == 0)) {
                echo "kit-grid-even ";
            } else {
                echo "kit-grid-odd ";
            }
            echo "\">
                            ";
            // line 114
            $this->displayBlock('kit_grid_tbody_before_column', $context, $blocks);
            // line 117
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "gridConfig", array()), "fieldList", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
                // line 118
                echo "                                ";
                if ($this->getAttribute($context["field"], "visible", array())) {
                    // line 119
                    echo "                                    <td class=\"kit-grid-cell-";
                    echo twig_escape_filter($this->env, twig_replace_filter($this->getAttribute($context["field"], "fieldName", array()), array("." => "-")), "html", null, true);
                    echo "\">
                                        ";
                    // line 120
                    if ($this->getAttribute($context["field"], "translatable", array())) {
                        // line 121
                        echo "                                            ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "displayGridValue", array(0 => $context["item"], 1 => $context["field"]), "method")), "html", null, true);
                        echo "
                                        ";
                    } else {
                        // line 123
                        echo "                                            ";
                        echo $this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "displayGridValue", array(0 => $context["item"], 1 => $context["field"]), "method");
                        echo "
                                        ";
                    }
                    // line 125
                    echo "                                    </td>
                                ";
                }
                // line 127
                echo "                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 128
            echo "                            ";
            $this->displayBlock("kit_grid_tbody_column", $context, $blocks);
            echo "
                        </tr>
                    ";
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 131
            echo "                        <tr>
                            <td colspan=\"";
            // line 132
            echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "gridConfig", array()), "fieldList", array())), "html", null, true);
            echo "\"
                                class=\"kit-grid-no-data\">";
            // line 133
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Registro não localizado"), "html", null, true);
            echo "</td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 136
        echo "                    </tbody>
                ";
    }

    // line 114
    public function block_kit_grid_tbody_before_column($context, array $blocks = array())
    {
        // line 115
        echo "                                <td><input type=\"checkbox\" class=\"checked\" name=\"check[]\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "item.id", array(), "array"), "html", null, true);
        echo "\" /></td>
                            ";
    }

    // line 142
    public function block_body_footer($context, array $blocks = array())
    {
        // line 143
        echo "    ";
        $this->displayBlock('kit_grid_after_table', $context, $blocks);
        // line 151
        echo "    ";
        $this->displayBlock('kit_grid_paginator', $context, $blocks);
        // line 155
        echo "    ";
        $this->displayBlock(" kit_grid_after_paginator", $context, $blocks);
        echo "
";
    }

    // line 143
    public function block_kit_grid_after_table($context, array $blocks = array())
    {
        // line 144
        echo "        <a href=\"#modal-dialog\" class=\"btn btn-danger modal-toggle\"
           data-toggle=\"modal\" data-href=\"";
        // line 145
        echo twig_escape_filter($this->env, (isset($context["delete"]) ? $context["delete"] : null), "html", null, true);
        echo "\"
           data-modal-type=\"confirm\" data-backdrop=\"static\" data-keyboard=\"false\">
            <i class=\"icon-trash\"></i><span class=\"fa fa-remove\"></span> Remover Registros Selecionados
        </a>
    </form>
";
    }

    // line 151
    public function block_kit_grid_paginator($context, array $blocks = array())
    {
        // line 152
        echo "        ";
        $this->loadTemplate("CMSBundle:Default:list.html.twig", "CMSBundle:Default:list.html.twig", 152, "1510026656")->display(array_merge($context, array("paginator" => $this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "paginator", array()))));
        // line 154
        echo "    ";
    }

    // line 157
    public function block_javascripts($context, array $blocks = array())
    {
        // line 158
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 159
        $this->loadTemplate("KitpagesDataGridBundle:Grid:javascript.html.twig", "CMSBundle:Default:list.html.twig", 159)->display($context);
        // line 160
        echo "    <script type=\"application/javascript\">
        \$(document).ready(function () {

            \$('#select').click(function (event) {  //on click
                if (this.checked) { // check select status
                    \$('.checked').each(function () { //loop through each checkbox
                        this.checked = true;  //select all checkboxes with class \"checkbox1\"
                    });
                } else {
                    \$('.checked').each(function () { //loop through each checkbox
                        this.checked = false; //deselect all checkboxes with class \"checkbox1\"
                    });
                }
            });
        });
        \$('#modal-dialog').on('show', function () {
            var link = \$(this).data('link'),
                    confirmBtn = \$(this).find('.confirm');
        })


        \$('#btnYes').click(function () {
            \$(\".mensagem_body\").hide();
            \$(\".modal-footer\").hide();
            \$(\".progress\").show();
            \$('form').submit();
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "CMSBundle:Default:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  497 => 160,  495 => 159,  490 => 158,  487 => 157,  483 => 154,  480 => 152,  477 => 151,  467 => 145,  464 => 144,  461 => 143,  454 => 155,  451 => 151,  448 => 143,  445 => 142,  438 => 115,  435 => 114,  430 => 136,  421 => 133,  417 => 132,  414 => 131,  397 => 128,  391 => 127,  387 => 125,  381 => 123,  375 => 121,  373 => 120,  368 => 119,  365 => 118,  360 => 117,  358 => 114,  349 => 113,  331 => 112,  328 => 111,  325 => 110,  318 => 91,  315 => 90,  306 => 106,  300 => 105,  296 => 103,  290 => 101,  282 => 99,  280 => 98,  275 => 97,  272 => 96,  267 => 95,  265 => 90,  261 => 88,  258 => 87,  251 => 83,  248 => 82,  244 => 80,  238 => 78,  235 => 77,  232 => 76,  220 => 68,  216 => 67,  212 => 66,  206 => 63,  199 => 59,  193 => 58,  189 => 57,  183 => 56,  175 => 51,  171 => 50,  167 => 48,  164 => 47,  160 => 46,  155 => 43,  144 => 40,  137 => 39,  133 => 38,  129 => 36,  126 => 35,  123 => 34,  116 => 138,  113 => 110,  111 => 87,  107 => 85,  105 => 82,  100 => 81,  97 => 76,  94 => 47,  92 => 34,  87 => 33,  84 => 32,  57 => 8,  54 => 7,  45 => 3,  42 => 2,  11 => 1,);
    }
}


/* CMSBundle:Default:list.html.twig */
class __TwigTemplate_f01821522ddd2bd8265b8b5a9ed81883b193de35d16a016d10517f38ee42fb33_1510026656 extends Twig_Template
{
    protected function doGetParent(array $context)
    {
        // line 152
        return $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["kitpages_data_grid"]) ? $context["kitpages_data_grid"] : null), "paginator", array()), "default_twig", array()), "CMSBundle:Default:list.html.twig", 152);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "CMSBundle:Default:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  551 => 152,  497 => 160,  495 => 159,  490 => 158,  487 => 157,  483 => 154,  480 => 152,  477 => 151,  467 => 145,  464 => 144,  461 => 143,  454 => 155,  451 => 151,  448 => 143,  445 => 142,  438 => 115,  435 => 114,  430 => 136,  421 => 133,  417 => 132,  414 => 131,  397 => 128,  391 => 127,  387 => 125,  381 => 123,  375 => 121,  373 => 120,  368 => 119,  365 => 118,  360 => 117,  358 => 114,  349 => 113,  331 => 112,  328 => 111,  325 => 110,  318 => 91,  315 => 90,  306 => 106,  300 => 105,  296 => 103,  290 => 101,  282 => 99,  280 => 98,  275 => 97,  272 => 96,  267 => 95,  265 => 90,  261 => 88,  258 => 87,  251 => 83,  248 => 82,  244 => 80,  238 => 78,  235 => 77,  232 => 76,  220 => 68,  216 => 67,  212 => 66,  206 => 63,  199 => 59,  193 => 58,  189 => 57,  183 => 56,  175 => 51,  171 => 50,  167 => 48,  164 => 47,  160 => 46,  155 => 43,  144 => 40,  137 => 39,  133 => 38,  129 => 36,  126 => 35,  123 => 34,  116 => 138,  113 => 110,  111 => 87,  107 => 85,  105 => 82,  100 => 81,  97 => 76,  94 => 47,  92 => 34,  87 => 33,  84 => 32,  57 => 8,  54 => 7,  45 => 3,  42 => 2,  11 => 1,);
    }
}
/* {% extends 'CMSBundle:Default:base.html.twig' %}*/
/* {% block body_header %}*/
/*     <a class="btn btn-success" href="{{ novo }}">*/
/*         <span class="glyphicon glyphicon-plus"></span> Novo Registro*/
/*     </a>*/
/* {% endblock %}*/
/* {% block modal %}*/
/*     <div id="modal-dialog" class="modal">*/
/*         <div class="modal-dialog">*/
/*             <div class="modal-content">*/
/*                 <div class="modal-header">*/
/*                     <h3>Atenção</h3>*/
/*                 </div>*/
/*                 <div class="modal-body">*/
/*                     <div class="mensagem_body">*/
/*                         <p>Você deseja remover os itens selecionados</p>*/
/*                     </div>*/
/*                     <div class="progress" style="display: none">*/
/*                         <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">*/
/*                             <span class="sr-only">Aguarde...</span>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="modal-footer">*/
/*                     <a href="#" id="btnYes" class="btn btn-danger confirm"><span class="fa fa-remove"></span> Remover Selecionados</a>*/
/*                     <a href="#" data-dismiss="modal" aria-hidden="true" class="btn btn-default secondary"><span class="fa fa-sign-out"></span> Cancelar</a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* {% block body %}*/
/*     <div class="kit-grid {{ grid.getGridCssName }}">*/
/*         {% block kit_grid_selector %}*/
/*             {% if grid.gridConfig.selectorList != null %}*/
/*                 <div class="kit-grid-selector">*/
/*                     <ul class="nav nav-pills">*/
/*                         {% for selector in grid.gridConfig.selectorList %}*/
/*                             <li class="{% if grid.isSelectorSelected(selector.field, selector.value) %}active{% endif %}">*/
/*                                 <a href="{{ grid.getSelectorUrl(selector.field, selector.value) }}">{{ selector.label }}</a>*/
/*                             </li>*/
/*                         {% endfor %}*/
/*                     </ul>*/
/*                 </div>*/
/*             {% endif %}*/
/*         {% endblock %}*/
/*         {% block kit_grid_filter %}*/
/*             <div class="kit-grid-filter">*/
/*                 <form class="form-inline"*/
/*                       action="{{ grid.requestUri }}"*/
/*                       id="{{ grid.filterFormName }}_form"*/
/*                       method="GET">*/
/*                     <div class="form-group mt mb">*/
/*                         <div class="input-group">*/
/*                             <label class="sr-only"*/
/*                                    for="{{ grid.filterFormName }}">{{ "kitpages_data_grid.Filter" | trans }}</label>*/
/*                             <input type="text" class="form-control" id="{{ grid.filterFormName }}"*/
/*                                    value="{{ grid.filterValue }}" name="{{ grid.filterFormName }}"*/
/*                                    placeholder="{{ "Pesquisar Registros" | trans }}"/>*/
/* */
/*                             <div class="input-group-btn">*/
/*                                 <button type="submit" class="btn btn-default">*/
/*                                     <span class="fa fa-search"></span> {{ "Pesquisar Registros" | trans }}*/
/*                                 </button>*/
/*                                 <a class="btn btn-default"*/
/*                                    href="{{ path( grid.requestCurrentRoute, grid.requestCurrentRouteParams ) }}"*/
/*                                    id="{{ grid.filterFormName }}_reset_button">*/
/*                                     <span class="fa fa-close"></span> {{ "Limpar Pesquisa" | trans }}*/
/*                                 </a>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </form>*/
/*             </div>*/
/*         {% endblock %}*/
/*         {% block kit_grid_debug %}*/
/*             {% if grid.debugMode %}*/
/*                 {{ grid.dump() | raw }}*/
/*             {% endif %}*/
/*         {% endblock %}*/
/*         {{ block('kit_grid_before_table') }}*/
/*         {% block kit_grid_before_table %}*/
/*         <form action="{{ delete }}" id="form_delete" method="POST">*/
/*         {% endblock %}*/
/*         <div class="mb">*/
/*             <table class="table table-striped table-hover">*/
/*                 {% block kit_grid_thead %}*/
/*                     <thead>*/
/*                     <tr>*/
/*                         {% block kit_grid_thead_before_column %}*/
/*                             <th>*/
/*                                 <input type="checkbox" name="todos" id="select" value="todos" /> <br/>*/
/*                             </th>*/
/*                         {% endblock %}*/
/*                         {% for field in grid.gridConfig.fieldList %}*/
/*                             {% if field.visible %}*/
/*                                 <th class="{{ grid.getSortCssClass(field.fieldName) }}">*/
/*                                     {% if field.sortable %}*/
/*                                         <a href="{{ grid.getSortUrl(field.fieldName) }}">{{ field.label | trans }}</a>*/
/*                                     {% else %}*/
/*                                         {{ field.label | trans }}*/
/*                                     {% endif %}*/
/*                                 </th>*/
/*                             {% endif %}*/
/*                         {% endfor %}*/
/*                         {{ block('kit_grid_thead_column') }}*/
/*                     </tr>*/
/*                     </thead>*/
/*                 {% endblock %}*/
/*                 {% block kit_grid_tbody %}*/
/*                     <tbody>*/
/*                     {% for item in grid.itemList %}*/
/*                         <tr class="{% if loop.index % 2 == 0 %}kit-grid-even {% else %}kit-grid-odd {% endif %}">*/
/*                             {% block kit_grid_tbody_before_column %}*/
/*                                 <td><input type="checkbox" class="checked" name="check[]" value="{{ item['item.id'] }}" /></td>*/
/*                             {% endblock %}*/
/*                             {% for field in grid.gridConfig.fieldList %}*/
/*                                 {% if field.visible %}*/
/*                                     <td class="kit-grid-cell-{{ field.fieldName | replace({'.': '-'}) }}">*/
/*                                         {% if field.translatable %}*/
/*                                             {{ grid.displayGridValue ( item, field) | raw | trans }}*/
/*                                         {% else %}*/
/*                                             {{ grid.displayGridValue ( item, field) | raw }}*/
/*                                         {% endif %}*/
/*                                     </td>*/
/*                                 {% endif %}*/
/*                             {% endfor %}*/
/*                             {{ block('kit_grid_tbody_column') }}*/
/*                         </tr>*/
/*                     {% else %}*/
/*                         <tr>*/
/*                             <td colspan="{{ grid.gridConfig.fieldList | length }}"*/
/*                                 class="kit-grid-no-data">{{ "Registro não localizado" | trans }}</td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 {% endblock %}*/
/*             </table>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* {% block body_footer %}*/
/*     {% block kit_grid_after_table %}*/
/*         <a href="#modal-dialog" class="btn btn-danger modal-toggle"*/
/*            data-toggle="modal" data-href="{{ delete }}"*/
/*            data-modal-type="confirm" data-backdrop="static" data-keyboard="false">*/
/*             <i class="icon-trash"></i><span class="fa fa-remove"></span> Remover Registros Selecionados*/
/*         </a>*/
/*     </form>*/
/* {% endblock %}*/
/*     {% block kit_grid_paginator %}*/
/*         {% embed kitpages_data_grid.paginator.default_twig with {'paginator':grid.paginator} %}*/
/*         {% endembed %}*/
/*     {% endblock %}*/
/*     {{ block(' kit_grid_after_paginator') }}*/
/* {% endblock %}*/
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     {% include "KitpagesDataGridBundle:Grid:javascript.html.twig" %}*/
/*     <script type="application/javascript">*/
/*         $(document).ready(function () {*/
/* */
/*             $('#select').click(function (event) {  //on click*/
/*                 if (this.checked) { // check select status*/
/*                     $('.checked').each(function () { //loop through each checkbox*/
/*                         this.checked = true;  //select all checkboxes with class "checkbox1"*/
/*                     });*/
/*                 } else {*/
/*                     $('.checked').each(function () { //loop through each checkbox*/
/*                         this.checked = false; //deselect all checkboxes with class "checkbox1"*/
/*                     });*/
/*                 }*/
/*             });*/
/*         });*/
/*         $('#modal-dialog').on('show', function () {*/
/*             var link = $(this).data('link'),*/
/*                     confirmBtn = $(this).find('.confirm');*/
/*         })*/
/* */
/* */
/*         $('#btnYes').click(function () {*/
/*             $(".mensagem_body").hide();*/
/*             $(".modal-footer").hide();*/
/*             $(".progress").show();*/
/*             $('form').submit();*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
/* */

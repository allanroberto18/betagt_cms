<?php

namespace BetaGT\Bundles\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Comentarios
 *
 * @ORM\Table(name="Comentarios", indexes={@ORM\Index(name="fk_comentario_noticia_idx", columns={"noticia_id"})})
 * @ORM\Entity
 */
class Comentarios extends EntityMaster
{
    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="text", nullable=true)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="text", nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="texto", type="text", nullable=true)
     */
    private $texto;

    /**
     * @var \Noticias
     *
     * @ORM\ManyToOne(targetEntity="Noticias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="noticia_id", referencedColumnName="id")
     * })
     */
    private $noticia;

    /**
     * Set nome
     *
     * @param string $nome
     * @return Comentarios
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string 
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Comentarios
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set texto
     *
     * @param string $texto
     * @return Comentarios
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string 
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Set noticia
     *
     * @param \BetaGT\Bundles\CMSBundle\Entity\Noticias $noticia
     * @return Comentarios
     */
    public function setNoticia(\BetaGT\Bundles\CMSBundle\Entity\Noticias $noticia = null)
    {
        $this->noticia = $noticia;

        return $this;
    }

    /**
     * Get noticia
     *
     * @return \BetaGT\Bundles\CMSBundle\Entity\Noticias
     */
    public function getNoticia()
    {
        return $this->noticia;
    }

    public function returnStatus()
    {
        switch ($this->status)
        {
            case 1:
                return "Aguardando Leitura";
                break;
            case 2:
                return "Liberado";
                break;
        }
    }
}

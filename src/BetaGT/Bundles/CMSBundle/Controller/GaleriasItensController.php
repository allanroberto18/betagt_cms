<?php

namespace BetaGT\Bundles\CMSBundle\Controller;

use BetaGT\Bundles\CMSBundle\Entity\GaleriasItens;
use BetaGT\Bundles\CMSBundle\Form\GaleriasItensType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class GaleriasItensController extends MainController
{
    /**
     * @Route("/galerias/{idGaleria}/itens/novo", name="cms_galerias_itens_novo")
     * @Template("CMSBundle:GaleriasItens:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction($idGaleria, Request $request)
    {
        $galeria = $this->checkParent($idGaleria, 'CMSBundle', 'Galerias', 'cms_galerias_listar', null);

        $entity = new GaleriasItens();

        $form = $this->createForm(new GaleriasItensType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setGaleria($galeria);

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_galerias_itens_novo'
                : 'cms_galerias_itens_listar';

            return $this->redirectToRoute($nextAction, array('idGaleria' => $idGaleria));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home'));
        $breadcrumbs->addItem('Galerias de Fotos: Listar Registros', $this->get('router')->generate('cms_galerias_listar', array()));
        $breadcrumbs->addItem($galeria->getTitulo(), $this->get('router')->generate('cms_galerias_visualizar', array('id' => $idGaleria)));
        $breadcrumbs->addItem('Itens da Galeria: Listar Registros', $this->get('router')->generate('cms_galerias_itens_listar', array('idGaleria' => $idGaleria)));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Itens da Galeria', 'descricao' => ''),
            'idGaleria' => $idGaleria,
        );
    }

    /**
     * @Route("/galerias/{idGaleria}/itens/{id}/atualizar", name="cms_galerias_itens_atualizar")
     * @Template("CMSBundle:GaleriasItens:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($idGaleria, $id, Request $request)
    {
        $galeria = $this->checkParent($idGaleria, 'CMSBundle', 'Galerias', 'cms_galerias_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:GaleriasItens')->find($id);
        if (!$entity instanceof GaleriasItens) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_galerias_itens_listar', array());
        }

        $form = $this->createForm(new GaleriasItensType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_galerias_itens_novo'
                : 'cms_galerias_itens_listar';

            return $this->redirectToRoute($nextAction, array('idGaleria' => $idGaleria));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Galerias de Fotos: Listar Registros', $this->get('router')->generate('cms_galerias_listar', array()));
        $breadcrumbs->addItem($galeria->getTitulo(), $this->get('router')->generate('cms_galerias_visualizar', array('id' => $idGaleria)));
        $breadcrumbs->addItem('Itens da Galeria: Listar Registros', $this->get('router')->generate('cms_galerias_itens_listar', array('idGaleria' => $idGaleria)));
        $breadcrumbs->addItem('Visualizar', $this->get('router')->generate('cms_galerias_itens_listar', array('idGaleria' => $idGaleria, 'id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Itens da Galeria', 'descricao' => ''),
            'idGaleria' => $idGaleria,
        );
    }

    /**
     * @Route("/galerias/{idGaleria}/itens/listar", name="cms_galerias_itens_listar")
     * @Template("CMSBundle:GaleriasItens:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction($idGaleria, Request $request)
    {
        $galeria = $this->checkParent($idGaleria, 'CMSBundle', 'Galerias', 'cms_galerias_listar', null);

        $repository = $this->getDoctrine()->getRepository('CMSBundle:GaleriasItens');
        $queryBuilder = $repository->createQueryBuilder('item')
            ->where('item.status = :status')
            ->andWhere('item.galeria = :galeria')
            ->setParameter('status', '1')
            ->setParameter('galeria', $idGaleria)
        ;

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('item.imageName', [
                'label' => 'Foto',
                'formatValueCallback' => function($value) {
                    print "<img src='/uploads/galerias/itens/{$value}' class='img-responsive img-thumbnail' width='140px' />";
                }
            ]))
            ->addField(new Field('item.legenda', array('label' => 'Legenda', 'filterable' => 'true', 'sortable' => true)))
        ;

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Galerias de Fotos: Listar Registros', $this->get('router')->generate('cms_galerias_listar', array()));
        $breadcrumbs->addItem($galeria->getTitulo(), $this->get('router')->generate('cms_galerias_visualizar', array('id' => $idGaleria)));
        $breadcrumbs->addItem('Itens da Galeria: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('cms_galerias_itens_delete_selecionado', ['idGaleria' => $idGaleria]),
            'novo' => $this->generateUrl('cms_galerias_itens_novo', ['idGaleria' => $idGaleria]),
            'modulo' => array('titulo' => 'Itens da Galeria', 'descricao' => ''),
            'idGaleria' => $idGaleria,
        );
    }

    /**
     * @Route("/galerias/{idGaleria}/itens/{id}/visualizar", name="cms_galerias_itens_visualizar")
     * @Template("CMSBundle:GaleriasItens:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($idGaleria, $id)
    {
        $galeria = $this->checkParent($idGaleria, 'CMSBundle', 'Galerias', 'cms_galerias_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:GaleriasItens')->find($id);
        if (!$entity instanceof GaleriasItens) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_galerias_itens_listar', array('idGaleria' => $idGaleria));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Galerias de Fotos: Listar Registros', $this->get('router')->generate('cms_galerias_listar', array()));
        $breadcrumbs->addItem($galeria->getTitulo(), $this->get('router')->generate('cms_galerias_visualizar', array('id' => $idGaleria)));
        $breadcrumbs->addItem('Itens da Galeria: Listar Registros', $this->get('router')->generate('cms_galerias_itens_listar', array('idGaleria' => $idGaleria)));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar',
            'entity' => $entity,
            'modulo' => array('titulo' => 'GaleriasItens', 'descricao' => ''),
            'idGaleria' => $idGaleria,
        );
    }

    /**
     * @Route("/galerias/{idGaleria}/itens/{id}/delete", name="cms_galerias_itens_delete")
     * @Method("GET")
     */
    public function deleteAction($idGaleria, $id)
    {
        $this->checkParent($idGaleria, 'CMSBundle', 'Galerias', 'cms_galerias_listar', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:GaleriasItens')->find($id);
        if (!$entity instanceof GaleriasItens) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_galerias_itens_listar', array('idGalerias' => $idGaleria));
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('cms_galerias_itens_listar', array('idGalerias' => $idGaleria));
    }

    /**
     * @Route("/galerias/{idGaleria}/itens/delete/selecionados", name="cms_galerias_itens_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction($idGaleria, Request $request)
    {
        $this->checkParent($idGaleria, 'CMSBundle', 'Galerias', 'cms_galerias_listar', null);

        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('cms_galerias_itens_listar', array('idGaleria' => $idGaleria));
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('CMSBundle:GaleriasItens')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('cms_galerias_itens_listar', array('idGaleria' => $idGaleria));
    }
}

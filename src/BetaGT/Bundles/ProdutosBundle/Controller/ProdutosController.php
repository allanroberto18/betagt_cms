<?php

namespace BetaGT\Bundles\ProdutosBundle\Controller;

use BetaGT\Bundles\ProdutosBundle\Entity\Produtos;
use BetaGT\Bundles\ProdutosBundle\Form\ProdutosType;

use BetaGT\Bundles\CMSBundle\Controller\MainController;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class ProdutosController extends MainController
{
    /**
     * @Route("/produtos/novo", name="pro_produtos_novo")
     * @Template("ProdutosBundle:Produtos:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction(Request $request)
    {
        $entity = new Produtos();

        $form = $this->createForm(new ProdutosType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'pro_produtos_novo'
                : 'pro_produtos_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home'));
        $breadcrumbs->addItem('Produtos: Listar Registros', $this->get('router')->generate('pro_produtos_listar', array()));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Produtos', 'descricao' => ''),
        );
    }

    /**
     * @Route("/produtos/{id}/atualizar", name="pro_produtos_atualizar")
     * @Template("ProdutosBundle:Produtos:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ProdutosBundle:Produtos')->find($id);
        if (!$entity instanceof Produtos) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('pro_produtos_listar', array());
        }

        $form = $this->createForm(new ProdutosType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'pro_produtos_novo'
                : 'pro_produtos_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Produtos: Listar Registros', $this->get('router')->generate('pro_produtos_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $entity->getTitulo(), $this->get('router')->generate('pro_produtos_visualizar', array('id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Produtos', 'descricao' => ''),
        );
    }

    /**
     * @Route("/produtos/listar", name="pro_produtos_listar")
     * @Template("ProdutosBundle:Produtos:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('ProdutosBundle:Produtos');
        $queryBuilder = $repository->createQueryBuilder('item')
            ->where('item.status = :status')
            ->setParameter('status', '1');

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('item.titulo', array('label' => 'Título', 'filterable' => 'true', 'sortable' => true)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Produtos: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('pro_produtos_delete_selecionado'),
            'novo' => $this->generateUrl('pro_produtos_novo'),
            'modulo' => array('titulo' => 'Produtos', 'descricao' => ''),
        );
    }

    /**
     * @Route("/produtos/{id}/visualizar", name="pro_produtos_visualizar")
     * @Template("ProdutosBundle:Produtos:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ProdutosBundle:Produtos')->find($id);
        if (!$entity instanceof Produtos) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('pro_produtos_listar', array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Produtos: Listar Registros', $this->get('router')->generate('pro_produtos_listar', array()));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar: ' . $entity->getTitulo(),
            'entity' => $entity,
            'modulo' => array('titulo' => 'Produtos', 'descricao' => ''),
        );
    }

    /**
     * @Route("/produtos/{id}/delete", name="pro_produtos_delete")
     * @Method("GET")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ProdutosBundle:Produtos')->find($id);
        if (!$entity instanceof Produtos) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('pro_produtos_listar', array());
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('pro_produtos_listar', array());
    }

    /**
     * @Route("/produtos/delete/selecionados", name="pro_produtos_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction(Request $request)
    {
        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('pro_produtos_listar', array());
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('ProdutosBundle:Produtos')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('pro_produtos_listar', array());
    }
}

<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_689a5f91a331df832b32c1fe0dedafebdafc5a1a5daad2b743b6bbd92732a9b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CMSBundle:Default:security.html.twig", "FOSUserBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CMSBundle:Default:security.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "    <p class=\"login-box-msg\">Iniciar sessão</p>
    ";
        // line 5
        echo "    <form action=\"";
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
        <div>
            ";
        // line 7
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 8
            echo "                <div>
                    <h5>
                        ";
            // line 10
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.logged_in_as", array("%username%" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo "
                    </h5>
                    <a class=\"btn btn-warning btn-block\" href=\"";
            // line 12
            echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
            echo "\">
                        <i class=\"fa fa-sign-out\"></i> ";
            // line 13
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.logout", array(), "FOSUserBundle"), "html", null, true);
            echo "
                    </a>
                    <br />
                </div>
            ";
        }
        // line 18
        echo "        </div>
        ";
        // line 19
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "hasPreviousSession", array())) {
            // line 20
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "all", array(), "method"));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 21
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 22
                    echo "                    <div class=\"flash-";
                    echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                    echo "\">
                        ";
                    // line 23
                    echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                    echo "
                    </div>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 26
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "        ";
        }
        // line 28
        echo "        ";
        if ((isset($context["error"]) ? $context["error"] : null)) {
            // line 29
            echo "            <div class=\"alert alert-danger\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                <strong><span class=\"fa fa-exclamation\"></span> Atenção</strong> ";
            // line 31
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messageData", array()), "security"), "html", null, true);
            echo "
            </div>
        ";
        }
        // line 34
        echo "        <div class=\"form-group has-feedback\">
            <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 35
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : null), "html", null, true);
        echo "\" />
            <input type=\"text\" class=\"form-control\" id=\"username\" name=\"_username\" value=\"";
        // line 36
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : null), "html", null, true);
        echo "\" required=\"required\" placeholder=\"Inserir o E-mail\"/>
            <span class=\"glyphicon glyphicon-envelope form-control-feedback\"></span>
        </div>
        <div class=\"form-group has-feedback\">
            <input type=\"password\" class=\"form-control\" id=\"password\" name=\"_password\" required=\"required\" placeholder=\"Inserir a senha\" />
            <span class=\"glyphicon glyphicon-lock form-control-feedback\"></span>
        </div>
        <div class=\"row\">
            <div class=\"col-xs-8\">
                <div class=\"checkbox icheck\">
                    <label>
                        <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\"> ";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("security.login.remember_me", array(), "FOSUserBundle"), "html", null, true);
        echo "
                    </label>
                </div>
            </div>
            <!-- /.col -->
            <div class=\"col-xs-4\">
                <button type=\"submit\" id=\"_submit\" name=\"_submit\" class=\"btn btn-primary btn-block btn-flat\"><span class=\"fa fa-sign-in\"></span> Entrar</button>
            </div>
            <!-- /.col -->
        </div>
    </form>
    <a href=\"";
        // line 58
        echo $this->env->getExtension('routing')->getPath("fos_user_resetting_request");
        echo "\">Esqueceu a Senha?</a><br>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 58,  135 => 47,  121 => 36,  117 => 35,  114 => 34,  108 => 31,  104 => 29,  101 => 28,  98 => 27,  92 => 26,  83 => 23,  78 => 22,  73 => 21,  68 => 20,  66 => 19,  63 => 18,  55 => 13,  51 => 12,  46 => 10,  42 => 8,  40 => 7,  34 => 5,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends 'CMSBundle:Default:security.html.twig' %}*/
/* {% block body %}*/
/*     <p class="login-box-msg">Iniciar sessão</p>*/
/*     {% trans_default_domain 'FOSUserBundle' %}*/
/*     <form action="{{ path("fos_user_security_check") }}" method="post">*/
/*         <div>*/
/*             {% if is_granted("IS_AUTHENTICATED_REMEMBERED") %}*/
/*                 <div>*/
/*                     <h5>*/
/*                         {{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }}*/
/*                     </h5>*/
/*                     <a class="btn btn-warning btn-block" href="{{ path('fos_user_security_logout') }}">*/
/*                         <i class="fa fa-sign-out"></i> {{ 'layout.logout'|trans({}, 'FOSUserBundle') }}*/
/*                     </a>*/
/*                     <br />*/
/*                 </div>*/
/*             {% endif %}*/
/*         </div>*/
/*         {% if app.request.hasPreviousSession %}*/
/*             {% for type, messages in app.session.flashbag.all() %}*/
/*                 {% for message in messages %}*/
/*                     <div class="flash-{{ type }}">*/
/*                         {{ message }}*/
/*                     </div>*/
/*                 {% endfor %}*/
/*             {% endfor %}*/
/*         {% endif %}*/
/*         {% if error %}*/
/*             <div class="alert alert-danger">*/
/*                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>*/
/*                 <strong><span class="fa fa-exclamation"></span> Atenção</strong> {{ error.messageKey|trans(error.messageData, 'security') }}*/
/*             </div>*/
/*         {% endif %}*/
/*         <div class="form-group has-feedback">*/
/*             <input type="hidden" name="_csrf_token" value="{{ csrf_token }}" />*/
/*             <input type="text" class="form-control" id="username" name="_username" value="{{ last_username }}" required="required" placeholder="Inserir o E-mail"/>*/
/*             <span class="glyphicon glyphicon-envelope form-control-feedback"></span>*/
/*         </div>*/
/*         <div class="form-group has-feedback">*/
/*             <input type="password" class="form-control" id="password" name="_password" required="required" placeholder="Inserir a senha" />*/
/*             <span class="glyphicon glyphicon-lock form-control-feedback"></span>*/
/*         </div>*/
/*         <div class="row">*/
/*             <div class="col-xs-8">*/
/*                 <div class="checkbox icheck">*/
/*                     <label>*/
/*                         <input type="checkbox" id="remember_me" name="_remember_me" value="on"> {{ 'security.login.remember_me'|trans }}*/
/*                     </label>*/
/*                 </div>*/
/*             </div>*/
/*             <!-- /.col -->*/
/*             <div class="col-xs-4">*/
/*                 <button type="submit" id="_submit" name="_submit" class="btn btn-primary btn-block btn-flat"><span class="fa fa-sign-in"></span> Entrar</button>*/
/*             </div>*/
/*             <!-- /.col -->*/
/*         </div>*/
/*     </form>*/
/*     <a href="{{ path('fos_user_resetting_request') }}">Esqueceu a Senha?</a><br>*/
/* {% endblock %}*/

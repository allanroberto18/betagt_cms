<?php

namespace BetaGT\Bundles\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Contatos
 *
 * @ORM\Table(name="contatos", indexes={@ORM\Index(name="fk_contatos_assunto_id_idx", columns={"assunto_id"})})
 * @ORM\Entity
 */
class Contatos extends EntityMaster
{
    /**
     * @var string
     * @Assert\NotBlank(message="O campo Texto é obrigatório")
     * @ORM\Column(name="texto", type="text", nullable=true)
     */
    private $texto;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Nome é obrigatório")
     * @ORM\Column(name="nome", type="string", length=255, nullable=true)
     */
    private $nome;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo E-mail é obrigatório")
     * @Assert\Email(message="O E-mail informado é inválido")
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="celular", type="string", length=45, nullable=true)
     */
    private $celular;

    /**
     * @var \Assuntos
     * @Assert\NotBlank(message="O campo Assunto é obrigatório")
     * @ORM\ManyToOne(targetEntity="Assuntos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="assunto_id", referencedColumnName="id")
     * })
     */
    private $assunto;

    /**
     * Set texto
     *
     * @param string $texto
     * @return Contatos
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string 
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return Contatos
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string 
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Contatos
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set celular
     *
     * @param string $celular
     * @return Contatos
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get celular
     *
     * @return string 
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * Set assunto
     *
     * @param \BetaGT\Bundles\CMSBundle\Entity\Assuntos $assunto
     * @return Contatos
     */
    public function setAssunto(\BetaGT\Bundles\CMSBundle\Entity\Assuntos $assunto = null)
    {
        $this->assunto = $assunto;

        return $this;
    }

    /**
     * Get assunto
     *
     * @return \BetaGT\Bundles\CMSBundle\Entity\Assuntos
     */
    public function getAssunto()
    {
        return $this->assunto;
    }

    public function returnStatus()
    {
        switch ($this->status)
        {
            case 1:
                return "Aguardando Leitura";
                break;
            case 2:
                return "Mensagem Lida";
                break;
        }
    }
}

<?php

/* CMSBundle:Default:security.html.twig */
class __TwigTemplate_f4f7987b9f58d084ad87fc4f059cd57615e09041426a5acd9e0790c6b5df961b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
    <!-- Bootstrap 3.3.4 -->
    <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <!-- Font Awesome Icons -->
    <link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\"/>
    <!-- Theme style -->
    <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/dist/css/AdminLTE.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
    <!-- iCheck -->
    <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/iCheck/square/blue.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
</head>
<body class=\"login-page\">
<div class=\"login-box\">
    <div class=\"login-logo\">
        <a href=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("fos_user_security_login");
        echo "\"><b>BetaGT</b> Administração</a>
    </div>
    <!-- /.login-logo -->
    <div class=\"login-box-body\">
        ";
        // line 32
        $this->displayBlock('body', $context, $blocks);
        // line 35
        echo "    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<!-- jQuery 2.1.4 -->
<script src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/jQuery/jQuery-2.1.4.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("admin/plugins/iCheck/icheck.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script>
    \$(function () {
        \$('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>
</html>
";
    }

    // line 32
    public function block_body($context, array $blocks = array())
    {
        // line 33
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "CMSBundle:Default:security.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 33,  99 => 32,  82 => 42,  78 => 41,  74 => 40,  67 => 35,  65 => 32,  58 => 28,  43 => 16,  38 => 14,  31 => 10,  20 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/* <head>*/
/*     <meta charset="utf-8">*/
/*     <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*     <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">*/
/*     <!-- Tell the browser to be responsive to screen width -->*/
/*     <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">*/
/*     <!-- Bootstrap 3.3.4 -->*/
/*     <link href="{{ asset('admin/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />*/
/*     <!-- Font Awesome Icons -->*/
/*     <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>*/
/*     <!-- Theme style -->*/
/*     <link href="{{ asset('admin/dist/css/AdminLTE.css') }}" rel="stylesheet" type="text/css" />*/
/*     <!-- iCheck -->*/
/*     <link href="{{ asset('admin/plugins/iCheck/square/blue.css') }}" rel="stylesheet" type="text/css"/>*/
/* */
/*     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->*/
/*     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->*/
/*     <!--[if lt IE 9]>*/
/*     <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>*/
/*     <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>*/
/*     <![endif]-->*/
/* </head>*/
/* <body class="login-page">*/
/* <div class="login-box">*/
/*     <div class="login-logo">*/
/*         <a href="{{ path('fos_user_security_login') }}"><b>BetaGT</b> Administração</a>*/
/*     </div>*/
/*     <!-- /.login-logo -->*/
/*     <div class="login-box-body">*/
/*         {% block body %}*/
/* */
/*         {% endblock %}*/
/*     </div>*/
/*     <!-- /.login-box-body -->*/
/* </div>*/
/* <!-- /.login-box -->*/
/* <!-- jQuery 2.1.4 -->*/
/* <script src="{{ asset('admin/plugins/jQuery/jQuery-2.1.4.min.js') }}" type="text/javascript"></script>*/
/* <script src="{{ asset('admin/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>*/
/* <script src="{{ asset('admin/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>*/
/* <script>*/
/*     $(function () {*/
/*         $('input').iCheck({*/
/*             checkboxClass: 'icheckbox_square-blue',*/
/*             radioClass: 'iradio_square-blue',*/
/*             increaseArea: '20%' // optional*/
/*         });*/
/*     });*/
/* </script>*/
/* </body>*/
/* </html>*/
/* */

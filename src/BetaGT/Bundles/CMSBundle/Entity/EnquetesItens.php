<?php

namespace BetaGT\Bundles\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EnquetesItens
 *
 * @ORM\Table(name="enquetes_itens", indexes={@ORM\Index(name="fk_enquete_item_enquete_idx", columns={"enquete_id"})})
 * @ORM\Entity
 */
class EnquetesItens extends EntityMaster
{
    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=true)
     */
    private $titulo;

    /**
     * @var \Enquetes
     *
     * @ORM\ManyToOne(targetEntity="Enquetes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="enquete_id", referencedColumnName="id")
     * })
     */
    private $enquete;

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return EnquetesItens
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set enquete
     *
     * @param \BetaGT\Bundles\CMSBundle\Entity\Enquetes $enquete
     * @return EnquetesItens
     */
    public function setEnquete(\BetaGT\Bundles\CMSBundle\Entity\Enquetes $enquete = null)
    {
        $this->enquete = $enquete;

        return $this;
    }

    /**
     * Get enquete
     *
     * @return \BetaGT\Bundles\CMSBundle\Entity\Enquetes
     */
    public function getEnquete()
    {
        return $this->enquete;
    }
}

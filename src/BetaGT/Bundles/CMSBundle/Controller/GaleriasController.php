<?php

namespace BetaGT\Bundles\CMSBundle\Controller;

use BetaGT\Bundles\CMSBundle\Entity\Galerias;
use BetaGT\Bundles\CMSBundle\Form\GaleriasType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class GaleriasController extends MainController
{
    /**
     * @Route("/galerias/novo", name="cms_galerias_novo")
     * @Template("CMSBundle:Galerias:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction(Request $request)
    {
        $entity = new Galerias();

        $form = $this->createForm(new GaleriasType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_galerias_novo'
                : 'cms_galerias_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home'));
        $breadcrumbs->addItem('Galerias de Fotos: Listar Registros', $this->get('router')->generate('cms_galerias_listar'));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Galerias', 'descricao' => 'Albuns de Fotos'),
        );
    }

    /**
     * @Route("/galerias/{id}/atualizar", name="cms_galerias_atualizar")
     * @Template("CMSBundle:Galerias:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Galerias')->find($id);
        if (!$entity instanceof Galerias) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_galerias_listar', array());
        }

        $form = $this->createForm(new GaleriasType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_galerias_novo'
                : 'cms_galerias_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Galerias de Fotos: Listar Registros', $this->get('router')->generate('cms_galerias_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $entity->getTitulo(), $this->get('router')->generate('cms_galerias_visualizar', array('id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Galerias', 'descricao' => 'Albuns de Fotos'),
        );
    }

    /**
     * @Route("/galerias/listar", name="cms_galerias_listar")
     * @Template("CMSBundle:Galerias:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('CMSBundle:Galerias');
        $queryBuilder = $repository->createQueryBuilder('item')
            ->where('item.status = :status')
            ->setParameter('status', '1');

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('item.imageName', [
                'label' => 'Foto', 'sortable' => true,
                'formatValueCallback' => function($value) {
                    print "<img src='/uploads/galerias/{$value}' class='img-responsive img-thumbnail' width='140px' />";
                }
            ]))
            ->addField(new Field('item.titulo', [ 'label' => 'Título', 'filterable' => 'true', 'sortable' => true ]))
        ;

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Galerias de Fotos: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('cms_galerias_delete_selecionado'),
            'novo' => $this->generateUrl('cms_galerias_novo'),
            'modulo' => array('titulo' => 'Galerias de Fotos', 'descricao' => 'Albuns de Fotos'),
        );
    }

    /**
     * @Route("/galerias/{id}/visualizar", name="cms_galerias_visualizar")
     * @Template("CMSBundle:Galerias:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Galerias')->find($id);
        if (!$entity instanceof Galerias) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_galerias_listar', array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Galerias de Fotos: Listar Registros', $this->get('router')->generate('cms_galerias_listar', array()));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar: ' . $entity->getTitulo(),
            'entity' => $entity,
            'modulo' => array('titulo' => 'Galerias', 'descricao' => 'Albuns de Fotos'),
        );
    }

    /**
     * @Route("/galerias/{id}/delete", name="cms_galerias_delete")
     * @Method("GET")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Galerias')->find($id);
        if (!$entity instanceof Galerias) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_galerias_listar', array());
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('cms_galerias_listar', array());
    }

    /**
     * @Route("/galerias/delete/selecionados", name="cms_galerias_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction(Request $request)
    {
        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('cms_galerias_listar', array());
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('CMSBundle:Galerias')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('cms_galerias_listar', array());
    }
}

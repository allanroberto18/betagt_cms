<?php

namespace BetaGT\Bundles\CMSBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContatosFrontType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('assunto')
            ->add('nome')
            ->add('email')
            ->add('celular', 'text', [ 'attr' => [ 'class' => 'telefone' ]])
            ->add('texto', 'textarea', [ 'attr' => [ 'class' => 'textarea', 'rows' => 13 ]])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BetaGT\Bundles\CMSBundle\Entity\Contatos',
            'csrf_protection' => false
        ));
    }

    public function getName()
    {
        return 'form';
    }
}

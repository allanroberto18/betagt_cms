<?php

namespace BetaGT\Bundles\PortalBundle\Controller;

use BetaGT\Bundles\CMSBundle\Entity\Galerias;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/Albuns")
 */
class GaleriasController extends MainController
{
    /**
     * @Route("/", name="front_galerias")
     * @Template("PortalBundle:galerias:index.html.twig")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $filterBuilder = $this->get('doctrine.orm.entity_manager')
            ->getRepository('CMSBundle:Galerias')
            ->createQueryBuilder('e');

        $form = $this->get('form.factory')->create(new ReceitasFilterType());

        if($request->query->has($form->getName()))
        {
            $form->submit($request->query->get($form->getName()));

            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);
        }

        $query = $filterBuilder->getQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query, $request->query->get('page', 1), 10);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('front_home'));
        $breadcrumbs->addItem('Galerias de Fotos');

        return array(
            "titulo" => "Galerias",
            "form" => $this->formContato(),
            "filter" => $form->createView(),
            "pagination" => $pagination,
            "pagFaleConosco" => $this->container->get('portal.paginas')->returnRegister(1),
            "assuntos" => $this->container->get('portal.assuntos')->returnAll(),
            "galerias" => $this->container->get('portal.galerias')->returnAll(),
            "receitas" => $this->container->get('portal.receitas')->returnAll(),
            "banners" => $this->container->get('portal.banners')->returnAll(),
            "produtos" => $this->container->get('portal.produtos')->returnAll(),
        );
    }

    /**
     * @Route("/{id}/{slug}", name="front_galeria")
     * @Template("PortalBundle:galerias:page.html.twig")
     * @Method("GET")
     */
    public function pageAction($id, $slug)
    {
        $entity = $this->container->get('portal.galerias')->returnRegister($id);
        if (!$entity instanceof Galerias)
        {
            return $this->redirect("/");
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('front_home'));
        $breadcrumbs->addItem('Galerias de Fotos', $this->get('router')->generate('front_galerias'));
        $breadcrumbs->addItem($entity->getTitulo());

        return array(
            "titulo" => "Galeria",
            "form" => $this->formContato(),
            "item" => $entity,
            "pagFaleConosco" => $this->container->get('portal.paginas')->returnRegister(1),
            "assuntos" => $this->container->get('portal.assuntos')->returnAll(),
            "galeria" => $this->container->get('portal.galerias')->returnAll(),
            "receitas" => $this->container->get('portal.receitas')->returnAll(),
            "banners" => $this->container->get('portal.banners')->returnAll(),
            "produtos" => $this->container->get('portal.produtos')->returnAll(),
        );
    }
}

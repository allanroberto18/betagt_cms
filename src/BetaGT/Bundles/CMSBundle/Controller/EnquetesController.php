<?php

namespace BetaGT\Bundles\CMSBundle\Controller;

use BetaGT\Bundles\CMSBundle\Entity\Enquetes;
use BetaGT\Bundles\CMSBundle\Form\EnquetesType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class EnquetesController extends MainController
{
    /**
     * @Route("/enquetes/novo", name="cms_enquetes_novo")
     * @Template("CMSBundle:Enquetes:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction(Request $request)
    {
        $entity = new Enquetes();

        $form = $this->createForm(new EnquetesType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_enquetes_novo'
                : 'cms_enquetes_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home'));
        $breadcrumbs->addItem('Enquetes: Listar Registros', $this->get('router')->generate('cms_enquetes_listar'));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Enquetes', 'descricao' => 'Pesquisas em geral entre os visitantes do site'),
        );
    }

    /**
     * @Route("/enquetes/{id}/atualizar", name="cms_enquetes_atualizar")
     * @Template("CMSBundle:Enquetes:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Enquetes')->find($id);
        if (!$entity instanceof Enquetes) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_enquetes_listar', array());
        }

        $form = $this->createForm(new EnquetesType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'cms_enquetes_novo'
                : 'cms_enquetes_listar';

            return $this->redirectToRoute($nextAction, array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Enquetes: Listar Registros', $this->get('router')->generate('cms_enquetes_listar', array()));
        $breadcrumbs->addItem('Visualizar: ' . $entity->getTitulo(), $this->get('router')->generate('cms_enquetes_visualizar', array('id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Enquetes', 'descricao' => ''),
        );
    }

    /**
     * @Route("/enquetes/listar", name="cms_enquetes_listar")
     * @Template("CMSBundle:Enquetes:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('CMSBundle:Enquetes');
        $queryBuilder = $repository->createQueryBuilder('item')
            ->where('item.status = :status')
            ->setParameter('status', '1');

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('item.titulo', array('label' => 'Título', 'filterable' => 'true', 'sortable' => true)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Enquetes: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('cms_enquetes_delete_selecionado'),
            'novo' => $this->generateUrl('cms_enquetes_novo'),
            'modulo' => array('titulo' => 'Enquetes', 'descricao' => 'Pesquisas em geral entre os visitantes do site'),
        );
    }

    /**
     * @Route("/enquetes/{id}/visualizar", name="cms_enquetes_visualizar")
     * @Template("CMSBundle:Enquetes:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Enquetes')->find($id);
        if (!$entity instanceof Enquetes) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_enquetes_listar', array());
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('cms_home', array()));
        $breadcrumbs->addItem('Listar Registros', $this->get('router')->generate('cms_enquetes_listar', array()));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar: ' . $entity->getTitulo(),
            'entity' => $entity,
            'modulo' => array('titulo' => 'Enquetes', 'descricao' => 'Pesquisas em geral entre os visitantes do site'),
        );
    }

    /**
     * @Route("/enquetes/{id}/delete", name="cms_enquetes_delete")
     * @Method("GET")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Enquetes')->find($id);
        if (!$entity instanceof Enquetes) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('cms_enquetes_listar', array());
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('cms_enquetes_listar', array());
    }

    /**
     * @Route("/enquetes/delete/selecionados", name="cms_enquetes_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction(Request $request)
    {
        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('cms_enquetes_listar', array());
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('CMSBundle:Enquetes')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('cms_enquetes_listar', array());
    }
}

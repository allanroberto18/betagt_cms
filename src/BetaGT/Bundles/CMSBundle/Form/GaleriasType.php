<?php

namespace BetaGT\Bundles\CMSBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GaleriasType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo')
            ->add('imageFile', 'vich_image', array(
                'label' => 'Selecione a Imagem',
                'required'      => false,
                'allow_delete'  => true, // not mandatory, default is true
                'download_link' => true, // not mandatory, default is true
            ))
            ->add('actions', 'form_actions', [
                'buttons' => [
                    'salvar' => ['type' => 'submit', 'options' => ['label' => 'Salvar', 'attr' => ['class' => 'btn btn-primary']]],
                    'salvarNovo' => ['type' => 'submit', 'options' => ['label' => 'Salvar e novo', 'attr' => ['class' => 'btn btn-primary']]],
                    'cancelar' => ['type' => 'reset', 'options' => ['label' => 'Cancelar', 'attr' => ['class' => 'btn btn-warning']]],
                ]
            ])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BetaGT\Bundles\CMSBundle\Entity\Galerias'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form';
    }
}
